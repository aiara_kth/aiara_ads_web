AdFlex - Advertising management system. 2017


System requirements:
- Dedicated or virtual server or hosting
- Domain
- Version php 5.3.7 or higher
- MySQL 5.0 or higher






Installation:
- Extract the contents of the archive into the root directory of the site
- Set the rights to the file CONFIG.INI (666 or 777)
- Set permissions on the IMAGES folder (777)
- Go to http://yoursite.com

Hereinafter, yoursite.com means your domain

- Fill in the form:

MySQL User - MySQL user name (check with your hosting provider)
MySQL Password - MySQL user password (check with your hosting provider)
MySQL Host - the address of the MySQL server (in most cases this is "localhost")
MySQL DB name is the name of the MySQL database, the installer will try to create a database with this name.
Note that on some hosting sites this may not work. You may need to create the database yourself in the server / hosting management panel.
If you created a database in the hosting control panel, just add its name in the field "MySQL DB name"

- Admin Email - Email administrator. Your email to enter the control panel.
Admin Password. Create a password to enter the control panel. The password is at least 8 characters long.
- Press the "INSTALL" button
This completes the installation.







Login to the system:
- Go to http://yoursite.com
- In the "Email" field, enter the Email of the administrator that you specified during the installation.
- In the "Password" field, enter the administrator password that you specified during the installation.
- Press the "LOG IN"






Add site:
Go to the list of sites "Sites"

In the right column are the buttons for managing the site.
- Blocks (n) - the number of ad units on the site
- Play - start advertising on the site
- Stop - stop advertising on the site
- Stats - detailed statistics on the site
- Remove - delete the site from the system. Blocks are also deleted
- Views - total number of impressions of ads on the site for all time
- Clicks - the total number of clicks on ads on the site for all time
- Ctr - the total Ctr site for all time
To add a new site, click "Add site"
Enter the site name and click "OK"

Video: https://youtu.be/LeQdo0VHwTo?t=143






Adding an ad unit:
Go to the list of sites "Sites"
Click on the "Blocks" button in front of the corresponding site.
Before you will be a list of all the blocks of this site
In the right column are the control buttons for the unit.

- Play - start the block
- Stop - stop the display of the block
- Stats - detailed block statistics
- Edit - edit block
- Copy - copy block
- Remove - delete the block
- Views - total number of block hits for all time
- Clicks - total number of clicks per block for all time
- Ctr - common Ctr block for all time

To add a new block, click the "Add block"
Enter the name of the block and click "Create"

Video: https://youtu.be/LeQdo0VHwTo?t=188





Ad unit setting:
The system provides you with the flexibility to customize ad units.
Changing any settings of the block, you immediately see the result in the form of a block preview.
Describe all possible block settings for too long.
The best way to see the process of setting the block on the video: https://youtu.be/LeQdo0VHwTo?t=188




Adding an ad campaign:
Go to the "Campaigns" campaign list page
Before you will be a list of all advertising campaigns.
In the right column are the buttons for managing the campaign.

- Ads (n) - add an advertisement to the campaign. Also, the button indicates the number of ads in the campaign.
- Play - launch of an advertising campaign
- Stop - stop advertising campaign
- Stats - detailed statistics of the advertising campaign
- Edit - editing an advertising campaign
- Remove - delete an advertising campaign
- Views - total number of impressions of an advertising campaign for all time
- Clicks - total number of clicks of the advertising campaign for all time
- Ctr - general Ctr advertising campaign for all time

Also, each campaign has a list of targeting icons.
When you hover over a mouse, the information on campaign targeting is displayed on them.

- Countries - Country Targeting
- Devices - device targeting
- OS-targeting
- Browsers - Browser targeting
- Active hours - active campaign display hours (00 to 23)
- Filtered sites - sites on which the given advertising campaign is NOT displayed


To add a new advertising campaign, click "Add campaign"
Enter the name of the campaign and click "create"
Before you find the campaign targeting page


Video: https://youtu.be/LeQdo0VHwTo?t=450







Setting up an ad campaign targeting:
- Campaign name - name of the campaign
- Active hours - hours of impressions. For example, you want to show the campaign only from 10am to 19pm.
- Campaign start date - the start date of the campaign.
- Campaign stop date - the date the campaign was stopped
- Geo targeting - targeting by country. For example, you want to display advertising only to visitors from USA and CA
- Device targeting - Device targeting. For example, you want to show ads only to visitors with PCs and Tablets
- OS targeting - targeting by operating systems. For example, you want to show ads only to visitors with MacOS
- Browser targeting - targeting by browsers. For example, you want to show ads only to visitors with Google Chrome
- To NOT show the campaign at the following sites: - list of sites on which this campaign will NOT be displayed
- Campaign link - the default link for all campaign ads. You can specify it later when creating the ad.
The link supports the insertion of macros (parameter passing).

[CAMP_ID] - Campaign id
[BLOCK_ID] - Block id
[AD_ID] - Ad id
[SITE_ID] - Site id
[TIME_CLIK] - Click the date in the format YYYY-MM-DD_HH-MM-SS (2017-06-15_10-35-15)

After making the settings, click "Save campaign"







Adding ads to an ad campaign:
Go to the "Campaigns" campaign list page
Click the "Ads (n)" button of the campaign in which you want to add the advertisement.
Before you will be a page with a list of announcements of this campaign.
Announcements as well as campaigns can be launched, stopped, edited, deleted.
To add an advertisement click on the button "New ad"
A pop-up window for adding an advertisement will appear.
Fill out the announcement data.
You can add an image to the announcement. It will be proportionally cut off.
All form fields (except images) are required.

Video: https://youtu.be/LeQdo0VHwTo?t=580






Statistics:
To view the statistics, go to the "Statistics" page
A detailed overview of statistics can be found on this video: https://youtu.be/LeQdo0VHwTo?t=813






Settings:
Go to the "Settings" page

The following settings are available:

- Time settings - set the time on the server. It is recommended to set the correct time according to your time zone.
- Default page settings - the page where you will be redirected after authorization.
- Statistics storage settings - delete old statistics.
- Password settings - change password

Video: https://youtu.be/LeQdo0VHwTo?t=730


Black list ip:
Go to the "Filter" page
Here you can enter ip addresses that will not be shown ads.

Video: https://youtu.be/LeQdo0VHwTo?t=688







Password recovery:
- Go to http://yoursite.com
- Follow the link "Forgot password?"
- In the "Email" field, enter the Email of the administrator that you specified during the installation.
- Press the "RESET PASSWORD"

After that, a letter with a link for password recovery will come to your inbox.
Follow this link and enter a new password in the form.
The link is one-time. This means that this link can only change the password once.
However, never give this link to other people.






Author:
Danil Gladchenko
adflexsystem@gmail.com
balambasik@gmail.com
