<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Filter_model extends CI_Model
{


    public function __construct()
    {
        parent::__construct();

        $this->load->database();
    }


    /**
     * Checking ip for ban
     *
     * @param string $ip
     * @return bool
     */
    public function isBannedIp($ip = '')
    {
        $query = $this->db->get_where('banned_ip', array('ip' => $ip));
        return (bool) $query->num_rows() > 0;
    }


    /**
     * Banned ips array
     *
     * @return array
     */
    public function getBannedIps()
    {
        $query = $this->db->get('banned_ip');

        if ($query->num_rows() > 0) {
            foreach ($query->result() as $row) {
                $out[] = $row->ip;
            }

            return $out;
        }

        return array();
    }


    /**
     * Banned ips list
     *
     * @return string
     */
    public function getBannedIpsList()
    {
        $ipsList = '';
        foreach ($this->getBannedIps() as $ip) {
            $ipsList .= trim($ip) . PHP_EOL;
        }

        return $ipsList;
    }


    /**
     * Validate ip
     *
     * @param string $ip
     * @return bool
     */
    public function isValidIp($ip = '')
    {
        if (filter_var(trim($ip), FILTER_VALIDATE_IP) === false) {
            return false;
        }

        return true;
    }


    /**
     * Saving banned ip
     *
     * @param array $ips
     */
    public function setBannedIps($ips)
    {
        $add_ips = array();
        $this->db->empty_table('banned_ip');

        foreach ($ips as $ip) {
            if ($this->isValidIp($ip)) {
                $add_ips[] = trim($ip);
            }
        }

        foreach ($add_ips as $ip) {
            $insertQuery = $this->db->insert_string('banned_ip', array('ip' => $ip));
            $insertQuery = str_replace('INSERT INTO', 'INSERT IGNORE INTO', $insertQuery);

            $this->db->query($insertQuery);
        }
    }

}
