<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Block_model extends CI_Model {


    public function __construct()
    {
        parent::__construct();

        $this->load->database();
        $this->load->library('encryptor');
        $this->load->helper('url');
    }


    /**
     * Clear block name
     *
     * @param $blockName
     * @return string
     */
    public function clearBlockName($blockName)
    {
        $blockName = preg_replace('/[^a-zA-Zа-яА-Я0-9-_\s]/ui', '', $blockName);
        return mb_substr(trim($blockName), 0, 70);
    }


    /**
     * Block exists
     *
     * @param $blockID
     * @return bool
     */
    public function blockExists($blockID)
    {
        $query = $this->db->get_where('blocks', array('id' => $blockID));
        return (bool) $query->num_rows();
    }


    /**
     * Is there a block name
     *
     * @param $blockName
     * @return bool
     */
    public function blockNameExists($blockName)
    {
        $query = $this->db->get_where('blocks', array('block_name' => $blockName));
        return (bool) $query->num_rows();
    }


    /**
     * Block data
     *
     * @param $blockID
     * @return bool | array
     */
    public function getBlockData($blockID)
    {
        $query = $this->db->get_where('blocks', array('id' => $blockID));

        if ($query->num_rows() > 0) {
            return $query->row_array();
        }

        return false;
    }


    /**
     * Block data
     *
     * @param $blockID
     * @return bool | array
     */
    public function getBlockPresetData($preset_id)
    {
        // return;

        $data['site_id']                = $this->input->post('siteID', true);
        $data['block_name']             = 'preset1';
        $data['block_type']             = 1;
        $data['block_status']           = 1;
        $data['block_ads_img_position'] = 'top';
        $data['block_count_ads_width']  = 3;
        $data['block_count_ads_height'] = 2;
        $data['block_link_visibility']  = 0;

        $data['block_css_styles'] = '.block_ads{ overflow:hidden; width:100%; height:100%; background:#f7f7f7; border-color:#a6a6a6; border-width:1px; border-style:solid; line-height:normal; padding:5px; margin:0px; } .cell_margin{ padding:10px; } .cell_ads{ width:100%; text-align:center; } .img_ads{ display:inline-block; object-fit:cover; -o-object-fit:cover; -webkit-object-fit:cover; width:130px; margin-right:5px; margin-bottom:5px; border-width:1px; border-color:#449860; border-style:solid; height:130px; float:none; } .img_ads:hover{ border-color:#ff0000; } .title_ads{ display:block; margin-bottom:5px; font-family:Verdana, Geneva, sans-serif; font-size:15px; font-weight:bold; font-style:normal; text-decoration:none; color:#0a4fff; } .title_ads:hover{ color:#0a4fff; font-weight:bold; font-style:normal; text-decoration:underline; } .description_ads{ display:block; font-size:13px; text-decoration:none; color:#707070; font-weight:normal; font-style:normal; margin-bottom:5px; font-family:Verdana, Geneva, sans-serif; } .link_ads{ font-size:13px; text-decoration:none; color:#59946b; margin-bottom:5px; font-family:Verdana, Geneva, sans-serif; font-weight:normal; font-style:italic; } .link_ads:hover{ text-decoration:underline; color:#ff0000; font-weight:normal; font-style:italic; } ';


//        $data['block_css_styles'] = '.block_ads{ overflow:hidden; width:100%; height:100%; background:#ffffff; border-color:; border-width:0px; border-style:solid; line-height:normal; padding:5px; }'
//                . ' .cell_margin{ padding:5px; }'
//                . ' .cell_ads{ width:100%; text-align:center; }'
//                . ' .img_ads{ display:inline-block; object-fit:cover; -o-object-fit:cover; -webkit-object-fit:cover; width:250px; margin-right:5px; margin-bottom:5px; border-width:1px; border-color:#449860; border-style:solid; height:150px; float:none; }'
//                . ' .img_ads:hover{ border-color:#ff0000; } .title_ads{ display:block; margin-bottom:5px; font-family:Verdana, Geneva, sans-serif; font-size:16px; font-weight:bold; font-style:normal; text-decoration:underline; color:#1300ff; }'
//                . ' .title_ads:hover{ color:#ff0000; font-weight:bold; font-style:normal; text-decoration:underline; }'
//                . ' .description_ads{ display:block; font-size:14px; text-decoration:none; color:#707070; font-weight:normal; font-style:normal; margin-bottom:5px; font-family:Verdana, Geneva, sans-serif; }'
//                . ' .link_ads{ font-size:12px; text-decoration:none; color:#59946b; margin-bottom:5px; font-family:Tahoma, Geneva, sans-serif; font-weight:normal; font-style:normal; } .link_ads:hover{ text-decoration:underline; color:#ff0000; font-weight:normal; font-style:normal; } ';
        //return $data;



        $data[1] = array(
            'site_id'                => $this->input->post('siteID', true),
            'block_name'             => 'preset1',
            'block_ads_img_position' => 'top',
            'block_count_ads_width'  => '3',
            'block_count_ads_height' => '2',
            'block_css_styles'       => '.block_ads{ overflow:hidden; width:100%; height:100%; background:#f7f7f7; border-color:#a6a6a6; border-width:1px; border-style:solid; line-height:normal; padding:5px; margin:0px; } .cell_margin{ padding:10px; } .cell_ads{ width:100%; text-align:center; } .img_ads{ display:inline-block; object-fit:cover; -o-object-fit:cover; -webkit-object-fit:cover; width:130px; margin-right:5px; margin-bottom:5px; border-width:1px; border-color:#449860; border-style:solid; height:130px; float:none; } .img_ads:hover{ border-color:#ff0000; } .title_ads{ display:block; margin-bottom:5px; font-family:Verdana, Geneva, sans-serif; font-size:15px; font-weight:bold; font-style:normal; text-decoration:none; color:#0a4fff; } .title_ads:hover{ color:#0a4fff; font-weight:bold; font-style:normal; text-decoration:underline; } .description_ads{ display:block; font-size:13px; text-decoration:none; color:#707070; font-weight:normal; font-style:normal; margin-bottom:5px; font-family:Verdana, Geneva, sans-serif; } .link_ads{ font-size:13px; text-decoration:none; color:#59946b; margin-bottom:5px; font-family:Verdana, Geneva, sans-serif; font-weight:normal; font-style:italic; } .link_ads:hover{ text-decoration:underline; color:#ff0000; font-weight:normal; font-style:italic; } ',
            'block_link_visibility'  => '717171',
        );


        $data[2] = array(
            'site_id'                => $this->input->post('siteID', true),
            'block_name'             => 'preset 2 (1x2)',
            'block_ads_img_position' => 'top',
            'block_count_ads_width'  => '1',
            'block_count_ads_height' => '2',
            'block_css_styles'       => '.block_ads{ overflow:hidden; width:100%; height:100%; background:#ffffff; border-color:; border-width:0px; border-style:solid; line-height:normal; padding:5px; } .cell_margin{ padding:10px; } .cell_ads{ width:100%; text-align:center; } .img_ads{ display:inline-block; object-fit:cover; -o-object-fit:cover; -webkit-object-fit:cover; width:300px; margin-right:5px; margin-bottom:5px; border-width:0px; border-color:#449860; border-style:solid; height:250px; float:none; } .img_ads:hover{ border-color:#ff0000; } .title_ads{ display:block; margin-bottom:5px; font-family:Verdana, Geneva, sans-serif; font-size:16px; font-weight:bold; font-style:normal; text-decoration:none; color:#ff005c; } .title_ads:hover{ color:#ff0000; font-weight:bold; font-style:normal; text-decoration:underline; } .description_ads{ display:block; font-size:14px; text-decoration:none; color:#707070; font-weight:normal; font-style:normal; margin-bottom:5px; font-family:Verdana, Geneva, sans-serif; } .link_ads{ font-size:14px; text-decoration:none; color:#59946b; margin-bottom:5px; font-family:Tahoma, Geneva, sans-serif; font-weight:bold; font-style:normal; } .link_ads:hover{ text-decoration:underline; color:#59946b; font-weight:bold; font-style:normal; } ',
            'block_link_visibility'  => '717171',
        );

        $data[3] = array(
            'site_id'                => $this->input->post('siteID', true),
            'block_name'             => 'preset3',
            'block_ads_img_position' => 'no',
            'block_count_ads_width'  => '1',
            'block_count_ads_height' => '2',
            'block_css_styles'       => '.block_ads{ overflow:hidden; width:100%; height:100%; background:#f5f5f5; border-color:#cccccc; border-width:1px; border-style:solid; line-height:normal; padding:5px; } .cell_margin{ padding:10px; } .cell_ads{ width:100%; text-align:left; } .img_ads{ display:inline-block; object-fit:cover; -o-object-fit:cover; -webkit-object-fit:cover; width:300px; margin-right:5px; margin-bottom:5px; border-width:0px; border-color:#449860; border-style:solid; height:250px; float:none; } .img_ads:hover{ border-color:#ff0000; } .title_ads{ display:block; margin-bottom:5px; font-family:Verdana, Geneva, sans-serif; font-size:16px; font-weight:bold; font-style:normal; text-decoration:underline; color:#000aff; } .title_ads:hover{ color:#ff0000; font-weight:bold; font-style:normal; text-decoration:underline; } .description_ads{ display:block; font-size:14px; text-decoration:none; color:#707070; font-weight:normal; font-style:normal; margin-bottom:5px; font-family:Verdana, Geneva, sans-serif; } .link_ads{ font-size:14px; text-decoration:none; color:#59946b; margin-bottom:5px; font-family:Tahoma, Geneva, sans-serif; font-weight:bold; font-style:normal; } .link_ads:hover{ text-decoration:underline; color:#59946b; font-weight:bold; font-style:normal; } ',
            'block_link_visibility'  => '717171',
        );

        $data[4] = array(
            'site_id'                => $this->input->post('siteID', true),
            'block_name'             => 'preset4',
            'block_ads_img_position' => 'left',
            'block_count_ads_width'  => '1',
            'block_count_ads_height' => '3',
            'block_css_styles'       => '.block_ads{ overflow:hidden; width:430px; height:100%; background:#0b08bd; border-color:#00b2ff; border-width:1px; border-style:solid; line-height:normal; padding:5px; } .cell_margin{ padding:10px; } .cell_ads{ width:100%; text-align:left; } .img_ads{ display:inline-block; object-fit:cover; -o-object-fit:cover; -webkit-object-fit:cover; width:80px; margin-right:10px; margin-bottom:10px; border-width:1px; border-color:#ffffff; border-style:solid; height:80px; float:left; } .img_ads:hover{ border-color:#ff0000; } .title_ads{ display:inline; margin-bottom:10px; font-family:Verdana, Geneva, sans-serif; font-size:14px; font-weight:bold; font-style:normal; text-decoration:underline; color:#ffffff; } .title_ads:hover{ color:#ff0000; font-weight:bold; font-style:normal; text-decoration:underline; } .description_ads{ display:block; font-size:13px; text-decoration:none; color:#ffffff; font-weight:normal; font-style:normal; margin-bottom:10px; font-family:Verdana, Geneva, sans-serif; } .link_ads{ font-size:13px; text-decoration:none; color:#ffffff; margin-bottom:10px; font-family:Verdana, Geneva, sans-serif; font-weight:normal; font-style:italic; } .link_ads:hover{ text-decoration:underline; color:#ff0000; font-weight:normal; font-style:italic; } ',
            'block_link_visibility'  => '717171',
        );

        $data[5] = array(
            'site_id'                => $this->input->post('siteID', true),
            'block_name'             => 'preset5',
            'block_ads_img_position' => 'left',
            'block_count_ads_width'  => '2',
            'block_count_ads_height' => '1',
            'block_css_styles'       => '.block_ads{ overflow:hidden; width:100%; height:100%; background:#006561; border-color:#18ebe1; border-width:1px; border-style:solid; line-height:normal; padding:5px; } .cell_margin{ padding:10px; } .cell_ads{ width:100%; text-align:left; } .img_ads{ display:inline-block; object-fit:cover; -o-object-fit:cover; -webkit-object-fit:cover; width:80px; margin-right:10px; margin-bottom:10px; border-width:1px; border-color:#18ebe1; border-style:solid; height:80px; float:left; } .img_ads:hover{ border-color:#ff0000; } .title_ads{ display:inline; margin-bottom:10px; font-family:Verdana, Geneva, sans-serif; font-size:14px; font-weight:bold; font-style:normal; text-decoration:none; color:#18ebe1; } .title_ads:hover{ color:#ff0000; font-weight:bold; font-style:normal; text-decoration:underline; } .description_ads{ display:block; font-size:13px; text-decoration:none; color:#f0f0f0; font-weight:normal; font-style:normal; margin-bottom:10px; font-family:Verdana, Geneva, sans-serif; } .link_ads{ font-size:13px; text-decoration:none; color:#18e657; margin-bottom:10px; font-family:Verdana, Geneva, sans-serif; font-weight:normal; font-style:normal; } .link_ads:hover{ text-decoration:underline; color:#18e657; font-weight:normal; font-style:normal; } ',
            'block_link_visibility'  => '717171',
        );

        $data[6] = array(
            'site_id'                => $this->input->post('siteID', true),
            'block_name'             => 'preset6',
            'block_ads_img_position' => 'left',
            'block_count_ads_width'  => '1',
            'block_count_ads_height' => '3',
            'block_css_styles'       => '.block_ads{ overflow:hidden; width:450px; height:100%; background:#5d016d; border-color:#c262d3; border-width:1px; border-style:solid; line-height:normal; padding:5px; } .cell_margin{ padding:10px; } .cell_ads{ width:100%; text-align:left; } .img_ads{ display:inline-block; object-fit:cover; -o-object-fit:cover; -webkit-object-fit:cover; width:100px; margin-right:10px; margin-bottom:10px; border-width:1px; border-color:#c262d3; border-style:solid; height:100px; float:left; } .img_ads:hover{ border-color:#ff0000; } .title_ads{ display:inline; margin-bottom:10px; font-family:Verdana, Geneva, sans-serif; font-size:14px; font-weight:bold; font-style:normal; text-decoration:none; color:#c262d3; } .title_ads:hover{ color:#c262d3; font-weight:bold; font-style:normal; text-decoration:underline; } .description_ads{ display:block; font-size:13px; text-decoration:none; color:#d5c2d9; font-weight:normal; font-style:normal; margin-bottom:10px; font-family:Verdana, Geneva, sans-serif; } .link_ads{ font-size:13px; text-decoration:none; color:#c262d3; margin-bottom:10px; font-family:Verdana, Geneva, sans-serif; font-weight:normal; font-style:normal; } .link_ads:hover{ text-decoration:underline; color:#c262d3; font-weight:normal; font-style:normal; } ',
            'block_link_visibility'  => '717171',
        );

        $data[7] = array(
            'site_id'                => $this->input->post('siteID', true),
            'block_name'             => 'preset7',
            'block_ads_img_position' => 'top',
            'block_count_ads_width'  => '1',
            'block_count_ads_height' => '3',
            'block_css_styles'       => '.block_ads{ overflow:hidden; width:190px; height:100%; background:#a66f00; border-color:#ffab00; border-width:1px; border-style:solid; line-height:normal; padding:5px; margin:0px; } .cell_margin{ padding:10px; } .cell_ads{ width:100%; text-align:center; } .img_ads{ display:inline-block; object-fit:cover; -o-object-fit:cover; -webkit-object-fit:cover; width:150px; margin-right:1px; margin-bottom:1px; border-width:1px; border-color:#ffab00; border-style:solid; height:150px; float:none; } .img_ads:hover{ border-color:#ffab00; } .title_ads{ display:block; margin-bottom:1px; font-family:Verdana, Geneva, sans-serif; font-size:14px; font-weight:bold; font-style:normal; text-decoration:none; color:#ffab00; } .title_ads:hover{ color:#ffab00; font-weight:bold; font-style:normal; text-decoration:underline; } .description_ads{ display:block; font-size:13px; text-decoration:none; color:#f0f0f0; font-weight:normal; font-style:normal; margin-bottom:1px; font-family:Verdana, Geneva, sans-serif; } .link_ads{ font-size:13px; text-decoration:none; color:#18e657; margin-bottom:1px; font-family:Verdana, Geneva, sans-serif; font-weight:normal; font-style:normal; } .link_ads:hover{ text-decoration:underline; color:#18e657; font-weight:normal; font-style:normal; } ',
            'block_link_visibility'  => '707171',
        );

        $data[8] = array(
            'site_id'                => $this->input->post('siteID', true),
            'block_name'             => 'preset8',
            'block_ads_img_position' => 'left',
            'block_count_ads_width'  => '3',
            'block_count_ads_height' => '1',
            'block_css_styles'       => '.block_ads{ overflow:hidden; width:100%; height:100%; background:#679b00; border-color:#9fee00; border-width:1px; border-style:solid; line-height:normal; padding:5px; margin:0px; } .cell_margin{ padding:10px; } .cell_ads{ width:100%; text-align:left; } .img_ads{ display:inline-block; object-fit:cover; -o-object-fit:cover; -webkit-object-fit:cover; width:70px; margin-right:5px; margin-bottom:5px; border-width:1px; border-color:#9fee00; border-style:solid; height:90px; float:left; } .img_ads:hover{ border-color:#9fee00; } .title_ads{ display:inline; margin-bottom:5px; font-family:Tahoma, Geneva, sans-serif; font-size:12px; font-weight:bold; font-style:normal; text-decoration:none; color:#9fee00; } .title_ads:hover{ color:#9fee00; font-weight:bold; font-style:normal; text-decoration:underline; } .description_ads{ display:block; font-size:12px; text-decoration:none; color:#f0f0f0; font-weight:normal; font-style:normal; margin-bottom:5px; font-family:Tahoma, Geneva, sans-serif; } .link_ads{ font-size:12px; text-decoration:none; color:#18e657; margin-bottom:5px; font-family:Tahoma, Geneva, sans-serif; font-weight:normal; font-style:normal; } .link_ads:hover{ text-decoration:underline; color:#18e657; font-weight:normal; font-style:normal; } ',
            'block_link_visibility'  => '717171',
        );

        $data[9] = array(
            'site_id'                => $this->input->post('siteID', true),
            'block_name'             => 'preset9',
            'block_ads_img_position' => 'no',
            'block_count_ads_width'  => '1',
            'block_count_ads_height' => '4',
            'block_css_styles'       => '.block_ads{ overflow:hidden; width:350px; height:100%; background:#034569; border-color:#b0e0fa; border-width:1px; border-style:solid; line-height:normal; padding:5px; margin:0px; } .cell_margin{ padding:10px; } .cell_ads{ width:100%; text-align:left; } .img_ads{ display:inline-block; object-fit:cover; -o-object-fit:cover; -webkit-object-fit:cover; width:70px; margin-right:5px; margin-bottom:5px; border-width:1px; border-color:#9fee00; border-style:solid; height:90px; float:left; } .img_ads:hover{ border-color:#9fee00; } .title_ads{ display:inline; margin-bottom:5px; font-family:Arial, Helvetica, sans-serif; font-size:14px; font-weight:bold; font-style:normal; text-decoration:none; color:#b0e0fa; } .title_ads:hover{ color:#b0e0fa; font-weight:bold; font-style:normal; text-decoration:underline; } .description_ads{ display:block; font-size:13px; text-decoration:none; color:#c7c7c7; font-weight:normal; font-style:normal; margin-bottom:5px; font-family:Arial, Helvetica, sans-serif; } .link_ads{ font-size:12px; text-decoration:none; color:#b0e0fa; margin-bottom:5px; font-family:Tahoma, Geneva, sans-serif; font-weight:normal; font-style:normal; } .link_ads:hover{ text-decoration:underline; color:#b0e0fa; font-weight:normal; font-style:normal; } ',
            'block_link_visibility'  => '717171',
        );

        $data[10] = array(
            'site_id'                => $this->input->post('siteID', true),
            'block_name'             => 'preset10',
            'block_ads_img_position' => 'top',
            'block_count_ads_width'  => '2',
            'block_count_ads_height' => '1',
            'block_css_styles'       => '.block_ads{ overflow:hidden; width:450px; height:100%; background:#85a000; border-color:#e3fb71; border-width:1px; border-style:solid; line-height:normal; padding:5px; margin:0px; } .cell_margin{ padding:10px; } .cell_ads{ width:100%; text-align:center; } .img_ads{ display:inline-block; object-fit:cover; -o-object-fit:cover; -webkit-object-fit:cover; width:180px; margin-right:5px; margin-bottom:5px; border-width:1px; border-color:#9fee00; border-style:solid; height:140px; float:none; } .img_ads:hover{ border-color:#9fee00; } .title_ads{ display:block; margin-bottom:5px; font-family:Verdana, Geneva, sans-serif; font-size:14px; font-weight:bold; font-style:normal; text-decoration:none; color:#e3fb71; } .title_ads:hover{ color:#e3fb71; font-weight:bold; font-style:normal; text-decoration:underline; } .description_ads{ display:block; font-size:13px; text-decoration:none; color:#ffffff; font-weight:normal; font-style:normal; margin-bottom:5px; font-family:Verdana, Geneva, sans-serif; } .link_ads{ font-size:12px; text-decoration:none; color:#e3fb71; margin-bottom:5px; font-family:Tahoma, Geneva, sans-serif; font-weight:normal; font-style:normal; } .link_ads:hover{ text-decoration:underline; color:#e3fb71; font-weight:normal; font-style:normal; } ',
            'block_link_visibility'  => '717171',
        );

        $data[11] = array(
            'site_id'                => $this->input->post('siteID', true),
            'block_name'             => 'preset11',
            'block_ads_img_position' => 'top',
            'block_count_ads_width'  => '2',
            'block_count_ads_height' => '2',
            'block_css_styles'       => '.block_ads{ overflow:hidden; width:500px; height:100%; background:#a60000; border-color:#ff6770; border-width:1px; border-style:solid; line-height:normal; padding:5px; margin:0px; } .cell_margin{ padding:10px; } .cell_ads{ width:100%; text-align:center; } .img_ads{ display:inline-block; object-fit:cover; -o-object-fit:cover; -webkit-object-fit:cover; width:120px; margin-right:5px; margin-bottom:5px; border-width:1px; border-color:#ee0000; border-style:solid; height:120px; float:none; } .img_ads:hover{ border-color:#ee0000; } .title_ads{ display:block; margin-bottom:5px; font-family:Times New Roman, Times, serif; font-size:16px; font-weight:bold; font-style:normal; text-decoration:none; color:#ff0000; } .title_ads:hover{ color:#ff001f; font-weight:bold; font-style:normal; text-decoration:underline; } .description_ads{ display:block; font-size:14px; text-decoration:none; color:#ffffff; font-weight:normal; font-style:normal; margin-bottom:5px; font-family:Times New Roman, Times, serif; } .link_ads{ font-size:12px; text-decoration:none; color:#ff0000; margin-bottom:5px; font-family:Times New Roman, Times, serif; font-weight:normal; font-style:normal; } .link_ads:hover{ text-decoration:underline; color:#ff0000; font-weight:normal; font-style:normal; } ',
            'block_link_visibility'  => '707171',
        );

        $data[12] = array(
            'site_id'                => $this->input->post('siteID', true),
            'block_name'             => 'preset12',
            'block_ads_img_position' => 'top',
            'block_count_ads_width'  => '1',
            'block_count_ads_height' => '1',
            'block_css_styles'       => '.block_ads{ overflow:hidden; width:100%; height:100%; background:; border-color:#000000; border-width:0px; border-style:solid; line-height:normal; padding:5px; } .cell_margin{ padding:10px; } .cell_ads{ width:100%; text-align:center; } .img_ads{ display:inline-block; object-fit:cover; -o-object-fit:cover; -webkit-object-fit:cover; width:300px; margin-right:5px; margin-bottom:5px; border-width:0px; border-color:#449860; border-style:solid; height:250px; float:none; } .img_ads:hover{ border-color:#ff0000; } .title_ads{ display:block; margin-bottom:5px; font-family:Verdana, Geneva, sans-serif; font-size:16px; font-weight:bold; font-style:normal; text-decoration:none; color:#ff005c; } .title_ads:hover{ color:#ff0000; font-weight:bold; font-style:normal; text-decoration:underline; } .description_ads{ display:block; font-size:14px; text-decoration:none; color:#707070; font-weight:normal; font-style:normal; margin-bottom:5px; font-family:Verdana, Geneva, sans-serif; } .link_ads{ font-size:14px; text-decoration:none; color:#59946b; margin-bottom:5px; font-family:Tahoma, Geneva, sans-serif; font-weight:bold; font-style:normal; } .link_ads:hover{ text-decoration:underline; color:#59946b; font-weight:bold; font-style:normal; } ',
            'block_link_visibility'  => '707070',
        );

        $data[14] = array(
            'site_id'                => $this->input->post('siteID', true),
            'block_name'             => 'preset14',
            'block_ads_img_position' => 'left',
            'block_count_ads_width'  => '1',
            'block_count_ads_height' => '2',
            'block_css_styles'       => '.block_ads{ overflow:hidden; width:400px; height:100%; background:#5b5643; border-color:#d5921a; border-width:2px; border-style:solid; line-height:normal; padding:5px; margin:0px; } .cell_margin{ padding:10px; } .cell_ads{ width:100%; text-align:left; } .img_ads{ display:inline-block; object-fit:cover; -o-object-fit:cover; -webkit-object-fit:cover; width:90px; margin-right:5px; margin-bottom:5px; border-width:2px; border-color:#d5921a; border-style:solid; height:90px; float:left; } .img_ads:hover{ border-color:#d5921a; } .title_ads{ display:inline; margin-bottom:5px; font-family:Times New Roman, Times, serif; font-size:16px; font-weight:bold; font-style:normal; text-decoration:none; color:#d5921a; } .title_ads:hover{ color:#d5921a; font-weight:bold; font-style:normal; text-decoration:underline; } .description_ads{ display:block; font-size:14px; text-decoration:none; color:#ffe600; font-weight:normal; font-style:normal; margin-bottom:5px; font-family:Times New Roman, Times, serif; } .link_ads{ font-size:12px; text-decoration:none; color:#d5921a; margin-bottom:5px; font-family:Times New Roman, Times, serif; font-weight:normal; font-style:normal; } .link_ads:hover{ text-decoration:underline; color:#d5921a; font-weight:normal; font-style:normal; } ',
            'block_link_visibility'  => '717171',
        );

        $data[15] = array(
            'site_id'                => $this->input->post('siteID', true),
            'block_name'             => 'preset15',
            'block_ads_img_position' => 'top',
            'block_count_ads_width'  => '3',
            'block_count_ads_height' => '2',
            'block_css_styles'       => '.block_ads{ overflow:hidden; width:600px; height:100%; background:#dae5ea; border-color:#0195ae; border-width:2px; border-style:solid; line-height:normal; padding:5px; margin:0px; } .cell_margin{ padding:10px; } .cell_ads{ width:100%; text-align:center; } .img_ads{ display:inline-block; object-fit:cover; -o-object-fit:cover; -webkit-object-fit:cover; width:90px; margin-right:5px; margin-bottom:5px; border-width:2px; border-color:#0195ae; border-style:solid; height:90px; float:none; } .img_ads:hover{ border-color:#0195ae; } .title_ads{ display:block; margin-bottom:5px; font-family:Arial, Helvetica, sans-serif; font-size:14px; font-weight:bold; font-style:normal; text-decoration:none; color:#0195ae; } .title_ads:hover{ color:#0195ae; font-weight:bold; font-style:normal; text-decoration:underline; } .description_ads{ display:block; font-size:13px; text-decoration:none; color:#00788c; font-weight:normal; font-style:normal; margin-bottom:5px; font-family:Arial, Helvetica, sans-serif; } .link_ads{ font-size:12px; text-decoration:none; color:#057a18; margin-bottom:5px; font-family:Arial, Helvetica, sans-serif; font-weight:normal; font-style:normal; } .link_ads:hover{ text-decoration:underline; color:#057a18; font-weight:normal; font-style:normal; } ',
            'block_link_visibility'  => '717171',
        );

        $data[16] = array(
            'site_id'                => $this->input->post('siteID', true),
            'block_name'             => 'preset16',
            'block_ads_img_position' => 'top',
            'block_count_ads_width'  => '3',
            'block_count_ads_height' => '2',
            'block_css_styles'       => '.block_ads{ overflow:hidden; width:600px; height:100%; background:#f2f2f2; border-color:#bdbdbd; border-width:1px; border-style:solid; line-height:normal; padding:5px; margin:0px; } .cell_margin{ padding:10px; } .cell_ads{ width:100%; text-align:center; } .img_ads{ display:inline-block; object-fit:cover; -o-object-fit:cover; -webkit-object-fit:cover; width:110px; margin-right:5px; margin-bottom:5px; border-width:1px; border-color:#bdbdbd; border-style:solid; height:110px; float:none; } .img_ads:hover{ border-color:#000000; } .title_ads{ display:block; margin-bottom:5px; font-family:Verdana, Geneva, sans-serif; font-size:14px; font-weight:bold; font-style:normal; text-decoration:none; color:#707070; } .title_ads:hover{ color:#000000; font-weight:bold; font-style:normal; text-decoration:underline; } .description_ads{ display:block; font-size:13px; text-decoration:none; color:#919191; font-weight:normal; font-style:normal; margin-bottom:5px; font-family:Verdana, Geneva, sans-serif; } .link_ads{ font-size:12px; text-decoration:none; color:#057a18; margin-bottom:5px; font-family:Verdana, Geneva, sans-serif; font-weight:normal; font-style:normal; } .link_ads:hover{ text-decoration:underline; color:#057a18; font-weight:normal; font-style:normal; } ',
            'block_link_visibility'  => '717171',
        );

        return isset($data[$preset_id]) ? $data[$preset_id] : $data[0];
    }


    /**
     * Get block name
     *
     * @param $blockID
     * @return mixed
     */
    public function getBlockName($blockID)
    {
        $block_data = $this->getBlockData($blockID);
        return $block_data['block_name'];
    }


    /**
     * Create new block
     *
     * @param $siteID
     * @param $blockName
     * @return bool
     */
    public function createBlock($siteID, $blockName)
    {
        if (!ctype_digit((string) $siteID) OR ! $blockName) {
            return false;
        }

        $data['site_id']                = $siteID;
        $data['block_name']             = $this->clearBlockName($blockName);
        $data['block_type']             = 1;
        $data['block_status']           = 1;
        $data['block_ads_img_position'] = 'top';
        $data['block_count_ads_width']  = 3;
        $data['block_count_ads_height'] = 2;
        $data['block_link_visibility']  = 1;
        $data['block_date_add']         = time();
//        $data['block_css_styles']       = '.block_ads{ overflow:hidden; width:100%; height:100%; background:#ececec; border-color:#0001ff;'
//                . ' border-width:1px; border-style:solid; line-height:normal; padding:5px; } .cell_margin{ padding:5px; }'
//                . ' .cell_ads{ width:100%; text-align:left; } .img_ads{ display:inline-block; object-fit: cover; -o-object-fit: cover; -webkit-object-fit: cover; width:90px; margin-right:5px;'
//                . ' margin-bottom:5px; border-width:1px; border-color:#449860; border-style:solid; height:90px; float:left; }'
//                . ' .img_ads:hover{ border-color:#ff0000; } .title_ads{ display:inline; margin-bottom:5px;'
//                . ' font-family:Tahoma, Geneva, sans-serif; font-size:14px; font-weight:bold; font-style:normal;'
//                . ' text-decoration:none; color:#1a0dbd; } .title_ads:hover{ color:#ff0000; font-weight:bold; font-style:normal;'
//                . ' text-decoration:underline; } .description_ads{ display:block; font-size:13px; text-decoration:none; color:#000000; font-weight: normal; font-style: normal; text-decoration: none;'
//                . ' margin-bottom:5px; font-family:Tahoma, Geneva, sans-serif; } .link_ads{font-size:12px; text-decoration:none;'
//                . ' color:#59946b; margin-bottom:5px; font-family:Tahoma, Geneva, sans-serif; font-weight:normal; font-style:normal; }'
//                . ' .link_ads:hover{ text-decoration:underline; color:#ff0000; font-weight:normal; font-style:normal; } ';
        $data['block_css_styles']       = '.block_ads{ overflow:hidden; width:100%; height:100%; background:#f7f7f7; border-color:#a6a6a6; border-width:1px; border-style:solid; line-height:normal; padding:5px; margin:0px; } .cell_margin{ padding:10px; } .cell_ads{ width:100%; text-align:center; } .img_ads{ display:inline-block; object-fit:cover; -o-object-fit:cover; -webkit-object-fit:cover; width:130px; margin-right:5px; margin-bottom:5px; border-width:1px; border-color:#449860; border-style:solid; height:130px; float:none; } .img_ads:hover{ border-color:#ff0000; } .title_ads{ display:block; margin-bottom:5px; font-family:Verdana, Geneva, sans-serif; font-size:15px; font-weight:bold; font-style:normal; text-decoration:none; color:#0a4fff; } .title_ads:hover{ color:#0a4fff; font-weight:bold; font-style:normal; text-decoration:underline; } .description_ads{ display:block; font-size:13px; text-decoration:none; color:#707070; font-weight:normal; font-style:normal; margin-bottom:5px; font-family:Verdana, Geneva, sans-serif; } .link_ads{ font-size:13px; text-decoration:none; color:#59946b; margin-bottom:5px; font-family:Verdana, Geneva, sans-serif; font-weight:normal; font-style:italic; } .link_ads:hover{ text-decoration:underline; color:#ff0000; font-weight:normal; font-style:italic; } ';

        $this->db->insert('blocks', $data);
        return $this->db->insert_id();
    }


    /**
     * Save block
     *
     * @param $blockParams
     */
    public function setBlock($blockParams)
    {
        $blockParams['block_name'] = $this->clearBlockName($blockParams['block_name']);
        $this->db->where('id', $blockParams['id']);
        $this->db->update('blocks', $blockParams);
    }


    /**
     * Delete block
     *
     * @param $blockID
     * @return bool
     */
    public function removeBlock($blockID)
    {
        if (ctype_digit((string) $blockID)) {
            $this->db->delete('blocks', array('id' => $blockID));
        }

        return false;
    }


    /**
     * Play block
     *
     * @param $blockID
     * @return bool
     */
    public function playBlock($blockID)
    {
        if (ctype_digit((string) $blockID)) {
            $this->db->where('id', $blockID)->update('blocks', array('block_status' => 1));
        }

        return false;
    }


    /**
     * Stop block
     *
     * @param $blockID
     * @return bool
     */
    public function stopBlock($blockID)
    {
        if (ctype_digit((string) $blockID)) {
            $this->db->where('id', $blockID)->update('blocks', array('block_status' => 0));
        }

        return false;
    }


    /**
     * Copy block
     *
     * @param $blockID
     * @param $pasteSiteID
     * @param $newBlockName
     * @return bool
     */
    public function copyBlock($blockID, $pasteSiteID, $newBlockName)
    {
        if (!ctype_digit((string) $blockID) OR ! ctype_digit((string) $pasteSiteID) OR ! $newBlockName) {
            return false;
        }

        // site ID exists
        if ($this->db->get_where('sites', array('id' => $pasteSiteID))->num_rows() == 0) {
            return false;
        }

        $query = $this->db->get_where('blocks', array('id' => $blockID));

        if ($query->num_rows() > 0) {
            $row = $query->row_array();

            $row['id']             = '';
            $row['site_id']        = $pasteSiteID;
            $row['block_views']    = '';
            $row['block_clicks']   = '';
            $row['block_date_add'] = time();
            $row['block_name']     = $this->clearBlockName($newBlockName);

            $this->db->insert('blocks', $row);

            return true;
        }

        return false;
    }


    /**
     * Checking the block for activity
     *
     * @param $blockData
     * @return bool
     */
    public function isActiveBlock($blockData)
    {
        return (bool) $blockData['block_status'];
    }


    /**
     * Generating the call code of the ad unit
     *
     * @param $siteID
     * @param $domainName
     * @param $blockID
     * @param $event
     * @return boolean|string
     */
    public function generateJsBlockCode($siteID, $domainName, $blockID, $event = 'view')
    {
        if (!ctype_digit((string) $siteID) OR ! ctype_digit((string) $blockID) OR empty($domainName)) {
            return false;
        }

        $linkData['type']        = $event;
        $linkData['site_id']     = $siteID;
        $linkData['site_domain'] = $domainName;
        $linkData['block_id']    = $blockID;

        $link = base_url('/view/' . $this->encryptor->xorEncrypt(json_encode($linkData)) . '/" + document.domain + ".js?t=" + token;');

        $link = ltrim($link, "http:");
        $link = ltrim($link, "https:");


        $code = '<div id="_' . $blockID . '"></div>' . PHP_EOL;
        $code .= '<script type="text/javascript">' . PHP_EOL;
        $code .= '(function() {' . PHP_EOL;
        $code .= 'var token = Math.random();' . PHP_EOL;
        $code .= 'var code = document.createElement("script");' . PHP_EOL;
        $code .= 'code.type = "text/javascript";' . PHP_EOL;
        $code .= 'code.async = true;' . PHP_EOL;
        $code .= 'code.src = "' . $link . PHP_EOL;
        $code .= 'var s = document.getElementsByTagName("script")[0];' . PHP_EOL;
        $code .= 's.parentNode.insertBefore(code, s);' . PHP_EOL;
        $code .= '})();' . PHP_EOL;
        $code .= '</script>' . PHP_EOL;

        return $code;
    }
}
