<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Set_stat_model extends CI_Model
{


    public function __construct()
    {
        parent::__construct();

        $this->load->database();
    }


    /**
     * @param $viewData
     * @param $blockData
     */
    public function setSiteStatView($viewData, $blockData)
    {
        $this->db->select('site_id');
        $this->db->where('event', 'w');
        $this->db->where('site_id', $blockData['site_id']);
        $this->db->where('date', date('Ymd'));
        $query = $this->db->get('_stat_sites');

        // update
        if ($query->num_rows() > 0) {
            $this->db->where('event', 'w');
            $this->db->where('date', date('Ymd'));
            $this->db->where('site_id', $blockData['site_id']);
            $this->db->set('total_views', 'total_views + 1', false);
            $this->db->set($viewData['type'], $viewData['type'] . ' + 1', false);
            $this->db->set($viewData['os'], $viewData['os'] . ' + 1', false);
            $this->db->set($viewData['browser'], $viewData['browser'] . ' + 1', false);
            $this->db->set($viewData['geo_alfa3'], $viewData['geo_alfa3'] . ' + 1', false);
            $this->db->update('_stat_sites');
        }

        // create
        else {
            $this->db->set('site_id', $blockData['site_id']);
            $this->db->set('event', 'w');
            $this->db->set('total_views', '1');
            $this->db->set('date', date('Ymd'));
            $this->db->set($viewData['type'], '1');
            $this->db->set($viewData['os'], '1');
            $this->db->set($viewData['browser'], '1');
            $this->db->set($viewData['geo_alfa3'], '1');
            $this->db->insert('_stat_sites');
        }
    }


    /**
     * @param $clickData
     * @param $deviceData
     */
    public function setSiteStatClick($clickData, $deviceData)
    {
        $this->db->select('site_id');
        $this->db->where('event', 'c');
        $this->db->where('site_id', $clickData['site_id']);
        $this->db->where('date', date('Ymd'));
        $query = $this->db->get('_stat_sites');

        // update
        if ($query->num_rows() > 0) {
            $this->db->where('event', 'c');
            $this->db->where('date', date('Ymd'));
            $this->db->where('site_id', $clickData['site_id']);
            $this->db->set('total_clicks', 'total_clicks + 1', false);
            $this->db->set($deviceData['type'], $deviceData['type'] . ' + 1', false);
            $this->db->set($deviceData['os'], $deviceData['os'] . ' + 1', false);
            $this->db->set($deviceData['browser'], $deviceData['browser'] . ' + 1', false);
            $this->db->set($deviceData['geo_alfa3'], $deviceData['geo_alfa3'] . ' + 1', false);
            $this->db->update('_stat_sites');
        }

        // create
        else {
            $this->db->set('site_id', $clickData['site_id']);
            $this->db->set('event', 'c');
            $this->db->set('total_clicks', '1');
            $this->db->set('date', date('Ymd'));
            $this->db->set($deviceData['type'], '1');
            $this->db->set($deviceData['os'], '1');
            $this->db->set($deviceData['browser'], '1');
            $this->db->set($deviceData['geo_alfa3'], '1');
            $this->db->insert('_stat_sites');
        }
    }


    /**
     * @param $viewData
     * @param $blockData
     */
    public function setBlockStatView($viewData, $blockData)
    {
        $this->db->select('block_id');
        $this->db->where('event', 'w');
        $this->db->where('block_id', $blockData['id']);
        $this->db->where('date', date('Ymd'));
        $query = $this->db->get('_stat_blocks');

        // update
        if ($query->num_rows() > 0) {
            $this->db->where('event', 'w');
            $this->db->where('date', date('Ymd'));
            $this->db->where('block_id', $blockData['id']);
            $this->db->set('total_views', 'total_views + 1', false);
            $this->db->set($viewData['type'], $viewData['type'] . ' + 1', false);
            $this->db->set($viewData['os'], $viewData['os'] . ' + 1', false);
            $this->db->set($viewData['browser'], $viewData['browser'] . ' + 1', false);
            $this->db->set($viewData['geo_alfa3'], $viewData['geo_alfa3'] . ' + 1', false);
            $this->db->update('_stat_blocks');
        }

        // create
        else {
            $this->db->set('block_id', $blockData['id']);
            $this->db->set('event', 'w');
            $this->db->set('total_views', '1');
            $this->db->set('date', date('Ymd'));
            $this->db->set($viewData['type'], '1');
            $this->db->set($viewData['os'], '1');
            $this->db->set($viewData['browser'], '1');
            $this->db->set($viewData['geo_alfa3'], '1');
            $this->db->insert('_stat_blocks');
        }
    }


    /**
     * @param $clickData
     * @param $deviceData
     */
    public function setBlockStatClick($clickData, $deviceData)
    {
        $this->db->select('block_id');
        $this->db->where('event', 'c');
        $this->db->where('block_id', $clickData['block_id']);
        $this->db->where('date', date('Ymd'));
        $query = $this->db->get('_stat_blocks');

        // update
        if ($query->num_rows() > 0) {
            $this->db->where('event', 'c');
            $this->db->where('date', date('Ymd'));
            $this->db->where('block_id', $clickData['block_id']);
            $this->db->set('total_clicks', 'total_clicks + 1', false);
            $this->db->set($deviceData['type'], $deviceData['type'] . ' + 1', false);
            $this->db->set($deviceData['os'], $deviceData['os'] . ' + 1', false);
            $this->db->set($deviceData['browser'], $deviceData['browser'] . ' + 1', false);
            $this->db->set($deviceData['geo_alfa3'], $deviceData['geo_alfa3'] . ' + 1', false);
            $this->db->update('_stat_blocks');
        }

        // create
        else {
            $this->db->set('block_id', $clickData['block_id']);
            $this->db->set('event', 'c');
            $this->db->set('total_clicks', '1');
            $this->db->set('date', date('Ymd'));
            $this->db->set($deviceData['type'], '1');
            $this->db->set($deviceData['os'], '1');
            $this->db->set($deviceData['browser'], '1');
            $this->db->set($deviceData['geo_alfa3'], '1');
            $this->db->insert('_stat_blocks');
        }
    }


    /**
     * @param $viewData
     * @param $adsArray
     */
    public function setCampsStatView($viewData, $adsArray)
    {
        foreach ($adsArray as $value) {
            $_camps[] = $value['camp_id'];
        }

        $camps = array_values(array_unique($_camps));

        for ($i = 0; $i < count($camps); $i++) {
            $this->db->select('camp_id');
            $this->db->where('event', 'w');
            $this->db->where('camp_id', $camps[$i]);
            $this->db->where('date', date('Ymd'));
            $query = $this->db->get('_stat_camps');

            // update
            if ($query->num_rows() > 0) {
                $this->db->where('event', 'w');
                $this->db->where('date', date('Ymd'));
                $this->db->where('camp_id', $camps[$i]);
                $this->db->set('total_views', 'total_views + 1', false);
                $this->db->set($viewData['type'], $viewData['type'] . ' + 1', false);
                $this->db->set($viewData['os'], $viewData['os'] . ' + 1', false);
                $this->db->set($viewData['browser'], $viewData['browser'] . ' + 1', false);
                $this->db->set($viewData['geo_alfa3'], $viewData['geo_alfa3'] . ' + 1', false);
                $this->db->update('_stat_camps');
            }

            // create
            else {
                $this->db->set('camp_id', $camps[$i]);
                $this->db->set('event', 'w');
                $this->db->set('total_views', '1');
                $this->db->set('date', date('Ymd'));
                $this->db->set($viewData['type'], '1');
                $this->db->set($viewData['os'], '1');
                $this->db->set($viewData['browser'], '1');
                $this->db->set($viewData['geo_alfa3'], '1');
                $this->db->insert('_stat_camps');
            }
        }
    }


    /**
     * @param $clickData
     * @param $deviceData
     */
    public function setCampStatClick($clickData, $deviceData)
    {
        $this->db->select('camp_id');
        $this->db->where('event', 'c');
        $this->db->where('camp_id', $clickData['camp_id']);
        $this->db->where('date', date('Ymd'));
        $query = $this->db->get('_stat_camps');

        // update
        if ($query->num_rows() > 0) {
            $this->db->where('event', 'c');
            $this->db->where('date', date('Ymd'));
            $this->db->where('camp_id', $clickData['camp_id']);
            $this->db->set('total_clicks', 'total_clicks + 1', false);
            $this->db->set($deviceData['type'], $deviceData['type'] . ' + 1', false);
            $this->db->set($deviceData['os'], $deviceData['os'] . ' + 1', false);
            $this->db->set($deviceData['browser'], $deviceData['browser'] . ' + 1', false);
            $this->db->set($deviceData['geo_alfa3'], $deviceData['geo_alfa3'] . ' + 1', false);
            $this->db->update('_stat_camps');
        }

        // create
        else {
            $this->db->set('camp_id', $clickData['camp_id']);
            $this->db->set('event', 'c');
            $this->db->set('total_clicks', '1');
            $this->db->set('date', date('Ymd'));
            $this->db->set($deviceData['type'], '1');
            $this->db->set($deviceData['os'], '1');
            $this->db->set($deviceData['browser'], '1');
            $this->db->set($deviceData['geo_alfa3'], '1');
            $this->db->insert('_stat_camps');
        }
    }


    /**
     * @param $viewData
     * @param $adsArray
     */
    public function setAdsStatView($viewData, $adsArray)
    {
        for ($i = 0; $i < count($adsArray); $i++) {
            $this->db->select('ad_id');
            $this->db->where('event', 'w');
            $this->db->where('ad_id', $adsArray[$i]['id']);
            $this->db->where('date', date('Ymd'));
            $query = $this->db->get('_stat_ads');

            // update
            if ($query->num_rows() > 0) {
                $this->db->where('event', 'w');
                $this->db->where('date', date('Ymd'));
                $this->db->where('ad_id', $adsArray[$i]['id']);
                $this->db->set('total_views', 'total_views + 1', false);
                $this->db->set($viewData['type'], $viewData['type'] . ' + 1', false);
                $this->db->set($viewData['os'], $viewData['os'] . ' + 1', false);
                $this->db->set($viewData['browser'], $viewData['browser'] . ' + 1', false);
                $this->db->set($viewData['geo_alfa3'], $viewData['geo_alfa3'] . ' + 1', false);
                $this->db->update('_stat_ads');
            }

            // create
            else {
                $this->db->set('ad_id', $adsArray[$i]['id']);
                $this->db->set('event', 'w');
                $this->db->set('total_views', '1');
                $this->db->set('date', date('Ymd'));
                $this->db->set($viewData['type'], '1');
                $this->db->set($viewData['os'], '1');
                $this->db->set($viewData['browser'], '1');
                $this->db->set($viewData['geo_alfa3'], '1');
                $this->db->insert('_stat_ads');
            }
        }
    }


    /**
     * @param $clickData
     * @param $deviceData
     */
    public function setOneAdStatClick($clickData, $deviceData)
    {
        $this->db->select('ad_id');
        $this->db->where('event', 'c');
        $this->db->where('ad_id', $clickData['ad_id']);
        $this->db->where('date', date('Ymd'));
        $query = $this->db->get('_stat_ads');

        // update
        if ($query->num_rows() > 0) {
            $this->db->where('event', 'c');
            $this->db->where('date', date('Ymd'));
            $this->db->where('ad_id', $clickData['ad_id']);
            $this->db->set('total_clicks', 'total_clicks + 1', false);
            $this->db->set($deviceData['type'], $deviceData['type'] . ' + 1', false);
            $this->db->set($deviceData['os'], $deviceData['os'] . ' + 1', false);
            $this->db->set($deviceData['browser'], $deviceData['browser'] . ' + 1', false);
            $this->db->set($deviceData['geo_alfa3'], $deviceData['geo_alfa3'] . ' + 1', false);
            $this->db->update('_stat_ads');
        }

        // create
        else {
            $this->db->set('ad_id', $clickData['ad_id']);
            $this->db->set('event', 'c');
            $this->db->set('total_clicks', '1');
            $this->db->set('date', date('Ymd'));
            $this->db->set($deviceData['type'], '1');
            $this->db->set($deviceData['os'], '1');
            $this->db->set($deviceData['browser'], '1');
            $this->db->set($deviceData['geo_alfa3'], '1');
            $this->db->insert('_stat_ads');
        }
    }


    /**
     * @param $siteID
     * @param $adsArray
     */
    public function setFastStatSiteViews($siteID, $adsArray)
    {
        $this->db->set('site_views', 'site_views + ' . count($adsArray), false);
        $this->db->where_in('id', $siteID);
        $this->db->update('sites');
    }


    /**
     * @param $siteID
     */
    public function setFastStatSiteClicks($siteID)
    {
        $this->db->set('site_clicks', 'site_clicks + 1', false);
        $this->db->where('id', $siteID);
        $this->db->update('sites');
    }


    /**
     * @param $block_ID
     */
    public function setFastStatBlockViews($block_ID)
    {
        $this->db->set('block_views', 'block_views + 1', false);
        $this->db->where('id', $block_ID);
        $this->db->update('blocks');
    }


    /**
     * @param $block_ID
     */
    public function setFastStatBlockClicks($block_ID)
    {
        $this->db->set('block_clicks', 'block_clicks + 1', false);
        $this->db->where('id', $block_ID);
        $this->db->update('blocks');
    }


    /**
     * @param $validAds
     */
    public function setFastStatCampViews($validAds)
    {
        // Get the campaign id of each ad
        foreach ($validAds as $ad) {
            $_camps[] = $ad['camp_id'];
        }

        // Remove duplicate id campaigns
        $camps = array_unique($_camps);

        // записываем показ в статистику каждой кампании
        foreach ($camps as $camp_id) {
            $this->db->set('camp_views', 'camp_views + 1', false);
            $this->db->where('id', $camp_id);
            $this->db->update('camps');
        }
    }


    /**
     * @param $campID
     */
    public function setFastStatCampClicks($campID)
    {
        $this->db->set('camp_clicks', 'camp_clicks + 1', false);
        $this->db->where('id', $campID);
        $this->db->update('camps');
    }


    /**
     * @param $adID
     */
    public function setFastStatAdViews($adID)
    {
        if (is_array($adID)) {
            foreach ($adID as $value) {
                $ad_ids[] = $value['id'];
            }

            $this->db->set('ad_views', 'ad_views + 1', false);
            $this->db->where_in('id', $ad_ids);
        } else {
            $this->db->set('ad_views', 'ad_views + 1', false);
            $this->db->where('id', $adID);
        }

        $this->db->update('ads');
    }


    /**
     * @param $adID
     */
    public function setFastStatAdClicks($adID)
    {
        $this->db->set('ad_clicks', 'ad_clicks + 1', false);
        $this->db->where('id', $adID);
        $this->db->update('ads');
    }


    /**
     * @param $event
     * @param $siteID
     * @param $blockID
     * @param $camp_ID
     * @param $adID
     * @param $ip
     * @return bool
     */
    public function setIpsStat($event, $siteID, $blockID, $camp_ID, $adID, $ip)
    {
        if (!$event OR ! $siteID OR ! $blockID OR ! $camp_ID OR ! $adID OR ! $ip) {
            return false;
        }

        if ($this->isClearIpsStat()) {
            $this->truncateIpsStatTable();
        }

        $this->db->set('date', date('Ymd'));
        $this->db->set('event', $event);
        $this->db->set('site_id', $siteID);
        $this->db->set('block_id', $blockID);
        $this->db->set('long_ip', $ip);

        // if camps array
        if (is_array($camp_ID)) {
            foreach ($camp_ID as $value) {
                $this->db->set('camp_id', $value);
            }
        } else {
            $this->db->set('camp_id', $camp_ID);
        }

        // if ads array
        if (is_array($adID)) {
            foreach ($adID as $value) {
                $this->db->set('ad_id', $value);
            }
        } else {
            $this->db->set('ad_id', $adID);
        }


        $this->db->insert('_stat_ip');
    }


    /**
     *
     */
    public function truncateIpsStatTable()
    {
        $this->db->truncate('_stat_ip');
    }


    /**
     * @return bool
     */
    public function isClearIpsStat()
    {
        $this->db->where('date', date("Ymd", time() - 86400));
        $this->db->limit(1);
        $query = $this->db->get('_stat_ip');
        return (bool) $query->num_rows();
    }


    /**
     * @param $period
     */
    public function clearAllStat($period)
    {
        if ($period == 'week') {
            $date = date('Ymd', (time() - 86400 * 7));
        } elseif ($period == '2week') {
            $date = date('Ymd', (time() - 86400 * 14));
        } elseif ($period == 'month') {
            $date = date('Ymd', (time() - 86400 * 30));
        } elseif ($period == 'year') {
            $date = date('Ymd', (time() - 86400 * 365));
        } elseif ($period == 'all') {
            $date = '20990101';
        }

        $this->db->where('date <', $date);
        $this->db->delete(array('_stat_ads', '_stat_blocks', '_stat_camps', '_stat_sites', '_stat_ip'));
    }

}
