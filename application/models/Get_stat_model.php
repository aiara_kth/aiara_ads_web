<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Get_stat_model extends CI_Model
{


    public function __construct()
    {
        parent::__construct();

        $this->load->database();
        $this->load->model('geo_model', 'geo');
    }


    /**
     * @param int $limit
     * @return array
     */
    public function getDaysStat($limit = 14)
    {
        $out     = array();
        $out2    = array();
        $result  = array();
        $outstat = array();

        $date    = date('Ymd'); // start date (today)
        $endDate = date('Ymd', time() - (86400 * (integer) $limit)); // end date (today - $limit days)
        // all sites ids
        if (!$sites = $this->getAllSitesIds()) {
            return false;
        }

        // get views
        $this->db->where_in('site_id', $sites);
        $this->db->where('event', 'w');
        $this->db->where('date >=', $endDate);
        $this->db->select('date');
        $this->db->select_sum('total_views');
        $this->db->group_by('date');
        $query  = $this->db->get('_stat_sites');
        $array1 = $query->result_array();


        // get clicks
        $this->db->where_in('site_id', $sites);
        $this->db->where('event', 'c');
        $this->db->where('date >=', $endDate);
        $this->db->select('date');
        $this->db->select_sum('total_clicks');
        $this->db->group_by('date');
        $query2 = $this->db->get('_stat_sites');
        $array2 = $query2->result_array();

        // prep
        foreach ($array1 as $key => $value) {
            $out[$value['date']]['total_views'] = $value['total_views'];
        }

        foreach ($array2 as $key => $value) {
            $out2[$value['date']]['total_clicks'] = $value['total_clicks'];
        }

        // set values
        for ($i = 0; $i < $limit; $i++) {
            if ($i != 0) {
                $date = date('Ymd', (strtotime($date) - 86400)); // day decrement
            }

            $result['date']        = $date;
            $result['format_date'] = substr($date, 0, 4) . "-" . substr($date, 4, 2) . "-" . substr($date, 6, 2); // date format
            $result['views']       = isset($out[$date]['total_views']) ? $out[$date]['total_views'] : 0;
            $result['clicks']      = isset($out2[$date]['total_clicks']) ? $out2[$date]['total_clicks'] : 0;
            $result['ctr']         = ($result['views'] == 0 OR ( $result['views'] == 0 AND $result['clicks'] == 0)) ? 0 : round(($result['clicks'] / $result['views']) * 100, 2);

            $outstat[] = $result;
        }

        return $outstat;
    }


    /**
     * @param $sites
     * @param null $date
     * @return array|bool
     */
    public function getAllSitesStat($sites, $date = null)
    {
        if (!$sites) {
            return false;
        }

        $stat = array();
        $out  = array();

        // views
        $this->db->where_in('site_id', $sites);
        $this->db->where('event', 'w');
        $this->whereDate($date);
        $this->db->select('site_id');
        $this->db->select_sum('total_views');
        $this->db->group_by('site_id');
        $query = $this->db->get('_stat_sites');

        $array = $query->result_array();

        foreach ($array as $value) {
            $stat['views'][] = $value;
        }

        // clicks
        $this->db->where_in('site_id', $sites);
        $this->db->where('event', 'c');
        $this->whereDate($date);
        $this->db->select('site_id');
        $this->db->select_sum('total_clicks');
        $this->db->group_by('site_id');
        $query = $this->db->get('_stat_sites');

        $array = $query->result_array();

        foreach ($array as $value) {
            $stat['clicks'][] = $value;
        }

        // prep array
        for ($i = 0; $i < count($sites); $i++) {
            $out[$i]['site_id'] = $sites[$i];
            $out[$i]['domain']  = $this->getSiteDomain($sites[$i]);

            // views
            $out[$i]['views'] = 0;

            for ($u = 0; $u < @count($stat['views']); $u++) {
                if ($stat['views'][$u]['site_id'] == $sites[$i]) {
                    $out[$i]['views'] = isset($stat['views'][$u]['total_views']) ? $stat['views'][$u]['total_views'] : 0;
                    break;
                }
            }

            // clicks
            $out[$i]['clicks'] = 0;

            for ($j = 0; $j < @count($stat['clicks']); $j++) {
                if ($stat['clicks'][$j]['site_id'] == $sites[$i]) {
                    $out[$i]['clicks'] = isset($stat['clicks'][$j]['total_clicks']) ? $stat['clicks'][$j]['total_clicks'] : 0;
                    break;
                }
            }

            // ctr
           $out[$i]['ctr'] = ($out[$i]['views'] == 0 OR ($out[$i]['views'] == 0 AND $out[$i]['clicks'] == 0)) ? 0 : round(($out[$i]['clicks'] / $out[$i]['views']) * 100, 2);
        }

        return empty($out) ? false : $out;
    }


    /**
     * @param $blocks
     * @param null $date
     * @return array|bool
     */
    public function getAllBlocksStat($blocks, $date = null)
    {
        if (!$blocks) {
            return false;
        }

        $stat = array();
        $out  = array();

        // views
        $this->db->where_in('block_id', $blocks);
        $this->db->where('event', 'w');
        $this->whereDate($date);
        $this->db->select('block_id');
        $this->db->select_sum('total_views');
        $this->db->group_by('block_id');
        $query = $this->db->get('_stat_blocks');

        $array = $query->result_array();

        foreach ($array as $value) {
            $stat['views'][] = $value;
        }

        // clicks
        $this->db->where_in('block_id', $blocks);
        $this->db->where('event', 'c');
        $this->whereDate($date);
        $this->db->select('block_id');
        $this->db->select_sum('total_clicks');
        $this->db->group_by('block_id');
        $query = $this->db->get('_stat_blocks');

        $array = $query->result_array();

        foreach ($array as $value) {
            $stat['clicks'][] = $value;
        }

        // prep array
        for ($i = 0; $i < count($blocks); $i++) {
            $out[$i]['block_id']   = $blocks[$i];
            $out[$i]['block_name'] = $this->getBlockName($blocks[$i]);

            // views
            $out[$i]['views'] = 0;

            for ($u = 0; $u < @count($stat['views']); $u++) {
                if ($stat['views'][$u]['block_id'] == $blocks[$i]) {
                    $out[$i]['views'] = isset($stat['views'][$u]['total_views']) ? $stat['views'][$u]['total_views'] : 0;
                    break;
                }
            }

            // clicks
            $out[$i]['clicks'] = 0;

            for ($j = 0; $j < @count($stat['clicks']); $j++) {
                if ($stat['clicks'][$j]['block_id'] == $blocks[$i]) {
                    $out[$i]['clicks'] = isset($stat['clicks'][$j]['total_clicks']) ? $stat['clicks'][$j]['total_clicks'] : 0;
                    break;
                }
            }

            // ctr
            $out[$i]['ctr'] = ($out[$i]['views'] == 0 OR ($out[$i]['views'] == 0 AND $out[$i]['clicks'] == 0)) ? 0 : round(($out[$i]['clicks'] / $out[$i]['views']) * 100, 2);
        }

        return empty($out) ? false : $out;
    }


    /**
     * @param $camps
     * @param null $date
     * @return array|bool
     */
    public function getAllCampsStat($camps, $date = null)
    {
        if (!$camps) {
            return false;
        }

        $stat = array();
        $out  = array();

        // views
        $this->db->where_in('camp_id', $camps);
        $this->db->where('event', 'w');
        $this->whereDate($date);
        $this->db->select('camp_id');
        $this->db->select_sum('total_views');
        $this->db->group_by('camp_id');
        $query = $this->db->get('_stat_camps');

        $array = $query->result_array();

        foreach ($array as $value) {
            $stat['views'][] = $value;
        }

        // clicks
        $this->db->where_in('camp_id', $camps);
        $this->db->where('event', 'c');
        $this->whereDate($date);
        $this->db->select('camp_id');
        $this->db->select_sum('total_clicks');
        $this->db->group_by('camp_id');
        $query = $this->db->get('_stat_camps');

        $array = $query->result_array();

        foreach ($array as $value) {
            $stat['clicks'][] = $value;
        }

        // prep array
        for ($i = 0; $i < count($camps); $i++) {
            $out[$i]['camp_id']   = $camps[$i];
            $out[$i]['camp_name'] = $this->getCampName($camps[$i]);

            // views
            $out[$i]['views'] = 0;

            for ($u = 0; $u < @count($stat['views']); $u++) {
                if ($stat['views'][$u]['camp_id'] == $camps[$i]) {
                    $out[$i]['views'] = isset($stat['views'][$u]['total_views']) ? $stat['views'][$u]['total_views'] : 0;
                    break;
                }
            }

            // clicks
            $out[$i]['clicks'] = 0;

            for ($j = 0; $j < @count($stat['clicks']); $j++) {
                if ($stat['clicks'][$j]['camp_id'] == $camps[$i]) {
                    $out[$i]['clicks'] = isset($stat['clicks'][$j]['total_clicks']) ? $stat['clicks'][$j]['total_clicks'] : 0;
                    break;
                }
            }

            // ctr
            $out[$i]['ctr'] = ($out[$i]['views'] == 0 OR ($out[$i]['views'] == 0 AND $out[$i]['clicks'] == 0)) ? 0 : round(($out[$i]['clicks'] / $out[$i]['views']) * 100, 2);
        }

        return empty($out) ? false : $out;
    }


    /**
     * @param $ads
     * @param string $date
     * @return array|bool
     */
    public function getAllAdsStat($ads, $date = '')
    {
        if (!$ads) {
            return false;
        }

        $stat = array();
        $out  = array();

        // views
        $this->db->where_in('ad_id', $ads);
        $this->db->where('event', 'w');
        $this->whereDate($date);
        $this->db->select('ad_id');
        $this->db->select_sum('total_views');
        $this->db->group_by('ad_id');
        $query = $this->db->get('_stat_ads');

        $array = $query->result_array();

        foreach ($array as $value) {
            $stat['views'][] = $value;
        }

        // clicks
        $this->db->where_in('ad_id', $ads);
        $this->db->where('event', 'c');
        $this->whereDate($date);
        $this->db->select('ad_id');
        $this->db->select_sum('total_clicks');
        $this->db->group_by('ad_id');
        $query = $this->db->get('_stat_ads');

        $array = $query->result_array();

        foreach ($array as $value) {
            $stat['clicks'][] = $value;
        }

        // prep array
        for ($i = 0; $i < count($ads); $i++) {
            $out[$i]['ad_id']   = $ads[$i];
            $out[$i]['ad_data'] = $this->getAdData($ads[$i]);

            // views
            $out[$i]['views'] = 0;

            for ($u = 0; $u < @count($stat['views']); $u++) {
                if ($stat['views'][$u]['ad_id'] == $ads[$i]) {
                    $out[$i]['views'] = isset($stat['views'][$u]['total_views']) ? $stat['views'][$u]['total_views'] : 0;
                    break;
                }
            }

            // clicks
            $out[$i]['clicks'] = 0;

            for ($j = 0; $j < @count($stat['clicks']); $j++) {
                if ($stat['clicks'][$j]['ad_id'] == $ads[$i]) {
                    $out[$i]['clicks'] = isset($stat['clicks'][$j]['total_clicks']) ? $stat['clicks'][$j]['total_clicks'] : 0;
                    break;
                }
            }

            // ctr
            $out[$i]['ctr'] = ($out[$i]['views'] == 0 OR ($out[$i]['views'] == 0 AND $out[$i]['clicks'] == 0)) ? 0 : round(($out[$i]['clicks'] / $out[$i]['views']) * 100, 2);
        }

        return $out ? $out : false;
    }


    /**
     * @param $siteID
     * @return mixed
     */
    public function getSiteDomain($siteID)
    {
        $this->db->where('id', $siteID);
        $this->db->select('domain');
        $query = $this->db->get('sites');
        $row   = $query->row_array();
        return $row['domain'];
    }


    /**
     * @param $blockID
     * @return mixed
     */
    public function getBlockName($blockID)
    {
        $this->db->where('id', $blockID);
        $this->db->select('block_name');
        $query = $this->db->get('blocks');
        $row   = $query->row_array();
        return $row['block_name'];
    }


    /**
     * @param $campID
     * @return mixed
     */
    public function getCampName($campID)
    {
        $this->db->where('id', $campID);
        $this->db->select('camp_name');
        $query = $this->db->get('camps');
        $row   = $query->row_array();
        return $row['camp_name'];
    }


    /**
     * @param $adID
     * @return mixed
     */
    public function getAdData($adID)
    {
        $this->db->where('id', $adID);
        $this->db->select('ad_title');
        $this->db->select('ad_descr');
        $this->db->select('ad_link');
        $this->db->select('ad_img');
        $query = $this->db->get('ads');
        $row   = $query->row_array();
        return $row;
    }


    /**
     * @param $date
     */
    public function whereDate($date)
    {
        // today
        if ($date == 'today') {
            $this->db->where('date', date('Ymd'));
        }

        // yesterday
        elseif ($date == 'yesterday') {
            $this->db->where('date', date('Ymd', (time() - 86400)));
        }

        // last 7 days
        elseif ($date == 'last_7') {
            $this->db->where('date >=', date('Ymd', (time() - (86400 * 7))));
            $this->db->where('date <=', date('Ymd'));
        }

        // last 30 days
        elseif ($date == 'last_30') {
            $this->db->where('date >=', date('Ymd', (time() - (86400 * 30))));
            $this->db->where('date <=', date('Ymd'));
        }

        // one date
        elseif (preg_match("/^[0-9]{8}$/", $date)) {
            $this->db->where('date', $date);
        }

        // date range
        elseif (preg_match("/^[0-9]{8}(\s*)-(\s*)[0-9]{8}$/", $date)) {
            $date_range = explode("-", $date);
            // sort($date_range);
            $this->db->where('date >=', trim($date_range[0]));
            $this->db->where('date <=', trim($date_range[1]));
        } elseif ($date == 'all') {
            // all dates (no where)
        }

        // no date (today)
        else {
            $this->db->where('date', date('Ymd'));
        }
    }


    /**
     * @param $date
     * @return false|string
     */
    public function parseDate($date)
    {
        // today
        if ($date == 'today') {
            return date('Y.m.d');
        }

        // yesterday
        elseif ($date == 'yesterday') {
            return date('Y.m.d', (time() - 86400));
        }

        // last 7 days
        elseif ($date == 'last_7') {
            return date('Y.m.d', (time() - (86400 * 7))) . ' - ' . date('Y.m.d');
        }

        // last 30 days
        elseif ($date == 'last_30') {
            return date('Y.m.d', (time() - (86400 * 30))) . ' - ' . date('Y.m.d');
        }

        // one date
        elseif (preg_match("/^[0-9]{8}$/", $date)) {
            return substr($date, 0, 4) . '.' . substr($date, 4, 2) . '.' . substr($date, 6, 2);
        }

        // date range
        elseif (preg_match("/^[0-9]{8}(\s*)-(\s*)[0-9]{8}$/", $date)) {
            $dateRange = explode("-", $date);

            sort($dateRange);

            return substr(trim($dateRange[0]), 0, 4) . '.' . substr(trim($dateRange[0]), 4, 2) . '.' . substr(trim($dateRange[0]), 6, 2) . ' - ' .
                    substr(trim($dateRange[1]), 0, 4) . '.' . substr(trim($dateRange[1]), 4, 2) . '.' . substr(trim($dateRange[1]), 6, 2);
        } elseif ($date == 'all') {
            return '1970.01.01-' . date('Y.m.d');
        }

        // no date (today)
        else {
            return date('Y.m.d');
        }
    }


    /**
     * @param $table
     * @param $event
     * @param $field_name
     * @param $field_value
     * @param string $date
     * @return bool
     */
    public function getCustomStat($table, $event, $field_name, $field_value, $date = '')
    {
        if (!$table OR ! $event OR ! $field_name OR ! $field_value) {
            return false;
        }

        $fields = $this->db->list_fields($table);
        $this->whereDate($date);
        $this->db->where('event', $event);
        $this->db->where_in($field_name, $field_value);

        foreach ($fields as $field) {
            $this->db->select_sum($field);
        }

        $query = $this->db->get($table);

        return $query->row_array();
    }


    /**
     * @param $views
     * @param $clicks
     * @return array|int|null
     */
    public function prepGeoStat($views, $clicks)
    {
        $outStat = null;

        $geo = array('XXX', 'AFG', 'ALA', 'ALB', 'DZA', 'ASM', '_AND', 'AGO', 'AIA', 'ATA', 'ATG', 'ARG', 'ARM', 'ABW', 'AUS', 'AUT', 'AZE', 'BHS', 'BHR', 'BGD', 'BRB', 'BLR',
            'BEL', 'BLZ', 'BEN', 'BMU', 'BTN', 'BOL', 'BES', 'BIH', 'BWA', 'BVT', 'BRA', 'IOT', 'BRN', 'BGR', 'BFA', 'BDI', 'KHM', 'CMR', 'CAN', 'CPV', 'CYM', 'CAF', 'TCD',
            'CHL', 'CHN', 'CXR', 'CCK', 'COL', 'COM', 'COG', 'COD', 'COK', 'CRI', 'CIV', 'HRV', 'CUB', 'CUW', 'CYP', 'CZE', 'DNK', 'DJI', 'DMA', 'DOM', 'ECU', 'EGY', 'SLV',
            'GNQ', 'ERI', 'EST', 'ETH', 'FLK', 'FRO', 'FJI', 'FIN', 'FRA', 'GUF', 'PYF', 'ATF', 'GAB', 'GMB', 'GEO', 'DEU', 'GHA', 'GIB', 'GRC', 'GRL', 'GRD', 'GLP', 'GUM',
            'GTM', 'GGY', 'GIN', 'GNB', 'GUY', 'HTI', 'HMD', 'VAT', 'HND', 'HKG', 'HUN', 'ISL', 'IND', 'IDN', 'IRN', 'IRQ', 'IRL', 'IMN', 'ISR', 'ITA', 'JAM', 'JPN', 'JEY',
            'JOR', 'KAZ', 'KEN', 'KIR', 'PRK', 'KOR', 'KWT', 'KGZ', 'LAO', 'LVA', 'LBN', 'LSO', 'LBR', 'LBY', 'LIE', 'LTU', 'LUX', 'MAC', 'MKD', 'MDG', 'MWI', 'MYS', 'MDV',
            'MLI', 'MLT', 'MHL', 'MTQ', 'MRT', 'MUS', 'MYT', 'MEX', 'FSM', 'MDA', 'MCO', 'MNG', 'MNE', 'MSR', 'MAR', 'MOZ', 'MMR', 'NAM', 'NRU', 'NPL', 'NLD', 'NCL', 'NZL',
            'NIC', 'NER', 'NGA', 'NIU', 'NFK', 'MNP', 'NOR', 'OMN', 'PAK', 'PLW', 'PSE', 'PAN', 'PNG', 'PRY', 'PER', 'PHL', 'PCN', 'POL', 'PRT', 'PRI', 'QAT', 'REU', 'ROU',
            'RUS', 'RWA', 'BLM', 'SHN', 'KNA', 'LCA', 'MAF', 'SPM', 'VCT', 'WSM', 'SMR', 'STP', 'SAU', 'SEN', 'SRB', 'SYC', 'SLE', 'SGP', 'SXM', 'SVK', 'SVN', 'SLB', 'SOM',
            'ZAF', 'SGS', 'SSD', 'ESP', 'LKA', 'SDN', 'SUR', 'SJM', 'SWZ', 'SWE', 'CHE', 'SYR', 'TWN', 'TJK', 'TZA', 'THA', 'TLS', 'TGO', 'TKL', 'TON', 'TTO', 'TUN', 'TUR',
            'TKM', 'TCA', 'TUV', 'UGA', 'UKR', 'ARE', 'GBR', 'USA', 'UMI', 'URY', 'UZB', 'VUT', 'VEN', 'VNM', 'VGB', 'VIR', 'WLF', 'ESH', 'YEM', 'ZMB', 'ZWE');

        $geoStat = array();

        foreach ($views as $key => $value) {
            if (in_array($key, $geo)) {
                if ($value OR $clicks[$key]) {
                    $geoStat['geo']    = $key;
                    $geoStat['views']  = isset($value) ? $value : 0;
                    $geoStat['clicks'] = isset($clicks[$key]) ? $clicks[$key] : 0;
                    $geoStat['ctr'] = ($geoStat['views'] == 0 OR ($geoStat['views'] == 0 AND $geoStat['clicks'] == 0)) ? 0 : round(($geoStat['clicks'] / $geoStat['views']) * 100, 2);

                    $outStat[] = $geoStat;
                }
            }
        }

        return empty($outStat) ? 0 : $outStat;
    }


    /**
     * @param $views
     * @param $clicks
     * @return array|int|null
     */
    public function prepOsStat($views, $clicks)
    {
        $outStat = null;

        $os = array('Windows_10', 'Windows_8_1', 'Windows_8', 'Windows_7', 'Windows_Vista',
            'Windows_Server', 'Windows_XP', 'Windows_2000', 'Windows_ME', 'Mac_OS', 'Linux',
            'Ubuntu', 'unknown_desctop_os', 'iOS', 'Android', 'Symbian', 'Black_Berry', 'Windows_Mobile',
            'Windows_Phone', 'unknown_mobile_os');

        $osStat = array();

        foreach ($views as $key => $value) {
            if (in_array($key, $os)) {
                if ($value OR $clicks[$key]) {
                    $osStat['os']     = $key;
                    $osStat['views']  = isset($value) ? $value : 0;
                    $osStat['clicks'] = isset($clicks[$key]) ? $clicks[$key] : 0;
                    $osStat['ctr'] = ($osStat['views'] == 0 OR ($osStat['views'] == 0 AND $osStat['clicks'] == 0)) ? 0 : round(($osStat['clicks'] / $osStat['views']) * 100, 2);

                    $outStat[] = $osStat;
                }
            }
        }

        return empty($outStat) ? 0 : $outStat;
    }


    /**
     * @param $views
     * @param $clicks
     * @return array|int|null
     */
    public function prepDevStat($views, $clicks)
    {
        $outStat = null;

        $dev = array('Computer', 'Tablet', 'Mobile', 'Bot');

        $devStat = array();

        foreach ($views as $key => $value) {
            if (in_array($key, $dev)) {
                if ($value OR $clicks[$key]) {
                    $devStat['dev']    = $key;
                    $devStat['views']  = isset($value) ? $value : 0;
                    $devStat['clicks'] = isset($clicks[$key]) ? $clicks[$key] : 0;
                    $devStat['ctr'] = ($devStat['views'] == 0 OR ($devStat['views'] == 0 AND $devStat['clicks'] == 0)) ? 0 : round(($devStat['clicks'] / $devStat['views']) * 100, 2);

                    $outStat[] = $devStat;
                }
            }
        }

        return empty($outStat) ? 0 : $outStat;
    }


    /**
     * @param $views
     * @param $clicks
     * @return array|int|null
     */
    public function prepBrowserStat($views, $clicks)
    {
        $outStat = null;

        $browsers = array('IE_d', 'Firefox_d', 'Safari_d', 'Chrome_d', 'Opera_d', 'Edge_d', 'Maxthon_d',
            'unknown_desktop_browser', 'Android_m', 'Chrome_m', 'Opera_m', 'Dolphin_m', 'Firefox_m',
            'UCBrowser_m', 'Puffin_m', 'Safari_m', 'Edge_m', 'IE_m', 'unknown_mobile_browser');

        $browsersStat = array();

        foreach ($views as $key => $value) {
            if (in_array($key, $browsers)) {
                if ($value OR $clicks[$key]) {
                    $browsersStat['browser'] = $key;
                    $browsersStat['views']   = isset($value) ? $value : 0;
                    $browsersStat['clicks']  = isset($clicks[$key]) ? $clicks[$key] : 0;
                    $browsersStat['ctr'] = ($browsersStat['views'] == 0 OR ($browsersStat['views'] == 0 AND $browsersStat['clicks'] == 0)) ? 0 : round(($browsersStat['clicks'] / $browsersStat['views']) * 100, 2);

                    $outStat[] = $browsersStat;
                }
            }
        }

        return empty($outStat) ? 0 : $outStat;
    }


    /**
     * @param $views
     * @param $clicks
     * @return mixed
     */
    public function prepTotalStat($views, $clicks)
    {
        $total['views']  = isset($views['total_views']) ? $views['total_views'] : 0;
        $total['clicks'] = isset($clicks['total_clicks']) ? $clicks['total_clicks'] : 0;
        $total['ctr'] = ($total['views'] == 0 OR ($total['views'] == 0 AND $total['clicks'] == 0)) ? 0 : round(($total['clicks'] / $total['views']) * 100, 2);

        return $total;
    }


    /**
     * @param $campID
     * @return array|bool|null
     */
    public function getOneCampAds_Ids($campID)
    {
        $this->db->select('id');
        $this->db->where('camp_id', $campID);
        $query = $this->db->get('ads');
        $array = $query->result_array();

        $out = null;

        foreach ($array as $value) {
            $out[] = $value['id'];
        }

        return $out ? $out : false;
    }


    /**
     * @param $siteID
     * @return array|bool|null
     */
    public function getOneSiteBlocksIds($siteID)
    {
        $this->db->select('id');
        $this->db->where('site_id', $siteID);
        $query = $this->db->get('blocks');
        $array = $query->result_array();

        $out = null;

        foreach ($array as $value) {
            $out[] = $value['id'];
        }

        return $out ? $out : false;
    }


    /**
     * @return array|bool|null
     */
    public function getAllSitesIds()
    {
        $this->db->select('id');
        $this->db->select('domain');
        $query = $this->db->get('sites');
        $array = $query->result_array();

        $out = null;

        foreach ($array as $value) {
            $out[] = $value['id'];
        }

        return $out ? $out : false;
    }


    /**
     * @return array|bool|null
     */
    public function getAllCampsIds()
    {
        $this->db->select('id');
        $query = $this->db->get('camps');
        $array = $query->result_array();

        $out = null;

        foreach ($array as $value) {
            $out[] = $value['id'];
        }

        return $out ? $out : false;
    }


    /**
     * @param $whereColumn
     * @param $whereValue
     * @param int $limit
     * @return array|int
     */
    public function getStatByIps($whereColumn, $whereValue, $limit = 50)
    {
        if (!in_array($whereColumn, array('site_id', 'block_id', 'camp_id', 'ad_id'))) {
            return false;
        }

        $_ips    = array();
        $outStat = array();
        $views   = array();
        $clicks  = array();

        //
        $this->db->select('long_ip');
        $this->db->select('COUNT(long_ip)');
        $this->db->where($whereColumn, $whereValue);
        $this->db->where('date', date('Ymd'));
        $this->db->where('event', 'c');
        $this->db->group_by('long_ip');
        $this->db->order_by('COUNT(long_ip)', 'DESC');
        $this->db->limit($limit);
        $query = $this->db->get('_stat_ip');

        foreach ($query->result_array() as $value) {
            $clicks[$value['long_ip']] = $value['COUNT(long_ip)'];
            $_ips[]                    = $value['long_ip'];
        }

        // views
        $this->db->select('long_ip');
        $this->db->select('COUNT(long_ip)');
        $this->db->where($whereColumn, $whereValue);

        ($_ips) ? $this->db->where_in('long_ip', $_ips) : '';

        $this->db->where('event', 'w');
        $this->db->where('date', date('Ymd'));
        $this->db->group_by('long_ip');
        $this->db->order_by('COUNT(long_ip)', 'DESC');
        $this->db->limit($limit);
        $query = $this->db->get('_stat_ip');

        foreach ($query->result_array() as $value) {
            $views[$value['long_ip']] = $value['COUNT(long_ip)'];
        }


        // views all
        $this->db->select('long_ip');
        $this->db->select('COUNT(long_ip)');
        $this->db->where($whereColumn, $whereValue);
        $this->db->where('event', 'w');
        $this->db->where('date', date('Ymd'));
        $this->db->group_by('long_ip');
        $this->db->order_by('COUNT(long_ip)', 'DESC');
        $this->db->limit($limit);
        $query = $this->db->get('_stat_ip');

        foreach ($query->result_array() as $value) {
            $views[$value['long_ip']] = $value['COUNT(long_ip)'];
        }


        foreach ($views as $key => $value) {
            $_stat['ip']     = long2ip($key);
            $_stat['geo']    = $this->geo->getGeoByIp(long2ip($key));
            $_stat['views']  = $value;
            $_stat['clicks'] = isset($clicks[$key]) ? $clicks[$key] : 0;
            $_stat['ctr'] = ($_stat['views'] == 0 OR ($_stat['views'] == 0 AND $_stat['clicks'] == 0)) ? 0 : round(($_stat['clicks'] / $_stat['views']) * 100, 2);


            $outStat[] = $_stat;
        }

        $output = array_slice($outStat, 0, $limit);

        return $output ? $output : 0;
    }

}
