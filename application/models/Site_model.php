<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Site_model extends CI_Model {


    public function __construct()
    {
        parent::__construct();

        $this->load->database();
    }


    /**
     * @param $siteID
     * @return bool
     */
    public function siteIdExists($siteID)
    {
        $query = $this->db->get_where('sites', array('id' => $siteID));

        if ($query->num_rows() > 0) {
            return true;
        }

        return false;
    }


    /**
     * @return array
     */
    public function getAllSitesData()
    {
        $out = array();

        $query = $this->db->get('sites');

        foreach ($query->result_array() as $row) {
            $row['count_blocks'] = $this->countSiteBlocks($row['id']);

            $out[] = $row;
        }

        return $out;
    }


    /**
     * @param $siteID
     * @return bool
     */
    public function getOneSiteData($siteID)
    {
        if (!$this->siteIdExists($siteID)) {
            return false;
        }

        $query = $this->db->get_where('sites', array('id' => $siteID));

        if ($query->num_rows() > 0) {
            return $query->row_array();
        }

        return false;
    }


    /**
     * @param $siteID
     * @return array
     */
    public function getSiteBlocks($siteID)
    {
        $out = array();

        $query = $this->db->get_where('blocks', array('site_id' => $siteID));

        foreach ($query->result_array() as $row) {
            $out[] = $row;
        }

        return $out;
    }


    /**
     * @return mixed
     */
    public function countAllSites()
    {
        $query = $this->db->get('sites');
        return $query->num_rows();
    }


    /**
     * @param $siteID
     * @return int
     */
    public function countSiteBlocks($siteID)
    {
        if (!is_numeric($siteID)) {
            return 0;
        }

        $query = $this->db->from('blocks')->where('site_id', $siteID);
        return $query->count_all_results();
    }


    /**
     * @param $domain
     * @return string
     */
    public function clearDomainName($domain)
    {
        if ($this->isRealDomain($domain)) {
            return $domain;
        }

        return false;
    }


    /**
     * Is real domain
     *
     * @param $domain
     * @return boolean
     */
    public function isRealDomain($domain)
    {
        $domain = preg_replace("(^https?://)", "", $domain);

        if (filter_var(gethostbyname($domain), FILTER_VALIDATE_IP)) {
            return true;
        }

        return false;
    }


    /**
     * @param $domain
     * @return bool
     */
    public function addSite($domain)
    {
        $domain = preg_replace("(^https?://)", "", $domain);
        $domain = ltrim($domain, "www.");

        // duplicate and empty
        if ($this->domainExists($domain) OR empty($domain)) {
            return false;
        }

        $this->db->insert('sites', array('domain' => $domain, 'status' => 1));

        if ($this->db->affected_rows() > 0) {
            return true;
        }

        return false;
    }


    /**
     * @param $domain
     * @return bool
     */
    public function domainExists($domain)
    {
        $query = $this->db->get_where('sites', array('domain' => $domain));

        if ($query->num_rows() > 0) {
            return true;
        }

        return false;
    }


    /**
     * @param $siteID
     */
    public function removeSite($siteID)
    {
        $this->db->delete('sites', array('id' => $siteID));
    }


    /**
     * @param $siteID
     */
    public function playSite($siteID)
    {
        $this->db->where('id', $siteID)->update('sites', array('status' => 1));
    }


    /**
     * @param $siteID
     */
    public function stopSite($siteID)
    {
        $this->db->where('id', $siteID)->update('sites', array('status' => 0));
    }


    /**
     * @param $siteID
     */
    public function removeSiteBlocks($siteID)
    {
        $this->db->delete('blocks', array('site_id' => $siteID));
    }


    /**
     * @param $siteID
     * @return bool
     */
    public function isActiveSite($siteID)
    {
        $site_data = $this->getOneSiteData($siteID);

        if (isset($site_data['status']) AND $site_data['status'] != 0) {
            return true;
        }

        return false;
    }
}
