<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Device_model extends CI_Model
{


    public function __construct()
    {
        parent::__construct();

        $this->load->library('CrawlerDetect');
        $this->load->library('Mobile_detect');
        $this->load->model('Geo_model', 'geo');
    }


    /**
     * Checking whether the user is a search bot
     *
     * @return mixed
     */
    public function isBot()
    {
        return $this->crawlerdetect->isCrawler();
    }


    /**
     * Information about the user's device
     *
     * @return array
     */
    public function getDeviceInfo()
    {
        $device['type']         = $this->getDeviceType();
        $device['type_code']    = $this->getDeviceTypeCode();
        $device['os']           = $this->getOs();
        $device['os_code']      = $this->getOsCode();
        $device['browser']      = $this->getBrowser();
        $device['browser_code'] = $this->getBrowserCode();
        $device['ip']           = $this->geo->getUserIp();
        $device['ip_long']      = $this->getLongIp();

        $geo = $this->geo->getGeoArray();
        return array_merge($device, $geo);
    }


    /**
     * User device type
     *
     * @return string
     */
    public function getDeviceType()
    {
        if ($this->mobile_detect->isTablet()) {
            return 'Tablet';
        } elseif ($this->mobile_detect->isMobile()) {
            return 'Mobile';
        } elseif ($this->crawlerdetect->isCrawler()) {
            return 'Bot';
        }

        return 'Computer';
    }


    /**
     * User device type code
     *
     * @return string
     */
    public function getDeviceTypeCode()
    {
        switch ($this->getDeviceType()) {
            case 'Computer': return '1';
            case 'Tablet': return '2';
            case 'Mobile': return '3';
            case 'Bot': return '4';
        }
    }


    /**
     * User device OS
     *
     * @return string
     */
    public function getOs()
    {
        $user_agent = $this->input->server('HTTP_USER_AGENT');

        if ($this->getDeviceType() != 'Computer') {
            if ($this->mobile_detect->isIOS()) {
                return 'iOS';
            } elseif ($this->mobile_detect->isAndroidOS()) {
                return 'Android';
            } elseif ($this->mobile_detect->isSymbianOS()) {
                return 'Symbian';
            } elseif ($this->mobile_detect->isBlackBerryOS()) {
                return 'Black_Berry';
            } elseif ($this->mobile_detect->isWindowsMobileOS()) {
                return 'Windows_Mobile';
            } elseif ($this->mobile_detect->isWindowsPhoneOS()) {
                return 'Windows_Phone';
            }
            return 'unknown_mobile_os';
        } else {
            if (preg_match('/windows nt 10/i', $user_agent)) {
                return 'Windows_10';
            } elseif (preg_match('/windows nt 6\.3/i', $user_agent)) {
                return 'Windows_8_1';
            } elseif (preg_match('/windows nt 6\.2/i', $user_agent)) {
                return 'Windows_8';
            } elseif (preg_match('/windows nt 6\.1/i', $user_agent)) {
                return 'Windows_7';
            } elseif (preg_match('/windows nt 5\.2/i', $user_agent)) {
                return 'Windows_Server';
            } elseif (preg_match('/windows nt 5\.1|windows xp/i', $user_agent)) {
                return 'Windows_XP';
            } elseif (preg_match('/windows nt 5\.0/i', $user_agent)) {
                return 'Windows_2000';
            } elseif (preg_match('/windows me/i', $user_agent)) {
                return 'Windows_ME';
            } elseif (preg_match('/macintosh|mac os x|mac_powerpc/i', $user_agent)) {
                return 'Mac_OS';
            } elseif (preg_match('/ubuntu/i', $user_agent)) {
                return 'Ubuntu';
            } elseif (preg_match('/linux/i', $user_agent)) {
                return 'Linux';
            } elseif (preg_match('/windows nt 6\.0/i', $user_agent)) {
                return 'Windows_Vista';
            }

            return 'unknown_desctop_os';
        }
    }


    /**
     * User device OS code
     *
     * @return string
     */
    public function getOsCode()
    {
        switch ($this->getOs()) {
            case 'unknown_desctop_os': return '0';
            case 'unknown_mobile_os': return '99';
            case 'Windows_10': return '1';
            case 'Windows_8_1': return '2';
            case 'Windows_8': return '3';
            case 'Windows_7': return '4';
            case 'Windows_Vista': return '5';
            case 'Windows_Server': return '6';
            case 'Windows_XP': return '7';
            case 'Windows_2000': return '8';
            case 'Windows_ME': return '9';
            case 'Mac_OS': return '10';
            case 'Linux': return '11';
            case 'Ubuntu': return '12';
            case 'iOS': return '13';
            case 'Android': return '14';
            case 'Symbian': return '15';
            case 'Black_Berry': return '16';
            case 'Windows_Mobile': return '17';
            case 'Windows_Phone': return '18';
        }

        return '0';
    }


    /**
     * User browser
     *
     * @return string
     */
    public function getBrowser()
    {
        $user_agent = $this->input->server('HTTP_USER_AGENT');

        if ($this->getDeviceType() != 'Computer') {
            if ($this->mobile_detect->isChrome()) {
                return 'Chrome_m';
            } elseif ($this->mobile_detect->isOpera()) {
                return 'Opera_m';
            } elseif ($this->mobile_detect->isDolfin()) {
                return 'Dolphin_m';
            } elseif ($this->mobile_detect->isFirefox()) {
                return 'Firefox_m';
            } elseif ($this->mobile_detect->isUCBrowser()) {
                return 'UCBrowser_m';
            } elseif ($this->mobile_detect->isPuffin()) {
                return 'Puffin_m';
            } elseif ($this->mobile_detect->isSafari()) {
                return 'Safari_m';
            } elseif ($this->mobile_detect->isEdge()) {
                return 'Edge_m';
            } elseif ($this->mobile_detect->isIE()) {
                return 'IE_m';
            } elseif (preg_match('/.*(Linux;.*AppleWebKit.*Version\/\d+\.\d+.*Mobile).*/i', $user_agent)) {
                return 'Android_m';
            }
            return 'unknown_mobile_browser';
        } else {

            if (preg_match('/firefox/i', $user_agent)) {
                return 'Firefox_d';
            } elseif (preg_match('/opr|opera/i', $user_agent)) {
                return 'Opera_d';
            } elseif (preg_match('/chrome/i', $user_agent)) {
                return 'Chrome_d';
            } elseif (preg_match('/maxthon/i', $user_agent)) {
                return 'Maxthon_d';
            } elseif (preg_match('/safari/i', $user_agent)) {
                return 'Safari_d';
            } elseif (preg_match('/edge/i', $user_agent)) {
                return 'Edge_d';
            } elseif (preg_match('/msie|trident/i', $user_agent)) {
                return 'IE_d';
            }

            return 'unknown_desktop_browser';
        }
    }


    /**
     * User browser code
     *
     * @return string
     */
    public function getBrowserCode()
    {
        switch ($this->getBrowser()) {
            case 'unknown_desktop_browser': return '0';
            case 'unknown_mobile_browser': return '99';
            case 'IE_d': return '1';
            case 'Firefox_d': return '2';
            case 'Safari_d': return '3';
            case 'Chrome_d': return '4';
            case 'Opera_d': return '5';
            case 'Edge_d': return '6';
            case 'Maxthon_d': return '7';
            case 'Android_m': return '8';
            case 'Chrome_m': return '9';
            case 'Opera_m': return '10';
            case 'Dolphin_m': return '11';
            case 'Firefox_m': return '12';
            case 'UCBrowser_m': return '13';
            case 'Puffin_m': return '14';
            case 'Safari_m': return '15';
            case 'Edge_m': return '16';
            case 'IE_m': return '17';
        }

        return '0';
    }


    /**
     * Long ip
     *
     * @return string
     */
    public function getLongIp()
    {
        return sprintf("%u", ip2long($this->geo->getUserIp()));
    }


    /**
     * Converting browser code to a name
     *
     * @param int $browserCode
     * @return string
     */
    public function browserCodeToName($browserCode = 0)
    {
        switch ((integer) $browserCode) {
            case 0: return 'unknown_desktop_browser';
            case 99: return 'unknown_mobile_browser';
            case 1: return 'IE_d';
            case 2: return 'Firefox_d';
            case 3: return 'Safari_d';
            case 4: return 'Chrome_d';
            case 5: return 'Opera_d';
            case 6: return 'Edge_d';
            case 7: return 'Maxthon_d';
            case 8: return 'Android_m';
            case 9: return 'Chrome_m';
            case 10: return 'Opera_m';
            case 11: return 'Dolphin_m';
            case 12: return 'Firefox_m';
            case 13: return 'UCBrowser_m';
            case 14: return 'Puffin_m';
            case 15: return 'Safari_m';
            case 16: return 'Edge_m';
            case 17: return 'IE_m';
        }

        return 'unknown_desktop_browser';
    }


    /**
     * Converting OS code to a name
     *
     * @param int $osCode
     * @return string
     */
    public function osCodeToName($osCode = 0)
    {
        switch ((integer) $osCode) {
            case 0: return 'unknown_desctop_os';
            case 99: return 'unknown_mobile_os';
            case 1: return 'Windows_10';
            case 2: return 'Windows_8_1';
            case 3: return 'Windows_8';
            case 4: return 'Windows_7';
            case 5: return 'Windows_Vista';
            case 6: return 'Windows_Server';
            case 7: return 'Windows_XP';
            case 8: return 'Windows_2000';
            case 9: return 'Windows_ME';
            case 10: return 'Mac_OS';
            case 11: return 'Linux';
            case 12: return 'Ubuntu';
            case 13: return 'iOS';
            case 14: return 'Android';
            case 15: return 'Symbian';
            case 16: return 'Black_Berry';
            case 17: return 'Windows_Mobile';
            case 18: return 'Windows_Phone';
        }

        return 'unknown_desctop_os';
    }


    /**
     * Converting the device type code to a name
     *
     * @param int $devTypeCode
     * @return string
     */
    public function devTypeCodeToName($devTypeCode = 0)
    {
        switch ((integer) $devTypeCode) {
            case 1: return 'Computer';
            case 2: return 'Tablet';
            case 3: return 'Mobile';
            case 4: return 'Bot';
        }
    }

}
