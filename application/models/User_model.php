<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class User_model extends CI_Model
{


    public function __construct()
    {
        parent::__construct();

        $this->load->database();
        $this->load->helper('cookie');
    }


    /**
     * @return bool
     */
    public function getUserData()
    {
        $query = $this->db->get_where('users', array('id' => 1));

        if ($query->num_rows() > 0) {
            return $query->row_array();
        }

        return false;
    }


    /**
     * @param $email
     * @param $password
     * @param $remember
     * @return bool
     */
    public function login($email, $password, $remember)
    {
        $user_data = $this->getUserData();

        if ($email == $user_data['user_email'] AND sha1($password) == $user_data['user_pass_hash']) {
            $cookie['name']     = '_sess';
            $cookie['value']    = sha1($user_data['user_email'] . $user_data['user_pass_hash'] . $this->input->server('HTTP_USER_AGENT') . $this->input->server('HTTP_ACCEPT_LANGUAGE'));
            $cookie['expire']   = ($remember) ? (60 * 60 * 24 * 7) : 0;
            $cookie['httponly'] = true;

            set_cookie($cookie);

            return true;
        }

        delete_cookie('_sess');

        return false;
    }


    /**
     *
     */
    public function logout()
    {
        delete_cookie('_sess');
    }


    /**
     * @return bool
     */
    public function loginStatus()
    {
        $user_data = $this->getUserData();

        if (get_cookie('_sess', true) == sha1($user_data['user_email'] . $user_data['user_pass_hash'] . $this->input->server('HTTP_USER_AGENT') . $this->input->server('HTTP_ACCEPT_LANGUAGE'))) {
            return true;
        }

        return false;
    }


    /**
     *
     */
    public function isLogin()
    {
        if (!$this->loginStatus()) {
            header('Location: /');

            exit;
        }
    }

}
