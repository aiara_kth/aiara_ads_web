<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Csrf_model extends CI_Model
{


    public function __construct()
    {
        parent::__construct();

        session_start();
    }


    /**
     * Compares the token from the html page and from the $_SESSION
     *
     * @param $csrfToken
     * @return bool
     */
    public function validateCsrfToken($csrfToken)
    {
        if ($csrfToken != $this->getCsrfToken()) {
            return false;
        }

        return true;
    }


    /**
     * Writes the token to the $_SESSION
     *
     * @param $csrfToken
     */
    public function setCsrfToken($csrfToken)
    {
        $_SESSION['csrfToken'] = $csrfToken;
    }


    /**
     * Reads a token from the $_SESSION
     *
     * @return mixed
     */
    public function getCsrfToken()
    {
        return $_SESSION['csrfToken'];
    }


    /**
     * Generate csrf token (sha1)
     *
     * @return string
     */
    public function generateCsrfToken()
    {
        $csrfToken = sha1(rand(0, 100000000) . time());
        $this->setCsrfToken($csrfToken);
        return $csrfToken;
    }

}
