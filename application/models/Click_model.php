<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Click_model extends CI_Model
{


    public function __construct()
    {
        parent::__construct();

        $this->load->database();
        $this->load->library('encryptor');
    }


    /**
     * Parse the encrypted data from the link
     *
     * @param $encryptedClickData
     * @return bool|mixed
     */
    public function getEncryptedClickData($encryptedClickData)
    {
        if ($click_data_array = json_decode($this->encryptor->xorDecrypt($encryptedClickData), true)) {
            return $click_data_array;
        }
        return false;
    }


    /**
     * Replace macroses
     *
     * @param $clickData
     * @return mixed
     */
    public function replaceMacroses($clickData)
    {
        $search               = array('[SITE_ID]', '[BLOCK_ID]', '[CAMP_ID]', '[AD_ID]', '[TIME_CLIK]');
        $replace              = array($clickData['site_id'], $clickData['block_id'], $clickData['camp_id'], $clickData['ad_id'], date('Y-m-d_H-i-s'));
        $clickData['ad_link'] = str_replace($search, $replace, $clickData['ad_link']);
        return $clickData;
    }

}
