<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Ads_model extends CI_Model {


    public function __construct()
    {
        parent::__construct();

        $this->load->database();
    }


    /**
     * Check for the existence of an ad with this ID
     *
     * @param $adID
     * @return bool
     */
    public function adExists($adID)
    {
        $query = $this->db->get_where('ads', array('id' => $adID));
        return (bool) $query->num_rows();
    }


    /**
     * The array of all campaign ads
     *
     * @param $campID
     * @return array
     */
    public function getCampaignAds($campID)
    {
        $data  = array();
        $this->db->order_by('ad_views', 'desc');
        $query = $this->db->get_where('ads', array('camp_id' => $campID));

        if ($query->num_rows() > 0) {
            foreach ($query->result_array() as $row) {
                $data[] = $row;
            }
        }
        return $data ? $data : array();
    }


    /**
     * Data of one ad
     *
     * @param $adID
     * @return bool
     */
    public function getOneAdData($adID)
    {
        $query = $this->db->get_where('ads', array('id' => $adID));

        if ($query->num_rows() > 0) {
            return $query->row_array();
        }
        return false;
    }


    /**
     * Deleting ad
     *
     * @param $adID
     */
    public function removeAd($adID)
    {
        $this->removeAdImage($adID);
        $this->db->where('id', $adID)->delete('ads');
    }


    /**
     * Deleting an ad image
     *
     * @param $adID
     * @return bool
     */
    public function removeAdImage($adID)
    {
        $adData = $this->getOneAdData($adID);

        @unlink('./images/' . $adData['ad_img']);

        $this->db->update('ads', array('ad_img' => ''), array('id' => $adID));

        if ($this->db->affected_rows() > 0) {
            return true;
        }
        return false;
    }


    /**
     * Play ad
     *
     * @param $adID
     * @return bool
     */
    public function playAd($adID)
    {
        if (!$this->adExists($adID)) {
            return false;
        }

        $this->db->where('id', $adID)->update('ads', array('ad_status' => 1));
        return true;
    }


    /**
     * Stop ad
     *
     * @param $adID
     * @return bool
     */
    public function stopAd($adID)
    {
        if (!$this->adExists($adID)) {
            return false;
        }

        $this->db->where('id', $adID)->update('ads', array('ad_status' => 0));
        return true;
    }


    /**
     * Upload ad image
     *
     * @return bool | string
     */
    public function uploadAdImage()
    {
        $config['upload_path']   = './images/';
        $config['allowed_types'] = 'gif|jpg|jpeg|png';
        $config['max_size']      = '1024';
        $config['max_width']     = '5000';
        $config['max_height']    = '5000';
        $config['encrypt_name']  = true;

        $this->load->library('upload', $config);

        if ($this->upload->do_upload("ad_image")) {
            $imageData = $this->upload->data();

            // is not image file - remove
            if ($imageData['is_image'] != 1) {
                @unlink($imageData['full_path']);
                return false;
            }

            // Make the image square (cut out of the center)
            if (!$this->propResizeImage($imageData['full_path'], $imageData['image_width'], $imageData['image_height'])) {
                @unlink($imageData['full_path']);
                return false;
            }

            // resize 200x200 px
            if (!$this->resizeImage($imageData['full_path'])) {
                @unlink($imageData['full_path']);
                return false;
            }

            // return image file name
            return $imageData['file_name'];
        }
    }


    /**
     * Proportional crop of images from the center
     *
     * @param $imgPath
     * @param $imgWidth
     * @param $imgHeight
     * @return bool
     */
    public function propResizeImage($imgPath, $imgWidth, $imgHeight)
    {
        $maxResolution = ($imgWidth > $imgHeight) ? $imgHeight : $imgWidth;

        $newWidth  = $maxResolution;
        $newHeight = $maxResolution;
        $offsetX   = floor(($imgWidth - $newWidth) / 2);
        $offsetY   = floor(($imgHeight - $newHeight) / 2);

        $config['image_library']  = 'gd2';
        $config['source_image']   = $imgPath;
        $config['width']          = $newWidth;
        $config['height']         = $newHeight;
        $config['maintain_ratio'] = false;
        $config['y_axis']         = $offsetY;
        $config['x_axis']         = $offsetX;

        $this->load->library('image_lib');
        $this->image_lib->initialize($config);

        if ($this->image_lib->crop()) {
            return true;
        }

        return false;
    }


    /**
     * Image resize
     *
     * @param $imgPath
     * @param int $newWidth
     * @param int $newHeight
     * @return bool
     */
    public function resizeImage($imgPath, $newWidth = 300, $newHeight = 300)
    {
        $config['image_library']  = 'gd2';
        $config['source_image']   = $imgPath;
        $config['width']          = $newWidth;
        $config['height']         = $newHeight;
        $config['maintain_ratio'] = false;

        $this->load->library('image_lib');
        $this->image_lib->initialize($config);

        if ($this->image_lib->resize()) {
            return true;
        }

        return false;
    }


    /**
     * Save ad
     *
     * @param $campID
     * @param $title
     * @param $description
     * @param $link
     * @param $adImageName
     */
    public function setAdData($campID, $title, $description, $link, $adImageName)
    {
        $this->db->set('camp_id', $campID);
        $this->db->set('ad_status', '1');
        $this->db->set('ad_title', $title);
        $this->db->set('ad_descr', $description);
        $this->db->set('ad_link', $link);

        if ($adImageName) {
            $this->db->set('ad_img', $adImageName);
        }

        $this->db->set('ad_date_add', time());
        $this->db->insert('ads');
    }


    /**
     * Update ad
     *
     * @param $adID
     * @param $adTitle
     * @param $adDescription
     * @param $adLink
     * @param $adImageName
     */
    public function updateAdData($adID, $adTitle, $adDescription, $adLink, $adImageName)
    {
        $this->db->where('id', $adID);
        $this->db->set('ad_title', $adTitle);
        $this->db->set('ad_descr', $adDescription);
        $this->db->set('ad_link', $adLink);

        if ($adImageName) {
            $this->db->set('ad_img', $adImageName);
        }

        $this->db->update('ads');
    }
}
