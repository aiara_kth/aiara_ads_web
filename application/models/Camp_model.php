<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Camp_model extends CI_Model
{


    public function __construct()
    {
        parent::__construct();

        $this->load->database();
    }


    /**
     * Campaign exists
     *
     * @param $campID
     * @return bool
     */
    public function campaignExists($campID)
    {
        $query = $this->db->get_where('camps', array('id' => $campID));
        return (bool) $query->num_rows();
    }


    /**
     * Clear campaign name
     *
     * @param $campName
     * @return string
     */
    public function clearCampaignName($campName)
    {
        $campName = preg_replace('/[^a-zA-Zа-яА-Я0-9-_\s]/ui', '', $campName);
        return mb_substr(trim($campName), 0, 70);
    }


    /**
     * Is there a company name
     *
     * @param $campName
     * @return bool
     */
    public function campaignNameExists($campName)
    {
        $query = $this->db->get_where('camps', array('camp_name' => $this->clearCampaignName($campName)));
        return (bool) $query->num_rows();
    }


    /**
     * Data from one campaign
     *
     * @param $campID
     * @return bool | array
     */
    public function getOneCampaignData($campID)
    {
        if (!$this->campaignExists($campID)) {
            return false;
        }

        $query = $this->db->get_where('camps', array('id' => $campID));

        if ($query->num_rows() > 0) {
            return $query->row_array();
        }

        return false;
    }


    /**
     * All campaigns data
     *
     * @return array
     */
    public function getAllCampaignsData()
    {
        $data  = array();
        $query = $this->db->get('camps');

        if ($query->num_rows() > 0) {
            foreach ($query->result_array() as $row) {
                $row['count_camp_ads'] = $this->countAdsCampaign($row['id']);
                $data[]                = $row;
            }
        }

        return $data;
    }


    /**
     * Count ads campaign
     *
     * @param $campID
     * @return mixed
     */
    public function countAdsCampaign($campID)
    {
        $query = $this->db->get_where('ads', array('camp_id' => $campID));
        return $query->num_rows();
    }


    /**
     * Create new campaign
     *
     * @param $campName
     * @return mixed
     */
    public function createCampaign($campName)
    {
        $data['camp_name']   = $this->clearCampaignName($campName);
        $data['camp_status'] = 1;
        $data['camp_geo']    = 'AFG,ALA,ALB,DZA,ASM,_AND,AGO,AIA,ATA,ATG,ARG,ARM,ABW,AUS,AUT,AZE,BHS,BHR,BGD,BRB,BLR,BEL,'
                . 'BLZ,BEN,BMU,BTN,BOL,BES,BIH,BWA,BVT,BRA,IOT,BRN,BGR,BFA,BDI,KHM,CMR,CAN,CPV,CYM,CAF,TCD,CHL,CHN,CXR,CCK,COL,COM,'
                . 'COG,COD,COK,CRI,CIV,HRV,CUB,CUW,CYP,CZE,DNK,DJI,DMA,DOM,ECU,EGY,SLV,GNQ,ERI,EST,ETH,FLK,FRO,FJI,FIN,FRA,GUF,PYF,'
                . 'ATF,GAB,GMB,GEO,DEU,GHA,GIB,GRC,GRL,GRD,GLP,GUM,GTM,GGY,GIN,GNB,GUY,HTI,HMD,VAT,HND,HKG,HUN,ISL,IND,IDN,IRN,IRQ,'
                . 'IRL,IMN,ISR,ITA,JAM,JPN,JEY,JOR,KAZ,KEN,KIR,PRK,KOR,KWT,KGZ,LAO,LVA,LBN,LSO,LBR,LBY,LIE,LTU,LUX,MAC,MKD,MDG,MWI,'
                . 'MYS,MDV,MLI,MLT,MHL,MTQ,MRT,MUS,MYT,MEX,FSM,MDA,MCO,MNG,MNE,MSR,MAR,MOZ,MMR,NAM,NRU,NPL,NLD,NCL,NZL,NIC,NER,NGA,'
                . 'NIU,NFK,MNP,NOR,OMN,PAK,PLW,PSE,PAN,PNG,PRY,PER,PHL,PCN,POL,PRT,PRI,QAT,REU,ROU,RUS,RWA,BLM,SHN,KNA,LCA,MAF,SPM,'
                . 'VCT,WSM,SMR,STP,SAU,SEN,SRB,SYC,SLE,SGP,SXM,SVK,SVN,SLB,SOM,ZAF,SGS,SSD,ESP,LKA,SDN,SUR,SJM,SWZ,SWE,CHE,SYR,TWN,'
                . 'TJK,TZA,THA,TLS,TGO,TKL,TON,TTO,TUN,TUR,TKM,TCA,TUV,UGA,UKR,ARE,GBR,USA,UMI,URY,UZB,VUT,VEN,VNM,VGB,VIR,WLF,ESH,'
                . 'YEM,ZMB,ZWE,XXX';

        $data['camp_dev_type'] = 'Computer,Tablet,Mobile';
        $data['camp_os']       = 'Windows_10,Windows_8_1,Windows_8,Windows_7,Windows_Vista,Windows_XP,Mac_OS,Ubuntu,Linux,unknown_desktop_os,iOS,'
                . 'Android,Windows_Phone,Symbian,Black_Berry,Windows_Mobile,unknown_mobile_os';

        $data['camp_browser'] = 'IE_d,Firefox_d,Opera_d,Chrome_d,Edge_d,Maxthon_d,Safari_d,unknown_desktop_browser,Chrome_m,Android_m,Opera_m,'
                . 'Dolphin_m,Firefox_m,UCBrowser_m,Puffin_m,Safari_m,Edge_m,IE_m,unknown_mobile_browser';

        $data['camp_date_start']   = time();
        $data['camp_date_stop']    = strtotime('+1 year', time());
        $data['camp_active_hours'] = '00,01,02,03,04,05,06,07,08,09,10,11,12,13,14,15,16,17,18,19,20,21,22,23';

        $this->db->insert('camps', $data);

        return $this->db->insert_id();
    }


    /**
     * Update campaign
     *
     * @param $campData
     * @return bool
     */
    public function updateCampaign($campData)
    {
        if (!$this->campaignExists($campData['camp_id']) OR
                empty($campData['camp_name']) OR
                empty($campData['camp_geo']) OR
                empty($campData['camp_dev_type']) OR
                empty($campData['camp_os']) OR
                empty($campData['camp_browser']) OR
                empty($campData['camp_date_start']) OR
                empty($campData['camp_date_stop']) OR
                empty($campData['camp_active_hours'])) {
            return false;
        }

        $campID = $campData['camp_id'];

        $campData['camp_date_start'] = strtotime($campData['camp_date_start']);
        $campData['camp_date_stop']  = strtotime($campData['camp_date_stop']);

        unset($campData['camp_id'], $campData['csrfToken']);

        $this->db->where('id', $campID);
        $this->db->update('camps', $campData);

        return true;
    }


    /**
     * Remove campaign
     *
     * @param $campID
     */
    public function removeCampaign($campID)
    {
        $this->db->where('id', $campID)->delete('camps');
    }


    /**
     * Deletes all campaign ads
     *
     * @param $campID
     */
    public function removeAdsCampaign($campID)
    {
        $this->db->where('camp_id', $campID)->delete('ads');
    }


    /**
     * Play campaign
     *
     * @param $campID
     */
    public function playCampaign($campID)
    {
        $this->db->where('id', $campID)->update('camps', array('camp_status' => 1));
    }


    /**
     * Stop campaign
     *
     * @param $campID
     */
    public function stopCampaign($campID)
    {
        $this->db->where('id', $campID)->update('camps', array('camp_status' => 0));
    }

}
