<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Adflex_config_model extends CI_Model
{

    /**
     * Configuration file name
     *
     * @var string
     */
    public $configFileputh;


    public function __construct()
    {
        parent::__construct();

        $this->setConfigFilepath();
    }


    /**
     * Set the name of the configuration file
     *
     * @param string $configFileputh
     */
    public function setConfigFilepath($configFileputh = '')
    {
        if ($configFileputh) {
            $this->configFileputh = $configFileputh;
        } else {
            $this->configFileputh = FCPATH . 'config.ini';
        }
    }


    /**
     * Value from the configuration file by its key
     *
     * @param string $key
     * @return bool | null
     */
    public function getAdflexConfigItem($key = '')
    {
        if (!$key) {
            return false;
        } elseif ($GLOBALS['adflex_config']) {
            return isset($GLOBALS['adflex_config'][$key]) ? $GLOBALS['adflex_config'][$key] : null;
        } else {
            $GLOBALS['adflex_config'] = @parse_ini_file($this->configFileputh);
            return isset($GLOBALS['adflex_config'][$key]) ? $GLOBALS['adflex_config'][$key] : null;
        }
    }


    /**
     * All configuration settings in the form of an array
     *
     * @return array|bool|mixed|null
     */
    public function getAdflexConfigAll()
    {
        if ($GLOBALS['adflex_config']) {
            return isset($GLOBALS['adflex_config']) ? $GLOBALS['adflex_config'] : null;
        } else {
            $GLOBALS['adflex_config'] = @parse_ini_file($this->configFileputh);
            return isset($GLOBALS['adflex_config']) ? $GLOBALS['adflex_config'] : null;
        }
    }


    /**
     * Adding values to the configuration
     *
     * @param string $key
     * @param string $value
     * @return bool
     */
    public function setAdflexConfigItem($key = '', $value = '')
    {
        $configArray = $this->getAdflexConfigAll();

        if (!$key && !$value) {
            return false;
        } elseif ($key AND $value AND ! is_array($key) AND ! is_array($value)) {
            $configArray[$key] = (string) $value;
        } elseif (is_array($key) AND ! $value) {
            foreach ($key as $k => $v) {
                $configArray[$k] = $v;
            }
        } else {
            return false;
        }

        $putString = null;

        foreach ($configArray as $key => $value) {

            if (is_string($value) OR is_numeric($value) OR is_bool($value) OR is_null($value)) {
                $putString .= trim($key) . ' = "' . trim($value) . '"' . PHP_EOL;
            }
        }

        return (bool) @file_put_contents($this->configFileputh, $putString);
    }

}
