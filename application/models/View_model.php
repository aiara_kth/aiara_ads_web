<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class View_model extends CI_Model {


    public function __construct()
    {
        parent::__construct();

        $this->load->database();
        $this->load->helper('url');
        $this->load->library('encryptor');
        $this->load->model('device_model', 'device');
        $this->load->model('block_model', 'block');
    }


    /**
     * @param $siteID
     * @param $blockID
     * @param $domain
     * @return bool
     */
    public function isValidDomainCodeCall($siteID, $blockID, $domain)
    {
        $query = $this->db->get_where('sites', array('id' => $siteID));

        if ($query->num_rows() > 0) {
            $row = $query->row();

            if ($row->domain == $domain) {
                return true;
            }
        }

        return false;
    }


    /**
     * @param $blockID
     * @return mixed
     */
    public function getBlockData($blockID)
    {
        return $this->block->getBlockData($blockID);
    }


    /**
     * @return mixed
     */
    public function getViewData()
    {
        return $this->device->getDeviceInfo();
    }


    /**
     * @param $viewData
     * @param $blockData
     * @param $domain
     * @return array|bool
     */
    public function getValidCamps($viewData, $blockData, $domain)
    {
        $this->db->select('id');
        $this->db->where('camp_status', 1);
        $this->db->where('camp_date_start <', time());
        $this->db->where('camp_date_stop >', time());
        $this->db->like('camp_geo', $viewData['geo_alfa3']);
        $this->db->like('camp_dev_type', $viewData['type']);
        $this->db->like('camp_os', $viewData['os']);
        $this->db->like('camp_browser', $viewData['browser']);
        $this->db->like('camp_active_hours', date('H', time()));
        $this->db->not_like('filter_sites', $domain);
        $this->db->order_by('id', 'random');
        $this->db->limit($blockData['block_count_ads_width'] * $blockData['block_count_ads_height']);

        $query = $this->db->get('camps');

        if ($query->num_rows() > 0) {
            foreach ($query->result_array() as $row) {
                $ids[] = $row['id'];
            }

            return $ids;
        }

        return false;
    }


    /**
     * @param $campsIds
     * @param $blockData
     * @return array|bool
     */
    public function getValidAds($campsIds, $blockData)
    {
        $this->db->where('ad_status', 1);
        $this->db->where('ad_title !=', '');
        $this->db->where('ad_descr !=', '');
        $this->db->where('ad_link !=', '');
        $this->db->where_in('camp_id', $campsIds);
        $this->db->order_by('id', 'random');
        $this->db->limit($blockData['block_count_ads_width'] * $blockData['block_count_ads_height']);

        $query = $this->db->get('ads');

        if ($query->num_rows() > 0) {
            foreach ($query->result_array() as $row) {
                $ads[] = $row;
            }

            return $ads;
        }

        return false;
    }


    /**
     * Add important to block styles
     *
     * @param $styles
     * @return string
     */
    public function addImportantToStyles($styles)
    {
        return str_replace(';', ' !important;', $styles);
    }


    /**
     * @param $blockData
     * @param $adsArray
     * @return string
     */
    public function generateAdBlock($blockData, $adsArray)
    {
        // css class name prefix (block id)
        $classPrefix = $blockData['id'];


        $importantStyles = $this->addImportantToStyles($blockData['block_css_styles']);


        // add prefix to block styles
        $styles = $this->addStylesPrefix($importantStyles, $classPrefix);

        // block generate
        $table = '<style>' . $styles . '</style>';
        $table .= '<table class="block_ads' . $classPrefix . '" border=0><tr>';

        for ($i = 0; $i < count($adsArray); $i++) {
            // link data
            $clickData['type']     = 'click'; // note that this link with the click parameters
            $clickData['site_id']  = $blockData['site_id']; // site ID
            $clickData['block_id'] = $blockData['id']; // block ID
            $clickData['camp_id']  = $adsArray[$i]['camp_id']; // camp ID
            $clickData['ad_id']    = $adsArray[$i]['id']; // ad ID
            $clickData['ad_link']  = $adsArray[$i]['ad_link']; // ad out link
            // Encrypt the data and form a link
            $clickUrl              = base_url('/click/' . $this->encryptor->xorEncrypt(json_encode($clickData)));

            if (($i % $blockData['block_count_ads_width']) == 0 && $i != 0) {
                $table .= '</tr><tr>';
            }

            //
            $visibility_params = $this->parseVisibilityParams($blockData['block_link_visibility']);


            $table .= '<td class="cell_margin' . $classPrefix . '" valign="top" style="width:' . round(100 / $blockData['block_count_ads_width']) . '%" >';
            $table .= '<table class="cell_ads' . $classPrefix . '" border="0"><tr><td>';

            // If the ad has an image and its display is allowed by the block settings
            if ($blockData['block_ads_img_position'] != 'no' && !empty($adsArray[$i]['ad_img'])) {
                $table .= '<a href="' . $clickUrl . '" target="_blank"><img class="img_ads' . $classPrefix . '" src="' . base_url('/images/' . $adsArray[$i]['ad_img']) . '" ></a>';
            }

            // show title below the ad
            if ($visibility_params['title'] == 1) {
                $table .= '<a class="title_ads' . $classPrefix . '" href="' . $clickUrl . '" target="_blank">' . $adsArray[$i]['ad_title'] . '</a>';
            }

            // show descr below the ad
            if ($visibility_params['descr'] == 1) {
                $table .= '<p class="description_ads' . $classPrefix . '">' . $adsArray[$i]['ad_descr'] . '</p>';
            }

            // show link below the ad
            if ($visibility_params['link'] == 1) {
                $table .= '<a class="link_ads' . $classPrefix . '" href="' . $clickUrl . '" target="_blank">' . parse_url($adsArray[$i]['ad_link'], PHP_URL_HOST) . '</a>';
            }

            $table .= '</td></tr></table>';
            $table .= '</td>';
        }

        $table .= "</tr></table>";

        return $table;
    }


    // по причине обратной совместимости, а точнее что бы не менять поля в БД, пришлось реализовать очень нехорошую вещь.
    // флаги активности отображения элементов рекламного блока - (ссылки, заголовка, описания) храняться в одном поле БД, да еще и типа INT(((
    // хотел сначала хранить так "1|1|0" (0 - элемент скрыт, 1 - элемент виден)
    // первое значение - ссылка рекламного блока, второе - заголовок рекламного блока, третье - описание рекламного блока
    // но поле в БД типа INT! Решено использовать в качестве разделителя цифру 7, пример: 1 7 1 7 1
    // но когда нужно записать такой вариант - 0|1|1 (ноль впереди) при сохранении в INT ведущий ноль обрезается!
    // я добавил еще одну семерку спереди. Получилось так 7 0 7 1 7 1
    // просто нужно запомнить что "7" это разделитель такой же как "|"
    public function parseVisibilityParams($visibility_params)
    {
        if ($visibility_params == 1) {
            return array(
                'link'  => 1,
                'title' => 1,
                'descr' => 1,
            );
        }

        if ($visibility_params == 0) {
            return array(
                'link'  => 0,
                'title' => 1,
                'descr' => 1,
            );
        }

        $arr_params = explode('7', $visibility_params);

        return array(
            'link'  => $arr_params[1],
            'title' => $arr_params[2],
            'descr' => $arr_params[3],
        );
    }


    /**
     * @param $styles
     * @param $classPrefix
     * @return string
     */
    public function addStylesPrefix($styles, $classPrefix)
    {
        $styles = str_replace(array("\r\n", "\r", "\n", "\t"), '', $styles);

        $arr = explode('}', $styles);

        for ($i = 0; $i < count($arr); $i++) {
            if (preg_match("/(:hover|:active|:visited)/is", $arr[$i])) {
                $arr[$i] = preg_replace('/\.(.+):/isU', '.${1}' . $classPrefix . ':', $arr[$i]);
            } else {
                $arr[$i] = str_replace('{', $classPrefix . '{', $arr[$i]);
            }
        }

        return implode('}', $arr);
    }


    /**
     * @param $adBlock
     * @param $blockID
     */
    public function showBlockJs($adBlock, $blockID)
    {
        $js = 'var a_block = "' . addslashes($adBlock) . '";';
        $js .= 'var elem = document.getElementById("_' . $blockID . '");';
        $js .= 'elem.innerHTML = a_block;';

        echo $this->clearAdsBlockJsCode($js);
    }


    public function clearAdsBlockJsCode($js_code)
    {
        return str_replace(array("\r\n", "\r", "\n", "\t"), '', $js_code);
    }
}
