<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Stat extends CI_Controller
{


    public function __construct()
    {
        parent::__construct();

        $this->load->model('csrf_model', 'csrf');
        $this->load->model('get_stat_model', 'get_stat');
        $this->load->model('site_model', 'site');
        $this->load->model('camp_model', 'camp');
        $this->load->model('block_model', 'block');
        $this->load->model('camp_model', 'camp');
        $this->load->model('ads_model', 'ads');

        $this->load->model('user_model', 'user');
        $this->user->isLogin(); // auth
    }


    /**
     * Days stat page
     */
    public function index()
    {
        $data['csrfToken'] = $this->csrf->generateCsrfToken();
        $data['user']      = $this->user->getUserData();
        $this->load->view('stats/date_stat', $data);
    }


    /**
     * All sites stat page
     */
    public function sites()
    {
        $data['csrfToken'] = $this->csrf->generateCsrfToken();
        $data['user']      = $this->user->getUserData();
        $this->load->view('stats/all_sites', $data);
    }


    /**
     * One site stat page
     *
     * @param string $siteID
     */
    public function site($siteID = '')
    {
        if (!$this->site->siteIdExists($siteID)) {
            show_404();
        }

        $data['csrfToken'] = $this->csrf->generateCsrfToken();
        $data['user']      = $this->user->getUserData();
        $this->load->view('stats/one_site', $data);
    }


    /**
     * One block stat page
     *
     * @param string $blockID
     */
    public function block($blockID = '')
    {
        if (!$this->block->blockExists($blockID)) {
            show_404();
        }

        $data['csrfToken'] = $this->csrf->generateCsrfToken();
        $data['user']      = $this->user->getUserData();
        $this->load->view('stats/one_block', $data);
    }


    /**
     * All camps stat page
     */
    public function camps()
    {
        $data['csrfToken'] = $this->csrf->generateCsrfToken();
        $data['user']      = $this->user->getUserData();
        $this->load->view('stats/all_camps', $data);
    }


    /**
     * One camp stat page
     *
     * @param string $campID
     */
    public function camp($campID = '')
    {
        if (!$this->camp->campaignExists($campID)) {
            show_404();
        }

        $data['csrfToken'] = $this->csrf->generateCsrfToken();
        $data['user']      = $this->user->getUserData();
        $this->load->view('stats/one_camp', $data);
    }


    /**
     * One camp stat page
     *
     * @param string $adID
     */
    public function ad($adID = '')
    {
        if (!$this->ads->adExists($adID)) {
            show_404();
        }

        $data['csrfToken'] = $this->csrf->generateCsrfToken();
        $data['user']      = $this->user->getUserData();
        $this->load->view('stats/one_ad', $data);
    }


    /**
     * API - days stat - json
     */
    public function apiGetStatByDays()
    {
        if (!$this->csrf->validateCsrfToken($this->input->post('csrfToken', true))) {
            exit;
        }

        $stat['dates']      = $this->get_stat->getDaysStat();
        $stat['list_sites'] = $this->site->getAllSitesData();
        $stat['list_camps'] = $this->camp->getAllCampaignsData();

        $response['status'] = 'ok';
        $response['msg']    = $stat;
        exit(json_encode($response));
    }


    /**
     * API - all sites stat - json
     */
    public function apiGetStatAllSites()
    {
        if (!$this->csrf->validateCsrfToken($this->input->post('csrfToken', true))) {
            exit;
        }

        $sites = $this->get_stat->getAllSitesIds();

        $stat['sites']      = $this->get_stat->getAllSitesStat($sites, $this->input->post('period', true));
        $stat['list_sites'] = $this->site->getAllSitesData();
        $stat['list_camps'] = $this->camp->getAllCampaignsData();

        $response['status'] = 'ok';
        $response['msg']    = $stat;

        exit(json_encode($response));
    }


    /**
     * API - one site stat - json
     */
    public function apiGetStatOneSite()
    {
        if (!$this->csrf->validateCsrfToken($this->input->post('csrfToken', true))) {
            exit;
        }

        $siteID = $this->input->post('siteID', true);
        $period = $this->input->post('period', true);

        if (!$this->site->siteIdExists($siteID)) {
            exit(json_encode(array('status' => 'error', 'msg' => 'site not exists')));
        }

        $custom['views']  = $this->get_stat->getCustomStat('_stat_sites', 'w', 'site_id', $siteID, $period);
        $custom['clicks'] = $this->get_stat->getCustomStat('_stat_sites', 'c', 'site_id', $siteID, $period);

        $stat['list_sites'] = $this->site->getAllSitesData();
        $stat['list_camps'] = $this->camp->getAllCampaignsData();
        $stat['date']       = $this->get_stat->parseDate($period);
        $stat['site_id']    = $siteID;
        $stat['site_name']  = $this->get_stat->getSiteDomain($siteID);
        $stat['total']      = $this->get_stat->prepTotalStat($custom['views'], $custom['clicks']);
        $blocks             = $this->get_stat->getOneSiteBlocksIds($siteID);
        $stat['blocks']     = $this->get_stat->getAllBlocksStat($blocks, $period);
        $stat['geo']        = $this->get_stat->prepGeoStat($custom['views'], $custom['clicks']);
        $stat['dev']        = $this->get_stat->prepDevStat($custom['views'], $custom['clicks']);
        $stat['os']         = $this->get_stat->prepOsStat($custom['views'], $custom['clicks']);
        $stat['browser']    = $this->get_stat->prepBrowserStat($custom['views'], $custom['clicks']);

        exit(json_encode(array('status' => 'ok', 'msg' => $stat)));
    }


    /**
     * API - one block stat - json
     */
    public function apiGetStatOneBlock()
    {
        if (!$this->csrf->validateCsrfToken($this->input->post('csrfToken', true))) {
            exit;
        }

        $blockID = $this->input->post('blockID', true);
        $period  = $this->input->post('period', true);

        if (!$this->block->blockExists($blockID)) {
            exit(json_encode(array('status' => 'error', 'msg' => 'block not exists')));
        }

        $custom['views']  = $this->get_stat->getCustomStat('_stat_blocks', 'w', 'block_id', $blockID, $period);
        $custom['clicks'] = $this->get_stat->getCustomStat('_stat_blocks', 'c', 'block_id', $blockID, $period);

        $stat['list_sites'] = $this->site->getAllSitesData();
        $stat['list_camps'] = $this->camp->getAllCampaignsData();
        $stat['date']       = $this->get_stat->parseDate($period);
        $block_data         = $this->block->getBlockData($blockID);
        $stat['block_id']   = $blockID;
        $stat['block_name'] = $block_data['block_name'];
        $stat['site_id']    = $block_data['site_id'];
        $stat['site_name']  = $this->get_stat->getSiteDomain($block_data['site_id']);
        $stat['total']      = $this->get_stat->prepTotalStat($custom['views'], $custom['clicks']);
        $stat['geo']        = $this->get_stat->prepGeoStat($custom['views'], $custom['clicks']);
        $stat['dev']        = $this->get_stat->prepDevStat($custom['views'], $custom['clicks']);
        $stat['os']         = $this->get_stat->prepOsStat($custom['views'], $custom['clicks']);
        $stat['browser']    = $this->get_stat->prepBrowserStat($custom['views'], $custom['clicks']);

        exit(json_encode(array('status' => 'ok', 'msg' => $stat)));
    }


    /**
     * API - all camps stat - json
     */
    public function apiGetStatAllCamps()
    {
        if (!$this->csrf->validateCsrfToken($this->input->post('csrfToken', true))) {
            exit;
        }

        $camps = $this->get_stat->getAllCampsIds();

        $stat['camps']      = $this->get_stat->getAllCampsStat($camps, $this->input->post('period', true));
        $stat['list_sites'] = $this->site->getAllSitesData();
        $stat['list_camps'] = $this->camp->getAllCampaignsData();

        $response['status'] = 'ok';
        $response['msg']    = $stat;

        exit(json_encode($response));
    }


    /**
     * API - one camp stat - json
     */
    public function apiGetStatOneCamp()
    {
        if (!$this->csrf->validateCsrfToken($this->input->post('csrfToken', true))) {
            exit;
        }

        $campID = $this->input->post('campID', true);
        $period = $this->input->post('period', true);

        if (!$this->camp->campaignExists($campID)) {
            exit(json_encode(array('status' => 'error', 'msg' => 'campaign not exists')));
        }

        $custom['views']  = $this->get_stat->getCustomStat('_stat_camps', 'w', 'camp_id', $campID, $period);
        $custom['clicks'] = $this->get_stat->getCustomStat('_stat_camps', 'c', 'camp_id', $campID, $period);

        $stat['list_sites'] = $this->site->getAllSitesData();
        $stat['list_camps'] = $this->camp->getAllCampaignsData();
        $stat['date']       = $this->get_stat->parseDate($period);
        $stat['camp_id']    = $campID;
        $stat['camp_name']  = $this->get_stat->getCampName($campID);
        $stat['total']      = $this->get_stat->prepTotalStat($custom['views'], $custom['clicks']);
        $ads                = $this->get_stat->getOneCampAds_Ids($campID);
        $stat['ads']        = $this->get_stat->getAllAdsStat($ads, $period);
        $stat['geo']        = $this->get_stat->prepGeoStat($custom['views'], $custom['clicks']);
        $stat['dev']        = $this->get_stat->prepDevStat($custom['views'], $custom['clicks']);
        $stat['os']         = $this->get_stat->prepOsStat($custom['views'], $custom['clicks']);
        $stat['browser']    = $this->get_stat->prepBrowserStat($custom['views'], $custom['clicks']);

        $stat['camp_ads'] = $this->ads->getCampaignAds($campID);

        exit(json_encode(array('status' => 'ok', 'msg' => $stat)));
    }


    /**
     * API - one ad stat - json
     */
    public function apiGetStatOneAd()
    {
        if (!$this->csrf->validateCsrfToken($this->input->post('csrfToken', true))) {
            exit;
        }

        $adID   = $this->input->post('adID', true);
        $period = $this->input->post('period', true);

        if (!$this->ads->adExists($adID)) {
            exit(json_encode(array('status' => 'error', 'msg' => 'ad not exists')));
        }

        $custom['views']  = $this->get_stat->getCustomStat('_stat_ads', 'w', 'ad_id', $adID, $period);
        $custom['clicks'] = $this->get_stat->getCustomStat('_stat_ads', 'c', 'ad_id', $adID, $period);

        $stat['list_sites'] = $this->site->getAllSitesData();
        $stat['list_camps'] = $this->camp->getAllCampaignsData();
        $stat['date']       = $this->get_stat->parseDate($period);
        $ad_data            = $this->ads->getOneAdData($adID);
        $stat['ad_id']      = $adID;
        $stat['camp_id']    = $ad_data['camp_id'];
        $stat['camp_name']  = $this->get_stat->getCampName($ad_data['camp_id']);
        $stat['camp_ads']   = $this->ads->getCampaignAds($ad_data['camp_id']);
        $stat['total']      = $this->get_stat->prepTotalStat($custom['views'], $custom['clicks']);
        $stat['geo']        = $this->get_stat->prepGeoStat($custom['views'], $custom['clicks']);
        $stat['dev']        = $this->get_stat->prepDevStat($custom['views'], $custom['clicks']);
        $stat['os']         = $this->get_stat->prepOsStat($custom['views'], $custom['clicks']);
        $stat['browser']    = $this->get_stat->prepBrowserStat($custom['views'], $custom['clicks']);


        exit(json_encode(array('status' => 'ok', 'msg' => $stat)));
    }


    /**
     * API - ips stat - json
     */
    public function apiGetStatIp()
    {
        if (!$this->csrf->validateCsrfToken($this->input->post('csrfToken', true))) {
            exit;
        }

        $column      = $this->input->post('column', true);
        $columnValue = $this->input->post('columnValue', true);

        $stat = $this->get_stat->getStatByIps($column, $columnValue);
        exit(json_encode(array('status' => 'ok', 'msg' => $stat)));
    }

}
