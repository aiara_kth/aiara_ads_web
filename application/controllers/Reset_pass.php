<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Reset_pass extends CI_Controller
{


    public function __construct()
    {
        parent::__construct();

        $this->load->model('csrf_model', 'csrf');
    }


    /**
     * Reset password page
     *
     * @param string $resetToken
     */
    public function index($resetToken = '')
    {
        $data['csrfToken'] = $this->csrf->generateCsrfToken();
        $this->load->view('reset_pass', $data);
    }

}
