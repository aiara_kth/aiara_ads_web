<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class View extends CI_Controller {


    public function __construct() {
        parent::__construct();

        $this->load->library('encryptor');
        $this->load->model('geo_model', 'geo');
        $this->load->model('device_model', 'device');
        $this->load->model('view_model', 'view');
        $this->load->model('site_model', 'site');
        $this->load->model('block_model', 'block');
        $this->load->model('set_stat_model', 'setStat');
        $this->load->model('filter_model', 'filter');
    }


    /**
     * @param string $encryptedViewData
     * @param string $_domain
     */
    public function index($encryptedViewData = '', $_domain = '') {

        $this->output->set_header("Content-Type: text/javascript");

        $domain = ltrim($_domain, "www.");

        // is banned ip
        if ($this->filter->isBannedIp($this->geo->getUserIp())) {
            exit('<!-- adflex_banned_ip -->');
        }

        // decrypt click link data
        if (!$viewParams = json_decode($this->encryptor->xorDecrypt($encryptedViewData), true)) {
            exit('<!-- adflex_error_link_data -->');
        }

        // validate decrypt data and check call code in not site
        if ($viewParams['site_domain'] != $domain OR $viewParams['type'] != 'view') {
            exit('<!-- adflex_error_domain -->');
        }

        // is bot
        if ($this->device->isBot()) {
            exit('<!-- adflex_is_bot_true -->');
        }

        // get dev data
        if (!$viewData = $this->view->getViewData()) {
            exit('<!-- adflex_no_view_data -->');
        }

        // get block data
        if (!$blockData = $this->view->getBlockData($viewParams['block_id'])) {
            exit('<!-- adflex_no_block_data -->');
        }


        // is active block
        if (!$this->block->isActiveBlock($blockData)) {
            exit('<!-- adflex_inactive_block -->');
        }

        // is active site
        if (!$this->site->isActiveSite($viewParams['site_id'])) {
            exit('<!-- adflex_inactive_site -->');
        }


        // get valid camps
        if (!$validCampsData = $this->view->getValidCamps($viewData, $blockData, $viewParams['site_domain'])) {
            exit('<!-- adflex_valid_camps_not_found -->');
        }

        // get valid ads
        if (!$validAdsData = $this->view->getValidAds($validCampsData, $blockData)) {
            exit('<!-- adflex_valid_ads_not_found -->');
        }

        // generate block
        $ad_block = $this->view->generateAdBlock($blockData, $validAdsData);

        // get array shown ad
        $adsIds   = array();
        $campsIds = array();

        foreach ($validAdsData as $value) {
            $adsIds[]   = $value['id'];
            $campsIds[] = $value['camp_id'];
        }

        $campsIds = array_unique($campsIds);
        $adsIds   = array_unique($adsIds);

        if ($ad_block) {
            // save view on the "general" site statistics
            $this->setStat->setFastStatSiteViews($viewParams['site_id'], $validAdsData);

            // save view on the "general" block statistics
            $this->setStat->setFastStatBlockViews($viewParams['block_id']);

            // save view on the "general" camps statistics
            $this->setStat->setFastStatCampViews($validAdsData);

            // save view on the "general" ads statistics
            $this->setStat->setFastStatAdViews($validAdsData);

            // save view on the site statistics
            $this->setStat->setSiteStatView($viewData, $blockData);

            // save view on the block statistics
            $this->setStat->setBlockStatView($viewData, $blockData);

            // save view on the camp statistics
            $this->setStat->setCampsStatView($viewData, $validAdsData);

            // save view on the ad statistics
            $this->setStat->setAdsStatView($viewData, $validAdsData);

            // save view on the ip statistics
            $this->setStat->setIpsStat('w', $viewParams['site_id'], $viewParams['block_id'], $campsIds, $adsIds, $viewData['ip_long']);

            // show block (js)
            $this->view->showBlockJs($ad_block, $blockData['id']);
        } else {
            echo '<!-- adflex_error -->';
        }
    }

}
