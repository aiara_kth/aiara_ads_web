<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Main extends CI_Controller {


    public function __construct() {
        parent::__construct();

        $this->load->model('csrf_model', 'csrf');
        $this->load->model('user_model', 'user');
        $this->load->model('install_model', 'install');
        $this->load->helper('url');
    }


    /**
     * Main page (empty page)
     */
    public function index() {
        $data['csrfToken'] = $this->csrf->generateCsrfToken();

        // install page
        if ($this->install->isInstallation()) {

            // clear file status cache
            clearstatcache();

            // php vesion
            if (version_compare(PHP_VERSION, '5.3.7', '>=')) {
                $data['phpVersion'] = '<span style="color:green;"><b>[installed ' . PHP_VERSION . ': OK]</b></span>';
            } else {
                $data['phpVersion'] = '<span style="color:red;"><b>[installed ' . PHP_VERSION . ': ERROR]</b></span>';
            }

            //  writable images folder
            if ($this->install->getFilePermission(FCPATH . 'images') == 777) {
                $data['writableImagesFolder'] = '<font color="green"><b>[chmod 777: OK]</b></font>';
            } else {
                $data['writableImagesFolder'] = '<font color="red"><b>[chmod ' . $this->install->getFilePermission(FCPATH . 'images') . ': ERROR]</b></font>';
            }

            // writable config file
            if ($this->install->getFilePermission(FCPATH . 'config.ini') == 777 OR $this->install->getFilePermission(FCPATH . 'config.ini') == 666) {
                $data['writableConfigFile'] = '<font color="green"><b>[chmod ' . $this->install->getFilePermission(FCPATH . 'images') . ': OK]</b></font>';
            } else {
                $data['writableConfigFile'] = '<font color="red"><b>[chmod ' . $this->install->getFilePermission(FCPATH . 'images') . ': ERROR]</b></font>';
            }

            if (function_exists('imagecreatefromjpeg')) {
                $data['gd'] = '<font color="green"><span>Installed! : OK</span></font>';
            } else {
                $data['gd'] = '<font color="red"><span>Not installed! : ERROR</span></font>';
            }

            if (extension_loaded('fileinfo')) {
                $data['fileinfo'] = '<font color="green"><span>Installed! : OK</span></font>';
            } else {
                $data['fileinfo'] = '<font color="red"><span>Not installed! : ERROR</span></font>';
            }

            // include install view
            $this->load->view('install', $data);

            // index page
        } else {
            if ($this->user->loginStatus()) {
                $data['user'] = $this->user->getUserData();
                if ($GLOBALS['adflex_config']['default_page'] != 'main') {
                    header('Location: /' . $GLOBALS['adflex_config']['default_page']);
                }
                $this->load->view('main', $data);
            } else {
                $this->load->view('login', $data);
            }
        }
    }

}
