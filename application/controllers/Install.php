<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Install extends CI_Controller
{


    public function __construct()
    {
        parent::__construct();

        $this->load->model('csrf_model', 'csrf');
        $this->load->model('install_model', 'install');
    }


    /**
     * API - install
     */
    public function index()
    {
        if (!$this->csrf->validateCsrfToken($this->input->post('csrfToken', true))) {
            exit;
        }

        // re-install protection
        if (!$this->install->isInstallation()) {
            exit;
        }

        // installation data
        $data = $this->install->getInstallationData();

        // validate data
        if (!$data) {
            $response['status'] = 'error';
            $response['msg']    = 'Not filled in all the fields!';

            exit(json_encode($response));
        }

        // validate email
        if (!filter_var($data['admin_email'], FILTER_VALIDATE_EMAIL)) {
            $response['status'] = 'error';
            $response['msg']    = 'Entered is not a valid email!';

            exit(json_encode($response));
        }

        // validate password
        if (!$this->install->checkPassword($data['admin_pass'])) {
            $response['status'] = 'error';
            $response['msg']    = 'A weak password of the administrator. The password must consist of at least 8 characters.';

            exit(json_encode($response));
        }

        // test write images folder
        if (!$this->install->isWritableFile('images/test.txt')) {
            $response['status'] = 'error';
            $response['msg']    = 'No rights to write to "images" folder (set permissions 777)';

            exit(json_encode($response));
        }

        // test write config.ini
        if (!$this->install->isWritableFile('config.ini')) {
            $response['status'] = 'error';
            $response['msg']    = 'No rights to write to the file "config.ini" (set permissions 777)';

            exit(json_encode($response));
        }

        // test db connect
        if (!$this->install->checkDbConnect($data['db_host'], $data['db_user'], $data['db_pass'])) {
            $response['status'] = 'error';
            $response['msg']    = 'Error connecting to database (check the correctness of entered data)';

            exit(json_encode($response));
        }

        // check mysql version
        if (!$this->install->checkMysqlVersion($data['db_host'], $data['db_user'], $data['db_pass'])) {
            $response['status'] = 'error';
            $response['msg']    = 'The version of mysql server below the required 5.1+ (installed ' . $this->install->getMysqlVersion($data['db_host'], $data['db_user'], $data['db_pass']) . ')';

            exit(json_encode($response));
        }

        // create db
        if (!$this->install->createDb($data['db_host'], $data['db_user'], $data['db_pass'], $data['db_name'])) {
            $response['status'] = 'error';
            $response['msg']    = 'Error creating database (perhaps a base with the same name already exists, specify a different database name)';

            exit(json_encode($response));
        }

        // create tables
        if (!$this->install->createTables($data['db_host'], $data['db_user'], $data['db_pass'], $data['db_name'])) {
            $response['status'] = 'error';
            $response['msg']    = 'Error creating database tables';

            exit(json_encode($response));
        }

        // save admin data to db
        if (!$this->install->setAdminData($data['db_host'], $data['db_user'], $data['db_pass'], $data['db_name'], $data['admin_email'], $data['admin_pass'])) {
            $response['status'] = 'error';
            $response['msg']    = 'Error creating administrator account';

            exit(json_encode($response));
        }

        // save config
        if (!$this->install->setConfigSettings($data['db_host'], $data['db_user'], $data['db_pass'], $data['db_name'])) {
            $response['status'] = 'error';
            $response['msg']    = 'Error creating configuration file (probably no rights to write to the file "config.ini")';

            exit(json_encode($response));
        } else {
            // send account data to email
            @$this->install->sendAccountDataToEmail($data['admin_email'], $data['admin_pass']);

            $response['status'] = 'ok';
            exit(json_encode($response));
        }
    }

}
