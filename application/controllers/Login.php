<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller
{


    public function __construct()
    {
        parent::__construct();

        $this->load->model('csrf_model', 'csrf');
        $this->load->database();
        $this->load->helper('url');
        $this->load->library('email');

        $this->load->model('user_model', 'user');
    }


    public function index()
    {
        // stub
    }


    /**
     * API - check login
     */
    public function apiLogin()
    {
        if (!$this->csrf->validateCsrfToken($this->input->post('csrfToken', true))) {
            exit;
        }

        $email    = $this->input->post('email', true);
        $password = $this->input->post('password', true);
        $remember = $this->input->post('remember', true);

        echo ($this->user->login($email, $password, $remember)) ? 'ok' : 'error';
    }


    /**
     * API - logout
     */
    public function logOut()
    {
        if (!$this->csrf->validateCsrfToken($this->input->post('csrfToken', true))) {
            exit;
        }

        $this->user->logout();
    }


    /**
     * API - reset password
     */
    public function apiResetPass()
    {
        if (!$this->csrf->validateCsrfToken($this->input->post('csrfToken', true))) {
            exit;
        }

        $resetEmail = $this->input->post('reset_email', true);

        $query = $this->db->get_where('users', array('user_email' => $resetEmail));

        if ($query->num_rows() > 0) {
            $data['reset_pass_token'] = sha1(rand(0, 10000000) . time());
            $this->db->update('users', $data, array('user_email' => $resetEmail));

            $this->setEmailResetLink($resetEmail, $data['reset_pass_token']);

            $response['status'] = 'ok';
            exit(json_encode($response));
        }

        $response['status'] = 'error';
        exit(json_encode($response));
    }


    /**
     * API - set new password
     */
    public function apiSetNewPass()
    {
        if (!$this->csrf->validateCsrfToken($this->input->post('csrfToken', true))) {
            exit;
        }

        $resetToken  = $this->input->post('reset_token', true);
        $newPassword = $this->input->post('new_pass', true);
        $userData    = $this->validateResetPassToken($resetToken);

        if (!$userData) {
            $response['status'] = 'error';
            $response['msg']    = '잘못된 token값!';

            exit(json_encode($response));
        }

        if (!preg_match("/.{8,}/i", $newPassword)) {
            $response['status'] = 'error';
            $response['msg']    = '비밀번호는 8자 이상이어야합니다.';
            exit(json_encode($response));
        }

        $data['reset_pass_token'] = '';
        $data['user_pass_hash']   = sha1($newPassword);

        $this->db->update('users', $data, array('id' => $userData['id']));
        $this->sendEmailNewPassword($userData['user_email'], $newPassword);

        $response['status'] = 'ok';
        exit(json_encode($response));
    }


    /**
     * Validate reset pass token
     *
     * @param string $resetToken
     * @return bool
     */
    private function validateResetPassToken($resetToken = '')
    {
        if (strlen($resetToken) > 0) {
            $query = $this->db->get_where('users', array('reset_pass_token' => (string) $resetToken));

            if ($query->num_rows() > 0) {
                return $query->row_array();
            }
        }

        return false;
    }


    /**
     * Send reset link to user email
     * @param type $resetEmail
     * @param type $resetToken
     */
    private function setEmailResetLink($resetEmail, $resetToken)
    {
        $reset_link = base_url('reset_pass/' . $resetToken);

        $this->email->from('mailer@' . $this->input->server('HTTP_HOST'), 'AdFlex mailer');
        $this->email->to($resetEmail);
        $this->email->subject('AdFlex - reset password');
        $this->email->message(" To change the password click on the link. \n\n $reset_link");
        $this->email->send();
    }


    /**
     * Send new pass to user email
     * @param type $userEmail
     * @param type $NewPassword
     */
    private function sendEmailNewPassword($userEmail, $NewPassword)
    {
        $this->email->from('mailer@' . $this->input->server('HTTP_HOST'), 'AdFlex mailer');
        $this->email->to($userEmail);
        $this->email->subject('AdFlex - new password');
        $this->email->message("New password: $NewPassword");
        $this->email->send();
    }

}
