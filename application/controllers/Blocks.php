<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Blocks extends CI_Controller {


    public function __construct()
    {
        parent::__construct();

        $this->load->model('csrf_model', 'csrf');
        $this->load->helper('url');
        $this->load->model('site_model', 'site');
        $this->load->model('block_model', 'block');
        $this->load->model('user_model', 'user');

        $this->user->isLogin(); // auth
    }


    /**
     * Site blocks page
     *
     * @param string $siteID
     */
    public function index($siteID = '')
    {
        if (!$this->site->siteIdExists($siteID)) {
            show_404();
        }

        $data['csrfToken'] = $this->csrf->generateCsrfToken();
        $data['user']      = $this->user->getUserData();
        $data['site']      = $this->site->getOneSiteData($siteID);
        $data['sites']     = $this->site->getAllSitesData();
        $data['blocks']    = $this->site->getSiteBlocks($siteID);

        $this->load->view('blocks', $data);
    }


    /**
     * Block builder page
     *
     * @param string $siteID
     * @param string $blockID
     */
    public function builder_block($siteID = '', $blockID = '')
    {
        // validate arguments
        if (!$this->site->siteIdExists($siteID) OR ! $this->block->blockExists($blockID)) {
            show_404();
        }

        $data['csrfToken']       = $this->csrf->generateCsrfToken();
        $data['block']           = $this->block->getBlockData($blockID);
        $data['site']            = $this->site->getOneSiteData($data['block']['site_id']);
        $data['user']            = $this->user->getUserData();
        $data['blockJsCallCode'] = $this->block->generateJsBlockCode($data['site']['id'], $data['site']['domain'], $data['block']['id']);

        $this->load->view('block_builder', $data);
    }


    /**
     * API - get block js code
     */
    public function apiGetBlockJsCode()
    {
        if (!$this->csrf->validateCsrfToken($this->input->post('csrfToken', true))) {
            exit;
        }

        $siteId     = $this->input->post('siteId', true);
        $siteDomain = $this->input->post('siteDomain', true);
        $blockId    = $this->input->post('blockId', true);

        exit($this->block->generateJsBlockCode($siteId, $siteDomain, $blockId));
    }


    /**
     * API - create new block
     */
    public function apiCreateBlock()
    {
        if (!$this->csrf->validateCsrfToken($this->input->post('csrfToken', true))) {
            exit;
        }

        $blockName = $this->input->post('blockName', true);
        $siteID    = $this->input->post('siteID', true);

        if (!$blockName) {
            $response['status'] = 'error';
            $response['msg']    = '블록 이름은 비워 둘 수 없습니다!';
            exit(json_encode($response));
        }

        if ($this->block->blockNameExists($blockName)) {
            $response['status'] = 'error';
            $response['msg']    = '이름이 같은 블록이 시스템에 이미 있습니다!';
            exit(json_encode($response));
        }

        if (!ctype_digit((string) $siteID)) {
            $response['status'] = 'error';
            $response['msg']    = '블록을 생성하면서 오류가 발생하였습니다.';
            exit(json_encode($response));
        }

        $response['status'] = 'ok';
        $response['msg']    = $this->block->createBlock($siteID, $blockName);
        exit(json_encode($response));
    }


    /**
     * API -  remove block
     */
    public function apiRemoveBlock()
    {
        if (!$this->csrf->validateCsrfToken($this->input->post('csrfToken', true))) {
            exit;
        }

        $this->block->removeBlock($this->input->post('blockID', true));
    }


    /**
     * API - play block
     */
    public function apiPlayBlock()
    {
        if (!$this->csrf->validateCsrfToken($this->input->post('csrfToken', true))) {
            exit;
        }

        $this->block->playBlock($this->input->post('blockID', true));
    }


    /**
     * API - stop block
     */
    public function apiStopBlock()
    {
        if (!$this->csrf->validateCsrfToken($this->input->post('csrfToken', true))) {
            exit;
        }

        $this->block->stopBlock($this->input->post('blockID', true));
    }


    /**
     * API - copy block
     */
    public function apiCopyBlock()
    {
        if (!$this->csrf->validateCsrfToken($this->input->post('csrfToken', true))) {
            exit;
        }

        $blockID      = $this->input->post('blockID', true);
        $pasteSiteID  = $this->input->post('pasteSiteID', true);
        $newBlockName = $this->input->post('newBlockName', true);

        if ($this->block->copyBlock($blockID, $pasteSiteID, $newBlockName)) {
            exit('ok');
        }
        exit('error');
    }


    /**
     * API(block builder) - save block
     */
    public function apiSaveBlock()
    {
        if (!$this->csrf->validateCsrfToken($this->input->post('csrfToken', true))) {
            exit;
        }

        $data['id']                     = $this->input->post('id', true);
        $data['block_name']             = $this->input->post('block_name', true);
        $data['block_ads_img_position'] = $this->input->post('block_ads_img_position', true);
        $data['block_count_ads_width']  = $this->input->post('block_count_ads_width', true);
        $data['block_count_ads_height'] = $this->input->post('block_count_ads_height', true);
        $data['block_css_styles']       = $this->input->post('block_css_styles', true);
        $data['block_third_party_code'] = $this->input->post('block_third_party_code', false);
        $data['block_link_visibility']  = $this->input->post('block_link_visibility', true);

        $this->block->setBlock($data);
    }


    /**
     * API(block builder) - block data - json
     */
    public function apiGetBlockJsonData()
    {
        if (!$this->csrf->validateCsrfToken($this->input->post('csrfToken', true))) {
            exit;
        }

        if ($this->block->blockExists($this->input->post('blockID', true))) {

            if ($this->input->post('preset', true) > 0) {
                $data['response_data']   = $this->block->getBlockPresetData($this->input->post('preset', true));
                $data['response_status'] = 'ok';
            } else {
                $data['response_data']   = $this->block->getBlockData($this->input->post('blockID', true));
                $data['response_status'] = 'ok';
            }

            exit(json_encode($data));
        }

        $data['response_data']   = '';
        $data['response_status'] = 'error';

        exit(json_encode($data));
    }
}
