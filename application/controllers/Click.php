<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Click extends CI_Controller
{


    public function __construct()
    {
        parent::__construct();

        $this->load->model('device_model', 'device');
        $this->load->model('click_model', 'click');
        $this->load->model('set_stat_model', 'setStat');
        $this->load->model('filter_model', 'filter');
    }


    /**
     * @param string $encryptedClickData
     */
    public function index($encryptedClickData = '')
    {
        // is banned ip
        if ($this->filter->isBannedIp()) {
            exit;
        }

        // decrypt click link data
        if (!$clickData = $this->click->getEncryptedClickData($encryptedClickData)) {
            exit;
        }

        // validate decrypt data
        if ($clickData['type'] == 'click') {
            // macroces
            $clickData = $this->click->replaceMacroses($clickData);

            // get dev data
            $deviceData = $this->device->getDeviceInfo();


            // save click on the "general" site statistics
            $this->setStat->setFastStatSiteClicks($clickData['site_id']);

            // save click on the "general" block statistics
            $this->setStat->setFastStatBlockClicks($clickData['block_id']);

            // save click on the "general" camp statistics
            $this->setStat->setFastStatCampClicks($clickData['camp_id']);

            // save clickon the "general" ad statistics
            $this->setStat->setFastStatAdClicks($clickData['ad_id']);

            // save click on the site statistics
            $this->setStat->setSiteStatClick($clickData, $deviceData);

            // save click on the block statistics
            $this->setStat->setBlockStatClick($clickData, $deviceData);

            // save click on the camp statistics
            $this->setStat->setCampStatClick($clickData, $deviceData);

            // save click on the ad statistics
            $this->setStat->setOneAdStatClick($clickData, $deviceData);

            // save click on the ip statistics
            $this->setStat->setIpsStat('c', $clickData['site_id'], $clickData['block_id'], $clickData['camp_id'], $clickData['ad_id'], $deviceData['ip_long']);

            // location user
            header('Location:' . $clickData['ad_link']);
        }
    }

}
