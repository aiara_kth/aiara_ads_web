<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Filter extends CI_Controller
{


    public function __construct()
    {
        parent::__construct();

        $this->load->model('csrf_model', 'csrf');
        $this->load->model('filter_model', 'filter');
        $this->load->model('user_model', 'user');
        

        $this->user->isLogin(); // auth
    }


    /**
     *  Filter page
     */
    public function index()
    {
        $data['csrfToken']     = $this->csrf->generateCsrfToken();
        $data['bannedIpsList'] = $this->filter->getBannedIpsList();
        $data['user']          = $this->user->getUserData();

        $this->load->view('filter', $data);
    }


    /**
     * API - set ips
     */
    public function apiSetIps()
    {
        if (!$this->csrf->validateCsrfToken($this->input->post('csrfToken', true))) {
            exit;
        }

        $ips = @explode("\n", $this->input->post('ips', true));
        $this->filter->setBannedIps($ips);
    }

}
