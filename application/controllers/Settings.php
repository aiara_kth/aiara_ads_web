<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Settings extends CI_Controller
{


    public function __construct()
    {
        parent::__construct();

        $this->load->model('csrf_model', 'csrf');
        $this->load->model('set_stat_model', 'set_stat');
        $this->load->model('adflex_config_model', 'adflex_config');
        $this->load->model('user_model', 'user');

        $this->user->isLogin(); // auth
    }


    /**
     * Settings page
     */
    public function index()
    {
        $data['csrfToken'] = $this->csrf->generateCsrfToken();
        $data['user']      = $this->user->getUserData();
        $this->load->view('settings', $data);
    }


    /**
     * API - get server time
     */
    public function apiGetServerTime()
    {
        if (!$this->csrf->validateCsrfToken($this->input->post('csrfToken', true))) {
            exit;
        }

        echo date('H:i');
    }


    /**
     * API - get server timezone
     */
    public function apiGetServerTimezone()
    {
        if (!$this->csrf->validateCsrfToken($this->input->post('csrfToken', true))) {
            exit;
        }

        $timezone = $this->adflex_config->getAdflexConfigItem('timezone');

        echo $timezone ? $timezone : 'Europe/London';
    }


    /**
     * API -  set server timezone
     */
    public function apiSetServerTimezone()
    {
        if (!$this->csrf->validateCsrfToken($this->input->post('csrfToken', true))) {
            exit;
        }

        $this->adflex_config->setAdflexConfigItem('timezone', $this->input->post('timezone', true));
    }


    /**
     * API - set new password
     */
    public function apiSetNewPassword()
    {
        if (!$this->csrf->validateCsrfToken($this->input->post('csrfToken', true))) {
            exit;
        }

        $user_data = $this->user->getUserData();

        if ($user_data['user_pass_hash'] != sha1($this->input->post('current_pass', true))) {
            exit('fail_current_pass');
        }

        if (!preg_match("/.{8,}/i", $this->input->post('new_pass', true))) {
            exit('fail_new_pass');
        }

        $this->db->where('id', 1);
        $this->db->set('user_pass_hash', sha1($this->input->post('new_pass', true)));
        $this->db->update('users');

        $cookie['name']     = '_sess';
        $cookie['value']    = sha1($user_data['user_email'] . sha1($this->input->post('new_pass', true)) . $this->input->server('HTTP_USER_AGENT', true) . $this->input->server('HTTP_ACCEPT_LANGUAGE', true));
        $cookie['expire']   = 0;
        $cookie['httponly'] = true;
        set_cookie($cookie);

        exit('pass_change_success');
    }


    /**
     * API - get default page
     */
    public function apiGetDefaultPage()
    {
        if (!$this->csrf->validateCsrfToken($this->input->post('csrfToken', true))) {
            exit;
        }

        echo $this->adflex_config->getAdflexConfigItem('default_page');
    }


    /**
     * API - set default page
     */
    public function apiSetDefaultPage()
    {
        if (!$this->csrf->validateCsrfToken($this->input->post('csrfToken', true))) {
            exit;
        }

        if (in_array($this->input->post('default_page', true), array('sites', 'campaigns', 'stat', 'main'))) {
            $this->adflex_config->setAdflexConfigItem('default_page', $this->input->post('default_page'));
        }
    }


    /**
     * API -clear stat
     */
    public function apiClearStat()
    {
        if (!$this->csrf->validateCsrfToken($this->input->post('csrfToken', true))) {
            exit;
        }

        if (!in_array($this->input->post('period', true), array('week', '2week', 'month', 'year', 'all'))) {
            exit('error');
        }

        $this->set_stat->clearAllStat($this->input->post('period', true));
    }

}
