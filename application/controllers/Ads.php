<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Ads extends CI_Controller {


    public function __construct()
    {
        parent::__construct();

        $this->load->model('csrf_model', 'csrf');
        $this->load->model('camp_model', 'camp');
        $this->load->model('ads_model', 'ads');
        $this->load->model('user_model', 'user');

        $this->user->isLogin(); // auth
    }


    /**
     * @param string $campID
     */
    public function index($campID = '')
    {
        if (!$this->camp->campaignExists($campID)) {
            show_404();
        }

        $data['csrfToken'] = $this->csrf->generateCsrfToken();
        $data['camp']      = $this->camp->getOneCampaignData($campID);
        $data['ads']       = $this->ads->getCampaignAds($campID);
        $data['user']      = $this->user->getUserData();

        $this->load->view('ads_builder', $data);
    }


    /**
     * API - save ad data
     */
    public function apiSetAdData()
    {
        if (!$this->csrf->validateCsrfToken($this->input->post('csrfToken', true))) {
            exit;
        }

        $adImageName = '';
        $adParams    = $this->input->post();

        // validate title
        if (mb_strlen($adParams['ad_title'], 'UTF-8') > 100 OR mb_strlen($adParams['ad_title'], 'UTF-8') == 0) {
            exit(json_encode(array('status' => 'error', 'msg' => 'Title can not be empty! Length header can not be more than 60 characters!')));
        }

        // validate descr
        if (mb_strlen($adParams['ad_description'], 'UTF-8') > 100 OR mb_strlen($adParams['ad_description'], 'UTF-8') == 0) {
            exit(json_encode(array('status' => 'error', 'msg' => 'Description can not be empty! Long description can not be more than 90 characters!')));
        }

        // validate link
        if (!filter_var($adParams['ad_link'], FILTER_VALIDATE_URL)) {
            exit(json_encode(array('status' => 'error', 'msg' => 'Invalid link!')));
        }

        // if img exists - save img
        if (isset($_FILES['ad_image'])) {
            if (!$adImageName = $this->ads->uploadAdImage()) {
                exit(json_encode(array('status' => 'error', 'msg' => 'Error saving image!')));
            }
        }

        // save data
        $this->ads->setAdData($adParams['camp_id'], $adParams['ad_title'], $adParams['ad_description'], $adParams['ad_link'], $adImageName);

        exit(json_encode(array('status' => 'ok')));
    }


    /**
     * API - update ad data
     */
    public function apiUpdateAdData()
    {
        if (!$this->csrf->validateCsrfToken($this->input->post('csrfToken', true))) {
            exit;
        }

        $adImageName = '';

        $adParams = $this->input->post();

        // is ad id exists
        if (!$this->ads->adExists($adParams['ad_id'])) {
            exit(json_encode(array('status' => 'error', 'msg' => 'Error! Ad not found')));
        }

        // validate link
        if (!filter_var($adParams['ad_link'], FILTER_VALIDATE_URL)) {
            exit(json_encode(array('status' => 'error', 'msg' => 'Invalid link!')));
        }

        // validate title
        if (mb_strlen($adParams['ad_title'], 'UTF-8') > 100 OR mb_strlen($adParams['ad_title'], 'UTF-8') == 0) {
            exit(json_encode(array('status' => 'error', 'msg' => 'Title can not be empty! Length header can not be more than 60 characters!')));
        }

        // validate descr
        if (mb_strlen($adParams['ad_description'], 'UTF-8') > 100 OR mb_strlen($adParams['ad_description'], 'UTF-8') == 0) {
            exit(json_encode(array('status' => 'error', 'msg' => 'Description can not be empty! Long description can not be more than 90 characters!')));
        }

        // if img exists - save img
        if (isset($_FILES['ad_image'])) {
            if (!$adImageName = $this->ads->uploadAdImage()) {
                exit(json_encode(array('status' => 'error', 'msg' => 'Error saving image!')));
            }
        }

        // save
        $this->ads->updateAdData($adParams['ad_id'], $adParams['ad_title'], $adParams['ad_description'], $adParams['ad_link'], $adImageName);

        exit(json_encode(array('status' => 'ok')));
    }


    /**
     * API - remove ad image
     */
    public function apiRemoveAdImage()
    {
        if (!$this->csrf->validateCsrfToken($this->input->post('csrfToken', true))) {
            exit;
        }

        $adID = $this->input->post('adID', true);

        if ($this->ads->adExists($adID)) {
            $this->ads->removeAdImage($adID);
        }
    }


    /**
     * API - remove ad
     */
    public function apiRemoveAd()
    {
        if (!$this->csrf->validateCsrfToken($this->input->post('csrfToken', true))) {
            exit;
        }

        $adID = $this->input->post('adID', true);

        if ($this->ads->adExists($adID)) {
            $this->ads->removeAd($adID);
        }
    }


    /**
     * API - play ad
     */
    public function apiPlayAd()
    {
        if (!$this->csrf->validateCsrfToken($this->input->post('csrfToken', true))) {
            exit;
        }

        $this->ads->playAd($this->input->post('adID', true));
    }


    /**
     * API - stop ad
     */
    public function apiStopAd()
    {
        if (!$this->csrf->validateCsrfToken($this->input->post('csrfToken', true))) {
            exit;
        }

        $this->ads->stopAd($this->input->post('adID', true));
    }
}
