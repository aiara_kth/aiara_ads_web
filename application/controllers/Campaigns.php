<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Campaigns extends CI_Controller
{


    public function __construct()
    {
        parent::__construct();

        $this->load->model('csrf_model', 'csrf');
        $this->load->model('camp_model', 'camp');
        $this->load->model('site_model', 'site');
        $this->load->model('user_model', 'user');

        $this->user->isLogin(); // auth
    }


    /**
     * Camps page
     */
    public function index()
    {
        $data['csrfToken'] = $this->csrf->generateCsrfToken();
        $data['user']      = $this->user->getUserData();
        $data['camps']     = $this->camp->getAllCampaignsData();

        $this->load->view('campaigns', $data);
    }


    /**
     * Camp builder/edit page
     *
     * @param string $campID
     */
    public function campaign_builder($campID = '')
    {
        if (!$this->camp->campaignExists($campID)) {
            show_404();
        }

        $data['csrfToken'] = $this->csrf->generateCsrfToken();
        $data['user']      = $this->user->getUserData();
        $data['camp']      = $this->camp->getOneCampaignData($campID);

        $this->load->view('campaign_builder', $data);
    }


    /**
     * API - create new campaign
     */
    public function apiCreateCampaign()
    {
        if (!$this->csrf->validateCsrfToken($this->input->post('csrfToken', true))) {
            exit;
        }

        $campName = $this->input->post('campName', true);

        if (!$campName) {
            $response['status'] = 'error';
            $response['msg']    = '켐페인 이름이 빈값입니다.';
            exit(json_encode($response));
        }

        if ($this->camp->campaignNameExists($campName)) {
            $response['status'] = 'error';
            $response['msg']    = '이미 있는 켐페인 이름입니다!';
            exit(json_encode($response));
        }

        $response['status'] = 'ok';
        $response['msg']    = $this->camp->createCampaign($campName);
        exit(json_encode($response));
    }


    /**
     * API - remove campaign and ampaign ads
     */
    public function apiRemoveCampaign()
    {
        if (!$this->csrf->validateCsrfToken($this->input->post('csrfToken', true))) {
            exit;
        }

        $campID = $this->input->post('campID', true);

        $this->camp->removeAdsCampaign($campID);
        $this->camp->removeCampaign($campID);
    }


    /**
     * API - play campaign
     */
    public function apiPlayCampaign()
    {
        if (!$this->csrf->validateCsrfToken($this->input->post('csrfToken', true))) {
            exit;
        }

        $this->camp->playCampaign($this->input->post('campID', true));
    }


    /**
     * API - stop campaign
     */
    public function apiStopCampaign()
    {
        if (!$this->csrf->validateCsrfToken($this->input->post('csrfToken', true))) {
            exit;
        }

        $this->camp->stopCampaign($this->input->post('campID', true));
    }


    /**
     * API - update campaign data
     */
    public function apiUpdateCampaign()
    {
        if (!$this->csrf->validateCsrfToken($this->input->post('csrfToken', true))) {
            exit;
        }

        $campData = $this->input->post();
        echo ($this->camp->updateCampaign($campData)) ? 'ok' : 'error';
    }


    /**
     * API - campaign data json
     */
    public function apiGetCampaignDataJson()
    {
        if (!$this->csrf->validateCsrfToken($this->input->post('csrfToken', true))) {
            exit;
        }

        $campID = $this->input->post('campID', true);

        if ($data = $this->camp->getOneCampaignData($campID)) {
            $data['camp_date_start'] = date('d-m-Y', $data['camp_date_start']);
            $data['camp_date_stop']  = date('d-m-Y', $data['camp_date_stop']);
            $data['sites']           = $this->site->getAllSitesData();

            exit(json_encode($data));
        }

        exit('error');
    }

}
