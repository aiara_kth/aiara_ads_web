<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Sites extends CI_Controller
{


    public function __construct()
    {
        parent::__construct();

        $this->load->model('csrf_model', 'csrf');
        $this->load->model('site_model', 'site');
        $this->load->model('user_model', 'user');

        $this->user->isLogin(); // auth
    }


    /**
     * Sites page
     */
    public function index()
    {
        $data['csrfToken'] = $this->csrf->generateCsrfToken();
        $data['user']      = $this->user->getUserData();
        $data['sites']     = $this->site->getAllSitesData();

        $this->load->view('sites', $data);
    }


    /**
     * API - add site
     */
    public function apiAddSite()
    {
        if (!$this->csrf->validateCsrfToken($this->input->post('csrfToken', true))) {
            exit;
        }

        if (!$this->input->post('domain', true)) {
            $response['status'] = 'error';
            $response['msg']    = '사이트주소를 입력해주세요.';
            exit(json_encode($response));
        }

        if (!$this->site->isRealDomain($this->input->post('domain', true))) {
            $response['status'] = 'error';
            $response['msg']    = '추가된 사이트는 실제 사이트여야 하며 온라인상에서 작동이 되여야 합니다..';
            exit(json_encode($response));
        }


        if ($this->site->domainExists($this->input->post('domain', true))) {
            $response['status'] = 'error';
            $response['msg']    = '이 사이트는 이미 시스템에 있습니다!';
            exit(json_encode($response));
        }

        if ($this->site->addSite($this->input->post('domain', true))) {
            $response['status'] = 'ok';
            exit(json_encode($response));
        }

        $response['status'] = 'error';
        $response['msg']    = '사이트를 추가하면서 오류가 발생하였습니다.';
        exit(json_encode($response));
    }


    /**
     * API - remove site and blocks
     */
    public function apiRemoveSite()
    {
        if (!$this->csrf->validateCsrfToken($this->input->post('csrfToken', true))) {
            exit;
        }

        if (!ctype_digit((string) $this->input->post('siteID', true))) {
            exit;
        }

        $this->site->removeSite($this->input->post('siteID', true));
        $this->site->removeSiteBlocks($this->input->post('siteID', true));
    }


    /**
     * API - play site
     */
    public function apiPlaySite()
    {
        if (!$this->csrf->validateCsrfToken($this->input->post('csrfToken', true))) {
            exit;
        }

        if (!ctype_digit((string) $this->input->post('siteID', true))) {
            exit;
        }

        $this->site->playSite($this->input->post('siteID', true));
    }


    /**
     * API - stop site
     */
    public function apiStopSite()
    {
        if (!$this->csrf->validateCsrfToken($this->input->post('csrfToken'))) {
            exit;
        }

        if (!ctype_digit((string) $this->input->post('siteID'))) {
            exit;
        }

        $this->site->stopSite($this->input->post('siteID'));
    }

}
