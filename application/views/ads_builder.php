<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<!doctype html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <title>Ads - <?php echo $camp['camp_name']; ?></title>
        <meta name="csrf-param" content="<?php echo $csrfToken; ?>">
        <link rel='stylesheet' href='/css/bootstrap.min.css' type='text/css' media='all'>
        <link rel='stylesheet' href='/css/bootstrap-dialog.css' type='text/css' media='all'>
        <link rel='stylesheet' href='/css/font-awesome.min.css' type='text/css' media='all'>
        <link rel='stylesheet' href='/css/icons.css' type='text/css' media='all'>
        <link rel='stylesheet' href='/css/style.css' type='text/css' media='all'>
        <link rel="shortcut icon" href="/img/favicon.ico">
        <script src="/js/jquery.min.js"></script>
        <script src="/js/bootstrap.min.js"></script>
        <script src="/js/bootstrap-dialog.js"></script>
        <script>
            $(document).ready(function () {

                // csrf token
                var csrfToken = $('[name="csrf-param"]').attr('content');

                // camp id
                var campID = location.href.split('/')[4];


                // obj FormData
                var formData = new FormData();


                // clear title
                $('#clear_title').on('click', function () {
                    $('#ad_title').val('');
                });


                // clear description
                $('#clear_description').on('click', function () {
                    $('#ad_description').val('');
                });


                // clear link
                $('#clear_link').on('click', function () {
                    $('#ad_link').val('');
                });


                // show / hide macrosese block
                $('#show_hide_info').on('click', function () {
                    $(this).toggleClass('active');
                    $('#ad_info').toggleClass('hidden');
                });


                // remove image
                $('#remove_image').on('click', function () {
                    formData.delete('ad_image');
                    $('#ad_no_image').removeClass('hidden');
                    $('#ad_canvas , #ad_image').addClass('hidden');
                    $('#ad_error_msg').text('').addClass('hidden');
                    //
                    if ($.data(document.body, 'ad_id')) {
                        $.post('/ads/apiRemoveAdImage/', {adID: $.data(document.body, 'ad_id'), csrfToken: csrfToken}, function () {
                            $.data(document.body, 'ad_id', '');
                        });
                    }
                });


                // add ad image
                $('#add_image').on('change', function () {
                    // get file of form
                    var imgFile = this.files[0];
                    // format validate
                    if (imgFile.type !== 'image/jpg' && imgFile.type !== 'image/jpeg' && imgFile.type !== 'image/gif' && imgFile.type !== 'image/png') {
                        $('#ad_error_msg').text('Error! Invalid file format!').removeClass('hidden');
                        return;
                    }
                    // file size validate
                    else if (imgFile.size > 1000000) {
                        $('#ad_error_msg').text('Error! Exceeded image size!').removeClass('hidden');
                        return;
                    }
                    // When changing the image of the ad, we delete the old image on the server.
                    if ($.data(document.body, 'ad_id')) {
                        $.post('/ads/apiRemoveAdImage/', {adID: $.data(document.body, 'ad_id'), csrfToken: csrfToken});
                    }
                    // Insert the cropped image in canvas
                    var ctx = document.getElementById('ad_canvas').getContext('2d');
                    var img = new Image;
                    img.src = URL.createObjectURL(imgFile);
                    // resize image
                    img.onload = function () {
                        var maxResolution = (img.width > img.height) ? img.height : img.width;
                        var offsetX = Math.floor((img.width - maxResolution) / 2);
                        var offsetY = Math.floor((img.height - maxResolution) / 2);
                        ctx.drawImage(img, offsetX, offsetY, maxResolution, maxResolution, 0, 0, 178, 178);
                    }
                    // Hide all unnecessary
                    $('#ad_error_msg').text('').addClass('hidden');
                    $('#ad_no_image, #ad_image').addClass('hidden');
                    $('#ad_canvas').removeClass('hidden');
                    // Add the image to the form_data object for subsequent sending to the server
                    formData.append('ad_image', imgFile);
                });


                // save ad
                $('#ad_save').on('click', function () {
                    // Add the animation to the save button
                    $('#ad_save').prop('disabled', true).html('<i class="fa fa-spinner fa-spin fa-fw"></i> Save');
                    // Add the creative data to the formdata object
                    formData.append('ad_title', $('#ad_title').val());
                    formData.append('ad_description', $('#ad_description').val());
                    formData.append('ad_link', $('#ad_link').val());
                    formData.append('csrfToken', csrfToken);

                    $.ajax({
                        type: "POST",
                        processData: false,
                        contentType: false,
                        timeout: 10000,
                        context: this,
                        url: $.data(document.body, 'request_link'), // Link for ajax request (depends on whether we add a new one or edit the existing advertisement)
                        data: formData,
                        complete: function (data) {
                            var response = $.parseJSON(data.responseText);
                            if (response.status === 'error') {
                                $('#ad_save').prop('disabled', false).html('<i class="fa fa-check" aria-hidden="true"></i> Save');
                                $('#ad_error_msg').text(response.msg).removeClass('hidden');
                            } else {
                                location.reload();
                            }
                        }
                    });
                });


                // Cleaning the form of adding an announcement when the window is closed
                $('.close').on('click', function () {
                    $('#ad_modal_title').text('');
                    $('#ad_title, #ad_description, #ad_link').val('');
                    $('#ad_error_msg').text('').addClass('hidden');
                    $('#ad_no_image').removeClass('hidden');
                    $('#ad_image').attr('src', '').addClass('hidden');
                    $('#ad_canvas').addClass('hidden');
                    $('#ad_save').prop('disabled', false).html('<i class="fa fa-check" aria-hidden="true"></i> Save');
                    $('#ad_info').addClass('hidden');
                    $.data(document.body, 'ad_id', '');
                });


                // create new ad
                $('#_create_ad').on('click', function () {
                    // Add the campaign's form_data id
                    formData.append('camp_id', campID);
                    // Set the link for ajax request (/ads/apiSetAdData/) - save the new ad
                    $.data(document.body, 'request_link', '/ads/apiSetAdData/');
                    // добавим в заголовок
                    $('#ad_modal_title').text('New ad');
                    // Show the modal window for adding a new ad
                    $('#_new_ad_modal').modal({
                        backdrop: 'static',
                        keyboard: false
                    });
                });


                // edit ad
                $('._ads_edit').on('click', function () {
                    // Set the link for ajax request (/ads/updateData/) - edit an existing ad
                    $.data(document.body, 'request_link', '/ads/apiUpdateAdData/');
                    // Set the id of the editable ad to the global variable
                    $.data(document.body, 'ad_id', $(this).attr('name'));

                    var adID = $(this).attr('name');
                    var adTitle = $('[name=' + adID + ']').filter('.ad_title').text();
                    var adDescription = $('[name=' + adID + ']').filter('.ad_descr').text();
                    var adLink = $('[name=' + adID + ']').filter('.ad_link').attr('title');
                    var adImageLink = $('[name=' + adID + ']').filter('.ad_image').attr('src');

                    $('#ad_modal_title').text('Edit ad: ' + adID);

                    $('#ad_title').val(adTitle);
                    $('#ad_description').val(adDescription);
                    $('#ad_link').val(adLink);

                    if (adImageLink) {
                        $('#ad_image').attr('src', adImageLink).removeClass('hidden');
                        $('#ad_no_image, #ad_canvas').addClass('hidden');
                    }

                    formData.append('ad_id', adID);

                    $('#_new_ad_modal').modal({
                        backdrop: 'static',
                        keyboard: false
                    });
                });


                // remove ad
                $('._ads_remove').on('click', function () {
                    var adID = $(this).attr('name');
                    $('._remove_ad_id').text(adID);
                    $('#_remove_ad_modal').modal();
                    $('._remove_ad_ok').on('click', function () {
                        $.post('/ads/apiRemoveAd/', {
                            adID: adID,
                            csrfToken: csrfToken
                        }, function () {
                            $('#_remove_ad_modal').modal('hide');
                            location.reload();
                        });
                    });
                });


                // play ad
                $('._ads_play').on('click', function () {
                    $.post('/ads/apiPlayAd/', {
                        adID: $(this).attr('name'),
                        csrfToken: csrfToken
                    }
                    , function () {
                        location.reload();
                    });
                });


                // stop ad
                $('._ads_stop').on('click', function () {
                    $.post('/ads/apiStopAd/', {
                        adID: $(this).attr('name'),
                        csrfToken: csrfToken
                    }
                    , function () {
                        location.reload();
                    });
                });


                // tips
                $('[data-toggle="tooltip"]').tooltip({
                    container: 'body',
                    html: true
                });


                // logout
                $('#logout').on('click', function () {
                    $.post('/login/logOut/', {csrfToken: csrfToken}, function () {
                        location.reload();
                    });
                });


            });
        </script>
    </head>
    <body>

        <div class="container-fluid">
            <div class="row">
                <div class="col-xlg-10 col-xlg-offset-1 col-xs-12 col-xs-offset-0">
                    <?php include_once ('nav.php') ?>
                    <ol class="breadcrumb">
                        <li><a href="/campaigns/">Campaigns</a></li>
                        <li><a href="/campaigns/<?php echo $camp['id']; ?>"><?php echo $camp['camp_name']; ?></a></li>
                        <li>ads</li>
                    </ol>
                    <?php if ($ads): ?>
                        <div class="panel panel-primary">
                            <div class="panel-heading"><b>Ads</b></div>
                            <table class="table table-bordered table-striped">
                                <tbody>
                                    <?php
                                    $i = 1;
                                    foreach ($ads as $item):
                                        ?>
                                        <tr style="height: 70px;">
                                            <td style="width: 35px; text-align: center; vertical-align: middle;"><?php echo $i++; ?></td>
                                            <td style="width: 450px; text-align: left; vertical-align: middle;">
                                                <div class="ad_prev">
                                                    <div class="label label-default">id: <?php echo $item['id']; ?></div>
                                                    <img class="ad_image <?php echo empty($item['ad_img']) ? 'hidden' : ''; ?>" name="<?php echo $item['id']; ?>" src="<?php echo empty($item['ad_img']) ? '' : '/images/' . $item['ad_img']; ?>" width="65" height="65">
                                                    <span class="ad_title" name="<?php echo $item['id']; ?>"><?php echo $item['ad_title']; ?></span><br>
                                                    <span class="ad_descr" name="<?php echo $item['id']; ?>"><?php echo $item['ad_descr']; ?></span><br>
                                                    <span class="ad_link" title="<?php echo $item['ad_link']; ?>" name="<?php echo $item['id']; ?>"><?php echo parse_url($item['ad_link'], PHP_URL_HOST); ?></span>
                                                </div>
                                            </td>
                                            <td style="text-align: center; vertical-align: middle; line-height: normal;">Views: <b><?php echo $item['ad_views']; ?></b></td>
                                            <td style="text-align: center; vertical-align: middle; line-height: normal;">Clicks: <b><?php echo $item['ad_clicks']; ?></b></td>
                                            <td style="text-align: center; vertical-align: middle; line-height: normal;">CTR: <b><?php echo ($item['ad_clicks'] == 0 OR ( $item['ad_views'] == 0 AND $item['ad_clicks'] == 0)) ? 0 : round(($item['ad_clicks'] / $item['ad_views']) * 100, 2); ?></b></td>
                                            <td style="width: 360px; text-align: center; vertical-align: middle; line-height: normal;">
                                                <div class="btn-group">
                                                    <button name="<?php echo $item['id']; ?>" class="_ads_play btn btn-sm btn-default <?php echo ($item['ad_status'] == 1) ? 'active' : ''; ?>"  <?php echo (empty($item['ad_title']) OR empty($item['ad_descr']) OR empty($item['ad_link'])) ? 'disabled' : ''; ?>  ><i class="fa fa-play" aria-hidden="true"></i> Play</button>
                                                    <button name="<?php echo $item['id']; ?>" class="_ads_stop btn btn-sm btn-default <?php echo ($item['ad_status'] == 0) ? 'active' : ''; ?>"><i class="fa fa-stop" aria-hidden="true"></i> Stop</button>
                                                    <a class="btn btn-default btn-sm" href="/stat/ad/<?php echo $item['id']; ?>"><i class="fa fa-bar-chart" aria-hidden="true"></i> Stats</a>
                                                    <button name="<?php echo $item['id']; ?>" class="_ads_edit btn btn-sm btn-default" type="button"><i class="fa fa-pencil" aria-hidden="true"></i> Edit</button>
                                                    <button name="<?php echo $item['id']; ?>" class="_ads_remove btn btn-danger btn-sm"><i class="fa fa-trash-o" aria-hidden="true"></i> Remove</button>
                                                </div>
                                            </td>
                                        </tr>
                                    <?php endforeach; ?>
                                </tbody>
                            </table>
                        </div>
                    <?php endif; ?>
                    <p>
                        <button style="margin-bottom: 20px;" id="_create_ad" class="btn btn-success center-block"><i class="fa fa-plus" aria-hidden="true"></i> New ad </button>
                    </p>
                </div>
            </div>
        </div>
        <!--start add and edit ad modal-->
        <div class="modal fade" id="_new_ad_modal">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header header-success">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title"><i class="fa fa-plus-circle" aria-hidden="true"></i> <span id="ad_modal_title"></span></h4>
                    </div>
                    <div style="overflow: hidden;" class="modal-body">
                        <div id="ad_error_msg" class="alert alert-danger hidden"></div>
                        <input id="remove_ad_image" type="hidden" value="" >
                        <div class="ad_img">
                            <i id="ad_no_image" style="color:#cccccc; padding: 55px 0 0 55px;" class="fa fa-picture-o fa-5x fa-fw"></i>
                            <img id="ad_image" class="upload_image_preview hidden" src="" width="180" height="180">
                            <canvas id="ad_canvas" class="hidden"  width="178" height="178"></canvas>
                        </div>
                        <div class="ad_form">
                            <div class="input-group form-group">
                                <input id="ad_title" type="text" class="form-control form-group" placeholder="Title">
                                <span id="clear_title" class="input-group-addon btn btn-default" data-toggle="tooltip" data-placement="right" title="Clear title"><i class="fa fa-eraser" aria-hidden="true"></i></span>
                            </div>
                            <div class="input-group form-group">
                                <textarea id="ad_description" class="custom-control form-control form-group " placeholder="Description" rows="3" style="resize:none; height: 85px;"></textarea>
                                <span id="clear_description" class="input-group-addon btn btn-default" data-toggle="tooltip" data-placement="right" title="Clear description"><i class="fa fa-eraser" aria-hidden="true"></i></span>
                            </div>
                            <div class="input-group">
                                <input id="ad_link" type="text" class="form-control" placeholder="Link" value="<?php echo $camp['camp_link']; ?>" >
                                <span id="clear_link" class="input-group-addon btn btn-default" data-toggle="tooltip" data-placement="right" title="Clear link"><i class="fa fa-eraser" aria-hidden="true"></i></span>
                            </div>
                        </div>
                    </div>
                    <div id="ad_info" class="hidden">
                        <div class="alert alert-success alert-dismissable" style="margin: 0 15px 15px 15px;">
                            <b>You can use these macros in the link:</b><br />
                            <b>[CAMP_ID]</b> - Campaign id<br/>
                            <b>[BLOCK_ID]</b> - Block id<br/>
                            <b>[AD_ID]</b> - Ad id<br/>
                            <b>[SITE_ID]</b> - Site id<br/>
                            <b>[TIME_CLIK]</b> - Click date in the format  <b>YYYY-MM-DD_HH-MM-SS</b> (2017-06-15_10-35-15)
                        </div>
                    </div>
                    <div class="modal-footer">
                        <div class="pull-left">
                            <label>
                                <input id="add_image" type="file">
                                <span class=" btn btn-sm btn-success" data-toggle="tooltip" title="Max size 1 mb <br>( JPG, PNG, GIF )"><i class="fa fa-plus" aria-hidden="true"></i> Add image</span>
                            </label>
                            <button id="remove_image" class="btn btn-danger btn-sm"><i class="fa fa-times" aria-hidden="true"></i> Remove image</button>
                        </div>
                        <button id="show_hide_info" class="btn btn-default btn-sm"><i class="fa fa-info-circle" aria-hidden="true"></i></button>
                        <button id="ad_save" class="btn btn-success btn-sm"><i class="fa fa-check" aria-hidden="true"></i> Save</button>
                    </div>
                </div>
            </div>
        </div>
        <!--end add and edit ad modal-->

        <!--start remove ad modal-->
        <div class="modal fade" id="_remove_ad_modal" style="display:none">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header header-danger">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title"><i class="fa fa-question-circle" aria-hidden="true"></i> Remove ad "<b class="_remove_ad_id"></b>"</h4>
                    </div>
                    <div class="modal-body">
                        Remove this ad?
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="_remove_ad_ok btn btn-danger btn-sm pull-left"><i class="fa fa-trash-o" aria-hidden="true"></i> Remove</button>
                        <button type="button" class="btn btn-default btn-sm" data-dismiss="modal"> Close</button>
                    </div>
                </div>
            </div>
        </div>
        <!--end remove ad modal-->
    </body>
</html>
