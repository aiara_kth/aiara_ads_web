<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<!doctype html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <title>캠페인</title>
        <meta name="csrf-param" content="<?php echo $csrfToken; ?>">
        <link rel='stylesheet' href='/css/bootstrap.min.css' type='text/css' media='all'>
        <link rel='stylesheet' href='/css/bootstrap-dialog.css' type='text/css' media='all'>
        <link rel='stylesheet' href='/css/font-awesome.min.css' type='text/css' media='all'>
        <link rel='stylesheet' href='/css/style.css' type='text/css' media='all'>
        <link rel="shortcut icon" href="/img/favicon.ico">
        <script src="/js/jquery.min.js"></script>
        <script src="/js/bootstrap.min.js"></script>
        <script src="/js/bootstrap-dialog.js"></script>
        <script>
            $(document).ready(function () {

                // csrf token
                var csrfToken = $('[name="csrf-param"]').attr('content');

                // add camp
                $('#_add_camp').on('click', function () {
                    $.post('/campaigns/apiCreateCampaign/', {
                        campName: $('#_camp_name').val(),
                        csrfToken: csrfToken
                    }, function (data) {
                        var response = $.parseJSON(data);
                        if (response.status === 'error') {
                            $('#_add_camp_error').html(response.msg).removeClass('hidden');
                        } else if (response.status === 'ok') {
                            $('#_add_camp_modal').modal('hide');
                            document.location.href = '/campaigns/' + response.msg;
                        }
                    });
                });


                // play camp
                $('._play_camp').on('click', function () {
                    $.post('/campaigns/apiPlayCampaign/', {
                        campID: $(this).attr('name'),
                        csrfToken: csrfToken
                    }, function () {
                        location.reload();
                    });
                });


                // stop camp
                $('._stop_camp').on('click', function () {
                    $.post('/campaigns/apiStopCampaign/', {
                        campID: $(this).attr('name'),
                        csrfToken: csrfToken
                    }, function () {
                        location.reload();
                    });
                });


                // remove camp
                $('._remove_camp').on('click', function () {
                    var name = $(this).attr('name');
                    var campID = $(this).attr('data');
                    $('._remove_camp_name').text(name);
                    $('#_remove_camp_modal').modal();
                    $('#_remove_camp_ok').on('click', function () {
                        $.post('/campaigns/apiRemoveCampaign/', {
                            campID: campID,
                            csrfToken: csrfToken
                        }, function () {
                            location.reload();
                        });
                    });
                });


                // tips
                $('[data-toggle="tooltip"]').tooltip({
                    container: 'body',
                    html: true
                });


                // logout
                $('#logout').on('click', function () {
                    $.post('/login/logOut/', {csrfToken: csrfToken}, function () {
                        location.reload();
                    });
                });


            });

        </script>
    </head>
    <body>

        <div class="container-fluid">
            <div class="row">
                <div class="col-xs-12">
                    <?php include_once ('nav.php') ?>

                    <div class="row">
                        <div class="col-xlg-10 col-xlg-offset-1 col-xs-12 col-xs-offset-0">

                            <?php if ($camps): ?>
                                <div class="panel panel-primary">
                                    <div class="panel-heading"><b>캠페인</b></div>
                                    <table class="table table-bordered table-striped">
                                        <tbody>
                                            <?php
                                            $i = 1;
                                            foreach ($camps as $item):
                                                ?>
                                                <tr style="height: 70px;">
                                                    <td style="width: 50px; text-align: center; vertical-align: middle;"><?php echo $i++; ?></td>
                                                    <td style="width: 200px; text-align: center; vertical-align: middle;"><span><b><?php echo $item['camp_name']; ?></b></span> <span style="vertical-align: top;" class="label label-default">id: <?php echo $item['id']; ?></span></td>
                                                    <td style="width: 220px; text-align: center; vertical-align: middle;">

                                                        <span style="font-size:13px" class="label label-primary" data-toggle="tooltip" data-placement="auto" title="<b>나라:</b> <br><?php echo str_replace(',', ', ', $item['camp_geo']); ?>"> <i class="fa fa-globe" aria-hidden="true"></i></span>
                                                        <span style="font-size:13px" class="label label-success" data-toggle="tooltip" data-placement="auto" title="<b>디바이스:</b> <br><?php echo $item['camp_dev_type']; ?>"> <i class="fa fa-desktop" aria-hidden="true"></i></span>
                                                        <span style="font-size:13px" class="label label-info" data-toggle="tooltip" data-placement="auto" title="<b>운영체제:</b> <br><?php echo str_replace(array('unknown_desktop_os', 'unknown_mobile_os', ',', '_'), array('Other desktop os', 'Other mobile os', ', ', ' '), $item['camp_os']); ?>"><i class="fa fa-windows" aria-hidden="true"></i></span>
                                                        <span style="font-size:13px" class="label label-warning" data-toggle="tooltip" data-placement="auto" title="<b>브라우저:</b> <br><?php echo str_replace(array('unknown_desktop_browser', 'unknown_mobile_browser', ',', '_d', '_m'), array('Other desktop browser', 'Other mobile browser', ', ', '', '(mobile)'), $item['camp_browser']); ?>"><i class="fa fa-internet-explorer" aria-hidden="true"></i></span>
                                                        <span style="font-size:13px" class="label label-default" data-toggle="tooltip" data-placement="auto" title="<b>동작시간:</b> <br><?php echo str_replace(',', ', ', $item['camp_active_hours']); ?>"><i class="fa fa-clock-o" aria-hidden="true"></i></span>
                                                        <span style="font-size:13px" class="label label-danger" data-toggle="tooltip" data-placement="auto" title="<b>숨김사이트:</b> <br> <?php echo str_replace(',', ', ', $item['filter_sites']); ?>"><i class="fa fa-filter" aria-hidden="true"></i></span>
                                                    </td>
                                                    <td style="text-align: center; vertical-align: middle;"> 조회수: <b><?php echo $item['camp_views']; ?></b> </td>
                                                    <td style="text-align: center; vertical-align: middle;"> 클릭수: <b><?php echo $item['camp_clicks']; ?></b> </td>
                                                    <td style="text-align: center; vertical-align: middle;"> CTR: <b><?php echo ($item['camp_clicks'] == 0 OR ( $item['camp_views'] == 0 AND $item['camp_clicks'] == 0)) ? 0 : round(($item['camp_clicks'] / $item['camp_views']) * 100, 2); ?></b> </td>
                                                    <td style="width: 420px; text-align: center; vertical-align: middle;">
                                                        <div class="text-center">
                                                            <a class="btn btn-warning btn-sm" href="/campaigns/<?php echo $item['id']; ?>/ads/"> 광고 (<?php echo $item['count_camp_ads']; ?>)</a>
                                                            <div class="btn-group ">
                                                                <button class="_play_camp btn btn-default btn-sm <?php echo $item['camp_status'] == 1 ? 'active' : ''; ?>" name="<?php echo $item['id']; ?>"><i class="fa fa-play" aria-hidden="true"></i> 게시</button>
                                                                <button class="_stop_camp btn btn-default btn-sm <?php echo $item['camp_status'] == 0 ? 'active' : ''; ?>" name="<?php echo $item['id']; ?>"><i class="fa fa-stop" aria-hidden="true"></i> 중단</button>
                                                                <a class="btn btn-default btn-sm" href="/stat/camp/<?php echo $item['id']; ?>"><i class="fa fa-bar-chart" aria-hidden="true"></i> 상태</a>
                                                                <a class="btn btn-default btn-sm" href="/campaigns/<?php echo $item['id']; ?>"><i class="fa fa-pencil" aria-hidden="true"></i> 수정</a>
                                                                <button class="_remove_camp btn btn-danger btn-sm" name="<?php echo $item['camp_name']; ?>" data="<?php echo $item['id']; ?>"><i class="fa fa-trash-o" aria-hidden="true"></i> 삭제</button>
                                                            </div>
                                                        </div>
                                                    </td>
                                                </tr>
                                            <?php endforeach; ?>
                                        </tbody>
                                    </table>
                                </div>
                            <?php endif; ?>

                            <p>
                                <button class="btn btn-success center-block" data-toggle="modal" data-target="#_add_camp_modal"><i class="fa fa-plus" aria-hidden="true"></i> 캠페인 추가 </button>
                            </p>


                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!--start add camp modal-->
        <div class="modal fade" id="_add_camp_modal">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header header-success">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title" ><i class="fa fa-plus-circle" aria-hidden="true"></i> 캠페인 추가</h4>
                    </div>
                    <div class="modal-body">
                        <div id="_add_camp_error" class="alert alert-danger hidden"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i></div>
                        <input id="_camp_name" type="text" class="form-control" placeholder="캠페인 이름">
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default pull-left btn-sm" data-dismiss="modal"> 닫기</button>
                        <button id="_add_camp" type="button" class="btn btn-success btn-sm"><i class="fa fa-check" aria-hidden="true"></i> 생성</button>
                    </div>
                </div>
            </div>
        </div>
        <!--end add camp modal-->

        <!--start remove camp modal-->
        <div class="modal fade" id="_remove_camp_modal" style="display:none">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header header-danger">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title"><i class="fa fa-question-circle" aria-hidden="true"></i> Remove campaign <b class="_remove_camp_name"></b></h4>
                    </div>
                    <div class="modal-body">
                        Remove this campaign? Just deleted all the campaign ads.
                    </div>
                    <div class="modal-footer">
                        <button id="_remove_camp_ok" type="button" class="btn btn-danger btn-sm pull-left"><i class="fa fa-trash-o" aria-hidden="true"></i> Remove campaign</button>
                        <button type="button" class="btn btn-default btn-sm" data-dismiss="modal"> Close</button>
                    </div>
                </div>
            </div>
        </div>
        <!--end remove camp modal-->

    </body>
</html>
