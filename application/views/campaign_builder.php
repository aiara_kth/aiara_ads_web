<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<!doctype html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <title>캠페인- <?php echo $camp['camp_name']; ?></title>
        <meta name="csrf-param" content="<?php echo $csrfToken; ?>">
        <link rel='stylesheet' href='/css/bootstrap.min.css' type='text/css' media='all'>
        <link rel='stylesheet' href='/css/bootstrap-dialog.css' type='text/css' media='all'>
        <link rel='stylesheet' href='/css/bootstrap-select.min.css' type='text/css' media='all'>
        <link rel='stylesheet' href='/css/bootstrap-colorpicker.min.css' type='text/css' media='all'>
        <link rel='stylesheet' href='/css/bootstrap-datepicker3.min.css' type='text/css' media='all'>
        <link rel='stylesheet' href='/css/font-awesome.min.css' type='text/css' media='all'>
        <link rel='stylesheet' href='/css/icons.css' type='text/css' media='all'>
        <link rel='stylesheet' href='/css/style.css' type='text/css' media='all'>
        <link rel="shortcut icon" href="/img/favicon.ico">
        <script src="/js/jquery.min.js"></script>
        <script src="/js/bootstrap.min.js"></script>
        <script src="/js/bootstrap-dialog.js"></script>
        <script src="/js/bootstrap-select.min.js"></script>
        <script src="/js/bootstrap-colorpicker.min.js"></script>
        <script src="/js/bootstrap-datepicker.min.js"></script>
        <script>
            $(document).ready(function () {

                // camp id
                var campID = location.href.split('/')[4];

                // csrf token
                var csrfToken = $('[name="csrf-param"]').attr('content');

                // set inputs values
                $.post('/campaigns/apiGetCampaignDataJson/', {
                    campID: campID,
                    csrfToken: csrfToken
                }, function (response) {
                    var data = $.parseJSON(response);

                    // filter sites list
                    for (var key in data.sites) {
                        $('#_filter_sites').append('<option value="' + data.sites[key].domain.toLowerCase() + '">' + data.sites[key].domain.toLowerCase() + '</option>');
                    }

                    // set filtered sites input
                    $('#_filter_sites > option').each(function () {
                        if (data.filter_sites.split(',').indexOf($(this).val()) != -1) {
                            $(this).prop('selected', true);
                        }
                    });

                    // set active hours input
                    $('#_camp_active_hours > option').each(function () {
                        if (data.camp_active_hours.split(',').indexOf($(this).val()) != -1) {
                            $(this).prop('selected', true);
                        }
                    });

                    // set geo input
                    $('#_camp_geo > option').each(function () {
                        if (data.camp_geo.split(',').indexOf($(this).val()) != -1) {
                            $(this).prop('selected', true);
                        }
                    });

                    // set dev input
                    $('#_camp_dev_type > option').each(function () {
                        if (data.camp_dev_type.split(',').indexOf($(this).val()) != -1) {
                            $(this).prop('selected', true);
                        }
                    });

                    // set OS input
                    $('#_camp_os option').each(function () {
                        if (data.camp_os.split(',').indexOf($(this).val()) != -1) {
                            $(this).prop('selected', true);
                        }
                    });

                    // set browsers input
                    $('#_camp_browser option').each(function () {
                        if (data.camp_browser.split(',').indexOf($(this).val()) != -1) {
                            $(this).prop('selected', true);
                        }
                    });

                    $('.selectpicker').selectpicker('refresh'); // reload inputs
                    $('#_camp_name').val(data.camp_name); // camp name
                    $('#_camp_link').val(data.camp_link); // camp link
                    $('#_camp_date_start').datepicker('update', data.camp_date_start); // camp date start
                    $('#_camp_date_stop').datepicker('update', data.camp_date_stop); // camp date stop
                });


                // date fields settings
                $('#_camp_date_start, #_camp_date_stop').datepicker({
                    todayBtn: "linked",
                    todayHighlight: true,
                    orientation: "auto right",
                    format: "dd-mm-yyyy"
                });


                // save camp
                $('._save_camp').on('click', function () {
                    if ($('#_camp_name').val().length > 0 &&
                            $('#_camp_geo').val() !== null &&
                            $('#_camp_dev_type').val() !== null &&
                            $('#_camp_os').val() !== null &&
                            $('#_camp_browser').val() !== null &&
                            $('#_camp_date_start > input').val().length > 0 &&
                            $('#_camp_date_stop > input').val().length > 0 &&
                            $('#_camp_active_hours').val() !== null) {
                        $.post('/campaigns/apiUpdateCampaign/', {
                            camp_id: campID,
                            camp_name: $('#_camp_name').val(),
                            camp_link: $('#_camp_link').val(),
                            camp_geo: $('#_camp_geo').val().join(','),
                            camp_dev_type: $('#_camp_dev_type').val().join(','),
                            camp_os: $('#_camp_os').val().join(','),
                            camp_browser: $('#_camp_browser').val().join(','),
                            camp_date_start: $('#_camp_date_start > input').val(),
                            camp_date_stop: $('#_camp_date_stop > input').val(),
                            camp_active_hours: $('#_camp_active_hours').val().join(','),
                            filter_sites: $('#_filter_sites').val() ? $('#_filter_sites').val().join(',') : '',
                            csrfToken: csrfToken
                        }, function () {
                            $('#_saved_camp_modal').modal();
                        });
                    } else {
                        $('#_no_data_modal').modal();
                    }
                });


                // remove camp
                $('#_remove_camp_ok').on('click', function () {
                    $.post('/campaigns/apiRemoveCampaign/', {
                        campID: campID,
                        csrfToken: csrfToken
                    }, function () {
                        document.location.href = '/campaigns/';
                    });
                });


                // macroses show, hide
                $('#_show_macroses').on('click', function () {
                    $('#_alert_macroses').toggleClass('hidden');
                    $('#_show_macroses').toggleClass('active');
                });


                // logout
                $('#logout').on('click', function () {
                    $.post('/login/logOut/', {csrfToken: csrfToken}, function () {
                        location.reload();
                    });
                });

            });

        </script>
    </head>
    <body>
        <div class="container-fluid">
            <div class="row">
                <div class="col-xs-12">
                    <?php include_once ('nav.php') ?>
                    <ol class="breadcrumb col-xlg-10 col-xlg-offset-1 col-xs-12 col-xs-offset-0">
                        <li><a href="/campaigns/">캠페인</a></li>
                        <li><b><?php echo $camp['camp_name']; ?></b></li>
                    </ol>
                    <div style="padding:0;" class="col-xlg-10 col-xlg-offset-1 col-xs-12 col-xs-offset-0">
                        <div class="panel panel-primary">
                            <div class="panel-heading"><b>캠페인 설정</b></div>
                            <div class="panel-body">
                                <div class="col-xs-6">
                                    <br>
                                    <div class="input-group input-group-sm form-group">
                                        <span style="min-width:170px;" class="input-group-addon">켐페인 이름</span>
                                        <input id="_camp_name" type="text" class="form-control" placeholder="켐페인 이름" value="">
                                    </div>
                                    <br>
                                    <div class="input-group form-group input-group-sm ">
                                        <span style="min-width:170px;" class="input-group-addon"><i class="fa fa-clock-o" aria-hidden="true"></i> 동작시간</span>
                                        <select id="_camp_active_hours" class="selectpicker" data-style="btn-default btn-sm" multiple data-width="100%" data-size="12" data-actions-box="true" data-selected-text-format="count > 0" >
                                            <option value="00">00 - 01</option>
                                            <option value="01">01 - 02</option>
                                            <option value="02">02 - 03</option>
                                            <option value="03">03 - 04</option>
                                            <option value="04">04 - 05</option>
                                            <option value="05">05 - 06</option>
                                            <option value="06">06 - 07</option>
                                            <option value="07">07 - 00</option>
                                            <option value="08">08 - 09</option>
                                            <option value="09">09 - 10</option>
                                            <option value="10">10 - 11</option>
                                            <option value="11">11 - 12</option>
                                            <option value="12">12 - 13</option>
                                            <option value="13">13 - 14</option>
                                            <option value="14">14 - 15</option>
                                            <option value="15">15 - 16</option>
                                            <option value="16">16 - 17</option>
                                            <option value="17">17 - 18</option>
                                            <option value="18">18 - 19</option>
                                            <option value="19">19 - 20</option>
                                            <option value="20">20 - 21</option>
                                            <option value="21">21 - 22</option>
                                            <option value="22">22 - 23</option>
                                            <option value="23">23 - 00</option>
                                        </select>
                                    </div>
                                    <br>
                                    <div id="_camp_date_start" class="input-group date form-group input-group-sm " data-provide="datepicker">
                                        <div style="min-width:170px;" class="input-group-addon"><i class="fa fa-calendar" aria-hidden="true"></i> 캠페인 시작일</div>
                                        <input type="text" class="form-control" placeholder="Campaign start date DD-MM-YYYY">
                                    </div>
                                    <br>
                                    <div id="_camp_date_stop" class="input-group date form-group input-group-sm" data-provide="datepicker">
                                        <div style="min-width:170px;" class="input-group-addon"> <i class="fa fa-calendar" aria-hidden="true"></i> 캠페인 종료일</div>
                                        <input type="text" class="form-control" placeholder="Campaign stop date DD-MM-YYYY">
                                    </div>
                                    <br>
                                </div>
                                <div class="col-xs-6">
                                    <br>
                                    <div class="input-group form-group">
                                        <span style="min-width:170px;" class="input-group-addon"><i class="fa fa-globe" aria-hidden="true"></i> 지역 타겟팅</span>
                                        <select id="_camp_geo" class="selectpicker" data-style="btn-default btn-sm" multiple data-width="500px" data-actions-box="true" data-live-search="true" data-size="20" data-selected-text-format="count > 0">
                                            <option data-subtext="AF" value="AFG" data-icon="ii af">Afghanistan</option>
                                            <option data-subtext="AX" value="ALA" data-icon="ii ax">Aland Islands</option>
                                            <option data-subtext="AL" value="ALB" data-icon="ii al">Albania</option>
                                            <option data-subtext="DZ" value="DZA" data-icon="ii dz">Algeria</option>
                                            <option data-subtext="AS" value="ASM" data-icon="ii as">American Samoa</option>
                                            <option data-subtext="AD" value="_AND" data-icon="ii ad">Andorra</option>
                                            <option data-subtext="AO" value="AGO" data-icon="ii ao">Angola</option>
                                            <option data-subtext="AI" value="AIA" data-icon="ii ai">Anguilla</option>
                                            <option data-subtext="AQ" value="ATA" data-icon="ii aq">Antarctica</option>
                                            <option data-subtext="AG" value="ATG" data-icon="ii ag">Antigua and Barbuda</option>
                                            <option data-subtext="AR" value="ARG" data-icon="ii ar">Argentina</option>
                                            <option data-subtext="AM" value="ARM" data-icon="ii am">Armenia</option>
                                            <option data-subtext="AW" value="ABW" data-icon="ii aw">Aruba</option>
                                            <option data-subtext="AU" value="AUS" data-icon="ii au">Australia</option>
                                            <option data-subtext="AT" value="AUT" data-icon="ii at">Austria</option>
                                            <option data-subtext="AZ" value="AZE" data-icon="ii az">Azerbaijan</option>
                                            <option data-subtext="BS" value="BHS" data-icon="ii bs">Bahamas</option>
                                            <option data-subtext="BH" value="BHR" data-icon="ii bh">Bahrain</option>
                                            <option data-subtext="BD" value="BGD" data-icon="ii bd">Bangladesh</option>
                                            <option data-subtext="BB" value="BRB" data-icon="ii bb">Barbados</option>
                                            <option data-subtext="BY" value="BLR" data-icon="ii by">Belarus</option>
                                            <option data-subtext="BE" value="BEL" data-icon="ii be">Belgium</option>
                                            <option data-subtext="BZ" value="BLZ" data-icon="ii bz">Belize</option>
                                            <option data-subtext="BJ" value="BEN" data-icon="ii bj">Benin</option>
                                            <option data-subtext="BM" value="BMU" data-icon="ii bm">Bermuda</option>
                                            <option data-subtext="BT" value="BTN" data-icon="ii bt">Bhutan</option>
                                            <option data-subtext="BO" value="BOL" data-icon="ii bo">Bolivia (Plurinational State of)</option>
                                            <option data-subtext="BQ" value="BES" data-icon="ii bq">Bonaire</option>
                                            <option data-subtext="BA" value="BIH" data-icon="ii ba">Bosnia and Herzegovina</option>
                                            <option data-subtext="BW" value="BWA" data-icon="ii bw">Botswana</option>
                                            <option data-subtext="BV" value="BVT" data-icon="ii bv">Bouvet Island</option>
                                            <option data-subtext="BR" value="BRA" data-icon="ii br">Brazil</option>
                                            <option data-subtext="IO" value="IOT" data-icon="ii io">British Indian Ocean Territory</option>
                                            <option data-subtext="BN" value="BRN" data-icon="ii bn">Brunei Darussalam</option>
                                            <option data-subtext="BG" value="BGR" data-icon="ii bg">Bulgaria</option>
                                            <option data-subtext="BF" value="BFA" data-icon="ii bf">Burkina Faso</option>
                                            <option data-subtext="BI" value="BDI" data-icon="ii bi">Burundi</option>
                                            <option data-subtext="KH" value="KHM" data-icon="ii kh">Cambodia</option>
                                            <option data-subtext="CM" value="CMR" data-icon="ii cm">Cameroon</option>
                                            <option data-subtext="CA" value="CAN" data-icon="ii ca">Canada</option>
                                            <option data-subtext="CV" value="CPV" data-icon="ii cv">Cabo Verde</option>
                                            <option data-subtext="KY" value="CYM" data-icon="ii ky">Cayman Islands</option>
                                            <option data-subtext="CF" value="CAF" data-icon="ii cf">Central African Republic</option>
                                            <option data-subtext="TD" value="TCD" data-icon="ii td">Chad</option>
                                            <option data-subtext="CL" value="CHL" data-icon="ii cl">Chile</option>
                                            <option data-subtext="CN" value="CHN" data-icon="ii cn">China</option>
                                            <option data-subtext="CX" value="CXR" data-icon="ii cx">Christmas Island</option>
                                            <option data-subtext="CC" value="CCK" data-icon="ii cc">Cocos (Keeling) Islands</option>
                                            <option data-subtext="CO" value="COL" data-icon="ii co">Colombia</option>
                                            <option data-subtext="KM" value="COM" data-icon="ii km">Comoros</option>
                                            <option data-subtext="CG" value="COG" data-icon="ii cg">Congo</option>
                                            <option data-subtext="CD" value="COD" data-icon="ii cd">Congo (Democratic Republic of the)</option>
                                            <option data-subtext="CK" value="COK" data-icon="ii ck">Cook Islands</option>
                                            <option data-subtext="CR" value="CRI" data-icon="ii cr">Costa Rica</option>
                                            <option data-subtext="CI" value="CIV" data-icon="ii ci">Cote d`Ivoire</option>
                                            <option data-subtext="HR" value="HRV" data-icon="ii hr">Croatia</option>
                                            <option data-subtext="CU" value="CUB" data-icon="ii cu">Cuba</option>
                                            <option data-subtext="CW" value="CUW" data-icon="ii cw">Curacao</option>
                                            <option data-subtext="CY" value="CYP" data-icon="ii cy">Cyprus</option>
                                            <option data-subtext="CZ" value="CZE" data-icon="ii cz">Czech Republic</option>
                                            <option data-subtext="DK" value="DNK" data-icon="ii dk">Denmark</option>
                                            <option data-subtext="DJ" value="DJI" data-icon="ii dj">Djibouti</option>
                                            <option data-subtext="DM" value="DMA" data-icon="ii dm">Dominica</option>
                                            <option data-subtext="DO" value="DOM" data-icon="ii do">Dominican Republic</option>
                                            <option data-subtext="EC" value="ECU" data-icon="ii ec">Ecuador</option>
                                            <option data-subtext="EG" value="EGY" data-icon="ii eg">Egypt</option>
                                            <option data-subtext="SV" value="SLV" data-icon="ii sv">El Salvador</option>
                                            <option data-subtext="GQ" value="GNQ" data-icon="ii gq">Equatorial Guinea</option>
                                            <option data-subtext="ER" value="ERI" data-icon="ii er">Eritrea</option>
                                            <option data-subtext="EE" value="EST" data-icon="ii ee">Estonia</option>
                                            <option data-subtext="ET" value="ETH" data-icon="ii et">Ethiopia</option>
                                            <option data-subtext="FK" value="FLK" data-icon="ii fk">Falkland Islands (Malvinas)</option>
                                            <option data-subtext="FO" value="FRO" data-icon="ii fo">Faroe Islands</option>
                                            <option data-subtext="FJ" value="FJI" data-icon="ii fj">Fiji</option>
                                            <option data-subtext="FI" value="FIN" data-icon="ii fi">Finland</option>
                                            <option data-subtext="FR" value="FRA" data-icon="ii fr">France</option>
                                            <option data-subtext="GF" value="GUF" data-icon="ii gf">French Guiana</option>
                                            <option data-subtext="PF" value="PYF" data-icon="ii pf">French Polynesia</option>
                                            <option data-subtext="TF" value="ATF" data-icon="ii tf">French Southern Territories</option>
                                            <option data-subtext="GA" value="GAB" data-icon="ii ga">Gabon</option>
                                            <option data-subtext="GM" value="GMB" data-icon="ii gm">Gambia</option>
                                            <option data-subtext="GE" value="GEO" data-icon="ii ge">Georgia</option>
                                            <option data-subtext="DE" value="DEU" data-icon="ii de">Germany</option>
                                            <option data-subtext="GH" value="GHA" data-icon="ii gh">Ghana</option>
                                            <option data-subtext="GI" value="GIB" data-icon="ii gi">Gibraltar</option>
                                            <option data-subtext="GR" value="GRC" data-icon="ii gr">Greece</option>
                                            <option data-subtext="GL" value="GRL" data-icon="ii gl">Greenland</option>
                                            <option data-subtext="GD" value="GRD" data-icon="ii gd">Grenada</option>
                                            <option data-subtext="GP" value="GLP" data-icon="ii gp">Guadeloupe</option>
                                            <option data-subtext="GU" value="GUM" data-icon="ii gu">Guam</option>
                                            <option data-subtext="GT" value="GTM" data-icon="ii gt">Guatemala</option>
                                            <option data-subtext="GG" value="GGY" data-icon="ii gg">Guernsey</option>
                                            <option data-subtext="GN" value="GIN" data-icon="ii gn">Guinea</option>
                                            <option data-subtext="GW" value="GNB" data-icon="ii gw">Guinea-Bissau</option>
                                            <option data-subtext="GY" value="GUY" data-icon="ii gy">Guyana</option>
                                            <option data-subtext="HT" value="HTI" data-icon="ii ht">Haiti</option>
                                            <option data-subtext="HM" value="HMD" data-icon="ii hm">Heard Island and McDonald Islands</option>
                                            <option data-subtext="VA" value="VAT" data-icon="ii va">Holy See</option>
                                            <option data-subtext="HN" value="HND" data-icon="ii hn">Honduras</option>
                                            <option data-subtext="HK" value="HKG" data-icon="ii hk">Hong Kong</option>
                                            <option data-subtext="HU" value="HUN" data-icon="ii hu">Hungary</option>
                                            <option data-subtext="IS" value="ISL" data-icon="ii is">Iceland</option>
                                            <option data-subtext="IN" value="IND" data-icon="ii _in">India</option>
                                            <option data-subtext="ID" value="IDN" data-icon="ii id">Indonesia</option>
                                            <option data-subtext="IR" value="IRN" data-icon="ii ir">Iran (Islamic Republic of)</option>
                                            <option data-subtext="IQ" value="IRQ" data-icon="ii iq">Iraq</option>
                                            <option data-subtext="IE" value="IRL" data-icon="ii ie">Ireland</option>
                                            <option data-subtext="IM" value="IMN" data-icon="ii im">Isle of Man</option>
                                            <option data-subtext="IL" value="ISR" data-icon="ii il">Israel</option>
                                            <option data-subtext="IT" value="ITA" data-icon="ii it">Italy</option>
                                            <option data-subtext="JM" value="JAM" data-icon="ii jm">Jamaica</option>
                                            <option data-subtext="JP" value="JPN" data-icon="ii jp">Japan</option>
                                            <option data-subtext="JE" value="JEY" data-icon="ii je">Jersey</option>
                                            <option data-subtext="JO" value="JOR" data-icon="ii jo">Jordan</option>
                                            <option data-subtext="KZ" value="KAZ" data-icon="ii kz">Kazakhstan</option>
                                            <option data-subtext="KE" value="KEN" data-icon="ii ke">Kenya</option>
                                            <option data-subtext="KI" value="KIR" data-icon="ii ki">Kiribati</option>
                                            <option data-subtext="KP" value="PRK" data-icon="ii kp">Korea (Democratic People`s Republic of)</option>
                                            <option data-subtext="KR" value="KOR" data-icon="ii kr">Korea (Republic of)</option>
                                            <option data-subtext="KW" value="KWT" data-icon="ii kw">Kuwait</option>
                                            <option data-subtext="KG" value="KGZ" data-icon="ii kg">Kyrgyzstan</option>
                                            <option data-subtext="LA" value="LAO" data-icon="ii la">Lao People`s Democratic Republic</option>
                                            <option data-subtext="LV" value="LVA" data-icon="ii lv">Latvia</option>
                                            <option data-subtext="LB" value="LBN" data-icon="ii lb">Lebanon</option>
                                            <option data-subtext="LS" value="LSO" data-icon="ii ls">Lesotho</option>
                                            <option data-subtext="LR" value="LBR" data-icon="ii lr">Liberia</option>
                                            <option data-subtext="LY" value="LBY" data-icon="ii ly">Libya</option>
                                            <option data-subtext="LI" value="LIE" data-icon="ii li">Liechtenstein</option>
                                            <option data-subtext="LT" value="LTU" data-icon="ii lt">Lithuania</option>
                                            <option data-subtext="LU" value="LUX" data-icon="ii lu">Luxembourg</option>
                                            <option data-subtext="MO" value="MAC" data-icon="ii mo">Macao</option>
                                            <option data-subtext="MK" value="MKD" data-icon="ii mk">Macedonia</option>
                                            <option data-subtext="MG" value="MDG" data-icon="ii mg">Madagascar</option>
                                            <option data-subtext="MW" value="MWI" data-icon="ii mw">Malawi</option>
                                            <option data-subtext="MY" value="MYS" data-icon="ii my">Malaysia</option>
                                            <option data-subtext="MV" value="MDV" data-icon="ii mv">Maldives</option>
                                            <option data-subtext="ML" value="MLI" data-icon="ii ml">Mali</option>
                                            <option data-subtext="MT" value="MLT" data-icon="ii mt">Malta</option>
                                            <option data-subtext="MH" value="MHL" data-icon="ii mh">Marshall Islands</option>
                                            <option data-subtext="MQ" value="MTQ" data-icon="ii mq">Martinique</option>
                                            <option data-subtext="MR" value="MRT" data-icon="ii mr">Mauritania</option>
                                            <option data-subtext="MU" value="MUS" data-icon="ii mu">Mauritius</option>
                                            <option data-subtext="YT" value="MYT" data-icon="ii yt">Mayotte</option>
                                            <option data-subtext="MX" value="MEX" data-icon="ii mx">Mexico</option>
                                            <option data-subtext="FM" value="FSM" data-icon="ii fm">Micronesia (Federated States of)</option>
                                            <option data-subtext="MD" value="MDA" data-icon="ii md">Moldova (Republic of)</option>
                                            <option data-subtext="MC" value="MCO" data-icon="ii mc">Monaco</option>
                                            <option data-subtext="MN" value="MNG" data-icon="ii mn">Mongolia</option>
                                            <option data-subtext="ME" value="MNE" data-icon="ii me">Montenegro</option>
                                            <option data-subtext="MS" value="MSR" data-icon="ii ms">Montserrat</option>
                                            <option data-subtext="MA" value="MAR" data-icon="ii ma">Morocco</option>
                                            <option data-subtext="MZ" value="MOZ" data-icon="ii mz">Mozambique</option>
                                            <option data-subtext="MM" value="MMR" data-icon="ii mm">Myanmar</option>
                                            <option data-subtext="NA" value="NAM" data-icon="ii na">Namibia</option>
                                            <option data-subtext="NR" value="NRU" data-icon="ii nr">Nauru</option>
                                            <option data-subtext="NP" value="NPL" data-icon="ii np">Nepal</option>
                                            <option data-subtext="NL" value="NLD" data-icon="ii nl">Netherlands</option>
                                            <option data-subtext="NC" value="NCL" data-icon="ii nc">New Caledonia</option>
                                            <option data-subtext="NZ" value="NZL" data-icon="ii nz">New Zealand</option>
                                            <option data-subtext="NI" value="NIC" data-icon="ii ni">Nicaragua</option>
                                            <option data-subtext="NE" value="NER" data-icon="ii ne">Niger</option>
                                            <option data-subtext="NG" value="NGA" data-icon="ii ng">Nigeria</option>
                                            <option data-subtext="NU" value="NIU" data-icon="ii nu">Niue</option>
                                            <option data-subtext="NF" value="NFK" data-icon="ii nf">Norfolk Island</option>
                                            <option data-subtext="MP" value="MNP" data-icon="ii mp">Northern Mariana Islands</option>
                                            <option data-subtext="NO" value="NOR" data-icon="ii no">Norway</option>
                                            <option data-subtext="OM" value="OMN" data-icon="ii om">Oman</option>
                                            <option data-subtext="PK" value="PAK" data-icon="ii pk">Pakistan</option>
                                            <option data-subtext="PW" value="PLW" data-icon="ii pw">Palau</option>
                                            <option data-subtext="PS" value="PSE" data-icon="ii ps">Palestine</option>
                                            <option data-subtext="PA" value="PAN" data-icon="ii pa">Panama</option>
                                            <option data-subtext="PG" value="PNG" data-icon="ii pg">Papua New Guinea</option>
                                            <option data-subtext="PY" value="PRY" data-icon="ii py">Paraguay</option>
                                            <option data-subtext="PE" value="PER" data-icon="ii pe">Peru</option>
                                            <option data-subtext="PH" value="PHL" data-icon="ii ph">Philippines</option>
                                            <option data-subtext="PN" value="PCN" data-icon="ii pn">Pitcairn</option>
                                            <option data-subtext="PL" value="POL" data-icon="ii pl">Poland</option>
                                            <option data-subtext="PT" value="PRT" data-icon="ii pt">Portugal</option>
                                            <option data-subtext="PR" value="PRI" data-icon="ii pr">Puerto Rico</option>
                                            <option data-subtext="QA" value="QAT" data-icon="ii qa">Qatar</option>
                                            <option data-subtext="RE" value="REU" data-icon="ii re">Reunion</option>
                                            <option data-subtext="RO" value="ROU" data-icon="ii ro">Romania</option>
                                            <option data-subtext="RU" value="RUS" data-icon="ii ru">Russian Federation</option>
                                            <option data-subtext="RW" value="RWA" data-icon="ii rw">Rwanda</option>
                                            <option data-subtext="BL" value="BLM" data-icon="ii bl">Saint Barthelemy</option>
                                            <option data-subtext="SH" value="SHN" data-icon="ii sh">Saint Helena</option>
                                            <option data-subtext="KN" value="KNA" data-icon="ii kn">Saint Kitts and Nevis</option>
                                            <option data-subtext="LC" value="LCA" data-icon="ii lc">Saint Lucia</option>
                                            <option data-subtext="MF" value="MAF" data-icon="ii mf">Saint Martin (French part)</option>
                                            <option data-subtext="PM" value="SPM" data-icon="ii pm">Saint Pierre and Miquelon</option>
                                            <option data-subtext="VC" value="VCT" data-icon="ii vc">Saint Vincent and the Grenadines</option>
                                            <option data-subtext="WS" value="WSM" data-icon="ii ws">Samoa</option>
                                            <option data-subtext="SM" value="SMR" data-icon="ii sm">San Marino</option>
                                            <option data-subtext="ST" value="STP" data-icon="ii st">Sao Tome and Principe</option>
                                            <option data-subtext="SA" value="SAU" data-icon="ii sa">Saudi Arabia</option>
                                            <option data-subtext="SN" value="SEN" data-icon="ii sn">Senegal</option>
                                            <option data-subtext="RS" value="SRB" data-icon="ii rs">Serbia</option>
                                            <option data-subtext="SC" value="SYC" data-icon="ii sc">Seychelles</option>
                                            <option data-subtext="SL" value="SLE" data-icon="ii sl">Sierra Leone</option>
                                            <option data-subtext="SG" value="SGP" data-icon="ii sg">Singapore</option>
                                            <option data-subtext="SX" value="SXM" data-icon="ii sx">Sint Maarten (Dutch part)</option>
                                            <option data-subtext="SK" value="SVK" data-icon="ii sk">Slovakia</option>
                                            <option data-subtext="SI" value="SVN" data-icon="ii si">Slovenia</option>
                                            <option data-subtext="SB" value="SLB" data-icon="ii sb">Solomon Islands</option>
                                            <option data-subtext="SO" value="SOM" data-icon="ii so">Somalia</option>
                                            <option data-subtext="ZA" value="ZAF" data-icon="ii za">South Africa</option>
                                            <option data-subtext="GS" value="SGS" data-icon="ii gs">South Georgia and the South Sandwich Islands</option>
                                            <option data-subtext="SS" value="SSD" data-icon="ii ss">South Sudan</option>
                                            <option data-subtext="ES" value="ESP" data-icon="ii es">Spain</option>
                                            <option data-subtext="LK" value="LKA" data-icon="ii lk">Sri Lanka</option>
                                            <option data-subtext="SD" value="SDN" data-icon="ii sd">Sudan</option>
                                            <option data-subtext="SR" value="SUR" data-icon="ii sr">Suriname</option>
                                            <option data-subtext="SJ" value="SJM" data-icon="ii sj">Svalbard and Jan Mayen</option>
                                            <option data-subtext="SZ" value="SWZ" data-icon="ii sz">Swaziland</option>
                                            <option data-subtext="SE" value="SWE" data-icon="ii se">Sweden</option>
                                            <option data-subtext="CH" value="CHE" data-icon="ii ch">Switzerland</option>
                                            <option data-subtext="SY" value="SYR" data-icon="ii sy">Syrian Arab Republic</option>
                                            <option data-subtext="TW" value="TWN" data-icon="ii tw">Taiwan (Province of China)</option>
                                            <option data-subtext="TJ" value="TJK" data-icon="ii tj">Tajikistan</option>
                                            <option data-subtext="TZ" value="TZA" data-icon="ii tz">Tanzania</option>
                                            <option data-subtext="TH" value="THA" data-icon="ii th">Thailand</option>
                                            <option data-subtext="TL" value="TLS" data-icon="ii tl">Timor-Leste</option>
                                            <option data-subtext="TG" value="TGO" data-icon="ii tg">Togo</option>
                                            <option data-subtext="TK" value="TKL" data-icon="ii tk">Tokelau</option>
                                            <option data-subtext="TO" value="TON" data-icon="ii to">Tonga</option>
                                            <option data-subtext="TT" value="TTO" data-icon="ii tt">Trinidad and Tobago</option>
                                            <option data-subtext="TN" value="TUN" data-icon="ii tn">Tunisia</option>
                                            <option data-subtext="TR" value="TUR" data-icon="ii tr">Turkey</option>
                                            <option data-subtext="TM" value="TKM" data-icon="ii tm">Turkmenistan</option>
                                            <option data-subtext="TC" value="TCA" data-icon="ii tc">Turks and Caicos Islands</option>
                                            <option data-subtext="TV" value="TUV" data-icon="ii tv">Tuvalu</option>
                                            <option data-subtext="UG" value="UGA" data-icon="ii ug">Uganda</option>
                                            <option data-subtext="UA" value="UKR" data-icon="ii ua">Ukraine</option>
                                            <option data-subtext="AE" value="ARE" data-icon="ii ae">United Arab Emirates</option>
                                            <option data-subtext="GB" value="GBR" data-icon="ii gb">United Kingdom of Great Britain and Northern Ireland</option>
                                            <option data-subtext="US" value="USA" data-icon="ii us">United States of America</option>
                                            <option data-subtext="UM" value="UMI" data-icon="ii um">United States Minor Outlying Islands</option>
                                            <option data-subtext="UY" value="URY" data-icon="ii uy">Uruguay</option>
                                            <option data-subtext="UZ" value="UZB" data-icon="ii uz">Uzbekistan</option>
                                            <option data-subtext="VU" value="VUT" data-icon="ii vu">Vanuatu</option>
                                            <option data-subtext="VE" value="VEN" data-icon="ii ve">Venezuela (Bolivarian Republic of)</option>
                                            <option data-subtext="VN" value="VNM" data-icon="ii vn">Viet Nam</option>
                                            <option data-subtext="VG" value="VGB" data-icon="ii vg">Virgin Islands (British)</option>
                                            <option data-subtext="VI" value="VIR" data-icon="ii vi">Virgin Islands (U.S.)</option>
                                            <option data-subtext="WF" value="WLF" data-icon="ii wf">Wallis and Futuna</option>
                                            <option data-subtext="EH" value="ESH" data-icon="ii eh">Western Sahara</option>
                                            <option data-subtext="YE" value="YEM" data-icon="ii ye">Yemen</option>
                                            <option data-subtext="ZM" value="ZMB" data-icon="ii zm">Zambia</option>
                                            <option data-subtext="ZW" value="ZWE" data-icon="ii zw">Zimbabwe</option>
                                        </select>
                                    </div>
                                    <br>
                                    <div class="input-group form-group">
                                        <span style="min-width:170px;" class="input-group-addon"><i class="fa fa-desktop" aria-hidden="true"></i> 디바이스 타겟팅</span>
                                        <select id="_camp_dev_type" class="selectpicker" data-style="btn-default btn-sm" multiple data-width="100%" data-selected-text-format="count > 0">
                                            <option data-icon="ii computer" value="Computer">Computer</option>
                                            <option data-icon="ii tablet" value="Tablet">Tablet</option>
                                            <option data-icon="ii mobile" value="Mobile">Mobile</option>
                                        </select>
                                    </div>
                                    <br>
                                    <div class="input-group form-group">
                                        <span style="min-width:170px;" class="input-group-addon"><i class="fa fa-windows" aria-hidden="true"></i> 운영체제 타겟팅</span>
                                        <select id="_camp_os" class="selectpicker" data-style="btn-default btn-sm" multiple data-width="100%" data-size="12" data-actions-box="true" data-selected-text-format="count > 0" >
                                            <optgroup label="Desktop OS">
                                                <option data-icon="ii windows_10" value="Windows_10">Windows 10</option>
                                                <option data-icon="ii windows_8" value="Windows_8_1">Windows 8.1</option>
                                                <option data-icon="ii windows_8" value="Windows_8">Windows 8</option>
                                                <option data-icon="ii windows_7" value="Windows_7">Windows 7</option>
                                                <option data-icon="ii windows_vista" value="Windows_Vista">Windows Vista</option>
                                                <option data-icon="ii windows_xp" value="Windows_XP">Windows XP</option>
                                                <option data-icon="ii apple" value="Mac_OS">Mac OS</option>
                                                <option data-icon="ii ubuntu" value="Ubuntu">Ubuntu</option>
                                                <option data-icon="ii linux" value="Linux">Linux</option>
                                                <option data-icon="ii computer" value="unknown_desktop_os">Other desktop OS</option>
                                            </optgroup>
                                            <optgroup label="Mobile OS">
                                                <option data-icon="ii apple" value="iOS">iOS</option>
                                                <option data-icon="ii android" value="Android">Android</option>
                                                <option data-icon="ii windows_phone" value="Windows_Phone">Windows Phone</option>
                                                <option data-icon="ii symbian" value="Symbian">Symbian</option>
                                                <option data-icon="ii black_berry" value="Black_Berry">Black Berry</option>
                                                <option data-icon="ii windows_xp" value="Windows_Mobile">Windows Mobile</option>
                                                <option data-icon="ii mobile" value="unknown_mobile_os">Other mobile OS</option>
                                            </optgroup>

                                        </select>
                                    </div>
                                    <br>
                                    <div class="input-group form-group">
                                        <span style="min-width:170px;" class="input-group-addon"><i class="fa fa-internet-explorer" aria-hidden="true"></i> 브라우저 타케팅</span>
                                        <select id="_camp_browser" class="selectpicker" data-style="btn-default btn-sm" multiple data-width="100%" data-actions-box="true" data-size="12" data-selected-text-format="count > 0" >
                                            <optgroup label="Desktop browsers">
                                                <option data-icon="ii chrome" value="Chrome_d">Chrome</option>
                                                <option data-icon="ii firefox" value="Firefox_d">Firefox</option>
                                                <option data-icon="ii opera" value="Opera_d">Opera</option>
                                                <option data-icon="ii iexplorer" value="IE_d">IE</option>
                                                <option data-icon="ii edge" value="Edge_d">Edge</option>
                                                <option data-icon="ii maxthon" value="Maxthon_d">Maxthon</option>
                                                <option data-icon="ii safari" value="Safari_d">Safari</option>
                                                <option data-icon="ii computer" value="unknown_desktop_browser">Other desktop browsers</option>
                                            </optgroup>
                                            <optgroup label="Mobile browsers">
                                                <option data-icon="ii chrome" value="Chrome_m">Chrome </option>
                                                <option data-icon="ii android_browser" value="Android_m">Android browser</option>
                                                <option data-icon="ii opera" value="Opera_m">Opera</option>
                                                <option data-icon="ii dolphin" value="Dolphin_m">Dolphin</option>
                                                <option data-icon="ii firefox" value="Firefox_m">Firefox</option>
                                                <option data-icon="ii uc_browser" value="UCBrowser_m">UCBrowser</option>
                                                <option data-icon="ii puffin" value="Puffin_m">Puffin</option>
                                                <option data-icon="ii safari" value="Safari_m">Safari</option>
                                                <option data-icon="ii edge" value="Edge_m">Edge</option>
                                                <option data-icon="ii iexplorer" value="IE_m">IE</option>
                                                <option data-icon="ii mobile" value="unknown_mobile_browser">Other mobile browsers</option>
                                            </optgroup>
                                        </select>
                                    </div>
                                    <br>
                                </div>
                                <div class="col-xs-12">
                                    <div class="input-group input-group-sm form-group">
                                        <span style="min-width:170px;" class="input-group-addon"><i class="fa fa-link" aria-hidden="true"></i> 켐페인 링크</span>
                                        <input id="_camp_link" type="text" class="form-control" placeholder="http://yourdomain.com/?campaign=[CAMP_ID]&block=[BLOCK_ID]&ad=[AD_ID]&site=[SITE_ID]&date=[TIME_CLIK]" value="">
                                    </div>
                                    <br>
                                    <div class="input-group form-group">
                                        <span style="min-width:170px;" class="input-group-addon"> <b><?php echo $camp['camp_name']; ?></b> 캠페인를 <b>숨김처리한</b> 사이트 :</span>
                                        <select id="_filter_sites" class="selectpicker" data-style="btn-default btn-sm" multiple data-width="100%" data-size="12" data-selected-text-format="count > 3" >
                                        </select>
                                    </div>
                                    <div id="_alert_macroses" class="alert alert-success alert-dismissable hidden" style="margin:10px 0 0 0;">
                                        <b>링크에서 다음 매크로를 사용할 수 있습니다:</b><br />
                                        <b>[CAMP_ID]</b> - 캠페인 id<br/>
                                        <b>[BLOCK_ID]</b> - 블록 id<br/>
                                        <b>[AD_ID]</b> - 광고 id<br/>
                                        <b>[SITE_ID]</b> - 사이트 id<br/>
                                        <b>[TIME_CLIK]</b> - 클릭날짜포맷 [<b>YYYY-MM-DD_HH-MM-SS</b>] (예: 2017-06-15_10-35-15)
                                    </div>
                                </div>
                            </div>
                            <div class="panel-footer">
                                <button class="btn btn-warning btn-sm" data-toggle="modal" data-target="#_remove_camp_modal"><i class="fa fa-trash-o" aria-hidden="true"></i> 캠패인 삭제</button>
                                <div class="pull-right">
                                    <button id="_show_macroses" class="btn btn-sm btn-default"> <i class="fa fa-info-circle" aria-hidden="true"></i> </button>
                                    <a href="/campaigns/<?php echo $camp['id']; ?>/ads/" class="btn btn-default btn-sm" ><i class="fa fa-arrow-up" aria-hidden="true"></i> 광고리스트로 이동</a>
                                    <button class="_save_camp btn btn-danger btn-sm" ><i class="fa fa-check" aria-hidden="true"></i> 캠페인 저장</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="modal fade" id="_remove_camp_modal">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header header-danger">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title"><i class="fa fa-question-circle" aria-hidden="true"></i> "<b><?php echo $camp['camp_name']; ?></b>" 캠패인 삭제</h4>
                    </div>
                    <div class="modal-body">
                        <b><?php echo $camp['camp_name']; ?></b> 캠페인을 삭제하시겠습니까?<br/>
                        포함된 광고도 같이 삭제처리가 됩니다.
                    </div>
                    <div class="modal-footer">
                        <button type="button" id="_remove_camp_ok" type="button" class="btn btn-warning btn-sm pull-left"><i class="fa fa-trash-o" aria-hidden="true"></i> 캠패인 삭제</button>
                        <button type="button" class="btn btn-default btn-sm" data-dismiss="modal"> 닫기</button>
                    </div>
                </div>
            </div>
        </div>

        <div class="modal fade" id="_no_data_modal" style="display:none;">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header header-danger">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title"><i class="fa fa-exclamation-circle" aria-hidden="true"></i> 입력폼에서 빈필드를 발견했습니다!</h4>
                    </div>
                    <div class="modal-body">
                       저장/수정하시라면 모든폼를 입력해야합니다!
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default btn-sm" data-dismiss="modal"> 닫기</button>
                    </div>
                </div>
            </div>
        </div>

        <div class="modal fade" id="_saved_camp_modal" style="display:none;">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header header-success">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title"><i class="fa fa-check-circle-o" aria-hidden="true"></i> 캠페인 저장(수정) 되었습니다.</h4>
                    </div>
                    <div class="modal-body">
                        캠페인 설정값이 문제없이 저장(수정)되었습니다.
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default btn-sm" data-dismiss="modal"> 닫기</button>
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>
