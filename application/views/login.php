<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<!doctype html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <title>Login</title>
        <meta name="csrf-param" content="<?php echo $csrfToken; ?>">
        <link rel='stylesheet' href='/css/bootstrap.min.css' type='text/css' media='all'>
        <link rel='stylesheet' href='/css/bootstrap-dialog.css' type='text/css' media='all'>
        <link rel='stylesheet' href='/css/bootstrap-select.min.css' type='text/css' media='all'>
        <link rel='stylesheet' href='/css/bootstrap-colorpicker.min.css' type='text/css' media='all'>
        <link rel='stylesheet' href='/css/font-awesome.min.css' type='text/css' media='all'>
        <link rel='stylesheet' href='/css/style.css' type='text/css' media='all'>
        <link rel="shortcut icon" href="/img/favicon.ico">
        <script src="/js/jquery.min.js"></script>
        <script src="/js/bootstrap.min.js"></script>
        <script src="/js/bootstrap-dialog.js"></script>
        <script src="/js/bootstrap-select.min.js"></script>
        <script src="/js/bootstrap-colorpicker.min.js"></script>
        <script src="/js/clipboard.min.js"></script>
        <script>
            $(document).ready(function () {

                // csrf token
                var csrfToken = $('[name="csrf-param"]').attr('content');

                // sign in
                $('#sign_in').click(function () {
                    $('#sign_in').prop('disabled', true).html('<i class="fa fa-spinner fa-spin fa-fw"></i>');
                    $.post('/login/apiLogin/', {
                        email: $('#email').val(),
                        password: $('#password').val(),
                        remember: $("#remember").prop("checked") ? 1 : 0,
                        csrfToken: csrfToken
                    }, function (data) {
                        if (data === 'ok') {
                            location.reload();
                        } else {
                            $('#error_login').removeClass('hidden');
                            $('#sign_in').prop('disabled', false).html('로그인');
                        }
                    });
                });


                // show reset pass form
                $('#reset_pass').on('click', function () {
                    $('#login_block').addClass('hidden');
                    $('#reset_pass_block').removeClass('hidden');
                });


                // back to login form
                $('#go_back_to_login').on('click', function () {
                    location.reload();
                });


                // reset pass
                $('#reset_pass_button').on('click', function () {
                    $('#reset_pass_button').prop('disabled', true).html('<i class="fa fa-spinner fa-spin fa-fw"></i>');
                    $.post('/login/apiResetPass/', {
                        reset_email: $('#reset_email').val(),
                        csrfToken: csrfToken
                    }, function (data) {
                        var response = $.parseJSON(data);
                        if (response.status === 'ok') {
                            $('#reset_pass_block').addClass('hidden');
                            $('#reset_sucsess_email').html($('#reset_email').val());
                            $('#reset_sucsess_block').removeClass('hidden');
                        } else {
                            $('#reset_pass_button').prop('disabled', false).html('RESET PASSWORD');
                            $('#error_reset_pass').removeClass('hidden');
                        }
                    });
                });

            });
        </script>
    </head>
    <body>
        <div class="container">
            <div id="login_block" class="row  vcenter">
                <div class="col-xs-4 col-xs-offset-4 ">
                    <div class="panel panel-primary">
                        <div class="panel-heading">
                            로그인 <span class="pull-right">AIARA ADmin</span>
                        </div>
                        <div class="panel-body text-center">
                            <div id="error_login" class="hidden alert alert-danger"><i class="fa fa-exclamation-triangle"></i> 로그인실패! 이메일 또는 암호를 확인해주세요.</div>
                            <div class="input-group form-group">
                                <span class="input-group-addon"><i class="fa fa-user fa-fw" aria-hidden="true"></i></span>
                                <input id="email" type="text" class="form-control form-group" placeholder="이메일">
                            </div>
                            <div class="input-group form-group">
                                <span class="input-group-addon"><i class="fa fa-lock fa-fw" aria-hidden="true"></i></span>
                                <input id="password" type="password" class="form-control form-group" placeholder="암호">
                            </div>
                            <button id="sign_in" type="button" class="btn btn-danger btn-block">로그인</button>
                            <div style="margin-top:7px;" class="pull-left">
                                <input type="checkbox" id="remember" name="remember"> <span style="position:relative; top:-1px; left:1px;">자동로그인설정</span>
                            </div>
                            <div class="pull-right" style="margin-top: 10px;">
                                <a id="reset_pass" href="#">비밀번호 재설정</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div id="reset_pass_block" class="hidden row vcenter">
                <div class="col-xs-4 col-xs-offset-4 ">
                    <div class="panel panel-primary">
                        <div class="panel-heading">
                             비밀번호 재설정<span class="pull-right">AIARA ADmin</span>
                        </div>
                        <div class="panel-body text-center">
                            <div id="error_reset_pass" class="hidden alert alert-danger">이메일을 잘못 입력했거나 가입되어 있지 않는 이메일입니다.</div>
                            <div class="input-group form-group">
                                <span class="input-group-addon"><i class="fa fa-user fa-fw" aria-hidden="true"></i></span>
                                <input id="reset_email" type="text" class="form-control form-group" placeholder="이메일">
                            </div>
                            <button id="reset_pass_button" type="button" class="btn btn-danger btn-block">비밀번호 초기화</button>
                            <div class="pull-left" style="margin-top: 10px;">
                                <a id="go_back_to_login" href="#"><i class="fa fa-arrow-left" aria-hidden="true"></i> 뒤로가기</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div id="reset_sucsess_block" class="hidden row vcenter">
                <div class="col-xs-4 col-xs-offset-4 ">
                    <div class="panel panel-primary">
                        <div class="panel-heading">
                            비밀번호 재설정 <span class="pull-right">AIARA ADmin</span>
                        </div>
                        <div class="panel-body text-center">
                            <h4>아래 이메일에 비밀번호 재설정 링크 보냈습니다.<br> <b><span id="reset_sucsess_email"></span></b></h4>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>
