<nav class="navbar navbar-default navbar-fixed-top">
    <div class="container-fluid">
        <div class="navbar-header">
            <a class="navbar-brand" href="/"><b>AIARA AD</b><span>min</span></a>
        </div>
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav">
                <li><a href="/sites/"><i class="fa fa-globe" aria-hidden="true"></i> 사이트관리</a></li>
                <li><a href="/campaigns/"><i class="fa fa-shopping-cart" aria-hidden="true"></i> 캠페인</a></li>
                <li><a href="/stat/"><i class="fa fa-bar-chart" aria-hidden="true"></i> 통계 </a></li>
                <li><a href="/filter/"><i class="fa fa-filter" aria-hidden="true"></i> 필터</a></li>
                <li><a href="/settings/"><i class="fa fa-cog" aria-hidden="true"></i> 설정</a></li>
            </ul>
            <ul class="nav navbar-nav navbar-right">
                <li><a><i class="fa fa-user" aria-hidden="true"></i> <?php echo $user['user_email']; ?>님</a></li>
                <li>
                    <div class="navbar-btn">
                        <button id="logout" class="btn btn-sm btn-default"><i class="fa fa-sign-out" aria-hidden="true"></i> 로그아웃</button>
                    </div>
                </li>
            </ul>
        </div>
    </div>
</nav>