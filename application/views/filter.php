<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<!doctype html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <title>Filter</title>
        <meta name="csrf-param" content="<?php echo $csrfToken; ?>">
        <link rel='stylesheet' href='/css/bootstrap.min.css' type='text/css' media='all'>
        <link rel='stylesheet' href='/css/bootstrap-dialog.css' type='text/css' media='all'>
        <link rel='stylesheet' href='/css/bootstrap-select.min.css' type='text/css' media='all'>
        <link rel='stylesheet' href='/css/bootstrap-colorpicker.min.css' type='text/css' media='all'>
        <link rel='stylesheet' href='/css/font-awesome.min.css' type='text/css' media='all'>
        <link rel='stylesheet' href='/css/style.css' type='text/css' media='all'>
        <link rel="shortcut icon" href="/img/favicon.ico">
        <script src="/js/jquery.min.js"></script>
        <script src="/js/bootstrap.min.js"></script>
        <script src="/js/bootstrap-dialog.js"></script>
        <script src="/js/bootstrap-select.min.js"></script>
        <script src="/js/clipboard.min.js"></script>
        <script>
            $(document).ready(function () {

                // csrf token
                var csrfToken = $('[name="csrf-param"]').attr('content');


                // clipboard lib
                new Clipboard('#clipboard_list');


                // clear ips input
                $('#clear_list').click(function () {
                    $('#ip_list').val('');
                });


                // save ips
                $('#save_list').on('click', function () {
                    $(this).prop('disabled', true).html('<i class="fa fa-spinner fa-spin fa-fw"></i> Save');
                    $.post('/filter/apiSetIps/', {
                        ips: $('#ip_list').val(),
                        csrfToken: csrfToken
                    }, function () {
                        location.reload();
                    });
                });


                // logout
                $('#logout').on('click', function () {
                    $.post('/login/logOut/', {csrfToken: csrfToken}, function () {
                        location.reload();
                    });
                });


            });

        </script>
    </head>
    <body>
        <div class="container-fluid">
            <div class="row">
                <div class="col-xlg-10 col-xlg-offset-1 col-xs-12 col-xs-offset-0">
                    <?php include_once ('nav.php') ?>

                    <div class="panel panel-primary ">
                        <div class="panel-heading"><b>Global blacklist ip</b></div>
                        <div class="panel-body">
                            <textarea class="form-control" rows="15" id="ip_list" placeholder="Each ip on a new line"><?php echo $bannedIpsList; ?></textarea>
                        </div>
                        <div class="panel-footer">
                            <button id="save_list" type="button" class="btn btn-sm btn-success"><i class="fa fa-check fa-fw" aria-hidden="true"></i> Save</button>
                            <button id="clipboard_list" type="button" class="btn btn-sm btn-default" data-clipboard-target="#ip_list"><i class="fa fa-clipboard" aria-hidden="true"></i> Copy to clipboard</button>
                            <button id="clear_list" type="button" class="btn btn-sm btn-default"><i class="fa fa-eraser" aria-hidden="true"></i> Clear</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>
