<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<!doctype html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <title><?php echo $site['domain']; ?> - <?php echo $block['block_name']; ?></title>
        <meta name="csrf-param" content="<?php echo $csrfToken; ?>">
        <link rel='stylesheet' href='/css/bootstrap.min.css' type='text/css' media='all'>
        <link rel='stylesheet' href='/css/bootstrap-dialog.css' type='text/css' media='all'>
        <link rel='stylesheet' href='/css/bootstrap-select.min.css' type='text/css' media='all'>
        <link rel='stylesheet' href='/css/bootstrap-colorpicker.min.css' type='text/css' media='all'>
        <link rel='stylesheet' href='/css/font-awesome.min.css' type='text/css' media='all'>
        <link rel='stylesheet' href='/css/style.css' type='text/css' media='all'>
        <link rel="shortcut icon" href="/img/favicon.ico">
        <script src="/js/jquery.min.js"></script>
        <script src="/js/bootstrap.min.js"></script>
        <script src="/js/bootstrap-dialog.js"></script>
        <script src="/js/bootstrap-select.min.js"></script>
        <script src="/js/bootstrap-colorpicker.min.js"></script>
        <script src="/js/clipboard.min.js"></script>
        <script src="/js/block-builder.js"></script>
        <script>
            $(document).ready(function () {

                // csrf token
                var csrfToken = $('[name="csrf-param"]').attr('content');

                // logout
                $('#logout').on('click', function () {
                    $.post('/login/logOut/', {csrfToken: csrfToken}, function () {
                        location.reload();
                    });
                });

            });

        </script>
        <style>

        </style>
    </head>
    <body>
        <div class="container-fluid">
            <div class="row">
                <div class="col-xs-12">
                    <?php include_once ('nav.php') ?>
                    <ol class="breadcrumb">
                        <li><a href="/sites/">Sites</a></li>
                        <li><a class="text-lowercase" href="/sites/<?php echo $site['id']; ?>/blocks/"><?php echo $site['domain']; ?></a></li>
                        <li><?php echo $block['block_name']; ?></li>
                    </ol>
                    <div style="padding-left:0;" class="col-xs-9 col-md-8">
                        <div class="panel panel-primary clearfix">
                            <div class="panel-heading"><b>Block preview</b></div>
                            <center>
                                <div class="panel-body bgc">
                                    <!--start block ads -->

                                    <!-- end block ads -->
                                </div>
                            </center>
                            <div class="panel-footer">
                                <div id="_block_view_bg" class="btn-group btn-group-sm" data-toggle="buttons">
                                    <label class="btn btn-default active"><input type="radio" name="bg" value="1" autocomplete="off">White</label>
                                    <label class="btn btn-default"><input type="radio" name="bg" value="2" autocomplete="off">Black</label>
                                    <label class="btn btn-default"><input type="radio" name="bg" value="3" autocomplete="off">Transparent</label>
                                </div>
                            </div>
                        </div>
                    </div>

                    <!--start block settings block-->
                    <div style="padding:0;" class="col-xs-3 col-md-4">
                        <div class="panel-group" id="collapse-group">

                            <div class="panel panel-primary">
                                <div class="panel-heading">
                                    <h4 class="panel-title">
                                        <a data-toggle="collapse" data-parent="#collapse-group" href="#el0">Presets</a> <span class="label label-success" style="position: relative; top:-5px;"> New!</span>
                                    </h4>
                                </div>
                                <div id="el0" class="panel-collapse collapse in">
                                    <div class="panel-body">

                                        <div class="row">
                                            <div class="col-xs-12">
                                                <div class="input-group form-group">
                                                    <span class="input-group-addon">Presets</span>
                                                    <select id="presets" class="selectpicker" data-style="btn-default btn-sm">
                                                        <option value="1">2x2 - Ddefault</option>
                                                        <option value="2">1x2 - Big</option>
                                                        <option value="3">1x2 - Text</option>
                                                        <option value="4">1x3 - Blue</option>
                                                        <option value="5">2x1 - Hunter green</option>
                                                        <option value="6">1x3 - Byzantium</option>
                                                        <option value="7">1x3 - Burnt sienna</option>
                                                        <option value="8">3x1 - Apple green</option>
                                                        <option value="9">1x4 - Midnight blue </option>
                                                        <option value="10">2x1 - Lime </option>
                                                        <option value="11">2x2 - Red </option>
                                                        <option value="12">1x1 - Banner 300x250 </option>
                                                        <option value="14">1x2 - Medium taupe </option>
                                                        <option value="15">3x2 - Baby blue </option>
                                                        <option value="16">3x2 - Gray  </option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>

                                        <!--end presets block-->
                                    </div>
                                </div>
                            </div>

                            <div class="panel panel-primary">
                                <div class="panel-heading">
                                    <h4 class="panel-title">
                                        <a data-toggle="collapse" data-parent="#collapse-group" href="#el1">Block settings</a>
                                    </h4>
                                </div>
                                <div id="el1" class="panel-collapse collapse">
                                    <div class="panel-body">

                                        <div class="row">
                                            <div class="col-xs-12">
                                                <div class="input-group input-group-sm form-group">
                                                    <span class="input-group-addon">Block name</span>
                                                    <input id="_block_name" type="text" class="form-control" placeholder="Block name (Visible only to you)" value="">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-xs-6">
                                                <div class="input-group input-group-sm form-group">
                                                    <span class="input-group-addon" data-toggle="tooltip" title="Block width"><i class="fa fa-arrows-h fa-fw" aria-hidden="true"></i> </span>
                                                    <input id="_block_width" type="text" class="form-control noborder-r" placeholder="Width" value="100">
                                                    <span class="input-group-addon"><input  type="radio" name="_block_width_units" value="px"> px</span>
                                                    <span class="input-group-addon"><input type="radio" name="_block_width_units" value="%"> %</span>
                                                </div>
                                            </div>
                                            <div class="col-xs-6">
                                                <div class="input-group input-group-sm form-group">
                                                    <span class="input-group-addon" data-toggle="tooltip" title="Block height"><i class="fa fa-arrows-v fa-fw" aria-hidden="true"></i> </span>
                                                    <input id="_block_height" type="text" class="form-control noborder-r" placeholder="Height" value="100">
                                                    <span class="input-group-addon"><input  type="radio" name="_block_height_units" value="px"> px</span>
                                                    <span class="input-group-addon"><input type="radio" name="_block_height_units" value="%"> %</span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-xs-6">
                                                <div class="input-group form-group">
                                                    <span class="input-group-addon">Count ads width</span>
                                                    <select id="_count_ads_width" class="selectpicker" data-style="btn-default btn-sm">
                                                        <option value="1">1</option>
                                                        <option value="2">2</option>
                                                        <option value="3">3</option>
                                                        <option value="4">4</option>
                                                        <option value="5">5</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-xs-6">
                                                <div class="input-group form-group">
                                                    <span class="input-group-addon">Count ads height</span>
                                                    <select id="_count_ads_height" class="selectpicker" data-style="btn-default btn-sm">
                                                        <option value="1">1</option>
                                                        <option value="2">2</option>
                                                        <option value="3">3</option>
                                                        <option value="4">4</option>
                                                        <option value="5">5</option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-xs-6">
                                                <div id="_bg_block" class="input-group input-group-sm colorpicker-component form-group">
                                                    <span class="input-group-addon" data-toggle="tooltip" title="Block background color. Transparent block - leave this field empty.">BGC</span>
                                                    <input type="text" value=""  placeholder="Transparent" class="form-control" />
                                                    <span class="input-group-addon fff"><i></i></span>
                                                </div>
                                                <script>
                                                    $(document).ready(function () {
                                                        $('#_bg_block').colorpicker({
                                                            format: "hex",
                                                            component: ".fff"
                                                        });
                                                    });
                                                </script>
                                            </div>
                                            <div class="col-xs-6">
                                                <div id="_block_border_color" class="input-group input-group-sm colorpicker-component">
                                                    <span class="input-group-addon" data-toggle="tooltip" title="Border color">BRC</span>
                                                    <input type="text" value="" class="form-control" />
                                                    <span class="input-group-addon fff"><i></i></span>
                                                </div>
                                                <script>
                                                    $(document).ready(function () {
                                                        $('#_block_border_color').colorpicker({
                                                            format: "hex",
                                                            component: ".fff"
                                                        });
                                                    });
                                                </script>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-xs-6">
                                                <div class="input-group">
                                                    <span class="input-group-addon">Block margin</span>
                                                    <select id="_block_padding" class="selectpicker" data-style="btn-default btn-sm">
                                                        <option value="0">0 px</option>
                                                        <option value="1">1 px</option>
                                                        <option value="2">2 px</option>
                                                        <option value="3">3 px</option>
                                                        <option value="4">4 px</option>
                                                        <option value="5">5 px</option>
                                                        <option value="6">6 px</option>
                                                        <option value="7">7 px</option>
                                                        <option value="8">8 px</option>
                                                        <option value="9">9 px</option>
                                                        <option value="10">10 px</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-xs-6">
                                                <div class="input-group">
                                                    <span class="input-group-addon">Block border</span>
                                                    <select id="_block_border" class="selectpicker" data-style="btn-default btn-sm">
                                                        <option value="1">1 px</option>
                                                        <option value="2">2 px</option>
                                                        <option value="3">3 px</option>
                                                        <option value="4">4 px</option>
                                                        <option value="5">5 px</option>
                                                        <option value="0" class="opt_divider">No border</option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                        <!--end block settings block-->
                                    </div>
                                </div>
                            </div>



                            <div class="panel panel-primary">
                                <div class="panel-heading">
                                    <h4 class="panel-title">
                                        <a data-toggle="collapse" data-parent="#collapse-group" href="#el2">Ad settings</a>
                                    </h4>
                                </div>
                                <div id="el2" class="panel-collapse collapse">
                                    <div class="panel-body">
                                        <!--start cell settings block-->
                                        <div class="row">
                                            <div class="col-xs-6">
                                                <div class="input-group form-group">
                                                    <span class="input-group-addon" data-toggle="tooltip" title="The image on the text position">Image position</span>
                                                    <select id="_img_position" class="selectpicker" data-style="btn-default btn-sm">
                                                        <option value="left">Left</option>
                                                        <option value="top">Top</option>
                                                        <option value="no" class="opt_divider">No image</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-xs-6">
                                                <div class="input-group">
                                                    <span class="input-group-addon" data-toggle="tooltip" title="Padding elements inside the cell">Paddings cell</span>
                                                    <select id="_cell_padding" class="selectpicker" data-style="btn-default btn-sm">
                                                        <option value="1">0 px</option>
                                                        <option value="1">1 px</option>
                                                        <option value="2">2 px</option>
                                                        <option value="3">3 px</option>
                                                        <option value="4">4 px</option>
                                                        <option value="5">5 px</option>
                                                        <option value="6">6 px</option>
                                                        <option value="7">7 px</option>
                                                        <option value="8">8 px</option>
                                                        <option value="9">9 px</option>
                                                        <option value="10">10 px</option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-xs-6">
                                                <div class="input-group ">
                                                    <span class="input-group-addon" >Align</span>
                                                    <select id="_cell_align" class="selectpicker" data-style="btn-default btn-sm">
                                                        <option value="left">Left</option>
                                                        <option value="center">Center</option>
                                                        <option value="right">Right</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-xs-6">
                                                <div class="input-group">
                                                    <span class="input-group-addon" data-toggle="tooltip" title="Margin between ads" >Margin ads</span>
                                                    <select id="_cells_margin" class="selectpicker" data-style="btn-default btn-sm">
                                                        <option value="1">0 px</option>
                                                        <option value="1">1 px</option>
                                                        <option value="2">2 px</option>
                                                        <option value="3">3 px</option>
                                                        <option value="4">4 px</option>
                                                        <option value="5">5 px</option>
                                                        <option value="6">6 px</option>
                                                        <option value="7">7 px</option>
                                                        <option value="8">8 px</option>
                                                        <option value="9">9 px</option>
                                                        <option value="10">10 px</option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                        <!--end cell settings block-->
                                    </div>
                                </div>
                            </div>
                            <div id="_img_b" class="panel panel-primary">
                                <div class="panel-heading">
                                    <h4 class="panel-title">
                                        <a data-toggle="collapse" data-parent="#collapse-group" href="#el3">Image settings</a>
                                    </h4>
                                </div>
                                <div id="el3" class="panel-collapse collapse">
                                    <div class="panel-body">
                                        <div class="row">
                                            <div class="col-xs-6">
                                                <div class="input-group form-group">
                                                    <span class="input-group-addon">Image Width</span>
                                                    <select id="_img_size" class="selectpicker" data-style="btn-default btn-sm">
                                                        <option value="70">70 px</option>
                                                        <option value="80">80 px</option>
                                                        <option value="90">90 px</option>
                                                        <option value="100">100 px</option>
                                                        <option value="110">110 px</option>
                                                        <option value="120">120 px</option>
                                                        <option value="130">130 px</option>
                                                        <option value="140">140 px</option>
                                                        <option value="150">150 px</option>
                                                        <option value="160">160 px</option>
                                                        <option value="170">170 px</option>
                                                        <option value="180">180 px</option>
                                                        <option value="190">190 px</option>
                                                        <option value="200">200 px</option>
                                                        <option value="250">250 px</option>
                                                        <option value="300">300 px</option>
                                                    </select>
                                                </div>
                                            </div>

                                            <div class="col-xs-6">
                                                <div class="input-group form-group">
                                                    <span class="input-group-addon">Image Height</span>
                                                    <select id="_img_size_height" class="selectpicker" data-style="btn-default btn-sm">
                                                        <option value="70">70 px</option>
                                                        <option value="80">80 px</option>
                                                        <option value="90">90 px</option>
                                                        <option value="100">100 px</option>
                                                        <option value="110">110 px</option>
                                                        <option value="120">120 px</option>
                                                        <option value="130">130 px</option>
                                                        <option value="140">140 px</option>
                                                        <option value="150">150 px</option>
                                                        <option value="160">160 px</option>
                                                        <option value="170">170 px</option>
                                                        <option value="180">180 px</option>
                                                        <option value="190">190 px</option>
                                                        <option value="200">200 px</option>
                                                        <option value="250">250 px</option>
                                                        <option value="300">300 px</option>
                                                    </select>
                                                </div>
                                            </div>

                                        </div>
                                        <div class="row">
                                            <div class="col-xs-6">
                                                <div id="_img_border_color" class="input-group form-group input-group-sm colorpicker-component">
                                                    <span class="input-group-addon" data-toggle="tooltip" title="Image border color">IMG BC</span>
                                                    <input type="text" value="" class="form-control"/>
                                                    <span class="input-group-addon fff"><i></i></span>
                                                </div>
                                                <script>
                                                    $(document).ready(function () {
                                                        $('#_img_border_color').colorpicker({
                                                            format: "hex",
                                                            component: ".fff"
                                                        });
                                                    });
                                                </script>
                                            </div>
                                            <div class="col-xs-6">
                                                <div id="_img_border_color_hover" class="input-group input-group-sm colorpicker-component">
                                                    <span class="input-group-addon" data-toggle="tooltip" title="Image border color - hover">IMG BC (H)</span>
                                                    <input type="text" value="" class="form-control" />
                                                    <span class="input-group-addon fff"><i></i></span>
                                                </div>
                                                <script>
                                                    $(document).ready(function () {
                                                        $('#_img_border_color_hover').colorpicker({
                                                            format: "hex",
                                                            component: ".fff"
                                                        });
                                                    });
                                                </script>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-xs-6">
                                                <div class="input-group">
                                                    <span class="input-group-addon">Image border</span>
                                                    <select id="_img_border" class="selectpicker" data-style="btn-default btn-sm">
                                                        <option value="1">1 px</option>
                                                        <option value="2">2 px</option>
                                                        <option value="3">3 px</option>
                                                        <option value="4">4 px</option>
                                                        <option value="5">5 px</option>
                                                        <option value="0" class="opt_divider">No border</option>
                                                    </select>
                                                </div>
                                            </div>

                                        </div>
                                        <!--end image settings block-->
                                    </div>
                                </div>
                            </div>
                            <div class="panel panel-primary">
                                <div class="panel-heading">
                                    <h4 class="panel-title">
                                        <a data-toggle="collapse" data-parent="#collapse-group" href="#el4">Title settings</a>
                                    </h4>
                                </div>
                                <div id="el4" class="panel-collapse collapse">
                                    <div class="panel-body">
                                        <!--start title settings block-->
                                        <div class="row">
                                            <div class="col-xs-6">
                                                <div class="input-group form-group">
                                                    <span class="input-group-addon">Font</span>
                                                    <select id="_title_font_family" class="selectpicker" data-style="btn-default btn-sm">
                                                        <option value="Arial, Helvetica, sans-serif">Arial</option>
                                                        <option value="Tahoma, Geneva, sans-serif" selected="selected">Tahoma</option>
                                                        <option value="Verdana, Geneva, sans-serif">Verdana</option>
                                                        <option value="Georgia, Times New Roman, Times, serif">Georgia</option>
                                                        <option value="Courier New, Courier, monospace">Courier New</option>
                                                        <option value="Trebuchet MS, Helvetica, sans-serif">Trebuchet MS</option>
                                                        <option value="Lucida Console, Monaco, monospace">Lucida Console</option>
                                                        <option value="Palatino Linotype, Book Antiqua, Palatino, serif">Palatino Linotype</option>
                                                        <option value="Times New Roman, Times, serif">Times New Roman</option>
                                                        <option data-divider="true"></option>
                                                        <option data-subtext="As on the site" value="inherit">None</option>


                                                    </select>
                                                </div>
                                                <div id="_title_font_color" class="input-group input-group-sm colorpicker-component form-group">
                                                    <span class="input-group-addon" data-toggle="tooltip" title="Title color">Color</span>
                                                    <input type="text" value="" class="form-control" />
                                                    <span class="input-group-addon fff"><i></i></span>
                                                </div>
                                                <script>
                                                    $(document).ready(function () {
                                                        $('#_title_font_color').colorpicker({
                                                            format: "hex",
                                                            component: ".fff"
                                                        });
                                                    });
                                                </script>

                                                <div class="btn-group btn-group-sm" role="group" data-toggle="tooltip" data-placement="left" title="Title styles">
                                                    <button id="_title_bold" type="button" class="btn btn-default"><b>Вold</b></button>
                                                    <button id="_title_italic" type="button" class="btn btn-default"><i>Italic</i></button>
                                                    <button id="_title_underline" type="button" class="btn btn-default"><span style="text-decoration: underline;">Underline</span></button>
                                                </div>

                                            </div>
                                            <div class="col-xs-6">
                                                <div class="input-group form-group">
                                                    <span class="input-group-addon">Size</span>
                                                    <select id="_title_font_size" class="selectpicker" data-style="btn-default btn-sm">
                                                        <option value="11">11 px</option>
                                                        <option value="12">12 px</option>
                                                        <option value="13">13 px</option>
                                                        <option value="14">14 px</option>
                                                        <option value="15">15 px</option>
                                                        <option value="16">16 px</option>
                                                        <option value="17">17 px</option>
                                                        <option value="18">18 px</option>
                                                    </select>
                                                </div>
                                                <div id="_title_font_color_hover" class="input-group form-group input-group-sm colorpicker-component form-group">
                                                    <span class="input-group-addon" data-toggle="tooltip" title="Title color - hover">Color hover</span>
                                                    <input type="text" value="" class="form-control" />
                                                    <span class="input-group-addon fff"><i></i></span>
                                                </div>
                                                <script>
                                                    $(document).ready(function () {
                                                        $('#_title_font_color_hover').colorpicker({
                                                            format: "hex",
                                                            component: ".fff"
                                                        });
                                                    });
                                                </script>
                                                <div class="btn-group btn-group-sm form-group" role="group" data-toggle="tooltip" data-placement="left" title="Title styles - hover">
                                                    <button id="_title_bold_hover" type="button" class="btn btn-default"><b>Вold</b></button>
                                                    <button id="_title_italic_hover" type="button" class="btn btn-default"><i>Italic</i></button>
                                                    <button id="_title_underline_hover" type="button" class="btn btn-default"><span style="text-decoration: underline;">Underline</span></button>
                                                </div>

                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col-xs-12">
                                                <div class="btn-group btn-group-sm" role="group" data-toggle="tooltip" data-placement="left" title="Show / Hide title">
                                                    <button id="title_enabled" type="button" class="btn btn-default">Enabled title</button>
                                                    <button id="title_disabled" type="button" class="btn btn-default">Disabled title</button>
                                                </div>
                                            </div>
                                        </div>
                                        <!--end text settings block-->
                                    </div>
                                </div>
                            </div>

                            <!--start description settings block-->
                            <div class="panel panel-primary">
                                <div class="panel-heading">
                                    <h4 class="panel-title">
                                        <a data-toggle="collapse" data-parent="#collapse-group" href="#el5">Description settings</a>
                                    </h4>
                                </div>
                                <div id="el5" class="panel-collapse collapse">
                                    <div class="panel-body">
                                        <div class="row">
                                            <div class="col-xs-6">
                                                <div class="input-group form-group">
                                                    <span class="input-group-addon">Font</span>
                                                    <select id="_description_font_family" class="selectpicker" data-style="btn-default btn-sm">
                                                        <option value="Arial, Helvetica, sans-serif">Arial</option>
                                                        <option value="Tahoma, Geneva, sans-serif" selected="selected">Tahoma</option>
                                                        <option value="Verdana, Geneva, sans-serif">Verdana</option>
                                                        <option value="Georgia, Times New Roman, Times, serif">Georgia</option>
                                                        <option value="Courier New, Courier, monospace">Courier New</option>
                                                        <option value="Trebuchet MS, Helvetica, sans-serif">Trebuchet MS</option>
                                                        <option value="Lucida Console, Monaco, monospace">Lucida Console</option>
                                                        <option value="Palatino Linotype, Book Antiqua, Palatino, serif">Palatino Linotype</option>
                                                        <option value="Times New Roman, Times, serif">Times New Roman</option>
                                                        <option data-divider="true"></option>
                                                        <option data-subtext="As on the site" value="inherit">None</option>
                                                    </select>
                                                </div>
                                                <div id="_description_font_color" class="input-group form-group input-group-sm colorpicker-component">
                                                    <span class="input-group-addon" data-toggle="tooltip" title="Description font color">Color</span>
                                                    <input type="text" value="" class="form-control" />
                                                    <span class="input-group-addon fff"><i></i></span>
                                                </div>
                                                <script>
                                                    $(document).ready(function () {
                                                        $('#_description_font_color').colorpicker({
                                                            format: "hex",
                                                            component: ".fff"
                                                        });
                                                    });
                                                </script>
                                            </div>
                                            <div class="col-xs-6">
                                                <div class="input-group form-group">
                                                    <span class="input-group-addon">Size</span>
                                                    <select id="_description_font_size" class="selectpicker" data-style="btn-default btn-sm">
                                                        <option value="11">11 px</option>
                                                        <option value="12">12 px</option>
                                                        <option value="13">13 px</option>
                                                        <option value="14">14 px</option>
                                                        <option value="15">15 px</option>
                                                        <option value="16">16 px</option>
                                                        <option value="17">17 px</option>
                                                        <option value="18">18 px</option>
                                                    </select>
                                                </div>

                                                <div class="btn-group btn-group-sm" role="group" data-toggle="tooltip" data-placement="left" title="Description styles">
                                                    <button id="_description_bold" type="button" class="btn btn-default"><b>Вold</b></button>
                                                    <button id="_description_italic" type="button" class="btn btn-default"><i>Italic</i></button>
                                                    <button id="_description_underline" type="button" class="btn btn-default"><span style="text-decoration: underline;">Underline</span></button>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col-xs-12">
                                                <div class="btn-group btn-group-sm" role="group" data-toggle="tooltip" data-placement="left" title="Show / Hide description">
                                                    <button id="descr_enabled" type="button" class="btn btn-default">Enabled description</button>
                                                    <button id="descr_disabled" type="button" class="btn btn-default">Disabled description</button>
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>
                            <!--end description settings block-->

                            <!--start link settings block-->
                            <div class="panel panel-primary">
                                <div class="panel-heading">
                                    <h4 class="panel-title">
                                        <a data-toggle="collapse" data-parent="#collapse-group" href="#el6">Link settings</a>
                                    </h4>
                                </div>
                                <div id="el6" class="panel-collapse collapse">
                                    <div class="panel-body">
                                        <div class="row">
                                            <div class="col-xs-6">
                                                <div class="input-group form-group">
                                                    <span class="input-group-addon">Font</span>
                                                    <select id="_link_font_family" class="selectpicker" data-style="btn-default btn-sm">
                                                        <option value="Arial, Helvetica, sans-serif">Arial</option>
                                                        <option value="Tahoma, Geneva, sans-serif" selected="selected">Tahoma</option>
                                                        <option value="Verdana, Geneva, sans-serif">Verdana</option>
                                                        <option value="Georgia, Times New Roman, Times, serif">Georgia</option>
                                                        <option value="Courier New, Courier, monospace">Courier New</option>
                                                        <option value="Trebuchet MS, Helvetica, sans-serif">Trebuchet MS</option>
                                                        <option value="Lucida Console, Monaco, monospace">Lucida Console</option>
                                                        <option value="Palatino Linotype, Book Antiqua, Palatino, serif">Palatino Linotype</option>
                                                        <option value="Times New Roman, Times, serif">Times New Roman</option>
                                                        <option data-divider="true"></option>
                                                        <option data-subtext="As on the site" value="inherit">None</option>
                                                    </select>
                                                </div>
                                                <div id="_link_font_color" class="input-group input-group-sm colorpicker-component form-group">
                                                    <span class="input-group-addon" data-toggle="tooltip" title="Link color">Color</span>
                                                    <input id="_link_font_color" type="text" value="" class="form-control" />
                                                    <span class="input-group-addon fff"><i></i></span>
                                                </div>
                                                <script>
                                                    $(document).ready(function () {
                                                        $('#_link_font_color').colorpicker({
                                                            format: "hex",
                                                            component: ".fff"
                                                        });
                                                    });
                                                </script>
                                                <div class="btn-group btn-group-sm" role="group" data-toggle="tooltip" data-placement="left" title="Link styles">
                                                    <button id="_link_bold" type="button" class="btn btn-default"><b>Вold</b></button>
                                                    <button id="_link_italic" type="button" class="btn btn-default"><i>Italic</i></button>
                                                    <button id="_link_underline" type="button" class="btn btn-default"><span style="text-decoration: underline;">Underline</span></button>
                                                </div>
                                            </div>
                                            <div class="col-xs-6">
                                                <div class="input-group form-group">
                                                    <span class="input-group-addon">Size</span>
                                                    <select id="_link_font_size" class="selectpicker" data-style="btn-default btn-sm">
                                                        <option value="11">11 px</option>
                                                        <option value="12">12 px</option>
                                                        <option value="13">13 px</option>
                                                        <option value="14">14 px</option>
                                                        <option value="15">15 px</option>
                                                        <option value="16">16 px</option>
                                                        <option value="17">17 px</option>
                                                        <option value="18">18 px</option>
                                                    </select>
                                                </div>
                                                <div class="input-group form-group">
                                                    <div id="_link_font_color_hover" class="input-group input-group-sm colorpicker-component form-group">
                                                        <span class="input-group-addon" data-toggle="tooltip" title="Link color - hover">Color hover</span>
                                                        <input type="text" value="" class="form-control" />
                                                        <span class="input-group-addon fff"><i></i></span>
                                                    </div>
                                                    <script>
                                                        $(document).ready(function () {
                                                            $('#_link_font_color_hover').colorpicker({
                                                                format: "hex",
                                                                component: ".fff"
                                                            });
                                                        });
                                                    </script>
                                                    <div class="btn-group btn-group-sm" role="group" data-toggle="tooltip" data-placement="left" title="Link styles - hover">
                                                        <button id="_link_bold_hover" type="button" class="btn btn-default"><b>Вold</b></button>
                                                        <button id="_link_italic_hover" type="button" class="btn btn-default"><i>Italic</i></button>
                                                        <button id="_link_underline_hover" type="button" class="btn btn-default"><span style="text-decoration: underline;">Underline</span></button>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-xs-12">
                                                <div class="btn-group btn-group-sm" role="group" data-toggle="tooltip" data-placement="left" title="Show / Hide link">
                                                    <button id="link_enabled" type="button" class="btn btn-default">Enabled link</button>
                                                    <button id="link_disabled" type="button" class="btn btn-default">Disabled link</button>
                                                </div>
                                            </div>


                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!--end link settings block-->

                            <!--Third-party settings block-->
                            <!--			    <div class="panel panel-primary">
                                                            <div class="panel-heading">
                                                                <h4 class="panel-title">
                                                                    <a data-toggle="collapse" data-parent="#collapse-group" href="#el7">Third-party code</a>
                                                                </h4>
                                                            </div>
                                                            <div id="el7" class="panel-collapse collapse">
                                                                <div class="panel-body">
                                                                    <div class="row">
                                                                        <div class="col-xs-12">

                                                                            <textarea class="form-control" rows="5" id="third-party" placeholder="Code third-party system. It will be shown in the absence of any ads."></textarea>

                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>-->
                            <!--end Third-party settings block-->

                        </div>
                        <!--start save block-->
                        <div style="margin-top:-12px;" class="btn-group btn-group-justified" role="group" aria-label="...">
                            <div class="btn-group" role="group">
                                <button id="_save_block" type="button" class="btn btn-success"><i class="fa fa-check" aria-hidden="true"></i> Save block and get code</button>
                            </div>
                            <div class="btn-group" role="group">
                                <button type="button" class="btn btn-danger" data-toggle="modal" data-target="#_remove_block_modal"><i class="fa fa-trash-o" aria-hidden="true"></i> Remove block</button>
                            </div>
                        </div>
                        <!--end save block-->

                        <!-- Modal -->
                        <div class="modal fade" id="_modal_get_code" tabindex="-1" role="dialog" >
                            <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                    <div class="modal-header header-success">
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                        <h4 class="modal-title" id="myModalLabel"><i class="fa fa-check-circle" aria-hidden="true"></i> Block settings saved</h4>
                                    </div>

                                    <div class="modal-body">

                                        <p> Copy this code to your website.</p>

                                        <ul class="nav nav-tabs">
                                            <li class="active"><a data-toggle="tab" href="#standart-block">Standart block</a></li>
                                            <li><a data-toggle="tab" href="#pc-block">PC block</a></li>
                                            <li><a data-toggle="tab" href="#mobile-block"> Mobile block</a></li>
                                        </ul>

                                        <div class="tab-content">
                                            <div id="standart-block" class="tab-pane fade in active">
                                                <br>
                                                <p class="alert alert-info">
                                                    <b>Standard block</b> - will be visible on all devices.
                                                </p>

                                                <textarea style="font-size: 11px;" id="textarea-standart-code" class="form-control" rows="20"><?php echo htmlspecialchars($blockJsCallCode); ?></textarea>
                                            </div>
                                            <div id="pc-block" class="tab-pane fade">
                                                <br>
                                                <p class="alert alert-info">
                                                    <b>PC block</b> - will be visible on devices with a screen width MORE than 768px, the unit will be hidden if the screen width is LESS than 768px.
                                                </p>
                                                <textarea style="font-size: 11px;" id="textarea-pc-code" class="form-control" rows="20"><?php echo htmlspecialchars("<style>\n.__v-pc{display: none;}\n@media only screen and (min-width: 769px) {.__v-pc{display: block !important;}}\n</style>\n") . htmlspecialchars("<div class='__v-pc'>\n" . $blockJsCallCode . "</div>"); ?>
                                                </textarea>
                                            </div>
                                            <div id="mobile-block" class="tab-pane fade">

                                                <br>
                                                <p class="alert alert-info">
                                                    <b>Mobile block</b> - will be visible on devices with a screen width of LESS than 768px, the unit will be hidden if the screen width is MORE than 768px.
                                                </p>
                                                <textarea style="font-size: 11px;" id="textarea-mobile-code" class="form-control" rows="20"><?php echo htmlspecialchars("<style>\n.__v-mob{display: none;}\n@media only screen and (max-width: 768px) {.__v-mob{display: block !important;}}\n</style>\n") . htmlspecialchars("<div class='__v-mob'>\n" . $blockJsCallCode . "</div>"); ?>
                                                </textarea>

                                            </div>
                                        </div>

                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-default btn-sm pull-left" data-dismiss="modal">Close</button>
                                        <button type="button" class="clipboard btn btn-sm btn-success" data-clipboard-target="#textarea-standart-code"><i class="fa fa-clipboard" aria-hidden="true"></i> Copy to clipboard</button>
                                        <script>
                                            $(document).on('shown.bs.tab', 'a[data-toggle="tab"]', function () {
                                                var textarea_code_selector = $(".tab-pane.active").find("textarea").attr("id");
                                                $(".clipboard").attr("data-clipboard-target", "#" + textarea_code_selector);
                                            });
                                            new Clipboard('.clipboard');
                                        </script>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <!--start remove block modal-->
                        <div class="modal fade" id="_remove_block_modal" tabindex="-1" role="dialog">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-header header-danger">
                                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                        <h4 class="modal-title"><i class="fa fa-question-circle" aria-hidden="true"></i> Remove block <b class="text-capitalize"><?php echo $block['block_name']; ?></b></h4>
                                    </div>
                                    <div class="modal-body">
                                        Remove block?
                                    </div>
                                    <div class="modal-footer">
                                        <button id="_remove_block_ok" type="button" class="btn btn-danger btn-sm pull-left"><i class="fa fa-trash-o" aria-hidden="true"></i> Remove block</button>
                                        <button type="button" class="btn btn-default btn-sm" data-dismiss="modal"> Close</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!--end remove block modal-->
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>
