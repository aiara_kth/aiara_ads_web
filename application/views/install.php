<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<!doctype html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <title>AdFlex - install</title>
        <meta name="csrf-param" content="<?php echo $csrfToken; ?>">
        <link rel='stylesheet' href='/css/bootstrap.min.css' type='text/css' media='all'>
        <link rel='stylesheet' href='/css/bootstrap-dialog.css' type='text/css' media='all'>
        <link rel='stylesheet' href='/css/bootstrap-select.min.css' type='text/css' media='all'>
        <link rel='stylesheet' href='/css/bootstrap-colorpicker.min.css' type='text/css' media='all'>
        <link rel='stylesheet' href='/css/font-awesome.min.css' type='text/css' media='all'>
        <link rel='stylesheet' href='/css/style.css' type='text/css' media='all'>
        <link rel="shortcut icon" href="/img/favicon.ico">
        <script src="/js/jquery.min.js"></script>
        <script src="/js/bootstrap.min.js"></script>
        <script src="/js/bootstrap-dialog.js"></script>
        <script src="/js/bootstrap-select.min.js"></script>
        <script src="/js/bootstrap-colorpicker.min.js"></script>
        <script src="/js/clipboard.min.js"></script>
        <script>
            $(document).ready(function () {

                // csrf token
                var csrfToken = $('[name="csrf-param"]').attr('content');

                // install
                $('#install_button').on('click', function () {
                    $('#install_button').prop('disabled', true).html('<i class="fa fa-spinner fa-spin fa-fw"></i>');
                    $.post('/install/', {
                        db_user: $('#db_user').val(),
                        db_pass: $('#db_pass').val(),
                        db_host: $('#db_host').val(),
                        db_name: $('#db_name').val(),
                        admin_email: $('#admin_email').val(),
                        admin_pass: $('#admin_pass').val(),
                        csrfToken: csrfToken

                    }, function (data) {
                        var response = $.parseJSON(data);
                        if (response.status === 'error') {
                            $('#error_msg').text(response.msg).removeClass('hidden');
                            $('#install_button').prop('disabled', false).html('INSTALL');
                        } else if (response.status === 'ok') {
                            $('#install_window').addClass('hidden');
                            $('#completed_window').removeClass('hidden');
                        }
                    });
                });


                // go to login form
                $('#login_button').on('click', function () {
                    location.reload();
                });


                // show / hide input
                $('.show-hide-input').on('click', function () {
                    var input = $(this).parent().parent().find('input');
                    if (input.attr('type') === 'password') {
                        input.attr('type', 'text');
                        $(this).find('i').removeClass('fa-eye-slash').addClass('fa-eye');
                    } else {
                        input.attr('type', 'password');
                        $(this).find('i').removeClass('fa-eye').addClass('fa-eye-slash');
                    }
                });

            });

        </script>
    </head>
    <body>
        <div class="container">
            <div id="install_window" class="row">
                <div class="col-xs-6 col-xs-offset-3" style="margin-top:50px;">
                    <div class="panel panel-primary">
                        <div class="panel-heading">
                            Install <span class="pull-right"><img src="/img/logo.png" height="14"></span>
                        </div>
                        <div class="panel-body text-center">
                            <div class="alert alert-warning">
                                <b>Warning!</b><br>
                                <div class="premissions text-left">
                                    <p>1. PHP version must be 5.3.7+<span class="pull-right"><?php echo $phpVersion; ?></span></p>
                                    <p>2. File <b>"config.ini"</b> must be writable! (chmod 666 or 777) <a href="http://www.google.com/?q=filezilla+chmod+777" target="_blanck">(?)</a>  <span class="pull-right"><?php echo $writableConfigFile; ?></span></p>
                                    <p>3. Folder <b>"images"</b> must be writable! (chmod 777) <a href="http://www.google.com/?q=filezilla+chmod+777" target="_blanck">(?)</a> <span class="pull-right"><?php echo $writableImagesFolder; ?></span></p>
                                    <p>4. MySQL server version must be 5.1+ <span class="pull-right"><b>[will be checked during installation]</b></span></p>
                                    <p>5. Extension <b>GD</b> must be installed <span class="pull-right"><b><?php echo $gd; ?></b></span></p>
                                    <p>6. Extension <b>fileinfo</b> must be installed <span class="pull-right"><b><?php echo $fileinfo; ?></b></span></p>
                                </div>
                            </div>
                            <div id="error_msg" class="hidden alert alert-danger"></div>
                            <div class="form-group input-group input-group-sm">
                                <span class="input-group-addon"><i class="fa fa-user fa-fw" aria-hidden="true"></i>	</span>
                                <input id="db_user" type="text" class="form-control form-group" placeholder="MySQL User">
                            </div>
                            <div class="form-group input-group input-group-sm">
                                <span class="input-group-addon"><i class="fa fa-lock fa-fw" aria-hidden="true"></i>	</span>
                                <input id="db_pass" type="password" class="form-control form-group" placeholder="MySQL Password">
                                <span class="input-group-btn">
                                    <button class="show-hide-input btn btn-default"><i class="fa fa-eye-slash" aria-hidden="true"></i></button>
                                </span>
                            </div>
                            <div class="form-group input-group input-group-sm">
                                <span class="input-group-addon"><i class="fa fa-link fa-fw" aria-hidden="true"></i>	</span>
                                <input id="db_host" type="text" class="form-control form-group" placeholder='MySQL Host (99% - "localhost")'>
                            </div>
                            <div class="form-group input-group input-group-sm">
                                <span class="input-group-addon"><i class="fa fa-database fa-fw" aria-hidden="true"></i></span>
                                <input id="db_name" type="text" class="form-control form-group" placeholder="MySQL DB name (will be created)">
                            </div>
                            <hr>
                            <div class="form-group input-group input-group-sm">
                                <span class="input-group-addon"><i class="fa fa-envelope fa-fw" aria-hidden="true"></i></span>
                                <input id="admin_email" type="text" class="form-control" placeholder="Admin Email">
                            </div>
                            <div class="form-group input-group input-group-sm">
                                <span class="input-group-addon"><i class="fa fa-lock fa-fw" aria-hidden="true"></i></span>
                                <input id="admin_pass" type="password" class="form-control" placeholder="Admin Password (min. 8 characters)">
                                <span class="input-group-btn">
                                    <button  class="show-hide-input btn btn-default"><i class="fa fa-eye-slash" aria-hidden="true"></i></button>
                                </span>
                            </div>
                            <hr>
                            <button id="install_button" type="button" class="btn btn-sm btn-success btn-block">INSTALL</button>
                        </div>
                    </div>
                </div>
            </div>

            <div id="completed_window" class="hidden row">
                <div class="col-xs-4 col-xs-offset-4" style="margin-top:50px;">
                    <div class="panel panel-primary">
                        <div class="panel-heading">Completed <span class="pull-right"><img src="/img/logo.png" height="14"></span></div>
                        <div class="panel-body text-center">
                            <h4>The installation successfully completed!</h4>
                            <hr>
                            <button id="login_button" type="button" class="btn btn-success btn-block">LOG IN</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>
