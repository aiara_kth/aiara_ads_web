<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<!doctype html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <title>Blocks - <?php echo $site['domain']; ?></title>
        <meta name="csrf-param" content="<?php echo $csrfToken; ?>">
        <link rel='stylesheet' href='/css/bootstrap.min.css' type='text/css' media='all'>
        <link rel='stylesheet' href='/css/bootstrap-dialog.css' type='text/css' media='all'>
        <link rel='stylesheet' href='/css/bootstrap-select.min.css' type='text/css' media='all'>
        <link rel='stylesheet' href='/css/bootstrap-colorpicker.min.css' type='text/css' media='all'>
        <link rel='stylesheet' href='/css/font-awesome.min.css' type='text/css' media='all'>
        <link rel='stylesheet' href='/css/style.css' type='text/css' media='all'>
        <link rel="shortcut icon" href="/img/favicon.ico">
        <script src="/js/jquery.min.js"></script>
        <script src="/js/bootstrap.min.js"></script>
        <script src="/js/bootstrap-dialog.js"></script>
        <script src="/js/bootstrap-select.min.js"></script>
        <script src="/js/bootstrap-colorpicker.min.js"></script>
        <script src="/js/clipboard.min.js"></script>
        <script>
            $(document).ready(function () {

                // site id
                var site_id = location.href.split('/')[4];


                // csrf token
                var csrfToken = $('[name="csrf-param"]').attr('content');


                // add new block
                $('#_add_block_button').on('click', function () {
                    $('#_add_block_button').prop('disabled', true).html('<i class="fa fa-spinner fa-spin fa-fw"></i> Create');
                    $.post('/blocks/apiCreateBlock/', {
                        siteID: site_id,
                        blockName: $('#_block_name').val(),
                        csrfToken: csrfToken
                    }, function (data) {
                        var response = $.parseJSON(data);
                        if (response.status === 'error') {
                            $('#_add_block_error').html(response.msg).removeClass('hidden');
                            $('#_add_block_button').prop('disabled', false).html('<i class="fa fa-check"></i> Create');
                        } else if (response.status === 'ok') {
                            $('#_block_modal').modal('hide');
                            document.location.href = '/sites/' + site_id + '/blocks/' + response.msg;
                        }
                    });
                });


                // play block
                $('._play_block').on('click', function () {
                    $.post('/blocks/apiPlayBlock/', {
                        blockID: $(this).attr('name'),
                        csrfToken: csrfToken
                    }, function () {
                        location.reload();
                    });
                });


                // stop block
                $('._stop_block').on('click', function () {
                    $.post('/blocks/apiStopBlock/', {
                        blockID: $(this).attr('name'),
                        csrfToken: csrfToken
                    }, function () {
                        location.reload();
                    });
                });


                // remove block
                $('._remove_block').on('click', function () {
                    var id = $(this).attr('data');
                    var name = $(this).attr('name');
                    $('._remove_block_name').text(name);
                    $('#_remove_block_modal').modal();
                    $('#_remove_block_ok').on('click', function () {
                        $('#_remove_block_ok').prop('disabled', true).html('<i class="fa fa-spinner fa-spin fa-fw"></i> Remove block');
                        $.post('/blocks/apiRemoveBlock/', {
                            blockID: id,
                            csrfToken: csrfToken
                        }, function () {
                            $('#_remove_block_modal').modal('hide');
                            location.reload();
                        });
                    });
                });


                // copy block
                $('._copy_block').on('click', function () {
                    var block_name = $(this).attr('name');
                    var block_id = $(this).attr('data');
                    $('._copy_block_name').text(block_name);
                    $('#_copy_block_modal').modal();
                    $('#_copy_block_ok').on('click', function () {
                        $.post('/blocks/apiCopyBlock/', {
                            blockID: block_id,
                            pasteSiteID: $('#_paste_site_id').val(),
                            newBlockName: $('#_paste_block_name').val(),
                            csrfToken: csrfToken
                        }, function (data) {
                            if (data === 'error') {
                                $('#_copy_block_error').removeClass('hidden');
                            } else {
                                location.reload();
                            }
                        });
                    });
                });


                // default site to paste block
                $("#_paste_site_id option[value=" + site_id + "]").attr('selected', 'true');


                // logout
                $('#logout').on('click', function () {
                    $.post('/login/logOut/', {csrfToken: csrfToken}, function () {
                        location.reload();
                    });
                });


                // get js block code
                $('._get_block_code').on('click', function () {

                    var data = $(this).attr('data').split(',');

                    $.post('/blocks/apiGetBlockJsCode/', {
                        csrfToken: csrfToken,
                        siteId: data[0],
                        siteDomain: data[1],
                        blockId: data[2]
                    }, function (data, status) {
                        if (status == 'success') {
                            $('#textarea-standart-code').val(data);
                            $('#textarea-pc-code').val("<style>\n.__v-pc{display: none;}\n@media only screen and (min-width: 769px) {.__v-pc{display: block !important;}}\n</style>\n<div class='__v-pc'>\n" + data + "</div>");
                            $('#textarea-mobile-code').val("<style>\n.__v-mob{display: none;}\n@media only screen and (max-width: 768px) {.__v-mob{display: block !important;}}\n</style>\n<div class='__v-mob'>\n" + data + "</div>");
                            $('#_modal_get_code').modal();
                        }

                    });
                });

            });

        </script>
    </head>
    <body>
        <div class="container-fluid">
            <div class="row">
                <div class="col-xlg-10 col-xlg-offset-1 col-xs-12 col-xs-offset-0">
                    <?php include_once ('nav.php') ?>
                    <ol class="breadcrumb">
                        <li><a href="/sites/">Sites</a></li>
                        <li><?php echo $site['domain']; ?></li>
                    </ol>

                    <?php if ($blocks): ?>
                        <div class="panel panel-primary">
                            <div class="panel-heading"><b>Blocks</b></div>
                            <table class="table table-bordered table-striped">
                                <tbody>
                                    <?php
                                    $i = 1;
                                    foreach ($blocks as $item):
                                        ?>
                                        <tr style="height: 70px;">
                                            <td style="width: 50px; text-align: center; vertical-align: middle;"><?php echo $i++; ?></td>
                                            <td style="width: 250px; text-align: center; vertical-align: middle;"><span class="text-capitalize"><b><?php echo $item['block_name']; ?> (<?php echo $item['block_count_ads_width'] . 'x' . $item['block_count_ads_height']; ?>) </b></span> <span style="vertical-align: top;" class="label label-default">id: <?php echo $item['id']; ?></span></td>
                                            <td style="text-align: center; vertical-align: middle;"> Views: <b><?php echo $item['block_views']; ?></b> </td>
                                            <td style="text-align: center; vertical-align: middle;"> Clicks: <b><?php echo $item['block_clicks']; ?></b> </td>
                                            <td style="text-align: center; vertical-align: middle;"> CTR: <b><?php echo ($item['block_clicks'] == 0 OR ( $item['block_views'] == 0 AND $item['block_clicks'] == 0)) ? 0 : round(($item['block_clicks'] / $item['block_views']) * 100, 2); ?></b> </td>
                                            <td style="width: 500px; text-align: center; vertical-align: middle;">

                                                <div class="text-center">
                                                    <div class="btn-group">
                                                        <button name="<?php echo $item['id']; ?>" type="button" class="_play_block btn btn-default btn-sm <?php echo $item['block_status'] == 1 ? 'active' : ''; ?>"><i class="fa fa-play" aria-hidden="true"></i> Play</button>
                                                        <button name="<?php echo $item['id']; ?>" type="button" class="_stop_block btn btn-default btn-sm <?php echo $item['block_status'] == 0 ? 'active' : ''; ?>"><i class="fa fa-stop" aria-hidden="true"></i> Stop</button>
                                                        <a class="btn btn-default btn-sm" href="/stat/block/<?php echo $item['id']; ?>/"><i class="fa fa-bar-chart" aria-hidden="true"></i> Stats</a>
                                                        <a class="btn btn-default btn-sm" href="/sites/<?php echo $site['id']; ?>/blocks/<?php echo $item['id']; ?>/"><i class="fa fa-pencil" aria-hidden="true"></i> Edit</a>
                                                        <button name="<?php echo $item['block_name']; ?>" data="<?php echo $item['id']; ?>" class="_copy_block btn btn-default btn-sm"><i class="fa fa-clone" aria-hidden="true"></i> Copy</button>
                                                        <button data="<?php echo $site['id'] . ',' . $site['domain'] . ',' . $item['id']; ?>" class="_get_block_code btn btn-success btn-sm"><i class="fa fa-code" aria-hidden="true"></i> Get code</button>
                                                        <button name="<?php echo $item['block_name']; ?>" data="<?php echo $item['id']; ?>" class="_remove_block btn btn-danger btn-sm"><i class="fa fa-trash-o" aria-hidden="true"></i> Remove</button>
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                    <?php endforeach; ?>
                                </tbody>
                            </table>
                        </div>
                    <?php endif; ?>

                    <p>
                        <button class="btn btn-success center-block" data-toggle="modal" data-target="#_block_modal"><i class="fa fa-plus" aria-hidden="true"></i> Add block </button>
                    </p>



                </div>
            </div>
        </div>

        <!--start add block modal-->
        <div class="modal fade" id="_block_modal">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header header-success">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title"><i class="fa fa-plus-circle"></i> Add block</h4>
                    </div>
                    <div class="modal-body">
                        <div id="_add_block_error" class="alert alert-danger hidden"><i class="fa fa-exclamation-triangle"></i> </div>
                        <input id="_block_name" type="text" class="form-control" placeholder="Block name">
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default pull-left btn-sm" data-dismiss="modal"> Close</button>
                        <button id="_add_block_button" type="button" class="btn btn-success btn-sm"><i class="fa fa-check"></i> Create</button>
                    </div>
                </div>
            </div>
        </div>
        <!--end add block modal-->

        <!--start remove block modal-->
        <div class="modal fade" id="_remove_block_modal">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header header-danger">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title"><i class="fa fa-question-circle"></i> Remove block <b class="_remove_block_name text-capitalize"></b></h4>
                    </div>
                    <div class="modal-body">
                        Remove block?
                    </div>
                    <div class="modal-footer">
                        <button id="_remove_block_ok" class="btn btn-danger btn-sm pull-left"><i class="fa fa-trash-o" aria-hidden="true"></i> Remove block</button>
                        <button class="btn btn-default btn-sm" data-dismiss="modal"> Close</button>
                    </div>
                </div>
            </div>
        </div>
        <!--end remove block modal-->

        <!--start copy block modal-->
        <div class="modal fade" id="_copy_block_modal">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header header-success">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title"><i class="fa fa-question-circle"></i> Copy block <b class="_copy_block_name text-capitalize"></b></h4>
                    </div>
                    <div class="modal-body">
                        <div id="_copy_block_error" class="alert alert-danger hidden"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i> Copy block error!</div>
                        <select id="_paste_site_id" class="selectpicker form-group show-tick" data-width="100%" data-header="Select a site in the block to be copied">
                            <?php foreach ($sites as $item): ?>
                                <option value="<?php echo $item['id'] ?>" ><?php echo $item['domain'] ?></option>
                            <?php endforeach; ?>
                        </select>
                        <input id="_paste_block_name" type="text" class="form-control" placeholder="The name of the new block (can not be empty)" aria-describedby="basic-addon1">
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default pull-left btn-sm" data-dismiss="modal"> Close</button>
                        <button id="_copy_block_ok" type="button" class="btn btn-success btn-sm"><i class="fa fa-clone" aria-hidden="true"></i> Copy</button>
                    </div>
                </div>
            </div>
        </div>
        <!--end copy block modal-->


        <!-- Modal -->
        <div class="modal fade" id="_modal_get_code" tabindex="-1" role="dialog" >
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header header-success">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="myModalLabel"><i class="fa fa-check-circle" aria-hidden="true"></i> Get block code</h4>
                    </div>

                    <div class="modal-body">

                        <p> Copy this code to your website.</p>

                        <ul class="nav nav-tabs">
                            <li class="active"><a data-toggle="tab" href="#standart-block">Standart block</a></li>
                            <li><a data-toggle="tab" href="#pc-block">PC block</a></li>
                            <li><a data-toggle="tab" href="#mobile-block"> Mobile block</a></li>
                        </ul>

                        <div class="tab-content">
                            <div id="standart-block" class="tab-pane fade in active">
                                <br>
                                <p class="alert alert-info">
                                    <b>Standard block</b> - will be visible on all devices.
                                </p>

                                <textarea style="font-size: 11px;" id="textarea-standart-code" class="form-control" rows="20"><?php echo htmlspecialchars($blockJsCallCode); ?></textarea>
                            </div>
                            <div id="pc-block" class="tab-pane fade">
                                <br>
                                <p class="alert alert-info">
                                    <b>PC block</b> - will be visible on devices with a screen width MORE than 768px, the unit will be hidden if the screen width is LESS than 768px.
                                </p>
                                <textarea style="font-size: 11px;" id="textarea-pc-code" class="form-control" rows="20"></textarea>
                            </div>
                            <div id="mobile-block" class="tab-pane fade">

                                <br>
                                <p class="alert alert-info">
                                    <b>Mobile block</b> - will be visible on devices with a screen width of LESS than 768px, the unit will be hidden if the screen width is MORE than 768px.
                                </p>
                                <textarea style="font-size: 11px;" id="textarea-mobile-code" class="form-control" rows="20"></textarea>
                            </div>
                        </div>

                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default btn-sm pull-left" data-dismiss="modal">Close</button>
                        <button type="button" class="clipboard btn btn-sm btn-success" data-clipboard-target="#textarea-standart-code"><i class="fa fa-clipboard" aria-hidden="true"></i> Copy to clipboard</button>
                        <script>
                            $(document).on('shown.bs.tab', 'a[data-toggle="tab"]', function () {
                                var textarea_code_selector = $(".tab-pane.active").find("textarea").attr("id");
                                $(".clipboard").attr("data-clipboard-target", "#" + textarea_code_selector);
                            });
                            new Clipboard('.clipboard');
                        </script>
                    </div>



                    <!--                    <div class="modal-body">
                                            <p> Copy this code to your website.</p>
                                            <textarea id="textarea-code" class="form-control" rows="15"></textarea>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-default btn-sm pull-left" data-dismiss="modal">Close</button>
                                            <button type="button" class="clipboard btn btn-sm btn-success" data-clipboard-target="#textarea-code"><i class="fa fa-clipboard" aria-hidden="true"></i> Copy to clipboard</button>
                                            <script>new Clipboard('.clipboard');</script>

                                        </div>-->
                </div>
            </div>
        </div>


    </body>
</html>
