<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<!doctype html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <title>설정</title>
        <meta name="csrf-param" content="<?php echo $csrfToken; ?>">
        <link rel='stylesheet' href='/css/bootstrap.min.css' type='text/css' media='all'>
        <link rel='stylesheet' href='/css/bootstrap-dialog.css' type='text/css' media='all'>
        <link rel='stylesheet' href='/css/bootstrap-select.min.css' type='text/css' media='all'>
        <link rel='stylesheet' href='/css/font-awesome.min.css' type='text/css' media='all'>
        <link rel='stylesheet' href='/css/style.css' type='text/css' media='all'>
        <link rel="shortcut icon" href="/img/favicon.ico">
        <script src="/js/jquery.min.js"></script>
        <script src="/js/bootstrap.min.js"></script>
        <script src="/js/bootstrap-dialog.js"></script>
        <script src="/js/bootstrap-select.min.js"></script>
        <script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script>
        <script>
            $(document).ready(function () {

                // csrf token
                var csrfToken = $('[name="csrf-param"]').attr('content');


                // set default page shake
                if (location.href.split('#')[1] !== 'undefinded') {
                    $('#' +location.href.split('#')[1]).delay(300).effect("shake");
                }


                // get server time
                (function showTimes() {
                    $.post('/settings/apiGetServerTime/', {
                        csrfToken: csrfToken
                    }, function (serverTime) {
                        $('#server_time').text(serverTime);
                    });
                    var pcTime = new Date();
                    $('#pc_time').text(('0' +pcTime.getHours()).slice(-2) +":" +('0' +pcTime.getMinutes()).slice(-2));
                    setTimeout(showTimes, 10000);
                })();


                // get timezone
                $.post('/settings/apiGetServerTimezone/', {
                    csrfToken: csrfToken
                }, function (serverTimezone) {
                    $('#timezones option[value="' +serverTimezone +'"]').attr('selected', 'selected');
                    $('.selectpicker').selectpicker('refresh');
                });


                // set timezone
                $('#set_timezone').on('click', function () {
                    $(this).prop('disabled', true).html('<i class="fa fa-spinner fa-spin fa-fw"></i> Set timezone');
                    $.post('/settings/apiSetServerTimezone/', {
                        timezone: $('#timezones option:selected').val(),
                        csrfToken: csrfToken
                    }, function () {
                        location.reload();
                    });
                });


                // clear stat
                $('#clear_stat_button').on('click', function () {
                    if (!$('#clear_stat option:selected').val()) {
                        $('#error_modal').modal().find('.modal-title').text('오류');
                        $('#error_modal').modal().find('.modal-body').text('기간를 선택해주세요.');
                        return;
                    }
                    $('#clear_stat_button').prop('disabled', true).html('<i class="fa fa-spinner fa-spin fa-fw"></i>');
                    $.post('/settings/apiClearStat/', {
                        period: $('#clear_stat option:selected').val(),
                        csrfToken: csrfToken
                    }, function () {
                        location.reload();
                    });
                });


                // change password
                $('#change_pass').on('click', function () {
                    $.post('/settings/apiSetNewPassword/', {
                        current_pass: $('#current_pass').val(),
                        new_pass: $('#new_pass').val(),
                        csrfToken: csrfToken
                    }, function (response) {
                        if (response === 'fail_current_pass') {
                            $('#error_modal').modal();
                            $('#error_modal').find('.modal-title').text('오류');
                            $('#error_modal').find('.modal-body').text('현재 비밀번호가 빈값이거나 잘못된 값입니다.');
                        } else if (response === 'fail_new_pass') {
                            $('#error_modal').modal();
                            $('#error_modal').find('.modal-title').text('오류!');
                            $('#error_modal').find('.modal-body').text('새로은 비밀번호가 빈값이거나 잘못된 값입니다.');
                        } else if (response === 'pass_change_success') {
                            $('#info_modal').modal();
                            $('#info_modal').find('.modal-title').text('성공');
                            $('#info_modal').find('.modal-body').text('비밀번호 변경 성공!');
                            $('#current_pass, #new_pass').val('');
                        }
                    });
                });


                // get default (login) page
                $.post('/settings/apiGetDefaultPage/', {
                    csrfToken: csrfToken
                }, function (default_page) {
                    $('#default_pages option[value="' +default_page +'"]').attr('selected', 'selected');
                    $('.selectpicker').selectpicker('refresh');
                });


                // set default (login) page
                $('#set_default_page').on('click', function () {
                    $.post('/settings/apiSetDefaultPage/', {
                        default_page: $('#default_pages option:selected').val(),
                        csrfToken: csrfToken
                    }, function () {
                        location.reload();
                    });
                });


                // logout
                $('#logout').on('click', function () {
                    $.post('/login/logOut/', {
                        csrfToken: csrfToken
                    }, function () {
                        location.reload();
                    });
                });


                // show/hide input
                $('.show-hide-input').on('click', function () {
                    var input = $(this).siblings('input');
                    if (input.attr('type') === 'password') {
                        input.attr('type', 'text');
                        $(this).find('i').removeClass('fa-eye-slash').addClass('fa-eye');
                    } else {
                        input.attr('type', 'password');
                        $(this).find('i').removeClass('fa-eye').addClass('fa-eye-slash');
                    }
                });


            });
        </script>

    </head>
    <body>
        <div class="container-fluid">
            <div class="row">
                <div class="col-xs-12">
                    <?php include_once ('nav.php') ?>

                    <div style="padding:0;" class="col-xlg-10 col-xlg-offset-1 col-xs-12 col-xs-offset-0">
                        <div class="panel panel-primary">
                            <div class="panel-heading"><i class="fa fa-clock-o" aria-hidden="true"></i> 시간설정</div>
                            <div class="panel-body">
                                <div class="col-xs-12">
                                    <div style="position:relative;" class="alert alert-success text-center">
                                        <h3><i class="fa fa-clock-o" aria-hidden="true"></i> 서버시간: <b id="server_time"></b></h3>
                                        <span style="position:absolute; right: 10px; bottom: 5px;"><i class="fa fa-desktop" aria-hidden="true"></i> PC시간: <b id="pc_time"></b></span>
                                    </div>
                                    <div class="input-group form-group">
                                        <span class="input-group-addon"><i class="fa fa-globe" aria-hidden="true"></i> 타임존</span>
                                        <select id="timezones" class="selectpicker" data-style="btn-default btn-sm" data-width="100%" data-size="15" >
                                            <option value = "Pacific/Midway"> (GMT -11:00) 사모아 미드웨이 섬 </option>
                                            <option value = "America/Adak"> (GMT -10:00) Hawaii-Aleutian </option>
                                            <option value = "Etc/GMT+10"> (GMT -10:00) 하와이 </option>
                                            <option value = "Pacific/Marquesas"> (GMT -09:30) 마퀘 사스 제도 </option>
                                            <option value = "Pacific/Gambier"> (GMT -09:00) 갬 비어 제도 </option>
                                            <option value = "America/Anchorage"> (GMT -09:00) 알래스카 </option>
                                            <option value = "America/Ensenada"> (GMT -08:00) 티후아나, 바하 캘리포니아 </option>
                                            <option value = "Etc/GMT+8"> (GMT -08:00) 핏 케언 제도 </option>
                                            <option value = "America/Los_Angeles"> (GMT -08:00) 태평양 표준시 (미국 및 캐나다) </option>
                                            <option value = "America/Denver"> (GMT -07:00) 산지 표준시 (미국 및 캐나다) </option>
                                            <option value = "America/Chihuahua"> (GMT -07:00) 치와와, 라파즈, 마사 틀란 </option>
                                            <option value = "America/Dawson_Creek"> (GMT -07:00) 애리조나 </option>
                                            <option value = "America/Belize"> (GMT -06:00) 중앙 아메리카 서스 캐처 원 </option>
                                            <option value = "America/Cancun"> (GMT -06:00) 멕시코 시티, 과달라하라 </option>
                                            <option value = "Chile/EasterIsland"> (GMT -06:00) 이스터 아일랜드 </option>
                                            <option value = "America/Chicago"> (GMT -06:00) 중부 표준시 (미국 및 캐나다) </option>
                                            <option value = "America/New_York"> (GMT -05:00) 동부 표준시 (미국 및 캐나다) </option>
                                            <option value = "America/Havana"> (GMT -05:00) 쿠바 </option>
                                            <option value = "America/Bogota"> (GMT -05:00) 보고타, 리마, 키토, 리오 브랑코 </option>
                                            <option value = "America/Caracas"> (GMT -04:30) 카라카스 </option>
                                            <option value = "America/Santiago"> (GMT -04:00) 산티아고 </option>
                                            <option value = "America/La_Paz"> (GMT -04:00) 라 파스 </option>
                                            <option value = "Atlantic/Stanley"> (GMT -04:00) 포클랜드 제도 </option>
                                            <option value = "America/Campo_Grande"> (GMT -04:00) 브라질 </option>
                                            <option value = "America/Goose_Bay"> (GMT -04:00) 대서양 표준시 (Goose Bay) </option>
                                            <option value = "America/Glace_Bay"> (GMT -04:00) 대서양 표준시 (캐나다) </option>
                                            <option value = "America/St_Johns"> (GMT -03:30) 뉴 펀들 랜드 </option>
                                            <option value = "America/Araguaina"> (GMT -03:00) UTC-3 </option>
                                            <option value = "America/Montevideo"> (GMT -03:00) 몬테비데오 </option>
                                            <option value = "America/Miquelon"> (GMT -03:00) 세인트 피에르 미 클롱 </option>
                                            <option value = "America/Godthab"> (GMT -03:00) 그린란드 </option>
                                            <option value = "America/Argentina/Buenos_Aires"> (GMT -03:00) 부에노스 아이레스 </option>
                                            <option value = "America/Sao_Paulo"> (GMT -03:00) 브라질리아 </option>
                                            <option value = "America/Noronha"> (GMT -02:00) 대서양 중동 </option>
                                            <option value = "Atlantic/Cape_Verde"> (GMT -01:00) 카보 베르데 섬 </option>
                                            <option value = "Atlantic/Azores"> (GMT -01:00) 아 조레스 제도</option>
                                            <option value = "Europe/London"> (GMT) 그리니치 표준시 </option>
                                            <option value = "Europe/Amsterdam"> (GMT +01:00) 암스테르담, 베를린, 베른, 로마, 스톡홀름, 비엔나 </option>
                                            <option value = "Europe/Belgrade"> (GMT +01:00) 베오그라드, 브라 티 슬라바, 부다페스트, 류블 랴나, 프라하 </option>
                                            <option value = "Europe/Brussels"> (GMT +01:00) 브뤼셀, 코펜하겐, 마드리드, 파리 </option>
                                            <option value = "Africa/Algiers"> (GMT +01:00) 서부 중앙 아프리카 </option>
                                            <option value = "Africa/Windhoek"> (GMT +01:00) 빈트 후크 </option>
                                            <option value = "Asia/Beirut"> (GMT +02:00) 베이루트 </option>
                                            <option value = "Africa/Cairo"> (GMT +02:00) 카이로 </option>
                                            <option value = "Asia/Gaza"> (GMT +02:00) 가자 </option>
                                            <option value = "Africa/Blantyre"> (GMT +02:00) Harare, Pretoria </option>
                                            <option value = "Asia/Jerusalem"> (GMT +02:00) 예루살렘 </option>
                                            <option value = "Europe/Minsk"> (GMT +02:00) 민스크 </option>
                                            <option value = "Asia/Damascus"> (GMT +02:00) 시리아 </option>
                                            <option value = "Europe/Moscow"> (GMT +03:00) 모스크바, 상트 페테르부르크, 볼고그라드 </option>
                                            <option value = "Africa/Addis_Ababa"> (GMT +03:00) 나이로비 </option>
                                            <option value = "Asia/Tehran"> (GMT +03:30) 테헤란 </option>
                                            <option value = "Asia/Dubai"> (GMT +04:00) 아부 다비, 무스카트 </option>
                                            <option value = "Asia/Yerevan"> (GMT +04:00) 예 레반 </option>
                                            <option value = "Asia/Kabul"> (GMT +04:30) 카불 </option>
                                            <option value = "Asia/Yekaterinburg"> (GMT +05:00) 예 카테 린 부르크 </option>
                                            <option value = "Asia/Tashkent"> (GMT +05:00) 타슈켄트 </option>
                                            <option value = "Asia/Kolkata"> (GMT +05:30) 첸나이, 콜카타, 뭄바이, 뉴 델리 </option>
                                            <option value = "Asia/Katmandu"> (GMT +05:45) 카트만두 </option>
                                            <option value = "Asia/Dhaka"> (GMT +06:00) 아스타나, 다카 </option>
                                            <option value = "Asia/Novosibirsk"> (GMT +06:00) 노보시비르스크 </option>
                                            <option value = "Asia/Rangoon"> (GMT +06:30) 양곤 (랑군) </option>
                                            <option value = "Asia/Bangkok"> (GMT +07:00) 방콕, 하노이, 자카르타 </option>
                                            <option value = "Asia/Krasnoyarsk"> (GMT +07:00) 크라스 노야 르 스크 </option>
                                            <option value = "Asia/Hong_Kong"> (GMT +08:00) 북경, 충칭, 홍콩, 우루무치 </option>
                                            <option value = "Asia/Irkutsk"> (GMT +08:00) 이르쿠츠크, 울란 바타르 </option>
                                            <option value = "Australia/Perth"> (GMT +08:00) 퍼스 </option>
                                            <option value = "Australia/Eucla"> (GMT +08:45) 유클라 </option>
                                            <option value = "Asia/Tokyo"> (GMT +09:00) 오사카, 삿포로, 도쿄 </option>
                                            <option value = "Asia/Seoul"> (GMT +09:00) 서울 </option>
                                            <option value = "Asia/Yakutsk"> (GMT +09:00) 야쿠 츠크 </option>
                                            <option value = "Australia/Adelaide"> (GMT +09:30) 애들레이드 </option>
                                            <option value = "Australia/Darwin"> (GMT +09:30) 다윈 </option>
                                            <option value = "Australia/Brisbane"> (GMT +10:00) 브리즈번 </option>
                                            <option value = "Australia/Hobart"> (GMT +10:00) 호바트 </option>
                                            <option value = "Asia/Vladivostok"> (GMT +10:00) 블라디보스토크 </option>
                                            <option value = "Australia/Lord_Howe"> (GMT +10:30) 로드 하우 섬</option>
                                            <option value = "Etc/GMT-11"> (GMT +11:00) 솔로몬 섬, 뉴 칼레도니아 </option>
                                            <option value = "Asia/Magadan"> (GMT +11:00) 마가 단 </option>
                                            <option value = "Pacific/Norfolk"> (GMT +11:30) 노퍽 섬 </option>
                                            <option value = "Asia/Anadyr"> (GMT +12:00) 아나디르,, 캄차카 </option>
                                            <option value = "Pacific/Auckland"> (GMT +12:00) 오클랜드, 웰링턴 </option>
                                            <option value = "Etc/GMT-12"> (GMT +12:00) 피지, 캄차카, 마샬아일랜드. </option>
                                            <option value = "Pacific/Chatham"> (GMT +12:45) 채텀 아일랜드 </option>
                                            <option value = "Pacific/Tongatapu"> (GMT +13:00) 누쿠 알로 파 </option>
                                            <option value = "Pacific/Kiritimati"> (GMT +14:00) 키리 티마티 </option>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <button id="set_timezone" type="button" class="btn btn-danger btn-sm"><i class="fa fa-check fa-fw" aria-hidden="true"></i> 타임존수정</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div style="padding:0;" class="col-xlg-10 col-xlg-offset-1 col-xs-12 col-xs-offset-0 no-top-margin">
                        <div class="panel panel-primary border-noradius">
                            <div class="panel-heading border-noradius"> <i class="fa fa-anchor" aria-hidden="true"></i> 기본페이지 설정</div>
                            <div class="panel-body">
                                <div class="col-xs-12">
                                    <div id="def_pages" class="input-group form-group">
                                        <span class="input-group-addon">기본페이지</span>
                                        <select id="default_pages" class="selectpicker" data-style="btn-default btn-sm" data-width="100%" title="Select default page..." >
                                            <option value="sites">사이트</option>
                                            <option value="campaigns">캠패인</option>
                                            <option value="stat">통계</option>
                                            <option value="main">메인(빈값)</option>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <button id="set_default_page" type="button" class="btn btn-danger btn-sm"><i class="fa fa-check" aria-hidden="true"></i> 기본페이지 설정</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div style="padding:0;" class="col-xlg-10 col-xlg-offset-1 col-xs-12 col-xs-offset-0 no-top-margin">
                        <div class="panel panel-primary border-noradius">
                            <div class="panel-heading border-noradius"> <i class="fa fa-database" aria-hidden="true"></i> 통계 저장소 설정</div>
                            <div class="panel-body">
                                <div class="col-xs-12">
                                    <div class="input-group form-group">
                                        <span class="input-group-addon">기간선택</span>
                                        <select id="clear_stat" class="selectpicker" data-style="btn-default btn-sm" data-width="100%" title="기간을 선택하십시오 ..." >
                                            <option value="week">1주 이상</option>
                                            <option value="2week">2주 이상</option>
                                            <option value="month">한달 이상</option>
                                            <option value="year">1년 이상</option>
                                            <option data-divider="true"></option>
                                            <option value="all">모든 통계 지우기</option>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <button id="clear_stat_button" type="button" class="btn btn-warning btn-sm"><i class="fa fa-trash" aria-hidden="true"></i> 통계 청소시작!</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div style="padding:0;" class="col-xlg-10 col-xlg-offset-1 col-xs-12 col-xs-offset-0 no-top-margin">
                        <div style="min-height: 200px;" class="panel panel-primary border-noradius">
                            <div class="panel-heading border-noradius"> <i class="fa fa-lock" aria-hidden="true"></i> 비밀번호 설정</div>
                            <div class="panel-body">
                                <div class="col-xs-12">
                                    <div class="form-group input-group input-group-sm ">
                                        <span style="min-width: 150px;" class="input-group-addon" id="basic-addon1">현재 비밀번호</span>
                                        <input id="current_pass" type="password" class="form-control" placeholder="">
                                        <span class="show-hide-input input-group-btn"><button class="btn btn-default" type="button"><i class="fa fa-eye-slash" aria-hidden="true"></i></button></span>
                                    </div>
                                    <div  class="form-group input-group input-group-sm">
                                        <span style="min-width: 150px;" class="input-group-addon" id="basic-addon1">새로운 비밀번호</span>
                                        <input id="new_pass" type="password" class="form-control" placeholder="">
                                        <span class="show-hide-input input-group-btn"><button class="btn btn-default" type="button"><i class="fa fa-eye-slash" aria-hidden="true"></i></button></span>
                                    </div>
                                    <button id="change_pass" type="button" class="btn btn-danger btn-sm"><i class="fa fa-check" aria-hidden="true"></i> 비밀번호 변경</button>
                                </div>
                            </div>
                        </div>
                    </div>

                    <!--start modal-->
                    <div class="modal fade" id="info_modal">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header header-success">
                                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                                    <h4 class="modal-title"></h4>
                                </div>
                                <div class="modal-body"></div>
                            </div>
                        </div>
                    </div>
                    <!--end modal-->

                    <!--start modal-->
                    <div class="modal fade" id="error_modal">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header header-danger">
                                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                                    <h4 class="modal-title"></h4>
                                </div>
                                <div class="modal-body"></div>
                            </div>
                        </div>
                    </div>
                    <!--end modal-->

                </div>
            </div>
        </div>
    </body>
</html>
