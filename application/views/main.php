<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<!doctype html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <title>메인</title>
        <meta name="csrf-param" content="<?php echo $csrfToken; ?>">
        <link rel='stylesheet' href='/css/bootstrap.min.css' type='text/css' media='all'>
        <link rel='stylesheet' href='/css/bootstrap-dialog.css' type='text/css' media='all'>
        <link rel='stylesheet' href='/css/bootstrap-select.min.css' type='text/css' media='all'>
        <link rel='stylesheet' href='/css/bootstrap-colorpicker.min.css' type='text/css' media='all'>
        <link rel='stylesheet' href='/css/font-awesome.min.css' type='text/css' media='all'>
        <link rel='stylesheet' href='/css/style.css' type='text/css' media='all'>
        <link rel="shortcut icon" href="/img/favicon.ico">
        <script src="/js/jquery.min.js"></script>
        <script src="/js/bootstrap.min.js"></script>
        <script src="/js/bootstrap-dialog.js"></script>
        <script src="/js/bootstrap-select.min.js"></script>
        <script src="/js/bootstrap-colorpicker.min.js"></script>
        <script src="/js/clipboard.min.js"></script>
        <script>
            $(document).ready(function () {

                // csrf token
                var csrfToken = $('[name="csrf-param"]').attr('content');

                // center button
                $(window).resize(function () {
                    $('#sett').css({
                        position: 'fixed',
                        left: ($(window).width() - $('#sett').outerWidth()) / 2,
                        top: ($(window).height() - $('#sett').outerHeight()) / 2
                    });
                });
                $(window).resize();


                // logout
                $('#logout').on('click', function () {
                    $.post('/login/logOut/', {csrfToken: csrfToken}, function () {
                        location.reload();
                    });
                });

            });

        </script>
    </head>
    <body>
        <div class="container-fluid">
            <div class="row">
                <div class="col-xs-12">
                    <?php include_once ('nav.php') ?>
                    <a id="sett" class="btn btn-default" href="/settings/#def_pages"><i class="fa fa-cogs"></i> 기본페이지 설정</a>
                </div>
            </div>
        </div>
    </body>
</html>
