<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

<div style="border:1px solid #990000;padding-left:20px;margin:0 0 10px 0;">

<h4>PHP에서 오류가 발생하였습니다.</h4>

<p>Severity: <?php echo $severity; ?></p>
<p>메세지:  <?php echo $message; ?></p>
<p>파일이름: <?php echo $filepath; ?></p>
<p>라인넘버: <?php echo $line; ?></p>

<?php if (defined('SHOW_DEBUG_BACKTRACE') && SHOW_DEBUG_BACKTRACE === TRUE): ?>

	<p>역추적:</p>
	<?php foreach (debug_backtrace() as $error): ?>

		<?php if (isset($error['file']) && strpos($error['file'], realpath(BASEPATH)) !== 0): ?>

			<p style="margin-left:10px">
			파일: <?php echo $error['file'] ?><br />
			라인: <?php echo $error['line'] ?><br />
			함수(기능): <?php echo $error['function'] ?>
			</p>

		<?php endif ?>

	<?php endforeach ?>

<?php endif ?>

</div>