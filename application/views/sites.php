<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<!doctype html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <title>Sites</title>
        <meta name="csrf-param" content="<?php echo $csrfToken; ?>">
        <link rel='stylesheet' href='/css/bootstrap.min.css' type='text/css' media='all'>
        <link rel='stylesheet' href='/css/bootstrap-dialog.css' type='text/css' media='all'>
        <link rel='stylesheet' href='/css/font-awesome.min.css' type='text/css' media='all'>
        <link rel='stylesheet' href='/css/style.css' type='text/css' media='all'>
        <link rel="shortcut icon" href="/img/favicon.ico">
        <script src="/js/jquery.min.js"></script>
        <script src="/js/bootstrap.min.js"></script>
        <script src="/js/bootstrap-dialog.js"></script>
        <script>
            $(document).ready(function () {

                // csrf token
                var csrfToken = $('[name="csrf-param"]').attr('content');
                // add site
                $('#_add_site_button').on('click', function () {
                    $('#_add_site_button').prop('disabled', true).html('<i class="fa fa-spinner fa-spin fa-fw"></i> OK');
                    $.post('/sites/apiAddSite/', {
                        domain: $('#_add_domain').val(),
                        csrfToken: csrfToken
                    }, function (data) {
                        var response = $.parseJSON(data);
                        if (response.status === 'error') {
                            $('#error_msg').text(response.msg).removeClass('hidden');
                            $('#_add_site_button').prop('disabled', false).html('<i class="fa fa-check" aria-hidden="true"></i> OK');
                        } else if (response.status === 'ok') {
                            location.reload();
                        }
                    });
                });
                // remove site
                $('._remove_site').on('click', function () {
                    var id = $(this).attr('data');
                    var domain = $(this).attr('name');
                    $('._remove_domain').text(domain);
                    $('#_remove_site_modal').removeClass('hidden').modal();
                    $('#_remove_site_button').on('click', function () {
                        $('#_remove_site_button').prop('disabled', true).html('<i class="fa fa-spinner fa-spin fa-fw"></i> Remove site and blocks');
                        $.post('/sites/apiRemoveSite/', {
                            siteID: id,
                            csrfToken: csrfToken
                        }, function () {
                            location.reload();
                        });
                    });
                });
                // play site
                $('._play_site').on('click', function () {
                    $.post('/sites/apiPlaySite/', {
                        siteID: $(this).attr('name'),
                        csrfToken: csrfToken
                    }, function () {
                        location.reload();
                    });
                });
                // stop site
                $('._stop_site').on('click', function () {
                    $.post('/sites/apiStopSite/', {
                        siteID: $(this).attr('name'),
                        csrfToken: csrfToken
                    }, function () {
                        location.reload();
                    });
                });
                // logout
                $('#logout').on('click', function () {
                    $.post('/login/logOut/', {
                        csrfToken: csrfToken
                    }, function () {
                        location.reload();
                    });
                });
            });
        </script>
    </head>
    <body>
        <div class="container-fluid">
            <div class="row">
                <div class="col-xlg-10 col-xlg-offset-1 col-xs-12 col-xs-offset-0">
                    <?php include_once ('nav.php') ?>
                    <?php if ($sites): ?>
                        <div class="panel panel-primary">
                            <div class="panel-heading"><b>Sites</b></div>
                            <table class="table table-bordered table-striped">
                                <tbody>
                                    <?php
                                    $i = 1;
                                    foreach ($sites as $item):
                                        ?>
                                        <tr style="height: 70px;">
                                            <td style="width: 50px; text-align: center; vertical-align: middle;"><?php echo $i++; ?></td>
                                            <td style="width: 250px; text-align: center; vertical-align: middle;">
                                                <span><b><?php echo $item['domain']; ?></b></span> <span style="vertical-align: top;" class="label label-default">id: <?php echo $item['id']; ?></span>
                                                <a href="http://<?php echo $item['domain']; ?>" target="_blank"><i class="fa fa-external-link" aria-hidden="true"></i></a>
                                            </td>
                                            <td style="text-align: center; vertical-align: middle;"> Views: <b><?php echo $item['site_views']; ?></b> </td>
                                            <td style="text-align: center; vertical-align: middle;"> Clicks: <b><?php echo $item['site_clicks']; ?></b> </td>
                                            <td style="text-align: center; vertical-align: middle;"> CTR: <b><?php echo ($item['site_clicks'] == 0 OR ( $item['site_views'] == 0 AND $item['site_clicks'] == 0)) ? 0 : round(($item['site_clicks'] / $item['site_views']) * 100, 2); ?></b> </td>
                                            <td style="width: 390px; text-align: center; vertical-align: middle;">
                                                <div class="text-center">
                                                    <a class="btn btn-warning btn-sm" href="/sites/<?php echo $item['id']; ?>/blocks/"> Blocks (<?php echo $item['count_blocks']; ?>)</a>
                                                    <div class="btn-group ">
                                                        <button name="<?php echo $item['id']; ?>" type="button" class="_play_site btn btn-default btn-sm <?php echo $item['status'] == 1 ? 'active' : ''; ?>"><i class="fa fa-play" aria-hidden="true"></i> Play</button>
                                                        <button name="<?php echo $item['id']; ?>" type="button" class="_stop_site btn btn-default btn-sm <?php echo $item['status'] == 0 ? 'active' : ''; ?>"><i class="fa fa-stop" aria-hidden="true"></i> Stop</button>
                                                        <a class="btn btn-default btn-sm" href="/stat/site/<?php echo $item['id']; ?>/"><i class="fa fa-bar-chart" aria-hidden="true"></i> Stats</a>
                                                        <button name="<?php echo $item['domain']; ?>" data="<?php echo $item['id']; ?>" class="_remove_site btn btn-danger btn-sm"><i class="fa fa-trash-o" aria-hidden="true"></i> Remove</button>
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                    <?php endforeach; ?>
                                </tbody>
                            </table>
                        </div>
                    <?php endif; ?>

                    <p>
                        <button class="btn btn-success center-block" data-toggle="modal" data-target="#_add_site_modal"><i class="fa fa-plus" aria-hidden="true"></i> Add site </button>
                    </p>
                    <!--start modal add site-->
                    <div class="modal fade" id="_add_site_modal">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header header-success">
                                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                                    <h4 class="modal-title"><i class="fa fa-plus-circle"></i> Add site</h4>
                                </div>
                                <div class="modal-body">
                                    <div id="error_msg" class="alert alert-danger hidden" role="alert"></div>


                                    <?php if (empty($_SERVER['HTTPS'])): ?>

                                        <div class="alert alert-danger" role="alert">
                                            Warning!<br>
                                            If the site you are adding is running <b>HTTPS</b><br>
                                            Then the ADFLEX system should also work on the <b>HTTPS</b> protocol.<br>
                                            Otherwise, the ad units will not be visible.
                                        </div>

                                    <?php endif; ?>

                                    <div class="input">
<!--                                        <span class="input-group-addon">www.</span>-->
                                        <input id="_add_domain" type="text" class="form-control" placeholder="domain.com">
                                    </div>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-default pull-left btn-sm" data-dismiss="modal"> Close</button>
                                    <button id="_add_site_button" type="button" class="btn btn-success btn-sm"><i class="fa fa-check" aria-hidden="true"></i> OK</button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--end modal add site-->

                    <!--start modal remove site-->
                    <div class="modal fade hidden" id="_remove_site_modal">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header header-danger">
                                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                                    <h4 class="modal-title"><i class="fa fa-question-circle"></i> Remove site <b class="_remove_domain text-capitalize"></b></h4>
                                </div>
                                <div class="modal-body">
                                    Remove the site? Also deletes all ad blocks!
                                </div>
                                <div class="modal-footer">
                                    <button id="_remove_site_button" class="btn btn-danger btn-sm pull-left"><i class="fa fa-trash-o" aria-hidden="true"></i> Remove site and blocks</button>
                                    <button type="button" class="btn btn-default btn-sm" data-dismiss="modal"> Close</button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--end modal remove site-->
                </div>
            </div>
        </div>
    </body>
</html>
