<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<!doctype html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <title>비밀번호 초기화</title>
        <meta name="csrf-param" content="<?php echo $csrfToken; ?>">
        <link rel='stylesheet' href='/css/bootstrap.min.css' type='text/css' media='all'>
        <link rel='stylesheet' href='/css/bootstrap-dialog.css' type='text/css' media='all'>
        <link rel='stylesheet' href='/css/bootstrap-select.min.css' type='text/css' media='all'>
        <link rel='stylesheet' href='/css/bootstrap-colorpicker.min.css' type='text/css' media='all'>
        <link rel='stylesheet' href='/css/font-awesome.min.css' type='text/css' media='all'>
        <link rel='stylesheet' href='/css/style.css' type='text/css' media='all'>
        <link rel="shortcut icon" href="/img/favicon.ico">
        <script src="/js/jquery.min.js"></script>
        <script src="/js/bootstrap.min.js"></script>
        <script src="/js/bootstrap-dialog.js"></script>
        <script src="/js/bootstrap-select.min.js"></script>
        <script src="/js/bootstrap-colorpicker.min.js"></script>
        <script src="/js/clipboard.min.js"></script>
        <script>
            $(document).ready(function () {

                // csrf token
                var csrfToken = $('[name="csrf-param"]').attr('content');

                // set new password
                $('#save_new_password_button').on('click', function () {
                    $('#save_new_password_button').prop('disabled', true).html('<i class="fa fa-spinner fa-spin fa-fw"></i>');
                    $.post('/login/apiSetNewPass/', {
                        new_pass: $('#new_password').val(),
                        reset_token: location.href.split('/')[4],
                        csrfToken: csrfToken
                    }, function (data) {
                        var response = $.parseJSON(data);
                        if (response.status === 'ok') {
                            $('#new_pass_block').addClass('hidden');
                            $('#new_pass_sucsess_block').removeClass('hidden');
                        } else {
                            $('#save_new_password_button').prop('disabled', false).html('SAVE');
                            $('#error_new_pass').text(response.msg).removeClass('hidden');
                        }
                    });

                });


                // show/hide password
                $('#show_hide_pass').on('click', function () {
                    if ($('#new_password').attr('type') === 'password') {
                        $('#new_password').attr('type', 'text');
                        $('#show_hide_pass > i').removeClass('fa-eye-slash').addClass('fa-eye');

                    } else if ($('#new_password').attr('type') === 'text') {
                        $('#new_password').attr('type', 'password');
                        $('#show_hide_pass > i').removeClass('fa-eye').addClass('fa-eye-slash');
                    }
                });

            });

        </script>
    </head>
    <body>
        <div class="container">
            <div id="new_pass_block" class="row vcenter">
                <div class="col-xs-4 col-xs-offset-4 ">
                    <div class="panel panel-primary">
                        <div class="panel-heading">New password <span class="pull-right"><img src="/img/logo.png" height="14"></span></div>
                        <div class="panel-body text-center">
                            <div id="error_new_pass" class="hidden alert alert-danger"><i class="fa fa-exclamation-triangle"></i></div>
                            <div class="input-group form-group">
                                <input id="new_password" type="password" class="form-control form-group" placeholder="New password (At least 8 characters)" autocomplete="off">
                                <span class="input-group-btn">
                                    <button id="show_hide_pass" class="btn btn-default" type="button"><i class="fa fa-eye-slash" aria-hidden="true"></i></button>
                                </span>
                            </div>
                            <button id="save_new_password_button" type="button" class="btn btn-success btn-block">SAVE</button>
                        </div>
                    </div>
                </div>
            </div>
            <div id="new_pass_sucsess_block" class="hidden row vcenter">
                <div class="col-xs-4 col-xs-offset-4 ">
                    <div class="panel panel-primary">
                        <div class="panel-heading">Successful <span class="pull-right"><img src="/img/logo.png" height="14"></span></div>
                        <div class="panel-body text-center">
                            <h4>The password change is successful.</h4>
                            <a href="/" class="btn btn-success btn-block">LOG IN</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>
