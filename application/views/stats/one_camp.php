<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<!doctype html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <title>Stat - </title>
        <meta name="csrf-param" content="<?php echo $csrfToken; ?>">
        <link rel='stylesheet' href='/css/bootstrap.min.css' type='text/css' media='all'>
        <link rel='stylesheet' href='/css/bootstrap-dialog.css' type='text/css' media='all'>
        <link rel='stylesheet' href='/css/bootstrap-datepicker3.min.css' type='text/css' media='all'>
        <link rel='stylesheet' href='/css/font-awesome.min.css' type='text/css' media='all'>
        <link rel='stylesheet' href='/css/style.css' type='text/css' media='all'>
        <link rel='stylesheet' href='/css/icons.css' type='text/css' media='all'>
        <link rel="shortcut icon" href="/img/favicon.ico">
        <script src="/js/jquery.min.js"></script>
        <script src="/js/bootstrap.min.js"></script>
        <script src="/js/bootstrap-dialog.js"></script>
        <script src="/js/bootstrap-datepicker.min.js"></script>
        <script src="/js/stat.js"></script>
        <script>
            $(document).ready(function () {

                // camp id
                var campID = location.href.split('/')[5];


                // csrf token
                var csrfToken = $('[name="csrf-param"]').attr('content');


                // Parse stat (default for today)
                $.post('/stat/apiGetStatOneCamp/', {
                    campID: campID,
                    csrfToken: csrfToken
                }, function (data) {
                    var statObj = saveGetStatObj($.parseJSON(data));

                    $('title').text($('title').text() + statObj.camp_name);
                    $('#button_camps').text(statObj.camp_name);
                    $('#range_dates').text(statObj.date);


                    showDropDownListSites(statObj);
                    showDropDownListCamps(statObj);

                    showTotalOneCampStatTable(statObj);
                    showAdsStatTable(statObj, 'views');
                    showDevsStatTable(statObj, 'views');
                    showOsStatTable(statObj, 'views');
                    showBrowsersStatTable(statObj, 'views');
                    showGeoStatTable(statObj, 'views');
                    showAdPrevHover(statObj);
                });


                // ips stat
                $.post('/stat/apiGetStatIp/', {
                    column: 'camp_id',
                    columnValue: campID,
                    csrfToken: csrfToken
                }, function (data) {
                    var ipsStatObj = saveGetStatObj($.parseJSON(data), 'ip_stat_obj');
                    showIpStatTable(ipsStatObj, 'views');
                });


                // Stat for today
                $('#today').on('click', function () {
                    load();
                    $('#today, #yesterday, #last_7, #last_30, #range').removeClass('active');
                    $(this).addClass('active');
                    $('#sort_views, #sort_clicks, #sort_ctr').removeClass('active');
                    $('#sort_views').addClass('active');

                    $.post('/stat/apiGetStatOneCamp/', {
                        campID: campID,
                        period: 'today',
                        csrfToken: csrfToken
                    }, function (data) {
                        var statObj = saveGetStatObj($.parseJSON(data));

                        $('#range_dates').text(statObj.date);

                        showTotalOneCampStatTable(statObj);
                        showAdsStatTable(statObj, 'views');
                        showDevsStatTable(statObj, 'views');
                        showOsStatTable(statObj, 'views');
                        showBrowsersStatTable(statObj, 'views');
                        showGeoStatTable(statObj, 'views');
                        showAdPrevHover(statObj);

                        $.post('/stat/apiGetStatIp/', {
                            column: 'camp_id',
                            columnValue: campID,
                            csrfToken: csrfToken
                        }, function (data) {
                            var ipsStatObj = saveGetStatObj($.parseJSON(data), 'ip_stat_obj');
                            showIpStatTable(ipsStatObj, 'views');
                        });
                    });
                });


                // Stat for yesterday
                $('#yesterday').on('click', function () {
                    load();
                    $('#ip_table').hide();
                    $('#today, #yesterday, #last_7, #last_30, #range').removeClass('active');
                    $(this).addClass('active');
                    $('#sort_views, #sort_clicks, #sort_ctr').removeClass('active');
                    $('#sort_views').addClass('active');

                    $.post('/stat/apiGetStatOneCamp/', {
                        campID: campID,
                        period: 'yesterday',
                        csrfToken: csrfToken
                    }, function (data) {
                        var statObj = saveGetStatObj($.parseJSON(data));

                        $('#range_dates').text(statObj.date);

                        showTotalOneCampStatTable(statObj);
                        showAdsStatTable(statObj, 'views');
                        showDevsStatTable(statObj, 'views');
                        showOsStatTable(statObj, 'views');
                        showBrowsersStatTable(statObj, 'views');
                        showGeoStatTable(statObj, 'views');
                        showAdPrevHover(statObj);
                    });
                });


                // Stats in the last 7 days
                $('#last_7').on('click', function () {
                    load();
                    $('#ip_table').hide();
                    $('#today, #yesterday, #last_7, #last_30, #range').removeClass('active');
                    $(this).addClass('active');
                    $('#sort_views, #sort_clicks, #sort_ctr').removeClass('active');
                    $('#sort_views').addClass('active');

                    $.post('/stat/apiGetStatOneCamp/', {
                        campID: campID,
                        period: 'last_7',
                        csrfToken: csrfToken
                    }, function (data) {
                        var statObj = saveGetStatObj($.parseJSON(data));

                        $('#range_dates').text(statObj.date);

                        showTotalOneCampStatTable(statObj);
                        showAdsStatTable(statObj, 'views');
                        showDevsStatTable(statObj, 'views');
                        showOsStatTable(statObj, 'views');
                        showBrowsersStatTable(statObj, 'views');
                        showGeoStatTable(statObj, 'views');
                        showAdPrevHover(statObj);
                    });
                });


                // Stats in the last 30 days
                $('#last_30').on('click', function () {
                    load();
                    $('#ip_table').hide();
                    $('#today, #yesterday, #last_7, #last_30, #range').removeClass('active');
                    $(this).addClass('active');
                    $('#sort_views, #sort_clicks, #sort_ctr').removeClass('active');
                    $('#sort_views').addClass('active');

                    $.post('/stat/apiGetStatOneCamp/', {
                        campID: campID,
                        period: 'last_30',
                        csrfToken: csrfToken
                    }, function (data) {
                        var statObj = saveGetStatObj($.parseJSON(data));

                        $('#range_dates').text(statObj.date);

                        showTotalOneCampStatTable(statObj);
                        showAdsStatTable(statObj, 'views');
                        showDevsStatTable(statObj, 'views');
                        showOsStatTable(statObj, 'views');
                        showBrowsersStatTable(statObj, 'views');
                        showGeoStatTable(statObj, 'views');
                        showAdPrevHover(statObj);
                    });
                });


                // Stat date range
                $('#select_date_ok').on('click', function () {
                    load();
                    $('#ip_table').hide();
                    $('#today, #yesterday, #last_7, #last_30, #range').removeClass('active');
                    $('#range').addClass('active');
                    $('#sort_views, #sort_clicks, #sort_ctr').removeClass('active');
                    $('#sort_views').addClass('active');

                    var range = $('#start_date').val().replace(/-/g, '') + '-' + $('#end_date').val().replace(/-/g, '');
                    $.post('/stat/apiGetStatOneCamp/', {
                        campID: campID,
                        period: range,
                        csrfToken: csrfToken
                    }, function (data) {
                        var statObj = saveGetStatObj($.parseJSON(data));

                        $('#range_dates').text(statObj.date);

                        showTotalOneCampStatTable(statObj);
                        showAdsStatTable(statObj, 'views');
                        showDevsStatTable(statObj, 'views');
                        showOsStatTable(statObj, 'views');
                        showBrowsersStatTable(statObj, 'views');
                        showGeoStatTable(statObj, 'views');
                        showAdPrevHover(statObj);
                        $('#select_date_modal').modal('hide');
                    });
                });


                // sort by views
                $('#sort_views').on('click', function () {
                    $(this).addClass('active');
                    $('#sort_clicks, #sort_ctr').removeClass('active');
                    var statObj = getStatObj();

                    showAdsStatTable(statObj, 'views');
                    showDevsStatTable(statObj, 'views');
                    showOsStatTable(statObj, 'views');
                    showBrowsersStatTable(statObj, 'views');
                    showGeoStatTable(statObj, 'views');
                    showAdPrevHover(statObj);

                    if ($('#today').hasClass('active')) {
                        var ipsStatObj = getStatObj('ip_stat_obj');
                        showIpStatTable(ipsStatObj, 'views');
                    }
                });


                // sort by clicks
                $('#sort_clicks').on('click', function () {
                    $(this).addClass('active');
                    $('#sort_views, #sort_ctr').removeClass('active');
                    var statObj = getStatObj();

                    showAdsStatTable(statObj, 'clicks');
                    showDevsStatTable(statObj, 'clicks');
                    showOsStatTable(statObj, 'clicks');
                    showBrowsersStatTable(statObj, 'clicks');
                    showGeoStatTable(statObj, 'clicks');
                    showAdPrevHover(statObj);

                    if ($('#today').hasClass('active')) {
                        var ipsStatObj = getStatObj('ip_stat_obj');
                        showIpStatTable(ipsStatObj, 'clicks');
                    }
                });


                // sort by ctr
                $('#sort_ctr').on('click', function () {
                    $(this).addClass('active');
                    $('#sort_views, #sort_clicks').removeClass('active');
                    var statObj = getStatObj();

                    showAdsStatTable(statObj, 'ctr');
                    showDevsStatTable(statObj, 'ctr');
                    showOsStatTable(statObj, 'ctr');
                    showBrowsersStatTable(statObj, 'ctr');
                    showGeoStatTable(statObj, 'ctr');
                    showAdPrevHover(statObj);

                    if ($('#today').hasClass('active')) {
                        var ipsStatObj = getStatObj('ip_stat_obj');
                        showIpStatTable(ipsStatObj, 'ctr');
                    }
                });


                // refresh stat
                $('#refresh_stat').on('click', function () {
                    location.reload();
                });


                // tips
                $('[data-toggle="tooltip"]').tooltip({
                    container: 'body',
                    html: true
                });


                // datepicker config
                $('#start_date, #end_date').datepicker({
                    todayBtn: "linked",
                    todayHighlight: true,
                    format: "yyyy-mm-dd"
                });


                // go top
                $('#up_scroll').click(function () {
                    $('html, body').animate({
                        scrollTop: 0
                    }, 500);
                });


                //
                $(window).scroll(function () {
                    if ($(document).scrollTop() > 100) {
                        $('#up_scroll').fadeIn(1000);
                    } else {
                        $('#up_scroll').fadeOut(1000);
                    }
                });


                //
//                $(window).scroll(function () {
//                    if ($(document).scrollTop() > 60) {
//                        $('#controls').css({
//                            'position': 'fixed',
//                            'top': '10px',
//                            'width': '1250px',
//                            'z-index': '999'
//                        });
//                        $('#bottom_controll').css({
//                            'height': '60px'
//                        });
//                        $('#bg_controll').css({
//                            'position': 'fixed',
//                            'top': '0',
//                            'height': '70px',
//                            'width': '100%',
//                            'background': '#fff',
//                            'z-index': '998'
//                        });
//                    } else {
//                        $('#controls').css({
//                            'position': '',
//                            'top': '',
//                            'width': ''
//                        });
//                        $('#bottom_controll').css({
//                            'height': ''
//                        });
//                        $('#bg_controll').css({
//                            'position': '',
//                            'top': '',
//                            'height': '',
//                            'width': '',
//                            'background': '',
//                            'z-index': ''
//                        });
//                    }
//                });


                // logout
                $('#logout').on('click', function () {
                    $.post('/login/logOut/', {
                        csrfToken: csrfToken
                    }, function () {
                        location.reload();
                    });
                });


            });
        </script>

    </head>
    <body>
        <div class="container-fluid">
            <div class="row">
                <div class="col-xlg-10 col-xlg-offset-1 col-xs-12 col-xs-offset-0">
                    <?php include_once dirname(__DIR__) . '/nav.php'; ?>
                    <div id="controls" class="panel panel-default">
                        <div class="panel-heading">
                            <div class="row">
                                <div class="col-xs-4 text-left">
                                    <div class="btn-group">
                                        <button id="today" type="button" class="active btn btn-default btn-sm">Today</button>
                                        <button id="yesterday" type="button" class="btn btn-default btn-sm">Yesterday</button>
                                        <button id="last_7" type="button" class="btn btn-default btn-sm">Last 7 days</button>
                                        <button id="last_30" type="button" class="btn btn-default btn-sm">Last 30 days</button>
                                        <button id="range" type="button" class="btn btn-default btn-sm" data-toggle="modal" data-target="#select_date_modal"><i class="fa fa-calendar" aria-hidden="true"></i></button>
                                    </div>
                                </div>
                                <div class="col-xs-4 text-center">
                                    <div class="btn-group">
                                        <button id="sort_views" type="button" class="active btn btn-default btn-sm"><i class="fa fa-sort-amount-desc" aria-hidden="true"></i> Views</button>
                                        <button id="sort_clicks" type="button" class="btn btn-default btn-sm"><i class="fa fa-sort-amount-desc" aria-hidden="true"></i> Clicks</button>
                                        <button id="sort_ctr" type="button" class="btn btn-default btn-sm"><i class="fa fa-sort-amount-desc" aria-hidden="true"></i> CTR</button>
                                    </div>
                                </div>

                                <div class="col-xs-4 text-right">
                                    <a id="button_dates" href="/stat" class="btn btn-sm btn-default">By days</a>
                                    <div class="btn-group">
                                        <a id="button_sites" href="/stat/sites" class="btn btn-sm btn-default">By sites</a>
                                        <button type="button" class="btn btn-sm btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                            <span class="caret"></span>
                                        </button>
                                        <ul id="sites_list" class="dropdown-menu">
                                        </ul>
                                    </div>
                                    <div class="btn-group">
                                        <a id="button_camps" href="/stat/camps" class="active btn btn-sm btn-default">By campaigns</a>
                                        <button type="button" class="btn btn-sm btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                            <span class="caret"></span>
                                        </button>
                                        <ul id="camps_list" class="dropdown-menu">
                                        </ul>
                                    </div>
                                    <button id="refresh_stat" type="button" class="btn btn-sm btn-success" data-toggle="tooltip" data-placement="right" title="Refresh stat" ><i class="fa fa-refresh" aria-hidden="true"></i></button>
                                </div>
                            </div>
                        </div>
                    </div>

                    <!--                    <div id="bg_controll"></div>
                                        <div id="bottom_controll"></div>-->

                    <div id="total_table" class="panel panel-primary">
                        <div class="panel-heading"><b>Total</b></div>
                        <table class="table table-bordered table-hover">
                            <thead class="zag" style="background: #EEEEEE; display:none;">
                                <tr>
                                    <th style="width: 20%;">Date</th>
                                    <th style="width: 20%;">Campaign</th>
                                    <th style="width: 20%;">Views</th>
                                    <th style="width: 20%;">Clicks</th>
                                    <th style="width: 20%;">CTR</th>
                                </tr>
                            </thead>
                            <tbody></tbody>
                            <div class="stat_load text-center"><h4><i class="fa fa-spinner fa-spin fa-2x fa-fw"></i></h4></div>
                            <div style="display:none;" class="stat_empty text-center"><h4>No data</h4></div>
                        </table>
                    </div>

                    <div id="ads_table" class="panel panel-primary">
                        <div class="panel-heading"><b>Ads</b></div>
                        <table class="table table-bordered table-hover">
                            <thead class="zag" style="background: #EEEEEE; display:none;">
                                <tr>
                                    <th style="width: 5%;">#</th>
                                    <th style="width: 25%;">Ad ID</th>
                                    <th style="width: 25%;">Views</th>
                                    <th style="width: 25%;">Clicks</th>
                                    <th style="width: 20%;">CTR</th>
                                </tr>
                            </thead>
                            <tbody></tbody>
                            <div class="stat_load text-center"><h4><i class="fa fa-spinner fa-spin fa-2x fa-fw"></i></h4></div>
                            <div style="display:none;" class="stat_empty text-center"><h4>No data</h4></div>
                        </table>
                    </div>

                    <div id="devs_table" class="panel panel-primary">
                        <div class="panel-heading"><b>Devices</b></div>
                        <table class="table table-bordered table-hover">
                            <thead class="zag" style="background: #EEEEEE; display:none;">
                                <tr>
                                    <th style="width: 5%;">#</th>
                                    <th style="width: 25%;">Device</th>
                                    <th style="width: 25%;">Views</th>
                                    <th style="width: 25%;">Clicks</th>
                                    <th style="width: 20%;">CTR</th>
                                </tr>
                            </thead>
                            <tbody></tbody>
                            <div class="stat_load text-center"><h4><i class="fa fa-spinner fa-spin fa-2x fa-fw"></i></h4></div>
                            <div style="display:none;" class="stat_empty text-center"><h4>No data</h4></div>
                        </table>
                    </div>

                    <div id="os_table" class="panel panel-primary">
                        <div class="panel-heading"><b>OS</b></div>
                        <table class="table table-bordered table-hover">
                            <thead class="zag" style="background: #EEEEEE; display:none;">
                                <tr>
                                    <th style="width: 5%;">#</th>
                                    <th style="width: 25%;">OS</th>
                                    <th style="width: 25%;">Views</th>
                                    <th style="width: 25%;">Clicks</th>
                                    <th style="width: 20%;">CTR</th>
                                </tr>
                            </thead>
                            <tbody></tbody>
                            <div class="stat_load text-center"><h4><i class="fa fa-spinner fa-spin fa-2x fa-fw"></i></h4></div>
                            <div style="display:none;" class="stat_empty text-center"><h4>No data</h4></div>
                        </table>
                    </div>

                    <div id="browsers_table" class="panel panel-primary">
                        <div class="panel-heading"><b>Browsers</b></div>
                        <table class="table table-bordered table-hover">
                            <thead class="zag" style="background: #EEEEEE; display:none;">
                                <tr>
                                    <th style="width: 5%;">#</th>
                                    <th style="width: 25%;">Browser</th>
                                    <th style="width: 25%;">Views</th>
                                    <th style="width: 25%;">Clicks</th>
                                    <th style="width: 20%;">CTR</th>
                                </tr>
                            </thead>
                            <tbody></tbody>
                            <div class="stat_load text-center"><h4><i class="fa fa-spinner fa-spin fa-2x fa-fw"></i></h4></div>
                            <div style="display:none;" class="stat_empty text-center"><h4>No data</h4></div>
                        </table>
                    </div>

                    <div id="geo_table" class="panel panel-primary">
                        <div class="panel-heading"><b>Geo</b></div>
                        <table class="table table-bordered table-hover">
                            <thead class="zag" style="background: #EEEEEE; display:none;">
                                <tr>
                                    <th style="width: 5%;">#</th>
                                    <th style="width: 25%;">Geo</th>
                                    <th style="width: 25%;">Views</th>
                                    <th style="width: 25%;">Clicks</th>
                                    <th style="width: 20%;">CTR</th>
                                </tr>
                            </thead>
                            <tbody></tbody>
                            <div class="stat_load text-center"><h4><i class="fa fa-spinner fa-spin fa-2x fa-fw"></i></h4></div>
                            <div style="display:none;" class="stat_empty text-center"><h4>No data</h4></div>
                        </table>
                    </div>

                    <div style="display:none;" id="ip_table" class="panel panel-primary">
                        <div class="panel-heading"><b>IP</b> - Statistics on IP is available only for today! 50 Most visited IP showing.</div>
                        <table class="table table-bordered table-hover">
                            <thead class="zag" style="background: #EEEEEE; display:none;">
                                <tr>
                                    <th style="width: 5%;">#</th>
                                    <th style="width: 25%;">IP</th>
                                    <th style="width: 25%;">Views</th>
                                    <th style="width: 25%;">Clicks</th>
                                    <th style="width: 20%;">CTR</th>
                                </tr>
                            </thead>
                            <tbody></tbody>
                            <div class="stat_load text-center"><h4><i class="fa fa-spinner fa-spin fa-2x fa-fw"></i></h4></div>
                            <div style="display:none;" class="stat_empty text-center"><h4>No data</h4></div>
                        </table>
                    </div>

                    <!--start select_date_modal-->
                    <div class="modal fade" id="select_date_modal">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                                    <h4 class="modal-title"><i class="fa fa-calendar" aria-hidden="true"></i> Select a start and end date</h4>
                                </div>
                                <div class="modal-body">
                                    <div class="input-daterange input-group" id="datepicker">
                                        <input id="start_date" type="text" class="input-sm form-control" name="start" value="<?php echo date('Y-m-d'); ?>" />
                                        <span class="input-group-addon">to</span>
                                        <input id="end_date" type="text" class="input-sm form-control" name="end" value="<?php echo date('Y-m-d'); ?>" />
                                    </div>
                                </div>
                                <div class="modal-footer">
                                    <button id="select_date_ok" type="button" class="btn btn-success btn-sm"><i class="fa fa-check"></i> OK</button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--end select_date_modal-->
                    <button style="display: none; " id="up_scroll" type="button" class="btn btn-primary btn-lg"><i class="fa fa-arrow-up fa-2x" aria-hidden="true"></i></button>
                </div>
            </div>
        </div>
    </body>
</html>
