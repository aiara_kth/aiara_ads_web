<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<!doctype html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <title>Stats - days</title>
        <meta name="csrf-param" content="<?php echo $csrfToken; ?>">
        <link rel='stylesheet' href='/css/bootstrap.min.css' type='text/css' media='all'>
        <link rel='stylesheet' href='/css/bootstrap-dialog.css' type='text/css' media='all'>
        <link rel='stylesheet' href='/css/font-awesome.min.css' type='text/css' media='all'>
        <link rel='stylesheet' href='/css/style.css' type='text/css' media='all'>
        <link rel='stylesheet' href='/css/icons.css' type='text/css' media='all'>
        <link rel="shortcut icon" href="/img/favicon.ico">
        <script src="/js/jquery.min.js"></script>
        <script src="/js/bootstrap.min.js"></script>
        <script src="/js/bootstrap-dialog.js"></script>
        <script src="/js/stat.js"></script>
        <script>
            $(document).ready(function () {

                // csrf token
                var csrfToken = $('[name="csrf-param"]').attr('content');


                // parse stat
                $.post('/stat/apiGetStatByDays', {
                    csrfToken: csrfToken
                }, function (data) {
                    var statObj = saveGetStatObj($.parseJSON(data));
                    showDropDownListSites(statObj);
                    showDropDownListCamps(statObj);
                    showDaysStatTable(statObj, 'date');
                });


                // sort by dates
                $('#sort_dates').on('click', function () {
                    $('#sort_dates, #sort_views, #sort_clicks, #sort_ctr').removeClass('active');
                    $(this).addClass('active');
                    var statObj = getStatObj();
                    showDaysStatTable(statObj, 'date');
                });


                // sort by views
                $('#sort_views').on('click', function () {
                    $('#sort_dates, #sort_views, #sort_clicks, #sort_ctr').removeClass('active');
                    $(this).addClass('active');
                    var statObj = getStatObj();
                    showDaysStatTable(statObj, 'views');
                });


                // sort by clicks
                $('#sort_clicks').on('click', function () {
                    $('#sort_dates, #sort_views, #sort_clicks, #sort_ctr').removeClass('active');
                    $(this).addClass('active');
                    var statObj = getStatObj();
                    showDaysStatTable(statObj, 'clicks');
                });


                // sort by ctr
                $('#sort_ctr').on('click', function () {
                    $('#sort_dates, #sort_views, #sort_clicks, #sort_ctr').removeClass('active');
                    $(this).addClass('active');
                    var statObj = getStatObj();
                    showDaysStatTable(statObj, 'ctr');
                });


                // refresh stat
                $('#refresh_stat').on('click', function () {
                    location.reload();
                });


                // tips
                $('[data-toggle="tooltip"]').tooltip({
                    container: 'body',
                    html: true
                });


                // logout
                $('#logout').on('click', function () {
                    $.post('/login/logOut/', {
                        csrfToken: csrfToken
                    }, function () {
                        location.reload();
                    });
                });

            });
        </script>
    </head>
    <body>
        <div class="container-fluid">
            <div class="row">
                <div class="col-xlg-10 col-xlg-offset-1 col-xs-12 col-xs-offset-0">
                    <?php include_once dirname(__DIR__) . '/nav.php'; ?>
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <div class="row">
                                <div class="col-xs-4 text-left">
                                </div>
                                <div class="col-xs-4 text-center">
                                    <div class="btn-group">
                                        <button id="sort_dates" type="button" class="active btn btn-default btn-sm"><i class="fa fa-sort-amount-desc" aria-hidden="true"></i> Dates</button>
                                        <button id="sort_views" type="button" class="btn btn-default btn-sm"><i class="fa fa-sort-amount-desc" aria-hidden="true"></i> Views</button>
                                        <button id="sort_clicks" type="button" class="btn btn-default btn-sm"><i class="fa fa-sort-amount-desc" aria-hidden="true"></i> Clicks</button>
                                        <button id="sort_ctr" type="button" class="btn btn-default btn-sm"><i class="fa fa-sort-amount-desc" aria-hidden="true"></i> CTR</button>
                                    </div>
                                </div>
                                <div class="col-xs-4 text-right">
                                    <a id="button_dates" href="/stat" class="active btn btn-sm btn-default">By days</a>
                                    <div class="btn-group">
                                        <a id="button_sites" href="/stat/sites" class="btn btn-sm btn-default">By sites</a>
                                        <button type="button" class="btn btn-sm btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                            <span class="caret"></span>
                                        </button>
                                        <ul id="sites_list" class="dropdown-menu">
                                        </ul>
                                    </div>
                                    <div class="btn-group">
                                        <a id="button_camps" href="/stat/camps" class="btn btn-sm btn-default">By campaigns</a>
                                        <button type="button" class="btn btn-sm btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                            <span class="caret"></span>
                                        </button>
                                        <ul id="camps_list" class="dropdown-menu">

                                        </ul>
                                    </div>
                                    <button id="refresh_stat" type="button" class="btn btn-sm btn-success" data-toggle="tooltip" data-placement="right" title="Refresh stat" ><i class="fa fa-refresh" aria-hidden="true"></i></button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div id="dates_table" class="panel panel-primary">
                        <div class="panel-heading"><b>Statistics by days</b></div>
                        <table class="table table-bordered table-hover">
                            <thead class="zag" style="background: #EEEEEE; display:none;">
                                <tr>
                                    <th style="width: 5%;">#</th>
                                    <th style="width: 25%;">Date</th>
                                    <th style="width: 25%;">Views</th>
                                    <th style="width: 25%;">Clicks</th>
                                    <th style="width: 20%;">CTR</th>
                                </tr>
                            </thead>
                            <tbody></tbody>
                            <div class="stat_load text-center"><h4><i class="fa fa-spinner fa-spin fa-2x fa-fw"></i></h4></div>
                            <div style="display:none;" class="stat_empty text-center"><h4>No data</h4></div>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>
