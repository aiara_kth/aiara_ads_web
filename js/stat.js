/**
 * Geo
 *
 * @param key
 * @returns {{country_name: string, alfa2: string, alfa3: string}}
 */
function getGeoArray(key) {

    var country = ['Unknown country', 'Afghanistan', 'Åland Islands', 'Albania', 'Algeria', 'American Samoa', 'Andorra', 'Angola', 'Anguilla', 'Antarctica',
        'Antigua and Barbuda', 'Argentina', 'Armenia', 'Aruba', 'Australia', 'Austria', 'Azerbaijan', 'Bahamas', 'Bahrain', 'Bangladesh', 'Barbados', 'Belarus',
        'Belgium', 'Belize', 'Benin', 'Bermuda', 'Bhutan', 'Bolivia', 'Bonaire', 'Bosnia and Herzegovina', 'Botswana', 'Bouvet Island', 'Brazil',
        'British Indian Ocean Territory', 'Brunei Darussalam', 'Bulgaria', 'Burkina Faso', 'Burundi', 'Cambodia', 'Cameroon', 'Canada', 'Cabo Verde',
        'Cayman Islands', 'Central African Republic', 'Chad', 'Chile', 'China', 'Christmas Island', 'Cocos Islands', 'Colombia', 'Comoros',
        'Congo', 'Congo', 'Cook Islands', 'Costa Rica', 'Côte d`Ivoire', 'Croatia', 'Cuba', 'Curaçao', 'Cyprus', 'Czech Republic',
        'Denmark', 'Djibouti', 'Dominica', 'Dominican Republic', 'Ecuador', 'Egypt', 'El Salvador', 'Equatorial Guinea', 'Eritrea', 'Estonia', 'Ethiopia',
        'Falkland Islands', 'Faroe Islands', 'Fiji', 'Finland', 'France', 'French Guiana', 'French Polynesia', 'French Southern Territories',
        'Gabon', 'Gambia', 'Georgia', 'Germany', 'Ghana', 'Gibraltar', 'Greece', 'Greenland', 'Grenada', 'Guadeloupe', 'Guam', 'Guatemala', 'Guernsey', 'Guinea',
        'Guinea-Bissau', 'Guyana', 'Haiti', 'Heard Isl. and McDonald Isl.', 'Holy See', 'Honduras', 'Hong Kong', 'Hungary', 'Iceland', 'India', 'Indonesia',
        'Iran', 'Iraq', 'Ireland', 'Isle of Man', 'Israel', 'Italy', 'Jamaica', 'Japan', 'Jersey', 'Jordan', 'Kazakhstan', 'Kenya',
        'Kiribati', 'Korea (Democratic People`s Republic of)', 'Korea (Republic of)', 'Kuwait', 'Kyrgyzstan', 'Lao People`s Democratic Republic',
        'Latvia', 'Lebanon', 'Lesotho', 'Liberia', 'Libya', 'Liechtenstein', 'Lithuania', 'Luxembourg', 'Macao', 'Macedonia', 'Madagascar', 'Malawi',
        'Malaysia', 'Maldives', 'Mali', 'Malta', 'Marshall Islands', 'Martinique', 'Mauritania', 'Mauritius', 'Mayotte', 'Mexico', 'Micronesia (Federated States of)',
        'Moldova (Republic of)', 'Monaco', 'Mongolia', 'Montenegro', 'Montserrat', 'Morocco', 'Mozambique', 'Myanmar', 'Namibia', 'Nauru', 'Nepal', 'Netherlands',
        'New Caledonia', 'New Zealand', 'Nicaragua', 'Niger', 'Nigeria', 'Niue', 'Norfolk Island', 'Northern Mariana Islands', 'Norway', 'Oman', 'Pakistan',
        'Palau', 'Palestine', 'Panama', 'Papua New Guinea', 'Paraguay', 'Peru', 'Philippines', 'Pitcairn', 'Poland', 'Portugal', 'Puerto Rico', 'Qatar', 'Réunion',
        'Romania', 'Russian Federation', 'Rwanda', 'Saint Barthélemy', 'Saint Helena', 'Saint Kitts and Nevis', 'Saint Lucia', 'Saint Martin (French part)',
        'Saint Pierre and Miquelon', 'Saint Vincent and the Grenadines', 'Samoa', 'San Marino', 'Sao Tome and Principe', 'Saudi Arabia', 'Senegal', 'Serbia',
        'Seychelles', 'Sierra Leone', 'Singapore', 'Sint Maarten (Dutch part)', 'Slovakia', 'Slovenia', 'Solomon Islands', 'Somalia', 'South Africa',
        'South Georgia and the South Sandwich Isl.', 'South Sudan', 'Spain', 'Sri Lanka', 'Sudan', 'Suriname', 'Svalbard and Jan Mayen', 'Swaziland',
        'Sweden', 'Switzerland', 'Syrian Arab Republic', 'Taiwan (Province of China)', 'Tajikistan', 'Tanzania', 'Thailand', 'Timor-Leste', 'Togo', 'Tokelau',
        'Tonga', 'Trinidad and Tobago', 'Tunisia', 'Turkey', 'Turkmenistan', 'Turks and Caicos Islands', 'Tuvalu', 'Uganda', 'Ukraine', 'United Arab Emirates',
        'United Kingdom', 'United States of America', 'United States Minor Outlying Isl.', 'Uruguay', 'Uzbekistan',
        'Vanuatu', 'Venezuela', 'Viet Nam', 'Virgin Islands (British)', 'Virgin Islands (U.S.)', 'Wallis and Futuna', 'Western Sahara',
        'Yemen', 'Zambia', 'Zimbabwe'
    ];

    var alfa2 = ['XX', 'AF', 'AX', 'AL', 'DZ', 'AS', 'AD', 'AO', 'AI', 'AQ', 'AG', 'AR', 'AM', 'AW', 'AU', 'AT', 'AZ', 'BS', 'BH', 'BD', 'BB', 'BY', 'BE', 'BZ', 'BJ', 'BM',
        'BT', 'BO', 'BQ', 'BA', 'BW', 'BV', 'BR', 'IO', 'BN', 'BG', 'BF', 'BI', 'KH', 'CM', 'CA', 'CV', 'KY', 'CF', 'TD', 'CL', 'CN', 'CX', 'CC', 'CO', 'KM', 'CG', 'CD',
        'CK', 'CR', 'CI', 'HR', 'CU', 'CW', 'CY', 'CZ', 'DK', 'DJ', 'DM', 'DO', 'EC', 'EG', 'SV', 'GQ', 'ER', 'EE', 'ET', 'FK', 'FO', 'FJ', 'FI', 'FR', 'GF', 'PF', 'TF',
        'GA', 'GM', 'GE', 'DE', 'GH', 'GI', 'GR', 'GL', 'GD', 'GP', 'GU', 'GT', 'GG', 'GN', 'GW', 'GY', 'HT', 'HM', 'VA', 'HN', 'HK', 'HU', 'IS', 'IN', 'ID', 'IR', 'IQ',
        'IE', 'IM', 'IL', 'IT', 'JM', 'JP', 'JE', 'JO', 'KZ', 'KE', 'KI', 'KP', 'KR', 'KW', 'KG', 'LA', 'LV', 'LB', 'LS', 'LR', 'LY', 'LI', 'LT', 'LU', 'MO', 'MK', 'MG',
        'MW', 'MY', 'MV', 'ML', 'MT', 'MH', 'MQ', 'MR', 'MU', 'YT', 'MX', 'FM', 'MD', 'MC', 'MN', 'ME', 'MS', 'MA', 'MZ', 'MM', 'NA', 'NR', 'NP', 'NL', 'NC', 'NZ', 'NI',
        'NE', 'NG', 'NU', 'NF', 'MP', 'NO', 'OM', 'PK', 'PW', 'PS', 'PA', 'PG', 'PY', 'PE', 'PH', 'PN', 'PL', 'PT', 'PR', 'QA', 'RE', 'RO', 'RU', 'RW', 'BL', 'SH', 'KN',
        'LC', 'MF', 'PM', 'VC', 'WS', 'SM', 'ST', 'SA', 'SN', 'RS', 'SC', 'SL', 'SG', 'SX', 'SK', 'SI', 'SB', 'SO', 'ZA', 'GS', 'SS', 'ES', 'LK', 'SD', 'SR', 'SJ', 'SZ',
        'SE', 'CH', 'SY', 'TW', 'TJ', 'TZ', 'TH', 'TL', 'TG', 'TK', 'TO', 'TT', 'TN', 'TR', 'TM', 'TC', 'TV', 'UG', 'UA', 'AE', 'GB', 'US', 'UM', 'UY', 'UZ', 'VU', 'VE',
        'VN', 'VG', 'VI', 'WF', 'EH', 'YE', 'ZM', 'ZW'
    ];

    var alfa3 = ['XXX', 'AFG', 'ALA', 'ALB', 'DZA', 'ASM', '_AND', 'AGO', 'AIA', 'ATA', 'ATG', 'ARG', 'ARM', 'ABW', 'AUS', 'AUT', 'AZE', 'BHS', 'BHR', 'BGD', 'BRB', 'BLR',
        'BEL', 'BLZ', 'BEN', 'BMU', 'BTN', 'BOL', 'BES', 'BIH', 'BWA', 'BVT', 'BRA', 'IOT', 'BRN', 'BGR', 'BFA', 'BDI', 'KHM', 'CMR', 'CAN', 'CPV', 'CYM', 'CAF', 'TCD',
        'CHL', 'CHN', 'CXR', 'CCK', 'COL', 'COM', 'COG', 'COD', 'COK', 'CRI', 'CIV', 'HRV', 'CUB', 'CUW', 'CYP', 'CZE', 'DNK', 'DJI', 'DMA', 'DOM', 'ECU', 'EGY', 'SLV',
        'GNQ', 'ERI', 'EST', 'ETH', 'FLK', 'FRO', 'FJI', 'FIN', 'FRA', 'GUF', 'PYF', 'ATF', 'GAB', 'GMB', 'GEO', 'DEU', 'GHA', 'GIB', 'GRC', 'GRL', 'GRD', 'GLP', 'GUM',
        'GTM', 'GGY', 'GIN', 'GNB', 'GUY', 'HTI', 'HMD', 'VAT', 'HND', 'HKG', 'HUN', 'ISL', 'IND', 'IDN', 'IRN', 'IRQ', 'IRL', 'IMN', 'ISR', 'ITA', 'JAM', 'JPN', 'JEY',
        'JOR', 'KAZ', 'KEN', 'KIR', 'PRK', 'KOR', 'KWT', 'KGZ', 'LAO', 'LVA', 'LBN', 'LSO', 'LBR', 'LBY', 'LIE', 'LTU', 'LUX', 'MAC', 'MKD', 'MDG', 'MWI', 'MYS', 'MDV',
        'MLI', 'MLT', 'MHL', 'MTQ', 'MRT', 'MUS', 'MYT', 'MEX', 'FSM', 'MDA', 'MCO', 'MNG', 'MNE', 'MSR', 'MAR', 'MOZ', 'MMR', 'NAM', 'NRU', 'NPL', 'NLD', 'NCL', 'NZL',
        'NIC', 'NER', 'NGA', 'NIU', 'NFK', 'MNP', 'NOR', 'OMN', 'PAK', 'PLW', 'PSE', 'PAN', 'PNG', 'PRY', 'PER', 'PHL', 'PCN', 'POL', 'PRT', 'PRI', 'QAT', 'REU', 'ROU',
        'RUS', 'RWA', 'BLM', 'SHN', 'KNA', 'LCA', 'MAF', 'SPM', 'VCT', 'WSM', 'SMR', 'STP', 'SAU', 'SEN', 'SRB', 'SYC', 'SLE', 'SGP', 'SXM', 'SVK', 'SVN', 'SLB', 'SOM',
        'ZAF', 'SGS', 'SSD', 'ESP', 'LKA', 'SDN', 'SUR', 'SJM', 'SWZ', 'SWE', 'CHE', 'SYR', 'TWN', 'TJK', 'TZA', 'THA', 'TLS', 'TGO', 'TKL', 'TON', 'TTO', 'TUN', 'TUR',
        'TKM', 'TCA', 'TUV', 'UGA', 'UKR', 'ARE', 'GBR', 'USA', 'UMI', 'URY', 'UZB', 'VUT', 'VEN', 'VNM', 'VGB', 'VIR', 'WLF', 'ESH', 'YEM', 'ZMB', 'ZWE'
    ];


    if (country.indexOf(key) < 0 && alfa2.indexOf(key) < 0 && alfa3.indexOf(key) < 0) {
        key = 'XX';
    }

    if (key.length > 4) {
        return {
            country_name: country[country.indexOf(key)],
            alfa2: alfa2[country.indexOf(key)],
            alfa3: alfa3[country.indexOf(key)]
        };
    }

    if (key.length === 2) {
        return {
            country_name: country[alfa2.indexOf(key)],
            alfa2: alfa2[alfa2.indexOf(key)],
            alfa3: alfa3[alfa2.indexOf(key)]
        };
    }

    if (key.length === 3 || key.length === 4) {
        return {
            country_name: country[alfa3.indexOf(key)],
            alfa2: alfa2[alfa3.indexOf(key)],
            alfa3: alfa3[alfa3.indexOf(key)]
        };
    }

    if (key.length > 4) {
        return {
            country_name: country[country.indexOf(key)],
            alfa2: alfa2[country.indexOf(key)],
            alfa3: alfa3[country.indexOf(key)]
        };
    }
}


/**
 * Sort an object with statistics
 *
 * @param obj
 * @param sortColumn
 */
function sortTable(obj, sortColumn) {
    if (!obj) {
        return;
    }
    obj.sort(function (a, b) {
        if (sortColumn === 'date') {
            return b.date - a.date;
        }
        if (sortColumn === 'ctr') {
            return b.ctr - a.ctr;
        }
        if (sortColumn === 'views') {
            return b.views - a.views;
        }
        if (sortColumn === 'clicks') {
            return b.clicks - a.clicks;
        }
    });
}


/**
 * Generation of a table with statistics by day
 *
 * @param obj
 * @param sortColumn
 * @param selector
 */
function showDaysStatTable(obj, sortColumn, selector) {

    selector = selector || '#dates_table';

    if (!obj.dates) {
        showNoStat(selector);
        return;
    }

    var table, num = '';

    sortTable(obj.dates, sortColumn);

    for (var key in obj.dates) {
        num++;
        table += '<tr><th>' + num + '</th>\n\
		  <td>' + obj.dates[key].format_date + '</td>\n\
		  <td>' + obj.dates[key].views + '</td>\n\
		  <td>' + obj.dates[key].clicks + '</td>\n\
		  <td>' + obj.dates[key].ctr + '</td></tr>\n';
    }

    showStat(selector, table);
}


/**
 * Generation of a table with statistics on sites
 *
 * @param obj
 * @param sortColumn
 * @param selector
 */
function showSitesStatTable(obj, sortColumn, selector) {

    selector = selector || '#sites_table';

    if (!obj.sites) {
        showNoStat(selector);
        return;
    }

    var table, num = '';

    sortTable(obj.sites, sortColumn);

    for (var key in obj.sites) {
        num++;
        table += '<tr><th>' + num + '</th>\n\
		  <td><img src="http://favicon.yandex.net/favicon/' + obj.sites[key].domain.toLowerCase().trim() + '" > <a href="/stat/site/' + obj.sites[key].site_id + '">' + obj.sites[key].domain.toLowerCase().trim() + '</a></td>\n\
		  <td>' + obj.sites[key].views + '</td>\n\
		  <td>' + obj.sites[key].clicks + '</td>\n\
		  <td>' + obj.sites[key].ctr + '</td></tr>\n';
    }

    showStat(selector, table);
}


/**
 * Generating a table with campaign statistics
 *
 * @param obj
 * @param sortColumn
 * @param selector
 */
function showCampsStatTable(obj, sortColumn, selector) {

    selector = selector || '#camps_table';

    if (!obj.camps) {
        showNoStat(selector);
        return;
    }

    var table, num = '';

    sortTable(obj.camps, sortColumn);

    for (var key in obj.camps) {
        num++;
        table += '<tr><th>' + num + '</th>\n\
		  <td><a href="/stat/camp/' + obj.camps[key].camp_id + '">' + obj.camps[key].camp_name.trim() + '</a></td>\n\
		  <td>' + obj.camps[key].views + '</td>\n\
		  <td>' + obj.camps[key].clicks + '</td>\n\
		  <td>' + obj.camps[key].ctr + '</td></tr>\n';
    }

    showStat(selector, table);
}


/**
 * Generating a table with common statistics for one site
 *
 * @param obj
 * @param selector
 */
function showTotalOneSiteStatTable(obj, selector) {

    selector = selector || '#total_table';

    var table = '<tr><td><b>' + obj.date + '</b></td>\n\
		    <td>' + obj.site_name + '</td>\n\
		 <td>' + obj.total.views + '</td>\n\
		 <td>' + obj.total.clicks + '</td>\n\
		 <td>' + obj.total.ctr + '</td></tr>\n';

    showStat(selector, table);
}


/**
 * Generating a table with common statistics for a single campaign
 *
 * @param obj
 * @param selector
 */
function showTotalOneCampStatTable(obj, selector) {

    selector = selector || '#total_table';

    var table = '<tr><td><b>' + obj.date + '</b></td>\n\
		    <td>' + obj.camp_name + '</td>\n\
		 <td>' + obj.total.views + '</td>\n\
		 <td>' + obj.total.clicks + '</td>\n\
		 <td>' + obj.total.ctr + '</td></tr>\n';

    showStat(selector, table);
}


/**
 * Generating a table with general statistics for one BLOCK
 *
 * @param obj
 * @param selector
 */
function showTotalOneBlockStatTable(obj, selector) {

    selector = selector || '#total_table';

    var table = '<tr><td><b>' + obj.date + '</b></td>\n\
		    <td>' + obj.block_name + '</td>\n\
		 <td>' + obj.total.views + '</td>\n\
		 <td>' + obj.total.clicks + '</td>\n\
		 <td>' + obj.total.ctr + '</td></tr>\n';

    showStat(selector, table);
}


/**
 * Generating a table with the total statistics by one AD
 *
 * @param obj
 * @param selector
 */
function showTotalOneAdStatTable(obj, selector) {

    selector = selector || '#total_table';

    var table = '<tr><td><b>' + obj.date + '</b></td>\n\
		    <td><a id="' + obj.ad_id + '" href="#">' + obj.ad_id + '</a></td>\n\
		 <td>' + obj.total.views + '</td>\n\
		 <td>' + obj.total.clicks + '</td>\n\
		 <td>' + obj.total.ctr + '</td></tr>\n';

    showStat(selector, table);
}


/**
 * Generating a table with block statistics
 *
 * @param obj
 * @param sortColumn
 * @param selector
 */
function showAllBlocksStatTable(obj, sortColumn, selector) {

    selector = selector || '#blocks_table';

    if (!obj.blocks) {
        showNoStat(selector);
        return;
    }

    var table, num = '';

    sortTable(obj.blocks, sortColumn);

    for (var key in obj.blocks) {
        num++;
        table += '<tr><th>' + num + '</th>\n\
		<td><a href="/stat/block/' + obj.blocks[key].block_id + '">' + obj.blocks[key].block_name.trim() + '</a></td>\n\
		<td>' + obj.blocks[key].views + '</td>\n\
		<td>' + obj.blocks[key].clicks + '</td>\n\
		<td>' + obj.blocks[key].ctr + '</td></tr>\n';
    }

    showStat(selector, table);
}


/**
 * Ad preview when hovering over it in statistics
 *
 * @param statObj
 */
function showAdPrevHover(statObj) {

    for (var key in statObj.camp_ads) {
        var adID = statObj.camp_ads[key].id;
        var imgSrc = statObj.camp_ads[key].ad_img.trim();
        var title = statObj.camp_ads[key].ad_title.trim();
        var descr = statObj.camp_ads[key].ad_descr.trim();
        var domain = statObj.camp_ads[key].ad_link.trim().split(/\/+/g)[1];

        var table = "<table><tr><td>";
        if (imgSrc) table += "<img style='margin:5px 0 5px 0;' src='/images/" + imgSrc + "' width='55' height='55'/>";
        table += "</td>";
        table += "<td align=left valign=top><b style=' color:#a9d0f3; padding:2px 0 0 5px; display:block;'>" + title + "</b>";
        table += "<span style='padding:2px 0 0 5px; display:block;'>" + descr + "</span>";
        table += "<span style='padding:2px 0 0 5px; color:#1EB31A; display:block;'>" + domain + "</span></td></tr></table>";

        $("#" + adID).attr('data-toggle', 'tooltip');
        $("#" + adID).attr('data-placement', 'right');
        $("#" + adID).attr('title', table);
        $("#" + adID).tooltip({
            container: 'body',
            html: true
        });
    }
}


/**
 * Generating a table with statistics on ads
 *
 * @param obj
 * @param sortColumn
 * @param selector
 */
function showAdsStatTable(obj, sortColumn, selector) {

    selector = selector || '#ads_table';

    if (!obj.ads) {
        showNoStat(selector);
        return;
    }

    var table, num = '';

    sortTable(obj.ads, sortColumn);

    for (var key in obj.ads) {
        num++;
        table += '<tr><th>' + num + '</th>\n\
		<td>' + obj.ads[key].ad_id + '<a class="nounderline" id="' + obj.ads[key].ad_id + '" href="/stat/ad/' + obj.ads[key].ad_id + '" > <i class="fa fa-external-link" aria-hidden="true"></i></a></td>\n\
		<td>' + obj.ads[key].views + '</td>\n\
		<td>' + obj.ads[key].clicks + '</td>\n\
		<td>' + obj.ads[key].ctr + '</td></tr>\n';
    }

    showStat(selector, table);
}


/**
 * Generation of a table with statistics on devices
 *
 * @param obj
 * @param sortColumn
 * @param selector
 */
function showDevsStatTable(obj, sortColumn, selector) {

    selector = selector || '#devs_table';

    if (!obj.dev) {
        showNoStat(selector);
        return;
    }

    var table, num = '';

    sortTable(obj.dev, sortColumn);

    for (var key in obj.dev) {
        num++;
        table += '<tr><th>' + num + '</th>\n\
		<td><span class="ii ' + obj.dev[key].dev.toLowerCase() + '"></span>  ' + obj.dev[key].dev + '</span></td>\n\
		<td>' + obj.dev[key].views + '</td>\n\
		<td>' + obj.dev[key].clicks + '</td>\n\
		<td>' + obj.dev[key].ctr + '</td></tr>\n';
    }

    showStat(selector, table);
}


/**
 * Generation of tables with statistics on operating systems
 *
 * @param obj
 * @param sortColumn
 * @param selector
 */
function showOsStatTable(obj, sortColumn, selector) {

    selector = selector || '#os_table';

    if (!obj.os) {
        showNoStat(selector);
        return;
    }

    var table, num = '';

    sortTable(obj.os, sortColumn);

    for (var key in obj.os) {
        num++;
        var ii_class = obj.os[key].os.replace('unknown_mobile_os', 'unknown')
                .replace('unknown_desctop_os', 'unknown')
                .toLowerCase();

        var name = obj.os[key].os.replace('unknown_mobile_os', 'Unknown OS <span style="font-size:10px;" class="text-muted">mobile</span>')
                .replace('unknown_desctop_os', 'Unknown OS')
                .replace('8_1', '8.1')
                .replace('_', ' ');

        table += '<tr><th>' + num + '</th>\n\
			<td><span class="ii ' + ii_class + '"></span> ' + name + '</span></td>\n\
			<td>' + obj.os[key].views + '</td>\n\
			<td>' + obj.os[key].clicks + '</td>\n\
			<td>' + obj.os[key].ctr + '</td></tr>\n';
    }

    showStat(selector, table);
}


/**
 * Generation of tables with statistics on browsers
 *
 * @param obj
 * @param sortColumn
 * @param selector
 */
function showBrowsersStatTable(obj, sortColumn, selector) {
    selector = selector || '#browsers_table';

    if (!obj.browser) {
        showNoStat(selector);
        return;
    }

    var table, num = '';

    sortTable(obj.browser, sortColumn);

    for (var key in obj.browser) {
        num++;
        var ii_class = obj.browser[key].browser.replace('unknown_mobile_browser', 'unknown')
                .replace('unknown_desctop_broewser', 'unknown')
                .replace('IE_d', 'iexplorer')
                .replace('IE_m', 'iexplorer')
                .toLowerCase()
                .replace(/\_.*/, '');

        var name = obj.browser[key].browser.replace('unknown_mobile_browser', 'Unknown <span style="font-size:10px;" class="text-muted">mobile</span>')
                .replace('unknown_desktop_browser', 'Unknown')
                .replace('_d', '').replace('_m', ' <span style="font-size:10px;" class="text-muted">mobile</span>');

        table += '<tr><th>' + num + '</th>\n\
			<td><span class="ii ' + ii_class + '"></span> ' + name + '</td>\n\
			<td>' + obj.browser[key].views + '</td>\n\
			<td>' + obj.browser[key].clicks + '</td>\n\
			<td>' + obj.browser[key].ctr + '</td></tr>\n';
    }

    showStat(selector, table);
}

/**
 * Showing blank statistics
 *
 * @param selector
 */
function showNoStat(selector) {
    $(selector).find('.zag').hide();
    $(selector).find('.stat_load').hide();
    $(selector).find('.stat_empty').show();
    $(selector).find('tbody').html('');
}


/**
 * Showing statistics
 *
 * @param selector
 * @param data
 */
function showStat(selector, data) {
    $(selector).find('.zag').show();
    $(selector).find('.stat_load').hide();
    $(selector).find('.stat_empty').hide();
    $(selector).find('tbody').html(data);
}


/**
 *
 */
function load() {
    //    var selectors = ['#total_table, #ads_table, #devs_table, #os_table, #browsers_table, #geo_table, #ip_table, #camps_table, #sites_table, #blocks_table'];
    //    for (var key in selectors) {
    //	$(selectors[key]).find('.zag').hide();
    //	$(selectors[key]).find('.stat_load').show();
    //	$(selectors[key]).find('.stat_empty').hide();
    //	$(selectors[key]).find('tbody').html('');
    //    }
}


/**
 * Generation of tables with geo statistics
 *
 * @param obj
 * @param sortColumn
 * @param selector
 */
function showGeoStatTable(obj, sortColumn, selector) {
    selector = selector || '#geo_table';

    if (!obj.geo) {
        showNoStat(selector);
        return;
    }

    var table, num = '';

    sortTable(obj.geo, sortColumn);

    for (var key in obj.geo) {
        num++;

        var geo = getGeoArray(obj.geo[key].geo);

        table += '<tr><th>' + num + '</th>\n\
			<td><span class="ii ' + geo.alfa2.toLowerCase() + '"></span>  ' + geo.country_name + ' <span style="font-size:10px;" class="text-muted">' + geo.alfa2 + '</span></span></td>\n\
			<td>' + obj.geo[key].views + '</td>\n\
			<td>' + obj.geo[key].clicks + '</td>\n\
			<td>' + obj.geo[key].ctr + '</td></tr>\n';
    }

    showStat(selector, table);
}


/**
 * Generation of a table with statistics on ip
 *
 * @param obj
 * @param sortColumn
 * @param selector
 */
function showIpStatTable(obj, sortColumn, selector) {
    selector = selector || '#ip_table';

    if (!obj) {
        showNoStat(selector);
        return;
    }

    var table, num = '';

    sortTable(obj, sortColumn);

    for (var key in obj) {
        num++;
        table += '<tr><th>' + num + '</th>\n\
			<td><span class="ii ' + obj[key].geo.toLowerCase() + '"></span> ' + obj[key].ip + ' <span style="font-size:10px;" class="text-muted">' + obj[key].geo + '</span></td>\n\
			<td>' + obj[key].views + '</td>\n\
			<td>' + obj[key].clicks + '</td>\n\
			<td>' + obj[key].ctr + '</td></tr>\n';
    }

    $('#ip_table').show();
    showStat('#ip_table', table);
}


/**
 * Generating a drop-down list of sites
 *
 * @param obj
 * @param selector
 */
function showDropDownListSites(obj, selector) {
    selector = selector || '#sites_list';

    if (!obj.list_sites) {
        return;
    }

    var list = '';
    for (var key in obj.list_sites) {
        list += '<li><a href="/stat/site/' + obj.list_sites[key].id + '">' + obj.list_sites[key].domain.toLowerCase() + '</a></li>\n';
    }

    list += '<li class="divider"></li>\n';
    list += '<li><a href="/stat/sites/">All sites</a></li>\n';

    $(selector).html(list);
}


/**
 * Generating a drop-down list of campaigns
 *
 * @param obj
 * @param selector
 */
function showDropDownListCamps(obj, selector) {
    selector = selector || '#camps_list';

    if (!obj.list_camps) {
        return;
    }

    var list = '';
    for (var key in obj.list_camps) {
        list += '<li><a href="/stat/camp/' + obj.list_camps[key].id + '">' + obj.list_camps[key].camp_name + '</a></li>\n';
    }

    list += '<li class="divider"></li>\n';
    list += '<li><a href="/stat/camps/">All camps</a></li>\n';

    $(selector).html(list);
}


/**
 * Saves the object with all statistics to a variable associated with document.body and immediately returns it
 *
 * @param obj
 * @param objName
 * @returns {*}
 */
function saveGetStatObj(obj, objName) {
    objName = objName || 'obj';
    $.data(document.body, objName, obj.msg);
    return obj.msg;
}


/**
 * Returns an object with all the statistics from the variable document.body
 *
 * @param objName
 */
function getStatObj(objName) {
    objName = objName || 'obj';
    return $.data(document.body, objName);
}
