/**
 * Get class name
 *
 * @param str
 * @returns string
 */
function getClassName(str) {
    var styles = str.split('{')[0];
    return styles.replace(/[\n\r\t ]/g, '');
}


/**
 * Get class params
 *
 * @param str
 * @returns {{}}
 */
function getClassParams(str) {
    var obj = {};
    var params = str.match(/{(.+)\}/)[1];
    var params_arr = params.split(';');
    for (var i = 0; i < params_arr.length - 1; i++) {
        obj[params_arr[i].split(':')[0].trim()] = params_arr[i].split(':')[1].trim();
    }
    return obj;
}


/**
 * Get block styles
 *
 * @returns {{}}
 */
function getStyles(raw_styles) {
    var obj = {};
    var styles = '';

    if (raw_styles) {
        styles = raw_styles;
    } else {
        styles = document.getElementsByTagName('style')[0].innerHTML;
    }

    var clear_styles = styles.replace(/[\n\r\t ]/g, ' ');
    var styles_arr = clear_styles.split('}');
    for (var i = 0; i < styles_arr.length - 1; i++) {
        obj[getClassName(styles_arr[i])] = getClassParams(styles_arr[i] + '}');
    }
    return obj;
}


/**
 * Set block styles
 *
 * @param obj
 */
function setStyles(obj) {
    var styles = '';
    for (var key in obj) {
        styles += key + "{\n";
        var params = obj[key];
        for (var key2 in params) {
            if (params[key2] !== false) {
                styles += key2 + ':' + params[key2] + ";\n";
            }
        }
        styles += "}\n";
    }
    document.getElementsByTagName('style')[0].innerHTML = styles;
}


/**
 * Styles
 *
 * @param className
 * @param param
 * @param value
 * @returns {*}
 */
function styles(className, param, value) {
    var styles = getStyles();
    if (className.length === 0 && param.length === 0 && value.length === 0) {
        return styles;
    } else if (className.length !== 0 && param.length !== 0 && value == '') {
        return styles[className][param];
    }
    // delete
    else if (className.length !== 0 && param.length !== 0 && value === false) {
        styles[className][param] = value;
        setStyles(styles);
    } else {
        styles[className][param] = value;
        setStyles(styles);
    }
}

function randomInteger(min, max) {
    var rand = min - 0.5 + Math.random() * (max - min + 1)
    rand = Math.round(rand);
    return rand;
}



function random_cell(imgPosition, linkVisibility, titleVisibility, descrVisibility) {
    var img = '';
    if (imgPosition !== false) {
        img = '<a href="#"><img class="img_ads" src="/img/placeholders/' + randomInteger(1, 17) + '.jpg" ></a>';
    }
    var cell = '<table class="cell_ads" border=0 ><tr><td>';
    cell += img;

    if (titleVisibility == 1) {
        cell += '<a class="title_ads" href="#">Lorem ipsum dolor sit amet elit</a>';
    }

    if (descrVisibility == 1) {
        cell += '<p class="description_ads">Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia</p>';
    }

    if (linkVisibility == 1) {
        cell += '<a class="link_ads" href="#">yourdomain.com</a>';
    }
    cell += '</td></tr></table>';
    ///////////////////
    return cell;
}


/**
 * Ad block generation
 *
 * @param colums
 * @param lines
 * @param imgPosition
 * @param linkVisibility
 * @returns {string}
 */
function generateAdBlock(colums, lines, imgPosition, linkVisibility, titleVisibility, descrVisibility) {
//    var img = '';
//    if (imgPosition !== false) {
//        img = '<a href="#"><img class="img_ads" src="/img/placeholders/' + randomInteger(1, 17) + '.jpg" ></a>';
//    }
//    var cell = '<table class="cell_ads" border=0 ><tr><td>';
//    cell += img;
//
//    if (titleVisibility == 1) {
//        cell += '<a class="title_ads" href="#">Lorem ipsum dolor sit amet elit</a>';
//    }
//
//    if (descrVisibility == 1) {
//        cell += '<p class="description_ads">Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia</p>';
//    }
//
//    if (linkVisibility == 1) {
//        cell += '<a class="link_ads" href="#">yourdomain.com</a>';
//    }
//    cell += '</td></tr></table>';

    var table = "<table class='block_ads'>\n<tr>\n";

    for (var i = 0; i < (colums * lines); i++) {
        if ((i % colums) === 0 && i !== 0) {
            table += "\n</tr>\n<tr>\n";
        }
        table += "\n<td class='cell_margin' valign='top'>\n" + random_cell(imgPosition, linkVisibility, titleVisibility, descrVisibility) + "\n</td>\n";
    }
    table += "\n</tr>\n</table>\n";
    return table;
}


function parseVisibilitySettings(visibility_settings) {

    if (visibility_settings == 1) {
        return {
            link: 1,
            title: 1,
            descr: 1

        };
    }

    if (visibility_settings == 0) {
        return {
            link: 0,
            title: 1,
            descr: 1

        };
    }

    var s = visibility_settings.split('7');

    return {
        link: s[1],
        title: s[2],
        descr: s[3]
    };
}

function getPresetID() {
    var url = new URL(location.href);
    return url.searchParams.get("preset") ? url.searchParams.get("preset") : '';
}

/**
 * Customization of the appearance of the ad unit
 */
$(document).ready(function () {

    var blockID = location.href.split('/')[6]; // block id
    var _siteID = location.href.split('/')[4]; // site id
    var csrfToken = $('[name="csrf-param"]').attr('content'); // csrf token

    $.post('/blocks/apiGetBlockJsonData/', {
        blockID: blockID,
        csrfToken: csrfToken,
        preset: getPresetID(),
        siteID: _siteID
    }, function (_data) {
        var data = $.parseJSON(_data);
        if (data.response_status == 'ok') {
            var siteID = data.response_data.site_id;
            var blockName = data.response_data.block_name;
            var colums = data.response_data.block_count_ads_width;
            var lines = data.response_data.block_count_ads_height;
            var imagePosition = data.response_data.block_ads_img_position;
            var blockCssStyles = data.response_data.block_css_styles;
            var blockThirdPartyCode = data.response_data.block_third_party_code;

            var _visible_settings = parseVisibilitySettings(data.response_data.block_link_visibility);

            var linkVisibility = _visible_settings.link;
            var titleVisibility = _visible_settings.title;
            var descrVisibility = _visible_settings.descr;


            // set block name input
            $('#_block_name').val(blockName);

            // set block styles
            $('style').html(blockCssStyles);

            //
            $('#third-party').val(blockThirdPartyCode);

            // table
            if (imagePosition == 'no') {
                $('.bgc').html(generateAdBlock(colums, lines, false, linkVisibility, titleVisibility, descrVisibility));
            } else if (imagePosition == 'top') {
                styles('.img_ads', 'float', false);
                styles('.title_ads', 'display', 'block');
                $('.bgc').html(generateAdBlock(colums, lines, true, linkVisibility, titleVisibility, descrVisibility));
            } else if (imagePosition == 'left') {
                styles('.img_ads', 'float', 'left');
                styles('.title_ads', 'display', 'inline');
                //_styles('.title_ads', 'min-width', '150px');
                //_styles('.description_ads', 'min-width', '150px');
                //_styles('.link_ads', 'min-width', '150px');
                $('.bgc').html(generateAdBlock(colums, lines, true, linkVisibility, titleVisibility, descrVisibility));
            }

            // styles obj
            var objStyles = getStyles();


            ///////////////// ( Block settings )

            // Variables
            var blockWidth = objStyles['.block_ads']['width'].match(/[0-9]+/)[0];
            var blockWidthUnits = objStyles['.block_ads']['width'].match(/([0-9]+)(.+)/)[2];
            var blockHeight = objStyles['.block_ads']['height'].match(/[0-9]+/)[0];
            var blockHeightUnits = objStyles['.block_ads']['height'].match(/([0-9]+)(.+)/)[2];
            var blockBgColor = objStyles['.block_ads']['background'];
            var blockBorderColor = objStyles['.block_ads']['border-color'];
            var blockBorderWidth = objStyles['.block_ads']['border-width'].match(/[0-9]+/)[0];
            var blockPadding = objStyles['.block_ads']['padding'].match(/[0-9]+/)[0];

            // Setting field values
            $('#_count_ads_width').val(colums);
            $('#_count_ads_height').val(lines);
            $('#_block_width').val(blockWidth);
            $('input[name="_block_width_units"]input[value="' + blockWidthUnits + '"]').attr('checked', 'checked');
            $('#_block_height').val(blockHeight);
            $('input[name="_block_height_units"]input[value="' + blockHeightUnits + '"]').attr('checked', 'checked');
            $('#_bg_block').colorpicker('setValue', blockBgColor);
            $('#_block_border_color').colorpicker('setValue', blockBorderColor);
            $('#_block_border').val(blockBorderWidth);
            $('#_block_padding').val(blockPadding);


            // Changing values

            // Number of ads per block width
            $('#_count_ads_width').on('change', function () {
                var colums = $('#_count_ads_width').val();
                var lines = $('#_count_ads_height').val();
                if (imagePosition == 'no') {
                    $('.bgc').html(generateAdBlock(colums, lines, false, linkVisibility, titleVisibility, descrVisibility));
                } else {
                    $('.bgc').html(generateAdBlock(colums, lines, true, linkVisibility, titleVisibility, descrVisibility));
                }
            });

            // Number of ads in the block by height
            $('#_count_ads_height').on('change', function () {
                var colums = $('#_count_ads_width').val();
                var lines = $('#_count_ads_height').val();
                if (imagePosition == 'no') {
                    $('.bgc').html(generateAdBlock(colums, lines, false, linkVisibility, titleVisibility, descrVisibility));
                } else {
                    $('.bgc').html(generateAdBlock(colums, lines, true, linkVisibility, titleVisibility, descrVisibility));
                }
            });

            // Ad block width
            $('#_block_width').bind('keyup change', function () {
                var width = $(this).val();
                var units = $("input[name='_block_width_units']:checked").val();
                styles('.block_ads', 'width', width + units);
            });

            // Ad block width (units)
            $("input[name='_block_width_units']").on('change', function () {
                var width = $('#_block_width').val();
                var units = $("input[name='_block_width_units']:checked").val();
                styles('.block_ads', 'width', width + units);
            });

            // Block height
            $('#_block_height').bind('keyup change', function () {
                var width = $(this).val();
                var units = $("input[name='_block_height_units']:checked").val();
                styles('.block_ads', 'height', width + units);
            });

            // Block height (units)
            $("input[name='_block_height_units']").on('change', function () {
                var width = $('#_block_height').val();
                var units = $("input[name='_block_height_units']:checked").val();
                styles('.block_ads', 'height', width + units);
            });

            // Block background color
            $('#_bg_block').on('click', function () {
                $(document).mousemove(function () {
                    var foo = $('#_bg_block input').val();
                    foo.length > 0 ? styles('.block_ads', 'background', foo) : styles('.block_ads', 'background', false);
                });

                $('#_bg_block input').bind('keyup change', function () {
                    var foo = $('#_bg_block input').val();
                    foo.length > 0 ? styles('.block_ads', 'background', foo) : styles('.block_ads', 'background', false);
                });
            });

            // Block border color
            $('#_block_border_color').on('click', function () {
                $(document).mousemove(function () {
                    var foo = $('#_block_border_color input').val();
                    foo.length > 0 ? styles('.block_ads', 'border-color', foo) : styles('.block_ads', 'border-color', false);
                });
                $('#_block_border_color input').bind('keyup change', function () {
                    var foo = $('#_block_border_color input').val();
                    foo.length > 0 ? styles('.block_ads', 'border-color', foo) : styles('.block_ads', 'border-color', false);
                });
            });

            // Block border thickness
            $('#_block_border').on('change', function () {
                var border = $(this).val();
                styles('.block_ads', 'border-width', border + 'px');
            });

            // block margin
            $("#_block_padding").on('change', function () {
                var padding = $('#_block_padding').val();
                styles('.block_ads', 'margin', padding + 'px');
            });




            ///////////////// ( Ad settings )

            // Variables
            var cellPadding = objStyles['.title_ads']['margin-bottom'].match(/[0-9]+/)[0];
            var cellAlign = objStyles['.cell_ads']['text-align'];
            var cellMargin = objStyles['.cell_margin']['padding'].match(/[0-9]+/)[0];


            // Setting field values
            $('#_img_position').val(imagePosition);
            $('#_cell_padding').val(cellPadding);
            $('#_cell_align').val(cellAlign);
            $('#_cells_margin').val(cellMargin);


            // Changing values

            // Image position
            $('#_img_position').on('change', function () {
                if ($(this).val() == 'no') {
                    $('.bgc').html(generateAdBlock($('#_count_ads_width').val(), $('#_count_ads_height').val(), false, linkVisibility, titleVisibility, descrVisibility));
                    $("#_img_b").fadeOut(500);
                    imagePosition = 'no';
                } else if ($(this).val() == 'top') {
                    styles('.img_ads', 'float', 'none');
                    styles('.title_ads', 'display', 'block');
                    $('.bgc').html(generateAdBlock($('#_count_ads_width').val(), $('#_count_ads_height').val(), true, linkVisibility, titleVisibility, descrVisibility));
                    $("#_img_b").fadeIn(500);
                    imagePosition = 'top';
                } else if ($(this).val() == 'left') {
                    styles('.img_ads', 'float', 'left');
                    styles('.title_ads', 'display', 'inline');
                    $('.bgc').html(generateAdBlock($('#_count_ads_width').val(), $('#_count_ads_height').val(), true, linkVisibility, titleVisibility, descrVisibility));
                    $("#_img_b").fadeIn(500);
                    imagePosition = 'left';
                }
            });

            // cell padding
            $("#_cell_padding").on('change', function () {
                var padding = $('#_cell_padding').val();
                styles('.img_ads', 'margin-right', padding + 'px');
                styles('.img_ads', 'margin-bottom', padding + 'px');
                styles('.title_ads', 'margin-bottom', padding + 'px');
                styles('.description_ads', 'margin-bottom', padding + 'px');
                styles('.link_ads', 'margin-bottom', padding + 'px');
            });

            // Aligning text within a cell
            $("#_cell_align").on('change', function () {
                var align = $('#_cell_align').val();
                styles('.cell_ads', 'text-align', align);
            });

            // cells margin
            $("#_cells_margin").on('change', function () {
                var margin = $('#_cells_margin').val();
                styles('.cell_margin', 'padding', margin + 'px');
            });




            ///////////////// ( Image settings )

            // Variables
            var imgSize = objStyles['.img_ads']['width'].match(/[0-9]+/)[0];
            var imgSizeHeight = objStyles['.img_ads']['height'].match(/[0-9]+/)[0];
            var imgBorder = objStyles['.img_ads']['border-width'].match(/[0-9]+/)[0];
            var imgBorderColor = objStyles['.img_ads']['border-color'];
            var imgBorderColorHover = objStyles['.img_ads:hover']['border-color'];


            // Setting field values
            $('#_img_size').val(imgSize);
            $('#_img_size_height').val(imgSizeHeight);
            $('#_img_border').val(imgBorder);
            $('#_img_border_color').colorpicker('setValue', imgBorderColor);
            $('#_img_border_color_hover').colorpicker('setValue', imgBorderColorHover);


            // Changing values

            // image size
            $("#_img_size").on('change', function () {
                var size = $('#_img_size').val();
                styles('.img_ads', 'width', size + 'px');
                //styles('.img_ads', 'height', size + 'px');
            });

            // image size
            $("#_img_size_height").on('change', function () {
                var size = $('#_img_size_height').val();
                styles('.img_ads', 'height', size + 'px');
                //styles('.img_ads', 'height', size + 'px');
            });

            // image border size
            $("#_img_border").on('change', function () {
                var size = $('#_img_border').val();
                styles('.img_ads', 'border-width', size + 'px');
            });

            // image border color
            $('#_img_border_color').on('click', function () {
                $(document).mousemove(function () {
                    var foo = $('#_img_border_color input').val();
                    foo.length > 0 ? styles('.img_ads', 'border-color', foo) : styles('.img_ads', 'border-color', false);
                });
                $('#_img_border_color input').bind('keyup change', function () {
                    var foo = $('#_img_border_color input').val();
                    foo.length > 0 ? styles('.img_ads', 'border-color', foo) : styles('.img_ads', 'border-color', false);
                });
            });

            // image border color (hover)
            $('#_img_border_color_hover').on('click', function () {
                $(document).mousemove(function () {
                    var foo = $('#_img_border_color_hover input').val();
                    foo.length > 0 ? styles('.img_ads:hover', 'border-color', foo) : styles('.img_ads:hover', 'border-color', false);
                });
                $('#_img_border_color_hover input').bind('keyup change', function () {
                    var foo = $('#_img_border_color_hover input').val();
                    foo.length > 0 ? styles('.img_ads:hover', 'border-color', foo) : styles('.img_ads:hover', 'border-color', false);
                });
            });


            ///////////////// ( Title settings )

            // Variables
            var titleFontFamily = objStyles['.title_ads']['font-family'];
            var titleFontSize = objStyles['.title_ads']['font-size'].match(/[0-9]+/)[0];
            var titleFontColor = objStyles['.title_ads']['color'];
            var titleBold = objStyles['.title_ads']['font-weight'];
            var titleItalic = objStyles['.title_ads']['font-style'];
            var titleUnderline = objStyles['.title_ads']['text-decoration'];
            var titleFontColorHover = objStyles['.title_ads:hover']['color'];
            var titleBoldHover = objStyles['.title_ads:hover']['font-weight'];
            var titleItalicHover = objStyles['.title_ads:hover']['font-style'];
            var titleUnderlineHover = objStyles['.title_ads:hover']['text-decoration'];

            // Setting field values
            $('#_title_font_family').val(titleFontFamily);
            $('#_title_font_size').val(titleFontSize);
            $('#_title_font_color').colorpicker('setValue', titleFontColor);
            titleBold == 'bold' ? $('#_title_bold').addClass('active') : $('#_title_bold').removeClass('active');
            titleItalic == 'italic' ? $('#_title_italic').addClass('active') : $('#_title_italic').removeClass('active');
            titleUnderline == 'underline' ? $('#_title_underline').addClass('active') : $('#_title_underline').removeClass('active');

            titleBoldHover == 'bold' ? $('#_title_bold_hover').addClass('active') : $('#_title_bold_hover').removeClass('active');
            titleItalicHover == 'italic' ? $('#_title_italic_hover').addClass('active') : $('#_title_italic_hover').removeClass('active');
            titleUnderlineHover == 'underline' ? $('#_title_underline_hover').addClass('active') : $('#_title_underline_hover').removeClass('active');
            $('#_title_font_color_hover').colorpicker('setValue', titleFontColorHover);


            if (titleVisibility == 1) {
                $('#title_enabled').addClass('active');
                $('#title_disabled').removeClass('active');
            } else {
                $('#title_disabled').addClass('active');
                $('#title_enabled').removeClass('active');
            }


            // Changing values

            // show title
            $('#title_enabled').on('click', function () {
                $('#title_enabled').addClass('active');
                $('#title_disabled').removeClass('active');
                titleVisibility = 1;

                $('.bgc').html(generateAdBlock($('#_count_ads_width').val(), $('#_count_ads_height').val(), (imagePosition == 'no') ? false : imagePosition, linkVisibility, titleVisibility, descrVisibility));
            });

            // hide title
            $('#title_disabled').on('click', function () {
                $('#title_disabled').addClass('active');
                $('#title_enabled').removeClass('active');
                titleVisibility = 0;
                $('.bgc').html(generateAdBlock($('#_count_ads_width').val(), $('#_count_ads_height').val(), (imagePosition == 'no') ? false : imagePosition, linkVisibility, titleVisibility, descrVisibility));
            });



            // font
            $("#_title_font_family").on('change', function () {
                styles('.title_ads', 'font-family', $(this).val());
            });

            // font size
            $("#_title_font_size").on('change', function () {
                styles('.title_ads', 'font-size', $(this).val() + 'px');
            });

            // font color
            $('#_title_font_color').on('click', function () {
                $(document).mousemove(function () {
                    var foo = $('#_title_font_color input').val();
                    foo.length > 0 ? styles('.title_ads', 'color', foo) : styles('.title_ads', 'color', false);
                });
                $('#_title_font_color input').bind('keyup change', function () {
                    var foo = $(this).val();
                    foo.length > 0 ? styles('.title_ads', 'color', foo) : styles('.title_ads', 'color', false);
                });
            });

            // font bold
            $('#_title_bold').on('click', function () {
                $(this).toggleClass('active');
                $(this).hasClass('active') ? styles('.title_ads', 'font-weight', 'bold') : styles('.title_ads', 'font-weight', 'normal');
            });

            // font italic
            $('#_title_italic').on('click', function () {
                $(this).toggleClass('active');
                $(this).hasClass('active') ? styles('.title_ads', 'font-style', 'italic') : styles('.title_ads', 'font-style', 'normal');
            });

            // font underline
            $('#_title_underline').on('click', function () {
                $(this).toggleClass('active');
                $(this).hasClass('active') ? styles('.title_ads', 'text-decoration', 'underline') : styles('.title_ads', 'text-decoration', 'none');
            });

            // font color(hover)
            $('#_title_font_color_hover').on('click', function () {
                $(document).mousemove(function () {
                    var foo = $('#_title_font_color_hover input').val();
                    foo.length > 0 ? styles('.title_ads:hover', 'color', foo) : styles('.title_ads:hover', 'color', false);
                });
                $('#_title_font_color_hover input').bind('keyup change', function () {
                    var foo = $(this).val();
                    foo.length > 0 ? styles('.title_ads:hover', 'color', foo) : styles('.title_ads:hover', 'color', false);
                });
            });

            // font bold (hover)
            $('#_title_bold_hover').on('click', function () {
                $(this).toggleClass('active');
                $(this).hasClass('active') ? styles('.title_ads:hover', 'font-weight', 'bold') : styles('.title_ads:hover', 'font-weight', 'normal');
            });

            // font italic (hover)
            $('#_title_italic_hover').on('click', function () {
                $(this).toggleClass('active');
                $(this).hasClass('active') ? styles('.title_ads:hover', 'font-style', 'italic') : styles('.title_ads:hover', 'font-style', 'normal');
            });

            // font underline (hover)
            $('#_title_underline_hover').on('click', function () {
                $(this).toggleClass('active');
                $(this).hasClass('active') ? styles('.title_ads:hover', 'text-decoration', 'underline') : styles('.title_ads:hover', 'text-decoration', 'none');
            });




            ///////////////// ( Description settings )

            // Variables
            var descriptionFontFamily = objStyles['.description_ads']['font-family'];
            var descriptionFontSize = objStyles['.description_ads']['font-size'].match(/[0-9]+/)[0];
            var descriptionFontColor = objStyles['.description_ads']['color'];
            var descriptionBold = objStyles['.description_ads']['font-weight'];
            var descriptionItalic = objStyles['.description_ads']['font-style'];
            var descriptionUnderline = objStyles['.description_ads']['text-decoration'];

            // Setting field values
            $('#_description_font_family').val(descriptionFontFamily);
            $('#_description_font_size').val(descriptionFontSize);
            $('#_description_font_color').colorpicker('setValue', descriptionFontColor);

            descriptionBold == 'bold' ? $('#_description_bold').addClass('active') : $('#_description_bold').removeClass('active');
            descriptionItalic == 'italic' ? $('#_description_italic').addClass('active') : $('#_description_italic').removeClass('active');
            descriptionUnderline == 'underline' ? $('#_description_underline').addClass('active') : $('#_description_underline').removeClass('active');


            if (descrVisibility == 1) {
                $('#descr_enabled').addClass('active');
                $('#descr_disabled').removeClass('active');
            } else {
                $('#descr_disabled').addClass('active');
                $('#descr_enabled').removeClass('active');
            }


            // Changing values

            // show descr
            $('#descr_enabled').on('click', function () {
                $('#descr_enabled').addClass('active');
                $('#descr_disabled').removeClass('active');
                descrVisibility = 1;

                $('.bgc').html(generateAdBlock($('#_count_ads_width').val(), $('#_count_ads_height').val(), (imagePosition == 'no') ? false : imagePosition, linkVisibility, titleVisibility, descrVisibility));
            });

            // hide descr
            $('#descr_disabled').on('click', function () {
                $('#descr_disabled').addClass('active');
                $('#descr_enabled').removeClass('active');
                descrVisibility = 0;
                $('.bgc').html(generateAdBlock($('#_count_ads_width').val(), $('#_count_ads_height').val(), (imagePosition == 'no') ? false : imagePosition, linkVisibility, titleVisibility, descrVisibility));
            });




            // font
            $("#_description_font_family").on('change', function () {
                styles('.description_ads', 'font-family', $(this).val());
            });

            // font size
            $("#_description_font_size").on('change', function () {
                styles('.description_ads', 'font-size', $(this).val() + 'px');
            });

            // font color
            $('#_description_font_color').on('click', function () {
                $(document).mousemove(function () {
                    var foo = $('#_description_font_color input').val();
                    foo.length > 0 ? styles('.description_ads', 'color', foo) : styles('.description_ads', 'color', false);
                });
                $('#_description_font_color input').bind('keyup change', function () {
                    var foo = $(this).val();
                    foo.length > 0 ? styles('.description_ads', 'color', foo) : styles('.description_ads', 'color', false);
                });
            });

            // font bold
            $('#_description_bold').on('click', function () {
                $(this).toggleClass('active');
                $(this).hasClass('active') ? styles('.description_ads', 'font-weight', 'bold') : styles('.description_ads', 'font-weight', 'normal');
            });

            // font italic
            $('#_description_italic').on('click', function () {
                $(this).toggleClass('active');
                $(this).hasClass('active') ? styles('.description_ads', 'font-style', 'italic') : styles('.description_ads', 'font-style', 'normal');
            });

            // font underline
            $('#_description_underline').on('click', function () {
                $(this).toggleClass('active');
                $(this).hasClass('active') ? styles('.description_ads', 'text-decoration', 'underline') : styles('.description_ads', 'text-decoration', 'none');
            });



            ///////////////// ( Link settings )

            // Variables
            var linkFontFamily = objStyles['.link_ads']['font-family'];
            var linkFontSize = objStyles['.link_ads']['font-size'].match(/[0-9]+/)[0];
            var linkFontColor = objStyles['.link_ads']['color'];
            var linkBold = objStyles['.link_ads']['font-weight'];
            var linkItalic = objStyles['.link_ads']['font-style'];
            var linkUnderline = objStyles['.link_ads']['text-decoration'];
            var linkFontColorHover = objStyles['.link_ads:hover']['color'];
            var linkBoldHover = objStyles['.link_ads:hover']['font-weight'];
            var linkItalicHover = objStyles['.link_ads:hover']['font-style'];
            var linkUnderlineHover = objStyles['.link_ads:hover']['text-decoration'];

            // Setting field values
            $('#_link_font_family').val(linkFontFamily);
            $('#_link_font_size').val(linkFontSize);
            $('#_link_font_color').colorpicker('setValue', linkFontColor);
            linkBold == 'bold' ? $('#_link_bold').addClass('active') : $('#_link_bold').removeClass('active');
            linkItalic == 'italic' ? $('#_link_italic').addClass('active') : $('#_link_italic').removeClass('active');
            linkUnderline == 'underline' ? $('#_link_underline').addClass('active') : $('#_link_underline').removeClass('active');

            linkBoldHover == 'bold' ? $('#_link_bold_hover').addClass('active') : $('#_link_bold_hover').removeClass('active');
            linkItalicHover == 'italic' ? $('#_link_italic_hover').addClass('active') : $('#_link_italic_hover').removeClass('active');
            linkUnderlineHover == 'underline' ? $('#_link_underline_hover').addClass('active') : $('#_link_underline_hover').removeClass('active');
            $('#_link_font_color_hover').colorpicker('setValue', linkFontColorHover);


            if (linkVisibility == 1) {
                $('#link_enabled').addClass('active');
                $('#link_disabled').removeClass('active');
            } else {
                $('#link_disabled').addClass('active');
                $('#link_enabled').removeClass('active');
            }



            // Changing values

            // show link
            $('#link_enabled').on('click', function () {
                $('#link_enabled').addClass('active');
                $('#link_disabled').removeClass('active');
                linkVisibility = 1;

                $('.bgc').html(generateAdBlock($('#_count_ads_width').val(), $('#_count_ads_height').val(), (imagePosition == 'no') ? false : imagePosition, linkVisibility, titleVisibility, descrVisibility));
            });

            // hide link
            $('#link_disabled').on('click', function () {
                $('#link_disabled').addClass('active');
                $('#link_enabled').removeClass('active');
                linkVisibility = 0;
                $('.bgc').html(generateAdBlock($('#_count_ads_width').val(), $('#_count_ads_height').val(), (imagePosition == 'no') ? false : imagePosition, linkVisibility, titleVisibility, descrVisibility));
            });

            // font
            $("#_link_font_family").on('change', function () {
                styles('.link_ads', 'font-family', $(this).val());
            });

            // font size
            $("#_link_font_size").on('change', function () {
                styles('.link_ads', 'font-size', $(this).val() + 'px');
            });

            // font color
            $('#_link_font_color').on('click', function () {
                $(document).mousemove(function () {
                    var foo = $('#_link_font_color input').val();
                    foo.length > 0 ? styles('.link_ads', 'color', foo) : styles('.link_ads', 'color', false);
                });
                $('#_link_font_color input').bind('keyup change', function () {
                    var foo = $(this).val();
                    foo.length > 0 ? styles('.link_ads', 'color', foo) : styles('.link_ads', 'color', false);
                });
            });

            // font bold
            $('#_link_bold').on('click', function () {
                $(this).toggleClass('active');
                $(this).hasClass('active') ? styles('.link_ads', 'font-weight', 'bold') : styles('.link_ads', 'font-weight', 'normal');
            });

            // font italic
            $('#_link_italic').on('click', function () {
                $(this).toggleClass('active');
                $(this).hasClass('active') ? styles('.link_ads', 'font-style', 'italic') : styles('.link_ads', 'font-style', 'normal');
            });

            // font underline
            $('#_link_underline').on('click', function () {
                $(this).toggleClass('active');
                $(this).hasClass('active') ? styles('.link_ads', 'text-decoration', 'underline') : styles('.link_ads', 'text-decoration', 'none');
            });

            // font color (hover)
            $('#_link_font_color_hover').on('click', function () {
                $(document).mousemove(function () {
                    var foo = $('#_link_font_color_hover input').val();
                    foo.length > 0 ? styles('.link_ads:hover', 'color', foo) : styles('.link_ads:hover', 'color', false);
                });
                $('#_link_font_color_hover input').bind('keyup change', function () {
                    var foo = $(this).val();
                    foo.length > 0 ? styles('.link_ads:hover', 'color', foo) : styles('.link_ads:hover', 'color', false);
                });
            });

            // font bold (hover)
            $('#_link_bold_hover').on('click', function () {
                $(this).toggleClass('active');
                $(this).hasClass('active') ? styles('.link_ads:hover', 'font-weight', 'bold') : styles('.link_ads:hover', 'font-weight', 'normal');
            });

            // font italic (hover)
            $('#_link_italic_hover').on('click', function () {
                $(this).toggleClass('active');
                $(this).hasClass('active') ? styles('.link_ads:hover', 'font-style', 'italic') : styles('.link_ads:hover', 'font-style', 'normal');
            });

            // font underline (hover)
            $('#_link_underline_hover').on('click', function () {
                $(this).toggleClass('active');
                $(this).hasClass('active') ? styles('.link_ads:hover', 'text-decoration', 'underline') : styles('.link_ads:hover', 'text-decoration', 'none');
            });


            ///////////////// ( Block preview settings )

            // Background block preview
            $('input[type=radio][name=bg]').on('change', function () {
                if (this.value == '3') {
                    $('.bgc').css('background', 'url(/img/bg_transparent.png)');
                } else if (this.value == '1') {
                    $('.bgc').css('background', '#fff');
                } else if (this.value == '2') {
                    $('.bgc').css('background', '#000');
                }
            });

            // tips
            $('[data-toggle="tooltip"]').tooltip({
                container: 'body',
                html: true
            });

            // save new block
            $('#_save_block').on('click', function () {

                var clean_uri = location.protocol + "//" + location.host + location.pathname;
                window.history.replaceState({}, document.title, clean_uri);


//                var obj = {
//                    site_id: '###',
//                    block_name: $('#_block_name').val(),
//                    block_ads_img_position: imagePosition,
//                    block_count_ads_width: $('#_count_ads_width').val(),
//                    block_count_ads_height: $('#_count_ads_height').val(),
//                    block_css_styles: $('style').html().replace(/[\n\r\t ]/g, ' '),
//                    block_third_party_code: $('#third-party').val(),
//                    block_link_visibility: '7' + linkVisibility + '7' + titleVisibility + '7' + descrVisibility
//                };
//
//                console.log(JSON.stringify(obj));


                $.post('/blocks/apiSaveBlock/', {
                    id: blockID,
                    block_name: $('#_block_name').val(),
                    block_ads_img_position: imagePosition,
                    block_count_ads_width: $('#_count_ads_width').val(),
                    block_count_ads_height: $('#_count_ads_height').val(),
                    block_css_styles: $('style').html().replace(/[\n\r\t ]/g, ' '),
                    block_third_party_code: $('#third-party').val(),
                    block_link_visibility: '7' + linkVisibility + '7' + titleVisibility + '7' + descrVisibility,
                    csrfToken: csrfToken
                }, function () {
                    $('#_modal_get_code').modal();
                });
            });

            // remove block
            $('#_remove_block_ok').on('click', function () {
                $.post('/blocks/apiRemoveBlock/', {
                    blockID: blockID,
                    csrfToken: csrfToken
                }, function () {
                    document.location = '/sites/' + siteID + '/blocks/';
                });
            });

            // refresh inputs
            $('.selectpicker').selectpicker('refresh');
        }
    });


    //
    $('#presets').on('change', function () {
        window.location.href = location.protocol + "//" + location.host + location.pathname + '?preset=' + parseInt($(this).val());
    });

    if (getPresetID()) {
        $('#presets').val(getPresetID());
    }


});